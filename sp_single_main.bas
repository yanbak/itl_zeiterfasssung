﻿Type=Class
Version=7.3
ModulesStructureVersion=1
B4A=true
@EndOfDesignText@

Sub Class_Globals

Dim parser As SaxParser
	Public TYPE_STRING   As String = "xsd:string"
	Public TYPE_INTEGER  As String = "xsd:int"
	Public TYPE_FLOAT    As String = "xsd:float"
	Public TYPE_DATE     As String = "xsd:date"
	Public TYPE_DOUBLE   As String = "xsd:double"
	Public TYPE_BINARY   As String = "xsd:base64Binary"

	Private UrlWS        As String

	Private Modules      As Object
	Private EN           As String
	Private xml_sendstriing As String
Private last1 As Boolean
	Dim method As String
End Sub

Sub P_logincheck_EndElement (Uri As String, Name As String, Text As StringBuilder)
		If parser.Parents.IndexOf("logincheckResponse") > -1 Then
	Select Case Name
		Case "logincheckResult"
			If SubExists(Modules,EN) Then
				CallSubDelayed2(Modules,EN,Text.ToString)
			End If
	End Select
	End If
End Sub
Sub P_quittung_EndElement (Uri As String, Name As String, Text As StringBuilder)
	
		If parser.Parents.IndexOf("up_quitResponse") > -1 Then
	Select Case Name
		Case "up_quitResult"
			Try
					Main.sql1.ExecNonQuery("Update tbl_quittung set sync=1 where id =" & Text.ToString)
			Log(Main.sql1.ExecQuerySingleResult("Select count(sync) from tbl_quittung where sync=1"))
			Catch
					If SubExists(Modules,EN) Then
				CallSubDelayed2(Modules,EN,Text.ToString)
				End If
			End Try
	End Select
	End If
End Sub

Sub P_all_EndElement (Uri As String, Name As String, Text As StringBuilder)
	Dim node As String= method & "Response"
		If parser.Parents.IndexOf(node) > -1 Then
			
	Select Case Name
		Case "send_infoResult"
			Try
					Main.sql1.ExecNonQuery("Update tbl_Antwort set sync=1 where id =" & Text.ToString)
			Log(Main.sql1.ExecQuerySingleResult("Select count(sync) from tbl_Antwort where sync=1"))
			Catch
					If SubExists(Modules,EN) Then
				CallSubDelayed2(Modules,EN,Text.ToString)
				End If
			End Try
				
				
		
		
			Case "up_scanResult"
			Try
						Main.sql1.ExecNonQuery("Update tbl_scan set sync=1 where id =" & Text.ToString)
			Log(Main.sql1.ExecQuerySingleResult("Select count(sync) from tbl_scan where sync=1"))
			Catch
				Log(LastException)	
				If SubExists(Modules,EN) Then
				CallSubDelayed2(Modules,EN,Text.ToString)
				
			
				
			End If
			End Try
				
		
		
	End Select
	End If
End Sub
Sub P_ankunft_EndElement (Uri As String, Name As String, Text As StringBuilder)
	
		If parser.Parents.IndexOf("up_ankunftResponse") > -1 Then

	Select Case Name
		Case "up_ankunftResult"
		Try
				Main.sql1.ExecNonQuery("Update tbl_Ankunft set sync=1 where id =" & Text.ToString)
			Log(Main.sql1.ExecQuerySingleResult("Select count(sync) from tbl_ankunft where sync=1"))
		Catch
			Log(LastException)	
			CallSubDelayed2(Modules,EN,Text.ToString)
		End Try
		
		
				
		
			
	End Select
	End If
End Sub
Sub P_eingabe_EndElement (Uri As String, Name As String, Text As StringBuilder)
	Dim node As String= method & "Response"
		If parser.Parents.IndexOf(node) > -1 Then
node = method & "Result"
	Select Case Name
		Case node
		Try
				Main.sql1.ExecNonQuery("Update tbl_nachtrag_log_app set sync=1 where id =" & Text.ToString)
			Log(Main.sql1.ExecQuerySingleResult("Select count(sync) from tbl_nachtrag_log_app where sync=1"))
		Catch
			CallSubDelayed2(Modules,EN,Text.ToString)
			Log(LastException)
		End Try
		
			
				

			
	End Select
	End If
End Sub
'Initializes the object. You can add parameters to this method if needed.
Public Sub initialize(Url As String,MethodName As String,module As Object, eventname As String)


		UrlWS        = Url
method=MethodName
	EN           = eventname
	Modules      = module
	

	xml_sendstriing =xml_sendstriing & "<?xml version=" & Chr(34) & "1.0"& Chr(34) & " encoding=" & Chr(34) & "utf-8"& Chr(34) & "?>"
xml_sendstriing= xml_sendstriing & "<soap:Envelope xmlns:xsi="& Chr(34) & "http://www.w3.org/2001/XMLSchema-instance"& Chr(34) & " xmlns:xsd="& Chr(34) & "http://www.w3.org/2001/XMLSchema"& Chr(34) & " xmlns:soap="& Chr(34) & "http://schemas.xmlsoap.org/soap/envelope/"& Chr(34) & ">"
xml_sendstriing= xml_sendstriing & "<soap:Body>"
    xml_sendstriing= xml_sendstriing & "<" & MethodName &" xmlns="& Chr(34) & "http://tempuri.org/"& Chr(34) & ">"
End Sub

Public Sub AddField(FieldName As String,FieldType As String,FieldValue As String)

	xml_sendstriing=xml_sendstriing &CRLF & $"<${FieldName} >${FieldValue}</${FieldName}>"$
End Sub

Sub SendRequest( MethodName As String,jobname As String, last As Boolean)

	last1=last
    xml_sendstriing= xml_sendstriing & "</" & MethodName &">"
  xml_sendstriing= xml_sendstriing & "</soap:Body>"
xml_sendstriing= xml_sendstriing & "</soap:Envelope>"
Log(xml_sendstriing)
Log("string ist fertig")
	Dim ht1 As HttpJob
	ht1.Initialize(jobname,Me)

	ht1.PostString(UrlWS,xml_sendstriing)
	

	ht1.GetRequest.SetHeader("SoapAction", "http://tempuri.org/" & MethodName &"")

	
	ht1.GetRequest.SetContentType("text/xml; charset=utf-8")
	ht1.GetRequest.Timeout=2000
	
End Sub

Private Sub JobDone(Job As HttpJob)
	
	parser.Initialize
	
	If Job.Success Then	 
	Select Case Job.JobName
		Case "logincheck" 
			 parser.Parse(Job.GetInputStream,"P_logincheck")
		Case "all"
			 parser.Parse(Job.GetInputStream,"P_all")
			
		Case "img_up"
				 
	    Case "send_ankunft"
				 parser.Parse(Job.GetInputStream,"P_ankunft")
			
			
				 
			Case "send_quittung"
									
									 parser.Parse(Job.GetInputStream,"P_quittung")
			
		
				 
								
		Case "send_eingabe"	
					 parser.Parse(Job.GetInputStream,"P_eingabe") 
					
		
		End Select
	
		End If
	
		End Sub
		Sub imageupload( img As String,meth As Object) 
		 '1- convertimos el archivo que queremos subir en cadena Base64Str
    Private Base64Con As Base64Image
	Base64Con.Initialize

	
    Private B64Str As String = Base64Con.EncodeFromImage(File.DirInternal,img)
   
    Private job1 As HttpJob
    '2-Construimos la cadena con la estructura XML igual al test del webservice substituyendo los marcadores de posicion por sus correspondientes valores
    Dim XML As String
	XML = ""
'   XML="POST /kmlaapservice/logintest.asmx HTTP/1.1 " 
' XML = XML & "Host: 192.168.200.12"
' XML = XML & "Content-Type: text/xml; charset=utf-8 "
' XML = XML & "Content-Length: " & b64l
' XML = XML & " SOAPAction: 'http://tempuri.org/UploadFile'"
   
   
    
     XML = XML & "<?xml version='1.0' encoding='utf-8'?>"
    XML = XML & "<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>"
    XML = XML & "<soap:Body>"
    XML = XML & "<UploadFile xmlns='http://tempuri.org/'>"
    XML = XML & "<f>" & B64Str & "</f>"
    XML = XML & "<fileName>" & img & "</fileName>"
    XML = XML & "</UploadFile>"
    XML = XML & "</soap:Body>"
    XML = XML & "</soap:Envelope>"
    XML = XML.Replace("'", Chr(34))
	'Log(XML)
    job1.Initialize("img_up", meth)
    '3º llamamos al servicio, definiendo el tipo de contenido
    job1.PostString (Main.use_service &"", XML)
    job1.GetRequest.SetContentType("text/xml")
	job1.GetRequest.SetHeader("SoapAction", "http://tempuri.org/UploadFile")
	End Sub
	