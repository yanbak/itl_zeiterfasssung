﻿Type=Class
Version=7.3
ModulesStructureVersion=1
B4A=true
@EndOfDesignText@
Sub Class_Globals
	Public TYPE_STRING   As String = "xsd:string"
	Public TYPE_INTEGER  As String = "xsd:int"
	Public TYPE_FLOAT    As String = "xsd:float"
	Public TYPE_DATE     As String = "xsd:date"
	Public TYPE_DOUBLE   As String = "xsd:double"
	Public TYPE_BINARY   As String = "xsd:base64Binary"
	Public xml_sendstriing As String
	'Dim update_send As update_sender
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub initialize(Url As String,MethodName As String)
	xml_sendstriing =xml_sendstriing & "<?xml version=" & Chr(34) & "1.0"& Chr(34) & " encoding=" & Chr(34) & "utf-8"& Chr(34) & "?>"
	xml_sendstriing= xml_sendstriing & "<soap:Envelope xmlns:xsi="& Chr(34) & "http://www.w3.org/2001/XMLSchema-instance"& Chr(34) & " xmlns:xsd="& Chr(34) & "http://www.w3.org/2001/XMLSchema"& Chr(34) & " xmlns:soap="& Chr(34) & "http://schemas.xmlsoap.org/soap/envelope/"& Chr(34) & ">"
	xml_sendstriing= xml_sendstriing & "<soap:Body>"
	xml_sendstriing= xml_sendstriing & "<" & MethodName &" xmlns="& Chr(34) & "http://tempuri.org/"& Chr(34) & ">"
End Sub

Public Sub AddField(FieldName As String,FieldType As String,FieldValue As String)
	xml_sendstriing=xml_sendstriing &CRLF & $"<${FieldName} >${FieldValue}</${FieldName}>"$
End Sub

Sub footer( MethodName As String)
	xml_sendstriing= xml_sendstriing & "</" & MethodName &">"
	xml_sendstriing= xml_sendstriing & "</soap:Body>"
	xml_sendstriing= xml_sendstriing & "</soap:Envelope>"
End Sub