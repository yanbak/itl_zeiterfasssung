﻿Type=Activity
Version=7.3
ModulesStructureVersion=1
B4A=true
@EndOfDesignText@
#Region  Activity Attributes 
	#FullScreen: True
	#IncludeTitle: False
#End Region

Sub Process_Globals
	'These global variables will be declared once when the application starts.
	'These variables can be accessed from all modules.
Dim u1 As update_sender_main
Dim SaxParser As SaxParser
End Sub

Sub Globals
	'These global variables will be redeclared each time the activity is created.
	'These variables can only be accessed from this module.
	Dim status As String
	Dim bu_zeit As Button
End Sub

Sub Activity_Create(FirstTime As Boolean)
	'Do not forget to load the layout file created with the visual designer. For example:
	'Activity.LoadLayout("Layout1")
	
	Try
		

	Dim c_status As Cursor
	c_status=Main.sql1.ExecQuery("select Status from tbl_ze where Zeit =(Select Max(zeit) from tbl_ze)")
	c_status.Position=0
		status=c_status.GetString("Status")
	Catch
		status="0"
	End Try
	scale.SetRate(0.3)
	Activity.LoadLayout("ze_basis1")
	If status="Kommen" Then 
		bu_zeit.Text="Gehen" 
		Else
			bu_zeit.text="Kommen"
	End If

End Sub

Sub Activity_Resume

End Sub

Sub Activity_Pause (UserClosed As Boolean)

End Sub
Sub bu_zeit_click
	DateTime.DateFormat="yyyy-MM-dd hh:mm:ss"

	Dim sb As StringBuilder
	sb.initialize
	sb.Append("insert into tbl_ze (usr,Zeit,Status) ")
	sb.Append("values ('" & u1.usr  & "',")
	sb.Append("'" & DateTime.Date(DateTime.Now)  & "',")
	sb.Append("'" & bu_zeit.Text &"');")
	
	
	Main.sql1.ExecNonQuery(sb.ToString)
	
	send_ZE
	
	
	
	'Activity_Create(True)
End Sub


Sub send_ZE
	Dim url As String
	url=Main.liveservice
	
	'get User, pw and zz
	Try
		Dim user As String = Main.sql1.ExecQuerySingleResult("Select username from tbl_usr")
		Dim pw As String = Main.sql1.ExecQuerySingleResult("Select pw from tbl_usr")
		Dim App_id As String = Main.sql1.ExecQuerySingleResult("Select max(ID) from tbl_ze")
		Dim zeit As String = Main.sql1.ExecQuerySingleResult("Select Zeit from tbl_ze where ID =(select max(ID) from tbl_ze)")
		Dim stat As String = Main.sql1.ExecQuerySingleResult("Select Status from tbl_ze where ID =(select max(ID) from tbl_ze)")
	Catch
		Log(LastException)
	End Try
	
	Dim SoapParser As soap_up
	SoapParser.initialize(url,"set_ze_pool")
	SoapParser.AddField("usr",SoapParser.TYPE_STRING,user)
	SoapParser.AddField("pwd",SoapParser.TYPE_STRING,pw)
	SoapParser.AddField("App_ID",SoapParser.TYPE_STRING,App_id)
	SoapParser.AddField("Zeit",SoapParser.TYPE_STRING,zeit)
	SoapParser.AddField("Status",SoapParser.TYPE_STRING,stat)
	SoapParser.footer("set_ze_pool")
	
	Dim set_ze_poolJob As HttpJob
	set_ze_poolJob.Initialize("",Me)
	set_ze_poolJob.PostString(url, SoapParser.xml_sendstriing)
	set_ze_poolJob.GetRequest.SetHeader("SoapAction", "http://tempuri.org/set_ze_pool")
	Log(SoapParser.xml_sendstriing)
	set_ze_poolJob.GetRequest.SetContentType("text/xml; charset=utf-8")
	set_ze_poolJob.GetRequest.Timeout=2000
	ProgressDialogShow("Daten werden hochgeladen..")
	
	Wait For (set_ze_poolJob) JobDone(set_ze_poolJob As HttpJob)
	
	If set_ze_poolJob.Success Then
		SaxParser.Initialize
		SaxParser.Parse(set_ze_poolJob.GetInputStream,"P_set_ze_pool")
		ProgressDialogHide
	Else
		ProgressDialogHide
		ToastMessageShow("Keine Verbindung zum Server möglich",False)
	End If
	set_ze_poolJob.Release
End Sub

Sub P_set_ze_pool_EndElement (Uri As String, Name As String, Text As StringBuilder)

	Dim node As String= "set_ze_pool" & "Response"
	If SaxParser.Parents.IndexOf(node) > -1 Then
		node = "set_ze_pool" & "Result"
		Select Case Name
			Case node
				Try
					Main.sql1.ExecNonQuery("Update tbl_ze set archiv=1 where id ='" & Name & "'")
					'Log(Main.sql1.ExecQuerySingleResult("Select count(sync) from tbl_nachtrag_log_app where sync=1"))
				Catch
					
					Log(LastException)
				End Try
		
			
				

			
		End Select
	End If

End Sub