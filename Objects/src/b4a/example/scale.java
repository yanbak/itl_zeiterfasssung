package b4a.example;


import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.debug.*;

public class scale {
private static scale mostCurrent = new scale();
public static Object getObject() {
    throw new RuntimeException("Code module does not support this method.");
}
 
public anywheresoftware.b4a.keywords.Common __c = null;
public static double _rate = 0;
public static double _cscalex = 0;
public static double _cscaley = 0;
public static double _cscaleds = 0;
public static int _crefwidth = 0;
public static int _crefheight = 0;
public static double _crefscale = 0;
public anywheresoftware.b4a.samples.httputils2.httputils2service _httputils2service = null;
public b4a.example.main _main = null;
public b4a.example.starter _starter = null;
public b4a.example.setup _setup = null;
public b4a.example.ze_basis _ze_basis = null;
public static String  _setrate(anywheresoftware.b4a.BA _ba,double _crate) throws Exception{
RDebugUtils.currentModule="scale";
if (Debug.shouldDelegate(null, "setrate"))
	return (String) Debug.delegate(null, "setrate", new Object[] {_ba,_crate});
RDebugUtils.currentLine=1310720;
 //BA.debugLineNum = 1310720;BA.debugLine="Public Sub SetRate(cRate As Double)";
RDebugUtils.currentLine=1310721;
 //BA.debugLineNum = 1310721;BA.debugLine="Rate = cRate";
_rate = _crate;
RDebugUtils.currentLine=1310722;
 //BA.debugLineNum = 1310722;BA.debugLine="Initialize";
_initialize(_ba);
RDebugUtils.currentLine=1310723;
 //BA.debugLineNum = 1310723;BA.debugLine="End Sub";
return "";
}
public static String  _scaleall(anywheresoftware.b4a.BA _ba,anywheresoftware.b4a.objects.ActivityWrapper _act,boolean _firsttime) throws Exception{
RDebugUtils.currentModule="scale";
if (Debug.shouldDelegate(null, "scaleall"))
	return (String) Debug.delegate(null, "scaleall", new Object[] {_ba,_act,_firsttime});
int _i = 0;
anywheresoftware.b4a.objects.ConcreteViewWrapper _v = null;
RDebugUtils.currentLine=1507328;
 //BA.debugLineNum = 1507328;BA.debugLine="Public Sub ScaleAll(act As Activity, FirstTime As";
RDebugUtils.currentLine=1507329;
 //BA.debugLineNum = 1507329;BA.debugLine="Dim I As Int";
_i = 0;
RDebugUtils.currentLine=1507332;
 //BA.debugLineNum = 1507332;BA.debugLine="If IsPanel(act) And FirstTime = True Then";
if (_ispanel(_ba,(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(_act.getObject()))) && _firsttime==anywheresoftware.b4a.keywords.Common.True) { 
RDebugUtils.currentLine=1507334;
 //BA.debugLineNum = 1507334;BA.debugLine="ScaleView(act)";
_scaleview(_ba,(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(_act.getObject())));
 }else {
RDebugUtils.currentLine=1507336;
 //BA.debugLineNum = 1507336;BA.debugLine="For I = 0 To act.NumberOfViews - 1";
{
final int step5 = 1;
final int limit5 = (int) (_act.getNumberOfViews()-1);
_i = (int) (0) ;
for (;(step5 > 0 && _i <= limit5) || (step5 < 0 && _i >= limit5) ;_i = ((int)(0 + _i + step5))  ) {
RDebugUtils.currentLine=1507337;
 //BA.debugLineNum = 1507337;BA.debugLine="Dim v As View";
_v = new anywheresoftware.b4a.objects.ConcreteViewWrapper();
RDebugUtils.currentLine=1507338;
 //BA.debugLineNum = 1507338;BA.debugLine="v = act.GetView(I)";
_v = _act.GetView(_i);
RDebugUtils.currentLine=1507339;
 //BA.debugLineNum = 1507339;BA.debugLine="ScaleView(v)";
_scaleview(_ba,_v);
 }
};
 };
RDebugUtils.currentLine=1507342;
 //BA.debugLineNum = 1507342;BA.debugLine="End Sub";
return "";
}
public static int  _bottom(anywheresoftware.b4a.BA _ba,anywheresoftware.b4a.objects.ConcreteViewWrapper _v) throws Exception{
RDebugUtils.currentModule="scale";
if (Debug.shouldDelegate(null, "bottom"))
	return (Integer) Debug.delegate(null, "bottom", new Object[] {_ba,_v});
RDebugUtils.currentLine=2424832;
 //BA.debugLineNum = 2424832;BA.debugLine="Public Sub Bottom(v As View) As Int";
RDebugUtils.currentLine=2424833;
 //BA.debugLineNum = 2424833;BA.debugLine="Return v.Top + v.Height";
if (true) return (int) (_v.getTop()+_v.getHeight());
RDebugUtils.currentLine=2424834;
 //BA.debugLineNum = 2424834;BA.debugLine="End Sub";
return 0;
}
public static float  _getdevicephysicalsize(anywheresoftware.b4a.BA _ba) throws Exception{
RDebugUtils.currentModule="scale";
if (Debug.shouldDelegate(null, "getdevicephysicalsize"))
	return (Float) Debug.delegate(null, "getdevicephysicalsize", new Object[] {_ba});
anywheresoftware.b4a.keywords.LayoutValues _lv = null;
RDebugUtils.currentLine=1376256;
 //BA.debugLineNum = 1376256;BA.debugLine="Public Sub GetDevicePhysicalSize As Float";
RDebugUtils.currentLine=1376257;
 //BA.debugLineNum = 1376257;BA.debugLine="Dim lv As LayoutValues";
_lv = new anywheresoftware.b4a.keywords.LayoutValues();
RDebugUtils.currentLine=1376259;
 //BA.debugLineNum = 1376259;BA.debugLine="lv = GetDeviceLayoutValues";
_lv = anywheresoftware.b4a.keywords.Common.GetDeviceLayoutValues(_ba);
RDebugUtils.currentLine=1376260;
 //BA.debugLineNum = 1376260;BA.debugLine="Return Sqrt(Power(lv.Height / lv.Scale / 160, 2)";
if (true) return (float) (anywheresoftware.b4a.keywords.Common.Sqrt(anywheresoftware.b4a.keywords.Common.Power(_lv.Height/(double)_lv.Scale/(double)160,2)+anywheresoftware.b4a.keywords.Common.Power(_lv.Width/(double)_lv.Scale/(double)160,2)));
RDebugUtils.currentLine=1376261;
 //BA.debugLineNum = 1376261;BA.debugLine="End Sub";
return 0f;
}
public static double  _getscaleds(anywheresoftware.b4a.BA _ba) throws Exception{
RDebugUtils.currentModule="scale";
if (Debug.shouldDelegate(null, "getscaleds"))
	return (Double) Debug.delegate(null, "getscaleds", new Object[] {_ba});
RDebugUtils.currentLine=1245184;
 //BA.debugLineNum = 1245184;BA.debugLine="Public Sub GetScaleDS As Double";
RDebugUtils.currentLine=1245185;
 //BA.debugLineNum = 1245185;BA.debugLine="Return cScaleDS";
if (true) return _cscaleds;
RDebugUtils.currentLine=1245186;
 //BA.debugLineNum = 1245186;BA.debugLine="End Sub";
return 0;
}
public static double  _getscalex(anywheresoftware.b4a.BA _ba) throws Exception{
RDebugUtils.currentModule="scale";
if (Debug.shouldDelegate(null, "getscalex"))
	return (Double) Debug.delegate(null, "getscalex", new Object[] {_ba});
RDebugUtils.currentLine=851968;
 //BA.debugLineNum = 851968;BA.debugLine="Public Sub GetScaleX As Double";
RDebugUtils.currentLine=851969;
 //BA.debugLineNum = 851969;BA.debugLine="Return cScaleX";
if (true) return _cscalex;
RDebugUtils.currentLine=851970;
 //BA.debugLineNum = 851970;BA.debugLine="End Sub";
return 0;
}
public static double  _getscalex_l(anywheresoftware.b4a.BA _ba) throws Exception{
RDebugUtils.currentModule="scale";
if (Debug.shouldDelegate(null, "getscalex_l"))
	return (Double) Debug.delegate(null, "getscalex_l", new Object[] {_ba});
RDebugUtils.currentLine=1114112;
 //BA.debugLineNum = 1114112;BA.debugLine="Public Sub GetScaleX_L As Double";
RDebugUtils.currentLine=1114113;
 //BA.debugLineNum = 1114113;BA.debugLine="If GetDevicePhysicalSize < 6 Then";
if (_getdevicephysicalsize(_ba)<6) { 
RDebugUtils.currentLine=1114114;
 //BA.debugLineNum = 1114114;BA.debugLine="Return (100%y / (cRefWidth - 50dip) / cRefScale)";
if (true) return (anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (100),_ba)/(double)(_crefwidth-anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (50)))/(double)_crefscale);
 }else {
RDebugUtils.currentLine=1114116;
 //BA.debugLineNum = 1114116;BA.debugLine="Return (1 + Rate * (100%y / (cRefWidth - 50dip)";
if (true) return (1+_rate*(anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (100),_ba)/(double)(_crefwidth-anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (50)))/(double)_crefscale-1));
 };
RDebugUtils.currentLine=1114118;
 //BA.debugLineNum = 1114118;BA.debugLine="End Sub";
return 0;
}
public static double  _getscalex_p(anywheresoftware.b4a.BA _ba) throws Exception{
RDebugUtils.currentModule="scale";
if (Debug.shouldDelegate(null, "getscalex_p"))
	return (Double) Debug.delegate(null, "getscalex_p", new Object[] {_ba});
RDebugUtils.currentLine=1179648;
 //BA.debugLineNum = 1179648;BA.debugLine="Public Sub GetScaleX_P As Double";
RDebugUtils.currentLine=1179649;
 //BA.debugLineNum = 1179649;BA.debugLine="If GetDevicePhysicalSize < 6 Then";
if (_getdevicephysicalsize(_ba)<6) { 
RDebugUtils.currentLine=1179650;
 //BA.debugLineNum = 1179650;BA.debugLine="Return (100%y / cRefWidth / cRefScale)";
if (true) return (anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (100),_ba)/(double)_crefwidth/(double)_crefscale);
 }else {
RDebugUtils.currentLine=1179652;
 //BA.debugLineNum = 1179652;BA.debugLine="Return (1 + Rate * (100%y / (cRefWidth - 50dip)";
if (true) return (1+_rate*(anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (100),_ba)/(double)(_crefwidth-anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (50)))/(double)_crefscale-1));
 };
RDebugUtils.currentLine=1179654;
 //BA.debugLineNum = 1179654;BA.debugLine="End Sub";
return 0;
}
public static double  _getscaley(anywheresoftware.b4a.BA _ba) throws Exception{
RDebugUtils.currentModule="scale";
if (Debug.shouldDelegate(null, "getscaley"))
	return (Double) Debug.delegate(null, "getscaley", new Object[] {_ba});
RDebugUtils.currentLine=917504;
 //BA.debugLineNum = 917504;BA.debugLine="Public Sub GetScaleY As Double";
RDebugUtils.currentLine=917505;
 //BA.debugLineNum = 917505;BA.debugLine="Return cScaleY";
if (true) return _cscaley;
RDebugUtils.currentLine=917506;
 //BA.debugLineNum = 917506;BA.debugLine="End Sub";
return 0;
}
public static double  _getscaley_l(anywheresoftware.b4a.BA _ba) throws Exception{
RDebugUtils.currentModule="scale";
if (Debug.shouldDelegate(null, "getscaley_l"))
	return (Double) Debug.delegate(null, "getscaley_l", new Object[] {_ba});
RDebugUtils.currentLine=983040;
 //BA.debugLineNum = 983040;BA.debugLine="Public Sub GetScaleY_L As Double";
RDebugUtils.currentLine=983041;
 //BA.debugLineNum = 983041;BA.debugLine="If GetDevicePhysicalSize < 6 Then";
if (_getdevicephysicalsize(_ba)<6) { 
RDebugUtils.currentLine=983042;
 //BA.debugLineNum = 983042;BA.debugLine="Return (100%y / (cRefWidth - 50dip) / cRefScale)";
if (true) return (anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (100),_ba)/(double)(_crefwidth-anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (50)))/(double)_crefscale);
 }else {
RDebugUtils.currentLine=983044;
 //BA.debugLineNum = 983044;BA.debugLine="Return (1 + Rate * (100%y / (cRefWidth - 50dip)";
if (true) return (1+_rate*(anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (100),_ba)/(double)(_crefwidth-anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (50)))/(double)_crefscale-1));
 };
RDebugUtils.currentLine=983046;
 //BA.debugLineNum = 983046;BA.debugLine="End Sub";
return 0;
}
public static double  _getscaley_p(anywheresoftware.b4a.BA _ba) throws Exception{
RDebugUtils.currentModule="scale";
if (Debug.shouldDelegate(null, "getscaley_p"))
	return (Double) Debug.delegate(null, "getscaley_p", new Object[] {_ba});
RDebugUtils.currentLine=1048576;
 //BA.debugLineNum = 1048576;BA.debugLine="Public Sub GetScaleY_P As Double";
RDebugUtils.currentLine=1048577;
 //BA.debugLineNum = 1048577;BA.debugLine="If GetDevicePhysicalSize < 6 Then";
if (_getdevicephysicalsize(_ba)<6) { 
RDebugUtils.currentLine=1048578;
 //BA.debugLineNum = 1048578;BA.debugLine="Return (100%y / (cRefHeight - 50dip) / cRefScale";
if (true) return (anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (100),_ba)/(double)(_crefheight-anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (50)))/(double)_crefscale);
 }else {
RDebugUtils.currentLine=1048580;
 //BA.debugLineNum = 1048580;BA.debugLine="Return (1 + Rate * (100%y / (cRefHeight - 50dip)";
if (true) return (1+_rate*(anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (100),_ba)/(double)(_crefheight-anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (50)))/(double)_crefscale-1));
 };
RDebugUtils.currentLine=1048582;
 //BA.debugLineNum = 1048582;BA.debugLine="End Sub";
return 0;
}
public static String  _horizontalcenter(anywheresoftware.b4a.BA _ba,anywheresoftware.b4a.objects.ConcreteViewWrapper _v) throws Exception{
RDebugUtils.currentModule="scale";
if (Debug.shouldDelegate(null, "horizontalcenter"))
	return (String) Debug.delegate(null, "horizontalcenter", new Object[] {_ba,_v});
RDebugUtils.currentLine=1703936;
 //BA.debugLineNum = 1703936;BA.debugLine="Public Sub HorizontalCenter(v As View)";
RDebugUtils.currentLine=1703937;
 //BA.debugLineNum = 1703937;BA.debugLine="v.Left = (100%x - v.Width) / 2";
_v.setLeft((int) ((anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (100),_ba)-_v.getWidth())/(double)2));
RDebugUtils.currentLine=1703938;
 //BA.debugLineNum = 1703938;BA.debugLine="End Sub";
return "";
}
public static String  _horizontalcenter2(anywheresoftware.b4a.BA _ba,anywheresoftware.b4a.objects.ConcreteViewWrapper _v,anywheresoftware.b4a.objects.ConcreteViewWrapper _vleft,anywheresoftware.b4a.objects.ConcreteViewWrapper _vright) throws Exception{
RDebugUtils.currentModule="scale";
if (Debug.shouldDelegate(null, "horizontalcenter2"))
	return (String) Debug.delegate(null, "horizontalcenter2", new Object[] {_ba,_v,_vleft,_vright});
RDebugUtils.currentLine=1769472;
 //BA.debugLineNum = 1769472;BA.debugLine="Public Sub HorizontalCenter2(v As View, vLeft As V";
RDebugUtils.currentLine=1769473;
 //BA.debugLineNum = 1769473;BA.debugLine="v.Left = vLeft.Left + vLeft.Width + (vRight.Left";
_v.setLeft((int) (_vleft.getLeft()+_vleft.getWidth()+(_vright.getLeft()-(_vleft.getLeft()+_vleft.getWidth())-_v.getWidth())/(double)2));
RDebugUtils.currentLine=1769474;
 //BA.debugLineNum = 1769474;BA.debugLine="If IsActivity(v) Then";
if (_isactivity(_ba,_v)) { 
RDebugUtils.currentLine=1769475;
 //BA.debugLineNum = 1769475;BA.debugLine="ToastMessageShow(\"The view is an Activity !\", Fa";
anywheresoftware.b4a.keywords.Common.ToastMessageShow(BA.ObjectToCharSequence("The view is an Activity !"),anywheresoftware.b4a.keywords.Common.False);
RDebugUtils.currentLine=1769476;
 //BA.debugLineNum = 1769476;BA.debugLine="Return";
if (true) return "";
 }else {
RDebugUtils.currentLine=1769478;
 //BA.debugLineNum = 1769478;BA.debugLine="If IsActivity(vLeft) Then";
if (_isactivity(_ba,_vleft)) { 
RDebugUtils.currentLine=1769479;
 //BA.debugLineNum = 1769479;BA.debugLine="If IsActivity(vRight) Then";
if (_isactivity(_ba,_vright)) { 
RDebugUtils.currentLine=1769480;
 //BA.debugLineNum = 1769480;BA.debugLine="v.Left = (100%x - v.Width) / 2";
_v.setLeft((int) ((anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (100),_ba)-_v.getWidth())/(double)2));
 }else {
RDebugUtils.currentLine=1769482;
 //BA.debugLineNum = 1769482;BA.debugLine="v.Left = (vRight.Left - v.Width) / 2";
_v.setLeft((int) ((_vright.getLeft()-_v.getWidth())/(double)2));
 };
 }else {
RDebugUtils.currentLine=1769485;
 //BA.debugLineNum = 1769485;BA.debugLine="If IsActivity(vRight) Then";
if (_isactivity(_ba,_vright)) { 
RDebugUtils.currentLine=1769486;
 //BA.debugLineNum = 1769486;BA.debugLine="v.Left = vLeft.Left + vLeft.Width + (100%x - (";
_v.setLeft((int) (_vleft.getLeft()+_vleft.getWidth()+(anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (100),_ba)-(_vleft.getLeft()+_vleft.getWidth())-_v.getWidth())/(double)2));
 }else {
RDebugUtils.currentLine=1769488;
 //BA.debugLineNum = 1769488;BA.debugLine="v.Left = vLeft.Left + vLeft.Width + (vRight.Le";
_v.setLeft((int) (_vleft.getLeft()+_vleft.getWidth()+(_vright.getLeft()-(_vleft.getLeft()+_vleft.getWidth())-_v.getWidth())/(double)2));
 };
 };
 };
RDebugUtils.currentLine=1769492;
 //BA.debugLineNum = 1769492;BA.debugLine="End Sub";
return "";
}
public static boolean  _isactivity(anywheresoftware.b4a.BA _ba,anywheresoftware.b4a.objects.ConcreteViewWrapper _v) throws Exception{
RDebugUtils.currentModule="scale";
if (Debug.shouldDelegate(null, "isactivity"))
	return (Boolean) Debug.delegate(null, "isactivity", new Object[] {_ba,_v});
RDebugUtils.currentLine=2555904;
 //BA.debugLineNum = 2555904;BA.debugLine="Public Sub IsActivity(v As View) As Boolean";
RDebugUtils.currentLine=2555905;
 //BA.debugLineNum = 2555905;BA.debugLine="Try";
try {RDebugUtils.currentLine=2555906;
 //BA.debugLineNum = 2555906;BA.debugLine="v.Left = v.Left";
_v.setLeft(_v.getLeft());
RDebugUtils.currentLine=2555907;
 //BA.debugLineNum = 2555907;BA.debugLine="Return False";
if (true) return anywheresoftware.b4a.keywords.Common.False;
 } 
       catch (Exception e5) {
			(_ba.processBA == null ? _ba : _ba.processBA).setLastException(e5);RDebugUtils.currentLine=2555909;
 //BA.debugLineNum = 2555909;BA.debugLine="Return True";
if (true) return anywheresoftware.b4a.keywords.Common.True;
 };
RDebugUtils.currentLine=2555911;
 //BA.debugLineNum = 2555911;BA.debugLine="End Sub";
return false;
}
public static String  _initialize(anywheresoftware.b4a.BA _ba) throws Exception{
RDebugUtils.currentModule="scale";
if (Debug.shouldDelegate(null, "initialize"))
	return (String) Debug.delegate(null, "initialize", new Object[] {_ba});
double _devicescale = 0;
RDebugUtils.currentLine=786432;
 //BA.debugLineNum = 786432;BA.debugLine="Public Sub Initialize";
RDebugUtils.currentLine=786433;
 //BA.debugLineNum = 786433;BA.debugLine="Dim DeviceScale As Double";
_devicescale = 0;
RDebugUtils.currentLine=786434;
 //BA.debugLineNum = 786434;BA.debugLine="DeviceScale = 100dip / 100";
_devicescale = anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (100))/(double)100;
RDebugUtils.currentLine=786436;
 //BA.debugLineNum = 786436;BA.debugLine="If cRefHeight <> 480 Or cRefWidth <> 320 Or cRefS";
if (_crefheight!=480 || _crefwidth!=320 || _crefscale!=1) { 
RDebugUtils.currentLine=786437;
 //BA.debugLineNum = 786437;BA.debugLine="If 100%x > 100%y Then";
if (anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (100),_ba)>anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (100),_ba)) { 
RDebugUtils.currentLine=786439;
 //BA.debugLineNum = 786439;BA.debugLine="cScaleX = 100%x / cRefHeight / cRefScale / Devi";
_cscalex = anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (100),_ba)/(double)_crefheight/(double)_crefscale/(double)_devicescale;
RDebugUtils.currentLine=786440;
 //BA.debugLineNum = 786440;BA.debugLine="cScaleY = 100%y / (cRefWidth - 50 * cRefScale)";
_cscaley = anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (100),_ba)/(double)(_crefwidth-50*_crefscale)/(double)_devicescale;
 }else {
RDebugUtils.currentLine=786443;
 //BA.debugLineNum = 786443;BA.debugLine="cScaleX = 100%x / cRefWidth / cRefScale / Devic";
_cscalex = anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (100),_ba)/(double)_crefwidth/(double)_crefscale/(double)_devicescale;
RDebugUtils.currentLine=786444;
 //BA.debugLineNum = 786444;BA.debugLine="cScaleY = 100%y / (cRefHeight - 50dip * cRefSca";
_cscaley = anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (100),_ba)/(double)(_crefheight-anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (50))*_crefscale)/(double)_devicescale;
 };
 }else {
RDebugUtils.currentLine=786447;
 //BA.debugLineNum = 786447;BA.debugLine="If GetDevicePhysicalSize < 6 Then";
if (_getdevicephysicalsize(_ba)<6) { 
RDebugUtils.currentLine=786448;
 //BA.debugLineNum = 786448;BA.debugLine="If 100%x > 100%y Then";
if (anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (100),_ba)>anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (100),_ba)) { 
RDebugUtils.currentLine=786450;
 //BA.debugLineNum = 786450;BA.debugLine="cScaleX = 100%x / cRefHeight / cRefScale / Dev";
_cscalex = anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (100),_ba)/(double)_crefheight/(double)_crefscale/(double)_devicescale;
RDebugUtils.currentLine=786451;
 //BA.debugLineNum = 786451;BA.debugLine="cScaleY = 100%y / (cRefWidth - 50 * cRefScale)";
_cscaley = anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (100),_ba)/(double)(_crefwidth-50*_crefscale)/(double)_devicescale;
 }else {
RDebugUtils.currentLine=786454;
 //BA.debugLineNum = 786454;BA.debugLine="cScaleX = 100%x / cRefWidth / cRefScale / Devi";
_cscalex = anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (100),_ba)/(double)_crefwidth/(double)_crefscale/(double)_devicescale;
RDebugUtils.currentLine=786455;
 //BA.debugLineNum = 786455;BA.debugLine="cScaleY = 100%y / (cRefHeight - 50 * cRefScale";
_cscaley = anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (100),_ba)/(double)(_crefheight-50*_crefscale)/(double)_devicescale;
 };
 }else {
RDebugUtils.currentLine=786458;
 //BA.debugLineNum = 786458;BA.debugLine="If 100%x > 100%y Then";
if (anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (100),_ba)>anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (100),_ba)) { 
RDebugUtils.currentLine=786460;
 //BA.debugLineNum = 786460;BA.debugLine="cScaleX = 1 + Rate * (100%x / cRefHeight / cRe";
_cscalex = 1+_rate*(anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (100),_ba)/(double)_crefheight/(double)_crefscale-1)/(double)_devicescale;
RDebugUtils.currentLine=786461;
 //BA.debugLineNum = 786461;BA.debugLine="cScaleY = 1 + Rate * (100%y / (cRefWidth - 50";
_cscaley = 1+_rate*(anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (100),_ba)/(double)(_crefwidth-50*_crefscale)-1)/(double)_devicescale;
 }else {
RDebugUtils.currentLine=786464;
 //BA.debugLineNum = 786464;BA.debugLine="cScaleX = 1 + Rate * (100%x / cRefWidth / cRef";
_cscalex = 1+_rate*(anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (100),_ba)/(double)_crefwidth/(double)_crefscale-1)/(double)_devicescale;
RDebugUtils.currentLine=786465;
 //BA.debugLineNum = 786465;BA.debugLine="cScaleY = 1 + Rate * (100%y / (cRefHeight - 50";
_cscaley = 1+_rate*(anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (100),_ba)/(double)(_crefheight-anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (50))*_crefscale)-1)/(double)_devicescale;
 };
 };
RDebugUtils.currentLine=786468;
 //BA.debugLineNum = 786468;BA.debugLine="cScaleDS = 1 + Rate * ((100%x + 100%y) / (cRefWi";
_cscaleds = 1+_rate*((anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (100),_ba)+anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (100),_ba))/(double)(_crefwidth+_crefheight-50*_crefscale)-1)/(double)_devicescale;
 };
RDebugUtils.currentLine=786470;
 //BA.debugLineNum = 786470;BA.debugLine="End Sub";
return "";
}
public static boolean  _ispanel(anywheresoftware.b4a.BA _ba,anywheresoftware.b4a.objects.ConcreteViewWrapper _v) throws Exception{
RDebugUtils.currentModule="scale";
if (Debug.shouldDelegate(null, "ispanel"))
	return (Boolean) Debug.delegate(null, "ispanel", new Object[] {_ba,_v});
RDebugUtils.currentLine=2490368;
 //BA.debugLineNum = 2490368;BA.debugLine="Public Sub IsPanel(v As View) As Boolean";
RDebugUtils.currentLine=2490369;
 //BA.debugLineNum = 2490369;BA.debugLine="If GetType(v) = \"anywheresoftware.b4a.BALayout\" T";
if ((anywheresoftware.b4a.keywords.Common.GetType((Object)(_v.getObject()))).equals("anywheresoftware.b4a.BALayout")) { 
RDebugUtils.currentLine=2490370;
 //BA.debugLineNum = 2490370;BA.debugLine="Try";
try {RDebugUtils.currentLine=2490371;
 //BA.debugLineNum = 2490371;BA.debugLine="v.Left = v.Left";
_v.setLeft(_v.getLeft());
RDebugUtils.currentLine=2490372;
 //BA.debugLineNum = 2490372;BA.debugLine="Return True";
if (true) return anywheresoftware.b4a.keywords.Common.True;
 } 
       catch (Exception e6) {
			(_ba.processBA == null ? _ba : _ba.processBA).setLastException(e6);RDebugUtils.currentLine=2490374;
 //BA.debugLineNum = 2490374;BA.debugLine="Return False";
if (true) return anywheresoftware.b4a.keywords.Common.False;
 };
 }else {
RDebugUtils.currentLine=2490377;
 //BA.debugLineNum = 2490377;BA.debugLine="Return False";
if (true) return anywheresoftware.b4a.keywords.Common.False;
 };
RDebugUtils.currentLine=2490379;
 //BA.debugLineNum = 2490379;BA.debugLine="End Sub";
return false;
}
public static int  _right(anywheresoftware.b4a.BA _ba,anywheresoftware.b4a.objects.ConcreteViewWrapper _v) throws Exception{
RDebugUtils.currentModule="scale";
if (Debug.shouldDelegate(null, "right"))
	return (Integer) Debug.delegate(null, "right", new Object[] {_ba,_v});
RDebugUtils.currentLine=2359296;
 //BA.debugLineNum = 2359296;BA.debugLine="Public Sub Right(v As View) As Int";
RDebugUtils.currentLine=2359297;
 //BA.debugLineNum = 2359297;BA.debugLine="Return v.Left + v.Width";
if (true) return (int) (_v.getLeft()+_v.getWidth());
RDebugUtils.currentLine=2359298;
 //BA.debugLineNum = 2359298;BA.debugLine="End Sub";
return 0;
}
public static String  _scaleview(anywheresoftware.b4a.BA _ba,anywheresoftware.b4a.objects.ConcreteViewWrapper _v) throws Exception{
RDebugUtils.currentModule="scale";
if (Debug.shouldDelegate(null, "scaleview"))
	return (String) Debug.delegate(null, "scaleview", new Object[] {_ba,_v});
anywheresoftware.b4a.objects.PanelWrapper _pnl = null;
anywheresoftware.b4a.objects.LabelWrapper _lbl = null;
anywheresoftware.b4a.objects.ScrollViewWrapper _scv = null;
anywheresoftware.b4a.objects.HorizontalScrollViewWrapper _hcv = null;
flm.b4a.scrollview2d.ScrollView2DWrapper _scv2d = null;
anywheresoftware.b4a.objects.ListViewWrapper _ltv = null;
anywheresoftware.b4a.objects.SpinnerWrapper _spn = null;
RDebugUtils.currentLine=1441792;
 //BA.debugLineNum = 1441792;BA.debugLine="Public Sub ScaleView(v As View)";
RDebugUtils.currentLine=1441793;
 //BA.debugLineNum = 1441793;BA.debugLine="If IsActivity(v) Then";
if (_isactivity(_ba,_v)) { 
RDebugUtils.currentLine=1441794;
 //BA.debugLineNum = 1441794;BA.debugLine="Return";
if (true) return "";
 };
RDebugUtils.currentLine=1441797;
 //BA.debugLineNum = 1441797;BA.debugLine="v.Left = v.Left * cScaleX";
_v.setLeft((int) (_v.getLeft()*_cscalex));
RDebugUtils.currentLine=1441798;
 //BA.debugLineNum = 1441798;BA.debugLine="v.Top = v.Top * cScaleY";
_v.setTop((int) (_v.getTop()*_cscaley));
RDebugUtils.currentLine=1441799;
 //BA.debugLineNum = 1441799;BA.debugLine="If IsPanel(v) Then";
if (_ispanel(_ba,_v)) { 
RDebugUtils.currentLine=1441800;
 //BA.debugLineNum = 1441800;BA.debugLine="Dim pnl As Panel";
_pnl = new anywheresoftware.b4a.objects.PanelWrapper();
RDebugUtils.currentLine=1441801;
 //BA.debugLineNum = 1441801;BA.debugLine="pnl = v";
_pnl.setObject((android.view.ViewGroup)(_v.getObject()));
RDebugUtils.currentLine=1441802;
 //BA.debugLineNum = 1441802;BA.debugLine="If pnl.Background Is BitmapDrawable Then";
if (_pnl.getBackground() instanceof android.graphics.drawable.BitmapDrawable) { 
RDebugUtils.currentLine=1441805;
 //BA.debugLineNum = 1441805;BA.debugLine="v.Width = v.Width * Min(cScaleX, cScaleY)";
_v.setWidth((int) (_v.getWidth()*anywheresoftware.b4a.keywords.Common.Min(_cscalex,_cscaley)));
RDebugUtils.currentLine=1441806;
 //BA.debugLineNum = 1441806;BA.debugLine="v.Height = v.Height * Min(cScaleX, cScaleY)";
_v.setHeight((int) (_v.getHeight()*anywheresoftware.b4a.keywords.Common.Min(_cscalex,_cscaley)));
 }else {
RDebugUtils.currentLine=1441808;
 //BA.debugLineNum = 1441808;BA.debugLine="v.Width = v.Width * cScaleX";
_v.setWidth((int) (_v.getWidth()*_cscalex));
RDebugUtils.currentLine=1441809;
 //BA.debugLineNum = 1441809;BA.debugLine="v.Height = v.Height * cScaleY";
_v.setHeight((int) (_v.getHeight()*_cscaley));
 };
RDebugUtils.currentLine=1441811;
 //BA.debugLineNum = 1441811;BA.debugLine="ScaleAll(pnl, False)";
_scaleall(_ba,(anywheresoftware.b4a.objects.ActivityWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ActivityWrapper(), (anywheresoftware.b4a.BALayout)(_pnl.getObject())),anywheresoftware.b4a.keywords.Common.False);
 }else 
{RDebugUtils.currentLine=1441812;
 //BA.debugLineNum = 1441812;BA.debugLine="Else If v Is ImageView Then";
if (_v.getObjectOrNull() instanceof android.widget.ImageView) { 
RDebugUtils.currentLine=1441815;
 //BA.debugLineNum = 1441815;BA.debugLine="v.Width = v.Width * Min(cScaleX, cScaleY)";
_v.setWidth((int) (_v.getWidth()*anywheresoftware.b4a.keywords.Common.Min(_cscalex,_cscaley)));
RDebugUtils.currentLine=1441816;
 //BA.debugLineNum = 1441816;BA.debugLine="v.Height = v.Height * Min(cScaleX, cScaleY)";
_v.setHeight((int) (_v.getHeight()*anywheresoftware.b4a.keywords.Common.Min(_cscalex,_cscaley)));
 }else {
RDebugUtils.currentLine=1441818;
 //BA.debugLineNum = 1441818;BA.debugLine="v.Width = v.Width * cScaleX";
_v.setWidth((int) (_v.getWidth()*_cscalex));
RDebugUtils.currentLine=1441819;
 //BA.debugLineNum = 1441819;BA.debugLine="v.Height = v.Height * cScaleY";
_v.setHeight((int) (_v.getHeight()*_cscaley));
 }}
;
RDebugUtils.currentLine=1441822;
 //BA.debugLineNum = 1441822;BA.debugLine="If v Is Label Then 'this will catch ALL views wit";
if (_v.getObjectOrNull() instanceof android.widget.TextView) { 
RDebugUtils.currentLine=1441823;
 //BA.debugLineNum = 1441823;BA.debugLine="Dim lbl As Label = v";
_lbl = new anywheresoftware.b4a.objects.LabelWrapper();
_lbl.setObject((android.widget.TextView)(_v.getObject()));
RDebugUtils.currentLine=1441824;
 //BA.debugLineNum = 1441824;BA.debugLine="lbl.TextSize = lbl.TextSize * cScaleX";
_lbl.setTextSize((float) (_lbl.getTextSize()*_cscalex));
 };
RDebugUtils.currentLine=1441827;
 //BA.debugLineNum = 1441827;BA.debugLine="If GetType(v) = \"anywheresoftware.b4a.objects.Scr";
if ((anywheresoftware.b4a.keywords.Common.GetType((Object)(_v.getObject()))).equals("anywheresoftware.b4a.objects.ScrollViewWrapper$MyScrollView")) { 
RDebugUtils.currentLine=1441830;
 //BA.debugLineNum = 1441830;BA.debugLine="Dim scv As ScrollView";
_scv = new anywheresoftware.b4a.objects.ScrollViewWrapper();
RDebugUtils.currentLine=1441831;
 //BA.debugLineNum = 1441831;BA.debugLine="scv = v";
_scv.setObject((android.widget.ScrollView)(_v.getObject()));
RDebugUtils.currentLine=1441832;
 //BA.debugLineNum = 1441832;BA.debugLine="ScaleAll(scv.Panel, False)";
_scaleall(_ba,(anywheresoftware.b4a.objects.ActivityWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ActivityWrapper(), (anywheresoftware.b4a.BALayout)(_scv.getPanel().getObject())),anywheresoftware.b4a.keywords.Common.False);
RDebugUtils.currentLine=1441833;
 //BA.debugLineNum = 1441833;BA.debugLine="scv.Panel.Height = scv.Panel.Height * cScaleY";
_scv.getPanel().setHeight((int) (_scv.getPanel().getHeight()*_cscaley));
 }else 
{RDebugUtils.currentLine=1441834;
 //BA.debugLineNum = 1441834;BA.debugLine="Else If GetType(v) = \"anywheresoftware.b4a.object";
if ((anywheresoftware.b4a.keywords.Common.GetType((Object)(_v.getObject()))).equals("anywheresoftware.b4a.objects.HorizontalScrollViewWrapper$MyHScrollView")) { 
RDebugUtils.currentLine=1441837;
 //BA.debugLineNum = 1441837;BA.debugLine="Dim hcv As HorizontalScrollView";
_hcv = new anywheresoftware.b4a.objects.HorizontalScrollViewWrapper();
RDebugUtils.currentLine=1441838;
 //BA.debugLineNum = 1441838;BA.debugLine="hcv = v";
_hcv.setObject((android.widget.HorizontalScrollView)(_v.getObject()));
RDebugUtils.currentLine=1441839;
 //BA.debugLineNum = 1441839;BA.debugLine="ScaleAll(hcv.Panel, False)";
_scaleall(_ba,(anywheresoftware.b4a.objects.ActivityWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ActivityWrapper(), (anywheresoftware.b4a.BALayout)(_hcv.getPanel().getObject())),anywheresoftware.b4a.keywords.Common.False);
RDebugUtils.currentLine=1441840;
 //BA.debugLineNum = 1441840;BA.debugLine="hcv.Panel.Width = hcv.Panel.Width * cScaleX";
_hcv.getPanel().setWidth((int) (_hcv.getPanel().getWidth()*_cscalex));
 }else 
{RDebugUtils.currentLine=1441841;
 //BA.debugLineNum = 1441841;BA.debugLine="Else If GetType(v) = \"flm.b4a.scrollview2d.Scroll";
if ((anywheresoftware.b4a.keywords.Common.GetType((Object)(_v.getObject()))).equals("flm.b4a.scrollview2d.ScrollView2DWrapper$MyScrollView")) { 
RDebugUtils.currentLine=1441844;
 //BA.debugLineNum = 1441844;BA.debugLine="Dim scv2d As ScrollView2D";
_scv2d = new flm.b4a.scrollview2d.ScrollView2DWrapper();
RDebugUtils.currentLine=1441845;
 //BA.debugLineNum = 1441845;BA.debugLine="scv2d = v";
_scv2d.setObject((flm.b4a.scrollview2d.TwoDScrollView)(_v.getObject()));
RDebugUtils.currentLine=1441846;
 //BA.debugLineNum = 1441846;BA.debugLine="ScaleAll(scv2d.Panel, False)";
_scaleall(_ba,(anywheresoftware.b4a.objects.ActivityWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ActivityWrapper(), (anywheresoftware.b4a.BALayout)(_scv2d.getPanel().getObject())),anywheresoftware.b4a.keywords.Common.False);
RDebugUtils.currentLine=1441847;
 //BA.debugLineNum = 1441847;BA.debugLine="scv2d.Panel.Width = scv2d.Panel.Width * cScaleX";
_scv2d.getPanel().setWidth((int) (_scv2d.getPanel().getWidth()*_cscalex));
RDebugUtils.currentLine=1441848;
 //BA.debugLineNum = 1441848;BA.debugLine="scv2d.Panel.Height = scv2d.Panel.Height * cScale";
_scv2d.getPanel().setHeight((int) (_scv2d.getPanel().getHeight()*_cscaley));
 }else 
{RDebugUtils.currentLine=1441849;
 //BA.debugLineNum = 1441849;BA.debugLine="Else If GetType(v) = \"anywheresoftware.b4a.object";
if ((anywheresoftware.b4a.keywords.Common.GetType((Object)(_v.getObject()))).equals("anywheresoftware.b4a.objects.ListViewWrapper$SimpleListView")) { 
RDebugUtils.currentLine=1441852;
 //BA.debugLineNum = 1441852;BA.debugLine="Dim ltv As ListView";
_ltv = new anywheresoftware.b4a.objects.ListViewWrapper();
RDebugUtils.currentLine=1441853;
 //BA.debugLineNum = 1441853;BA.debugLine="ltv = v";
_ltv.setObject((anywheresoftware.b4a.objects.ListViewWrapper.SimpleListView)(_v.getObject()));
RDebugUtils.currentLine=1441854;
 //BA.debugLineNum = 1441854;BA.debugLine="ScaleView(ltv.SingleLineLayout.Label)";
_scaleview(_ba,(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(_ltv.getSingleLineLayout().Label.getObject())));
RDebugUtils.currentLine=1441855;
 //BA.debugLineNum = 1441855;BA.debugLine="ltv.SingleLineLayout.ItemHeight = ltv.SingleLine";
_ltv.getSingleLineLayout().setItemHeight((int) (_ltv.getSingleLineLayout().getItemHeight()*_cscaley));
RDebugUtils.currentLine=1441857;
 //BA.debugLineNum = 1441857;BA.debugLine="ScaleView(ltv.TwoLinesLayout.Label)";
_scaleview(_ba,(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(_ltv.getTwoLinesLayout().Label.getObject())));
RDebugUtils.currentLine=1441858;
 //BA.debugLineNum = 1441858;BA.debugLine="ScaleView(ltv.TwoLinesLayout.SecondLabel)";
_scaleview(_ba,(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(_ltv.getTwoLinesLayout().SecondLabel.getObject())));
RDebugUtils.currentLine=1441859;
 //BA.debugLineNum = 1441859;BA.debugLine="ltv.TwoLinesLayout.ItemHeight = ltv.TwoLinesLayo";
_ltv.getTwoLinesLayout().setItemHeight((int) (_ltv.getTwoLinesLayout().getItemHeight()*_cscaley));
RDebugUtils.currentLine=1441861;
 //BA.debugLineNum = 1441861;BA.debugLine="ScaleView(ltv.TwoLinesAndBitmap.Label)";
_scaleview(_ba,(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(_ltv.getTwoLinesAndBitmap().Label.getObject())));
RDebugUtils.currentLine=1441862;
 //BA.debugLineNum = 1441862;BA.debugLine="ScaleView(ltv.TwoLinesAndBitmap.SecondLabel)";
_scaleview(_ba,(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(_ltv.getTwoLinesAndBitmap().SecondLabel.getObject())));
RDebugUtils.currentLine=1441863;
 //BA.debugLineNum = 1441863;BA.debugLine="ScaleView(ltv.TwoLinesAndBitmap.ImageView)";
_scaleview(_ba,(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(_ltv.getTwoLinesAndBitmap().ImageView.getObject())));
RDebugUtils.currentLine=1441864;
 //BA.debugLineNum = 1441864;BA.debugLine="ltv.TwoLinesAndBitmap.ItemHeight = ltv.TwoLinesA";
_ltv.getTwoLinesAndBitmap().setItemHeight((int) (_ltv.getTwoLinesAndBitmap().getItemHeight()*_cscaley));
RDebugUtils.currentLine=1441866;
 //BA.debugLineNum = 1441866;BA.debugLine="ltv.TwoLinesAndBitmap.ImageView.Top = (ltv.TwoLi";
_ltv.getTwoLinesAndBitmap().ImageView.setTop((int) ((_ltv.getTwoLinesAndBitmap().getItemHeight()-_ltv.getTwoLinesAndBitmap().ImageView.getHeight())/(double)2));
 }else 
{RDebugUtils.currentLine=1441867;
 //BA.debugLineNum = 1441867;BA.debugLine="Else If GetType(v) = \"anywheresoftware.b4a.object";
if ((anywheresoftware.b4a.keywords.Common.GetType((Object)(_v.getObject()))).equals("anywheresoftware.b4a.objects.SpinnerWrapper$B4ASpinner")) { 
RDebugUtils.currentLine=1441870;
 //BA.debugLineNum = 1441870;BA.debugLine="Dim spn As Spinner";
_spn = new anywheresoftware.b4a.objects.SpinnerWrapper();
RDebugUtils.currentLine=1441871;
 //BA.debugLineNum = 1441871;BA.debugLine="spn = v";
_spn.setObject((anywheresoftware.b4a.objects.SpinnerWrapper.B4ASpinner)(_v.getObject()));
RDebugUtils.currentLine=1441872;
 //BA.debugLineNum = 1441872;BA.debugLine="spn.TextSize = spn.TextSize * cScaleX";
_spn.setTextSize((float) (_spn.getTextSize()*_cscalex));
 }}}}}
;
RDebugUtils.currentLine=1441874;
 //BA.debugLineNum = 1441874;BA.debugLine="End Sub";
return "";
}
public static String  _scaleallds(anywheresoftware.b4a.BA _ba,anywheresoftware.b4a.objects.ActivityWrapper _act,boolean _firsttime) throws Exception{
RDebugUtils.currentModule="scale";
if (Debug.shouldDelegate(null, "scaleallds"))
	return (String) Debug.delegate(null, "scaleallds", new Object[] {_ba,_act,_firsttime});
int _i = 0;
anywheresoftware.b4a.objects.ConcreteViewWrapper _v = null;
RDebugUtils.currentLine=1638400;
 //BA.debugLineNum = 1638400;BA.debugLine="Public Sub ScaleAllDS(act As Activity, FirstTime A";
RDebugUtils.currentLine=1638401;
 //BA.debugLineNum = 1638401;BA.debugLine="Dim I As Int";
_i = 0;
RDebugUtils.currentLine=1638404;
 //BA.debugLineNum = 1638404;BA.debugLine="If IsPanel(act) AND FirstTime = True Then";
if (_ispanel(_ba,(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(_act.getObject()))) && _firsttime==anywheresoftware.b4a.keywords.Common.True) { 
RDebugUtils.currentLine=1638406;
 //BA.debugLineNum = 1638406;BA.debugLine="ScaleViewDS(act)";
_scaleviewds(_ba,(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(_act.getObject())));
 }else {
RDebugUtils.currentLine=1638408;
 //BA.debugLineNum = 1638408;BA.debugLine="For I = 0 To act.NumberOfViews - 1";
{
final int step5 = 1;
final int limit5 = (int) (_act.getNumberOfViews()-1);
_i = (int) (0) ;
for (;(step5 > 0 && _i <= limit5) || (step5 < 0 && _i >= limit5) ;_i = ((int)(0 + _i + step5))  ) {
RDebugUtils.currentLine=1638409;
 //BA.debugLineNum = 1638409;BA.debugLine="Dim v As View";
_v = new anywheresoftware.b4a.objects.ConcreteViewWrapper();
RDebugUtils.currentLine=1638410;
 //BA.debugLineNum = 1638410;BA.debugLine="v = act.GetView(I)";
_v = _act.GetView(_i);
RDebugUtils.currentLine=1638411;
 //BA.debugLineNum = 1638411;BA.debugLine="ScaleViewDS(v)";
_scaleviewds(_ba,_v);
 }
};
 };
RDebugUtils.currentLine=1638414;
 //BA.debugLineNum = 1638414;BA.debugLine="End Sub";
return "";
}
public static String  _scaleviewds(anywheresoftware.b4a.BA _ba,anywheresoftware.b4a.objects.ConcreteViewWrapper _v) throws Exception{
RDebugUtils.currentModule="scale";
if (Debug.shouldDelegate(null, "scaleviewds"))
	return (String) Debug.delegate(null, "scaleviewds", new Object[] {_ba,_v});
anywheresoftware.b4a.objects.PanelWrapper _pnl = null;
anywheresoftware.b4a.objects.LabelWrapper _lbl = null;
anywheresoftware.b4a.objects.ScrollViewWrapper _scv = null;
anywheresoftware.b4a.objects.HorizontalScrollViewWrapper _hcv = null;
flm.b4a.scrollview2d.ScrollView2DWrapper _scv2d = null;
anywheresoftware.b4a.objects.ListViewWrapper _ltv = null;
anywheresoftware.b4a.objects.SpinnerWrapper _spn = null;
RDebugUtils.currentLine=1572864;
 //BA.debugLineNum = 1572864;BA.debugLine="Public Sub ScaleViewDS(v As View)";
RDebugUtils.currentLine=1572865;
 //BA.debugLineNum = 1572865;BA.debugLine="If IsActivity(v) Then";
if (_isactivity(_ba,_v)) { 
RDebugUtils.currentLine=1572866;
 //BA.debugLineNum = 1572866;BA.debugLine="Return";
if (true) return "";
 };
RDebugUtils.currentLine=1572869;
 //BA.debugLineNum = 1572869;BA.debugLine="v.Left = v.Left * cScaleDS";
_v.setLeft((int) (_v.getLeft()*_cscaleds));
RDebugUtils.currentLine=1572870;
 //BA.debugLineNum = 1572870;BA.debugLine="v.Top = v.Top * cScaleDS";
_v.setTop((int) (_v.getTop()*_cscaleds));
RDebugUtils.currentLine=1572871;
 //BA.debugLineNum = 1572871;BA.debugLine="v.Width = v.Width * cScaleDS";
_v.setWidth((int) (_v.getWidth()*_cscaleds));
RDebugUtils.currentLine=1572872;
 //BA.debugLineNum = 1572872;BA.debugLine="v.Height = v.Height * cScaleDS";
_v.setHeight((int) (_v.getHeight()*_cscaleds));
RDebugUtils.currentLine=1572874;
 //BA.debugLineNum = 1572874;BA.debugLine="If IsPanel(v) Then";
if (_ispanel(_ba,_v)) { 
RDebugUtils.currentLine=1572875;
 //BA.debugLineNum = 1572875;BA.debugLine="Dim pnl As Panel";
_pnl = new anywheresoftware.b4a.objects.PanelWrapper();
RDebugUtils.currentLine=1572876;
 //BA.debugLineNum = 1572876;BA.debugLine="pnl = v";
_pnl.setObject((android.view.ViewGroup)(_v.getObject()));
RDebugUtils.currentLine=1572877;
 //BA.debugLineNum = 1572877;BA.debugLine="ScaleAllDS(pnl, False)";
_scaleallds(_ba,(anywheresoftware.b4a.objects.ActivityWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ActivityWrapper(), (anywheresoftware.b4a.BALayout)(_pnl.getObject())),anywheresoftware.b4a.keywords.Common.False);
 };
RDebugUtils.currentLine=1572880;
 //BA.debugLineNum = 1572880;BA.debugLine="If v Is Label Then 'this will catch ALL views wit";
if (_v.getObjectOrNull() instanceof android.widget.TextView) { 
RDebugUtils.currentLine=1572881;
 //BA.debugLineNum = 1572881;BA.debugLine="Dim lbl As Label = v";
_lbl = new anywheresoftware.b4a.objects.LabelWrapper();
_lbl.setObject((android.widget.TextView)(_v.getObject()));
RDebugUtils.currentLine=1572882;
 //BA.debugLineNum = 1572882;BA.debugLine="lbl.TextSize = lbl.TextSize * cScaleDS";
_lbl.setTextSize((float) (_lbl.getTextSize()*_cscaleds));
 };
RDebugUtils.currentLine=1572885;
 //BA.debugLineNum = 1572885;BA.debugLine="If GetType(v) = \"anywheresoftware.b4a.objects.Scr";
if ((anywheresoftware.b4a.keywords.Common.GetType((Object)(_v.getObject()))).equals("anywheresoftware.b4a.objects.ScrollViewWrapper$MyScrollView")) { 
RDebugUtils.currentLine=1572888;
 //BA.debugLineNum = 1572888;BA.debugLine="Dim scv As ScrollView";
_scv = new anywheresoftware.b4a.objects.ScrollViewWrapper();
RDebugUtils.currentLine=1572889;
 //BA.debugLineNum = 1572889;BA.debugLine="scv = v";
_scv.setObject((android.widget.ScrollView)(_v.getObject()));
RDebugUtils.currentLine=1572890;
 //BA.debugLineNum = 1572890;BA.debugLine="ScaleAllDS(scv.Panel, False)";
_scaleallds(_ba,(anywheresoftware.b4a.objects.ActivityWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ActivityWrapper(), (anywheresoftware.b4a.BALayout)(_scv.getPanel().getObject())),anywheresoftware.b4a.keywords.Common.False);
RDebugUtils.currentLine=1572891;
 //BA.debugLineNum = 1572891;BA.debugLine="scv.Panel.Height = scv.Panel.Height * cScaleDS";
_scv.getPanel().setHeight((int) (_scv.getPanel().getHeight()*_cscaleds));
 }else 
{RDebugUtils.currentLine=1572892;
 //BA.debugLineNum = 1572892;BA.debugLine="Else If GetType(v) = \"anywheresoftware.b4a.object";
if ((anywheresoftware.b4a.keywords.Common.GetType((Object)(_v.getObject()))).equals("anywheresoftware.b4a.objects.HorizontalScrollViewWrapper$MyHScrollView")) { 
RDebugUtils.currentLine=1572895;
 //BA.debugLineNum = 1572895;BA.debugLine="Dim hcv As HorizontalScrollView";
_hcv = new anywheresoftware.b4a.objects.HorizontalScrollViewWrapper();
RDebugUtils.currentLine=1572896;
 //BA.debugLineNum = 1572896;BA.debugLine="hcv = v";
_hcv.setObject((android.widget.HorizontalScrollView)(_v.getObject()));
RDebugUtils.currentLine=1572897;
 //BA.debugLineNum = 1572897;BA.debugLine="ScaleAllDS(hcv.Panel, False)";
_scaleallds(_ba,(anywheresoftware.b4a.objects.ActivityWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ActivityWrapper(), (anywheresoftware.b4a.BALayout)(_hcv.getPanel().getObject())),anywheresoftware.b4a.keywords.Common.False);
RDebugUtils.currentLine=1572898;
 //BA.debugLineNum = 1572898;BA.debugLine="hcv.Panel.Width = hcv.Panel.Width * cScaleDS";
_hcv.getPanel().setWidth((int) (_hcv.getPanel().getWidth()*_cscaleds));
 }else 
{RDebugUtils.currentLine=1572899;
 //BA.debugLineNum = 1572899;BA.debugLine="Else If GetType(v) = \"flm.b4a.scrollview2d.Scroll";
if ((anywheresoftware.b4a.keywords.Common.GetType((Object)(_v.getObject()))).equals("flm.b4a.scrollview2d.ScrollView2DWrapper$MyScrollView")) { 
RDebugUtils.currentLine=1572902;
 //BA.debugLineNum = 1572902;BA.debugLine="Dim scv2d As ScrollView2D";
_scv2d = new flm.b4a.scrollview2d.ScrollView2DWrapper();
RDebugUtils.currentLine=1572903;
 //BA.debugLineNum = 1572903;BA.debugLine="scv2d = v";
_scv2d.setObject((flm.b4a.scrollview2d.TwoDScrollView)(_v.getObject()));
RDebugUtils.currentLine=1572904;
 //BA.debugLineNum = 1572904;BA.debugLine="ScaleAllDS(scv2d.Panel, False)";
_scaleallds(_ba,(anywheresoftware.b4a.objects.ActivityWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ActivityWrapper(), (anywheresoftware.b4a.BALayout)(_scv2d.getPanel().getObject())),anywheresoftware.b4a.keywords.Common.False);
RDebugUtils.currentLine=1572905;
 //BA.debugLineNum = 1572905;BA.debugLine="scv2d.Panel.Width = scv2d.Panel.Width * cScaleDS";
_scv2d.getPanel().setWidth((int) (_scv2d.getPanel().getWidth()*_cscaleds));
RDebugUtils.currentLine=1572906;
 //BA.debugLineNum = 1572906;BA.debugLine="scv2d.Panel.Height = scv2d.Panel.Height * cScale";
_scv2d.getPanel().setHeight((int) (_scv2d.getPanel().getHeight()*_cscaleds));
 }else 
{RDebugUtils.currentLine=1572907;
 //BA.debugLineNum = 1572907;BA.debugLine="Else If GetType(v) = \"anywheresoftware.b4a.object";
if ((anywheresoftware.b4a.keywords.Common.GetType((Object)(_v.getObject()))).equals("anywheresoftware.b4a.objects.ListViewWrapper$SimpleListView")) { 
RDebugUtils.currentLine=1572910;
 //BA.debugLineNum = 1572910;BA.debugLine="Dim ltv As ListView";
_ltv = new anywheresoftware.b4a.objects.ListViewWrapper();
RDebugUtils.currentLine=1572911;
 //BA.debugLineNum = 1572911;BA.debugLine="ltv = v";
_ltv.setObject((anywheresoftware.b4a.objects.ListViewWrapper.SimpleListView)(_v.getObject()));
RDebugUtils.currentLine=1572912;
 //BA.debugLineNum = 1572912;BA.debugLine="ScaleViewDS(ltv.SingleLineLayout.Label)";
_scaleviewds(_ba,(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(_ltv.getSingleLineLayout().Label.getObject())));
RDebugUtils.currentLine=1572913;
 //BA.debugLineNum = 1572913;BA.debugLine="ltv.SingleLineLayout.ItemHeight = ltv.SingleLine";
_ltv.getSingleLineLayout().setItemHeight((int) (_ltv.getSingleLineLayout().getItemHeight()*_cscaleds));
RDebugUtils.currentLine=1572915;
 //BA.debugLineNum = 1572915;BA.debugLine="ScaleViewDS(ltv.TwoLinesLayout.Label)";
_scaleviewds(_ba,(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(_ltv.getTwoLinesLayout().Label.getObject())));
RDebugUtils.currentLine=1572916;
 //BA.debugLineNum = 1572916;BA.debugLine="ScaleViewDS(ltv.TwoLinesLayout.SecondLabel)";
_scaleviewds(_ba,(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(_ltv.getTwoLinesLayout().SecondLabel.getObject())));
RDebugUtils.currentLine=1572917;
 //BA.debugLineNum = 1572917;BA.debugLine="ltv.TwoLinesLayout.ItemHeight = ltv.TwoLinesLayo";
_ltv.getTwoLinesLayout().setItemHeight((int) (_ltv.getTwoLinesLayout().getItemHeight()*_cscaleds));
RDebugUtils.currentLine=1572919;
 //BA.debugLineNum = 1572919;BA.debugLine="ScaleViewDS(ltv.TwoLinesAndBitmap.Label)";
_scaleviewds(_ba,(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(_ltv.getTwoLinesAndBitmap().Label.getObject())));
RDebugUtils.currentLine=1572920;
 //BA.debugLineNum = 1572920;BA.debugLine="ScaleViewDS(ltv.TwoLinesAndBitmap.SecondLabel)";
_scaleviewds(_ba,(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(_ltv.getTwoLinesAndBitmap().SecondLabel.getObject())));
RDebugUtils.currentLine=1572921;
 //BA.debugLineNum = 1572921;BA.debugLine="ScaleViewDS(ltv.TwoLinesAndBitmap.ImageView)";
_scaleviewds(_ba,(anywheresoftware.b4a.objects.ConcreteViewWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.ConcreteViewWrapper(), (android.view.View)(_ltv.getTwoLinesAndBitmap().ImageView.getObject())));
RDebugUtils.currentLine=1572922;
 //BA.debugLineNum = 1572922;BA.debugLine="ltv.TwoLinesAndBitmap.ItemHeight = ltv.TwoLinesA";
_ltv.getTwoLinesAndBitmap().setItemHeight((int) (_ltv.getTwoLinesAndBitmap().getItemHeight()*_cscaleds));
RDebugUtils.currentLine=1572924;
 //BA.debugLineNum = 1572924;BA.debugLine="ltv.TwoLinesAndBitmap.ImageView.Top = (ltv.TwoLi";
_ltv.getTwoLinesAndBitmap().ImageView.setTop((int) ((_ltv.getTwoLinesAndBitmap().getItemHeight()-_ltv.getTwoLinesAndBitmap().ImageView.getHeight())/(double)2));
 }else 
{RDebugUtils.currentLine=1572925;
 //BA.debugLineNum = 1572925;BA.debugLine="Else If GetType(v) = \"anywheresoftware.b4a.object";
if ((anywheresoftware.b4a.keywords.Common.GetType((Object)(_v.getObject()))).equals("anywheresoftware.b4a.objects.SpinnerWrapper$B4ASpinner")) { 
RDebugUtils.currentLine=1572928;
 //BA.debugLineNum = 1572928;BA.debugLine="Dim spn As Spinner";
_spn = new anywheresoftware.b4a.objects.SpinnerWrapper();
RDebugUtils.currentLine=1572929;
 //BA.debugLineNum = 1572929;BA.debugLine="spn = v";
_spn.setObject((anywheresoftware.b4a.objects.SpinnerWrapper.B4ASpinner)(_v.getObject()));
RDebugUtils.currentLine=1572930;
 //BA.debugLineNum = 1572930;BA.debugLine="spn.TextSize = spn.TextSize * cScaleDS";
_spn.setTextSize((float) (_spn.getTextSize()*_cscaleds));
 }}}}}
;
RDebugUtils.currentLine=1572932;
 //BA.debugLineNum = 1572932;BA.debugLine="End Sub";
return "";
}
public static String  _setbottom(anywheresoftware.b4a.BA _ba,anywheresoftware.b4a.objects.ConcreteViewWrapper _v,int _ybottom) throws Exception{
RDebugUtils.currentModule="scale";
if (Debug.shouldDelegate(null, "setbottom"))
	return (String) Debug.delegate(null, "setbottom", new Object[] {_ba,_v,_ybottom});
RDebugUtils.currentLine=2031616;
 //BA.debugLineNum = 2031616;BA.debugLine="Sub SetBottom(v As View, yBottom As Int)";
RDebugUtils.currentLine=2031617;
 //BA.debugLineNum = 2031617;BA.debugLine="v.Top = yBottom - v.Height";
_v.setTop((int) (_ybottom-_v.getHeight()));
RDebugUtils.currentLine=2031618;
 //BA.debugLineNum = 2031618;BA.debugLine="End Sub";
return "";
}
public static String  _setleftandright(anywheresoftware.b4a.BA _ba,anywheresoftware.b4a.objects.ConcreteViewWrapper _v,int _xleft,int _xright) throws Exception{
RDebugUtils.currentModule="scale";
if (Debug.shouldDelegate(null, "setleftandright"))
	return (String) Debug.delegate(null, "setleftandright", new Object[] {_ba,_v,_xleft,_xright});
RDebugUtils.currentLine=2097152;
 //BA.debugLineNum = 2097152;BA.debugLine="Public Sub SetLeftAndRight(v As View, xLeft As Int";
RDebugUtils.currentLine=2097154;
 //BA.debugLineNum = 2097154;BA.debugLine="v.Left = Min(xLeft, xRight)";
_v.setLeft((int) (anywheresoftware.b4a.keywords.Common.Min(_xleft,_xright)));
RDebugUtils.currentLine=2097155;
 //BA.debugLineNum = 2097155;BA.debugLine="v.Width = Max(xLeft, xRight) - v.Left";
_v.setWidth((int) (anywheresoftware.b4a.keywords.Common.Max(_xleft,_xright)-_v.getLeft()));
RDebugUtils.currentLine=2097156;
 //BA.debugLineNum = 2097156;BA.debugLine="End Sub";
return "";
}
public static String  _setleftandright2(anywheresoftware.b4a.BA _ba,anywheresoftware.b4a.objects.ConcreteViewWrapper _v,anywheresoftware.b4a.objects.ConcreteViewWrapper _vleft,int _dxl,anywheresoftware.b4a.objects.ConcreteViewWrapper _vright,int _dxr) throws Exception{
RDebugUtils.currentModule="scale";
if (Debug.shouldDelegate(null, "setleftandright2"))
	return (String) Debug.delegate(null, "setleftandright2", new Object[] {_ba,_v,_vleft,_dxl,_vright,_dxr});
anywheresoftware.b4a.objects.ConcreteViewWrapper _v1 = null;
RDebugUtils.currentLine=2162688;
 //BA.debugLineNum = 2162688;BA.debugLine="Public Sub SetLeftAndRight2(v As View, vLeft As Vi";
RDebugUtils.currentLine=2162690;
 //BA.debugLineNum = 2162690;BA.debugLine="If IsActivity(v) Then";
if (_isactivity(_ba,_v)) { 
RDebugUtils.currentLine=2162691;
 //BA.debugLineNum = 2162691;BA.debugLine="ToastMessageShow(\"The view is an Activity !\", Fa";
anywheresoftware.b4a.keywords.Common.ToastMessageShow(BA.ObjectToCharSequence("The view is an Activity !"),anywheresoftware.b4a.keywords.Common.False);
RDebugUtils.currentLine=2162692;
 //BA.debugLineNum = 2162692;BA.debugLine="Return";
if (true) return "";
 };
RDebugUtils.currentLine=2162696;
 //BA.debugLineNum = 2162696;BA.debugLine="If IsActivity(vLeft) = False AND IsActivity(vRigh";
if (_isactivity(_ba,_vleft)==anywheresoftware.b4a.keywords.Common.False && _isactivity(_ba,_vright)==anywheresoftware.b4a.keywords.Common.False) { 
RDebugUtils.currentLine=2162697;
 //BA.debugLineNum = 2162697;BA.debugLine="If vLeft.Left > vRight.Left Then";
if (_vleft.getLeft()>_vright.getLeft()) { 
RDebugUtils.currentLine=2162698;
 //BA.debugLineNum = 2162698;BA.debugLine="Dim v1 As View";
_v1 = new anywheresoftware.b4a.objects.ConcreteViewWrapper();
RDebugUtils.currentLine=2162699;
 //BA.debugLineNum = 2162699;BA.debugLine="v1 = vLeft";
_v1 = _vleft;
RDebugUtils.currentLine=2162700;
 //BA.debugLineNum = 2162700;BA.debugLine="vLeft = vRight";
_vleft = _vright;
RDebugUtils.currentLine=2162701;
 //BA.debugLineNum = 2162701;BA.debugLine="vRight = v1";
_vright = _v1;
 };
 };
RDebugUtils.currentLine=2162705;
 //BA.debugLineNum = 2162705;BA.debugLine="If IsActivity(vLeft) Then";
if (_isactivity(_ba,_vleft)) { 
RDebugUtils.currentLine=2162706;
 //BA.debugLineNum = 2162706;BA.debugLine="v.Left = dxL";
_v.setLeft(_dxl);
RDebugUtils.currentLine=2162707;
 //BA.debugLineNum = 2162707;BA.debugLine="If IsActivity(vRight) Then";
if (_isactivity(_ba,_vright)) { 
RDebugUtils.currentLine=2162708;
 //BA.debugLineNum = 2162708;BA.debugLine="v.Width = 100%x - dxL - dxR";
_v.setWidth((int) (anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (100),_ba)-_dxl-_dxr));
 }else {
RDebugUtils.currentLine=2162710;
 //BA.debugLineNum = 2162710;BA.debugLine="v.Width = vRight.Left - dxL - dxR";
_v.setWidth((int) (_vright.getLeft()-_dxl-_dxr));
 };
 }else {
RDebugUtils.currentLine=2162713;
 //BA.debugLineNum = 2162713;BA.debugLine="v.Left = vLeft.Left + vLeft.Width + dxL";
_v.setLeft((int) (_vleft.getLeft()+_vleft.getWidth()+_dxl));
RDebugUtils.currentLine=2162714;
 //BA.debugLineNum = 2162714;BA.debugLine="If IsActivity(vRight) Then";
if (_isactivity(_ba,_vright)) { 
RDebugUtils.currentLine=2162715;
 //BA.debugLineNum = 2162715;BA.debugLine="v.Width = 100%x - v.Left";
_v.setWidth((int) (anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (100),_ba)-_v.getLeft()));
 }else {
RDebugUtils.currentLine=2162717;
 //BA.debugLineNum = 2162717;BA.debugLine="v.Width = vRight.Left - v.Left - dxR";
_v.setWidth((int) (_vright.getLeft()-_v.getLeft()-_dxr));
 };
 };
RDebugUtils.currentLine=2162720;
 //BA.debugLineNum = 2162720;BA.debugLine="End Sub";
return "";
}
public static String  _setreferencelayout(anywheresoftware.b4a.BA _ba,int _width,int _height,double _scale) throws Exception{
RDebugUtils.currentModule="scale";
if (Debug.shouldDelegate(null, "setreferencelayout"))
	return (String) Debug.delegate(null, "setreferencelayout", new Object[] {_ba,_width,_height,_scale});
RDebugUtils.currentLine=2621440;
 //BA.debugLineNum = 2621440;BA.debugLine="Public Sub SetReferenceLayout(Width As Int, Height";
RDebugUtils.currentLine=2621441;
 //BA.debugLineNum = 2621441;BA.debugLine="If cRefWidth < cRefHeight Then";
if (_crefwidth<_crefheight) { 
RDebugUtils.currentLine=2621442;
 //BA.debugLineNum = 2621442;BA.debugLine="cRefWidth = Width";
_crefwidth = _width;
RDebugUtils.currentLine=2621443;
 //BA.debugLineNum = 2621443;BA.debugLine="cRefHeight = Height";
_crefheight = _height;
 }else {
RDebugUtils.currentLine=2621445;
 //BA.debugLineNum = 2621445;BA.debugLine="cRefWidth = Height";
_crefwidth = _height;
RDebugUtils.currentLine=2621446;
 //BA.debugLineNum = 2621446;BA.debugLine="cRefHeight = Width";
_crefheight = _width;
 };
RDebugUtils.currentLine=2621448;
 //BA.debugLineNum = 2621448;BA.debugLine="cRefScale = Scale";
_crefscale = _scale;
RDebugUtils.currentLine=2621449;
 //BA.debugLineNum = 2621449;BA.debugLine="Initialize";
_initialize(_ba);
RDebugUtils.currentLine=2621450;
 //BA.debugLineNum = 2621450;BA.debugLine="End Sub";
return "";
}
public static String  _setright(anywheresoftware.b4a.BA _ba,anywheresoftware.b4a.objects.ConcreteViewWrapper _v,int _xright) throws Exception{
RDebugUtils.currentModule="scale";
if (Debug.shouldDelegate(null, "setright"))
	return (String) Debug.delegate(null, "setright", new Object[] {_ba,_v,_xright});
RDebugUtils.currentLine=1966080;
 //BA.debugLineNum = 1966080;BA.debugLine="Sub SetRight(v As View, xRight As Int)";
RDebugUtils.currentLine=1966081;
 //BA.debugLineNum = 1966081;BA.debugLine="v.Left = xRight - v.Width";
_v.setLeft((int) (_xright-_v.getWidth()));
RDebugUtils.currentLine=1966082;
 //BA.debugLineNum = 1966082;BA.debugLine="End Sub";
return "";
}
public static String  _settopandbottom(anywheresoftware.b4a.BA _ba,anywheresoftware.b4a.objects.ConcreteViewWrapper _v,int _ytop,int _ybottom) throws Exception{
RDebugUtils.currentModule="scale";
if (Debug.shouldDelegate(null, "settopandbottom"))
	return (String) Debug.delegate(null, "settopandbottom", new Object[] {_ba,_v,_ytop,_ybottom});
RDebugUtils.currentLine=2228224;
 //BA.debugLineNum = 2228224;BA.debugLine="Public Sub SetTopAndBottom(v As View, yTop As Int,";
RDebugUtils.currentLine=2228226;
 //BA.debugLineNum = 2228226;BA.debugLine="v.Top = Min(yTop, yBottom)";
_v.setTop((int) (anywheresoftware.b4a.keywords.Common.Min(_ytop,_ybottom)));
RDebugUtils.currentLine=2228227;
 //BA.debugLineNum = 2228227;BA.debugLine="v.Height = Max(yTop, yBottom) - v.Top";
_v.setHeight((int) (anywheresoftware.b4a.keywords.Common.Max(_ytop,_ybottom)-_v.getTop()));
RDebugUtils.currentLine=2228228;
 //BA.debugLineNum = 2228228;BA.debugLine="End Sub";
return "";
}
public static String  _settopandbottom2(anywheresoftware.b4a.BA _ba,anywheresoftware.b4a.objects.ConcreteViewWrapper _v,anywheresoftware.b4a.objects.ConcreteViewWrapper _vtop,int _dyt,anywheresoftware.b4a.objects.ConcreteViewWrapper _vbottom,int _dyb) throws Exception{
RDebugUtils.currentModule="scale";
if (Debug.shouldDelegate(null, "settopandbottom2"))
	return (String) Debug.delegate(null, "settopandbottom2", new Object[] {_ba,_v,_vtop,_dyt,_vbottom,_dyb});
anywheresoftware.b4a.objects.ConcreteViewWrapper _v1 = null;
RDebugUtils.currentLine=2293760;
 //BA.debugLineNum = 2293760;BA.debugLine="Public Sub SetTopAndBottom2(v As View, vTop As Vie";
RDebugUtils.currentLine=2293762;
 //BA.debugLineNum = 2293762;BA.debugLine="If IsActivity(v) Then";
if (_isactivity(_ba,_v)) { 
RDebugUtils.currentLine=2293763;
 //BA.debugLineNum = 2293763;BA.debugLine="ToastMessageShow(\"The view is an Activity !\", Fa";
anywheresoftware.b4a.keywords.Common.ToastMessageShow(BA.ObjectToCharSequence("The view is an Activity !"),anywheresoftware.b4a.keywords.Common.False);
RDebugUtils.currentLine=2293764;
 //BA.debugLineNum = 2293764;BA.debugLine="Return";
if (true) return "";
 };
RDebugUtils.currentLine=2293767;
 //BA.debugLineNum = 2293767;BA.debugLine="If IsActivity(vTop) = False AND IsActivity(vBotto";
if (_isactivity(_ba,_vtop)==anywheresoftware.b4a.keywords.Common.False && _isactivity(_ba,_vbottom)==anywheresoftware.b4a.keywords.Common.False) { 
RDebugUtils.currentLine=2293769;
 //BA.debugLineNum = 2293769;BA.debugLine="If vTop.Top > vBottom.Top Then";
if (_vtop.getTop()>_vbottom.getTop()) { 
RDebugUtils.currentLine=2293770;
 //BA.debugLineNum = 2293770;BA.debugLine="Dim v1 As View";
_v1 = new anywheresoftware.b4a.objects.ConcreteViewWrapper();
RDebugUtils.currentLine=2293771;
 //BA.debugLineNum = 2293771;BA.debugLine="v1 = vTop";
_v1 = _vtop;
RDebugUtils.currentLine=2293772;
 //BA.debugLineNum = 2293772;BA.debugLine="vTop = vBottom";
_vtop = _vbottom;
RDebugUtils.currentLine=2293773;
 //BA.debugLineNum = 2293773;BA.debugLine="vBottom = v1";
_vbottom = _v1;
 };
 };
RDebugUtils.currentLine=2293777;
 //BA.debugLineNum = 2293777;BA.debugLine="If IsActivity(vTop) Then";
if (_isactivity(_ba,_vtop)) { 
RDebugUtils.currentLine=2293778;
 //BA.debugLineNum = 2293778;BA.debugLine="v.Top = dyT";
_v.setTop(_dyt);
RDebugUtils.currentLine=2293779;
 //BA.debugLineNum = 2293779;BA.debugLine="If IsActivity(vBottom) Then";
if (_isactivity(_ba,_vbottom)) { 
RDebugUtils.currentLine=2293780;
 //BA.debugLineNum = 2293780;BA.debugLine="v.Height = 100%y - dyT - dyB";
_v.setHeight((int) (anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (100),_ba)-_dyt-_dyb));
 }else {
RDebugUtils.currentLine=2293782;
 //BA.debugLineNum = 2293782;BA.debugLine="v.Height = vBottom.Top - dyT - dyB";
_v.setHeight((int) (_vbottom.getTop()-_dyt-_dyb));
 };
 }else {
RDebugUtils.currentLine=2293785;
 //BA.debugLineNum = 2293785;BA.debugLine="v.Top = vTop.Top + vTop.Height + dyT";
_v.setTop((int) (_vtop.getTop()+_vtop.getHeight()+_dyt));
RDebugUtils.currentLine=2293786;
 //BA.debugLineNum = 2293786;BA.debugLine="If IsActivity(vBottom) Then";
if (_isactivity(_ba,_vbottom)) { 
RDebugUtils.currentLine=2293787;
 //BA.debugLineNum = 2293787;BA.debugLine="v.Height = 100%y - v.Top - dyB";
_v.setHeight((int) (anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (100),_ba)-_v.getTop()-_dyb));
 }else {
RDebugUtils.currentLine=2293789;
 //BA.debugLineNum = 2293789;BA.debugLine="v.Height = vBottom.Top - v.Top - dyB";
_v.setHeight((int) (_vbottom.getTop()-_v.getTop()-_dyb));
 };
 };
RDebugUtils.currentLine=2293792;
 //BA.debugLineNum = 2293792;BA.debugLine="End Sub";
return "";
}
public static String  _verticalcenter(anywheresoftware.b4a.BA _ba,anywheresoftware.b4a.objects.ConcreteViewWrapper _v) throws Exception{
RDebugUtils.currentModule="scale";
if (Debug.shouldDelegate(null, "verticalcenter"))
	return (String) Debug.delegate(null, "verticalcenter", new Object[] {_ba,_v});
RDebugUtils.currentLine=1835008;
 //BA.debugLineNum = 1835008;BA.debugLine="Public Sub VerticalCenter(v As View)";
RDebugUtils.currentLine=1835009;
 //BA.debugLineNum = 1835009;BA.debugLine="v.Top = (100%y - v.Height) / 2";
_v.setTop((int) ((anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (100),_ba)-_v.getHeight())/(double)2));
RDebugUtils.currentLine=1835010;
 //BA.debugLineNum = 1835010;BA.debugLine="End Sub";
return "";
}
public static String  _verticalcenter2(anywheresoftware.b4a.BA _ba,anywheresoftware.b4a.objects.ConcreteViewWrapper _v,anywheresoftware.b4a.objects.ConcreteViewWrapper _vtop,anywheresoftware.b4a.objects.ConcreteViewWrapper _vbottom) throws Exception{
RDebugUtils.currentModule="scale";
if (Debug.shouldDelegate(null, "verticalcenter2"))
	return (String) Debug.delegate(null, "verticalcenter2", new Object[] {_ba,_v,_vtop,_vbottom});
RDebugUtils.currentLine=1900544;
 //BA.debugLineNum = 1900544;BA.debugLine="Public Sub VerticalCenter2(v As View, vTop As View";
RDebugUtils.currentLine=1900545;
 //BA.debugLineNum = 1900545;BA.debugLine="If IsActivity(v) Then";
if (_isactivity(_ba,_v)) { 
RDebugUtils.currentLine=1900546;
 //BA.debugLineNum = 1900546;BA.debugLine="ToastMessageShow(\"The view is an Activity !\", Fa";
anywheresoftware.b4a.keywords.Common.ToastMessageShow(BA.ObjectToCharSequence("The view is an Activity !"),anywheresoftware.b4a.keywords.Common.False);
RDebugUtils.currentLine=1900547;
 //BA.debugLineNum = 1900547;BA.debugLine="Return";
if (true) return "";
 }else {
RDebugUtils.currentLine=1900549;
 //BA.debugLineNum = 1900549;BA.debugLine="If IsActivity(vTop) Then";
if (_isactivity(_ba,_vtop)) { 
RDebugUtils.currentLine=1900550;
 //BA.debugLineNum = 1900550;BA.debugLine="If IsActivity(vBottom) Then";
if (_isactivity(_ba,_vbottom)) { 
RDebugUtils.currentLine=1900551;
 //BA.debugLineNum = 1900551;BA.debugLine="v.Top = (100%y - v.Height) / 2";
_v.setTop((int) ((anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (100),_ba)-_v.getHeight())/(double)2));
 }else {
RDebugUtils.currentLine=1900553;
 //BA.debugLineNum = 1900553;BA.debugLine="v.Top = (vBottom.Top - v.Height) / 2";
_v.setTop((int) ((_vbottom.getTop()-_v.getHeight())/(double)2));
 };
 }else {
RDebugUtils.currentLine=1900556;
 //BA.debugLineNum = 1900556;BA.debugLine="If IsActivity(vBottom) Then";
if (_isactivity(_ba,_vbottom)) { 
RDebugUtils.currentLine=1900557;
 //BA.debugLineNum = 1900557;BA.debugLine="v.Top = vTop.Top + vTop.Height + (100%y - (vTo";
_v.setTop((int) (_vtop.getTop()+_vtop.getHeight()+(anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (100),_ba)-(_vtop.getTop()+_vtop.getHeight())-_v.getHeight())/(double)2));
 }else {
RDebugUtils.currentLine=1900559;
 //BA.debugLineNum = 1900559;BA.debugLine="v.Top = vTop.Top + vTop.Height + (vBottom.Top";
_v.setTop((int) (_vtop.getTop()+_vtop.getHeight()+(_vbottom.getTop()-(_vtop.getTop()+_vtop.getHeight())-_v.getHeight())/(double)2));
 };
 };
 };
RDebugUtils.currentLine=1900563;
 //BA.debugLineNum = 1900563;BA.debugLine="End Sub";
return "";
}
}