package b4a.example;


import anywheresoftware.b4a.B4AMenuItem;
import android.app.Activity;
import android.os.Bundle;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.B4AActivity;
import anywheresoftware.b4a.ObjectWrapper;
import anywheresoftware.b4a.objects.ActivityWrapper;
import java.lang.reflect.InvocationTargetException;
import anywheresoftware.b4a.B4AUncaughtException;
import anywheresoftware.b4a.debug.*;
import java.lang.ref.WeakReference;

public class ze_basis extends Activity implements B4AActivity{
	public static ze_basis mostCurrent;
	static boolean afterFirstLayout;
	static boolean isFirst = true;
    private static boolean processGlobalsRun = false;
	BALayout layout;
	public static BA processBA;
	BA activityBA;
    ActivityWrapper _activity;
    java.util.ArrayList<B4AMenuItem> menuItems;
	public static final boolean fullScreen = true;
	public static final boolean includeTitle = false;
    public static WeakReference<Activity> previousOne;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (isFirst) {
			processBA = new anywheresoftware.b4a.ShellBA(this.getApplicationContext(), null, null, "b4a.example", "b4a.example.ze_basis");
			processBA.loadHtSubs(this.getClass());
	        float deviceScale = getApplicationContext().getResources().getDisplayMetrics().density;
	        BALayout.setDeviceScale(deviceScale);
            
		}
		else if (previousOne != null) {
			Activity p = previousOne.get();
			if (p != null && p != this) {
                BA.LogInfo("Killing previous instance (ze_basis).");
				p.finish();
			}
		}
        processBA.runHook("oncreate", this, null);
		if (!includeTitle) {
        	this.getWindow().requestFeature(android.view.Window.FEATURE_NO_TITLE);
        }
        if (fullScreen) {
        	getWindow().setFlags(android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN,   
        			android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
		mostCurrent = this;
        processBA.sharedProcessBA.activityBA = null;
		layout = new BALayout(this);
		setContentView(layout);
		afterFirstLayout = false;
        WaitForLayout wl = new WaitForLayout();
        if (anywheresoftware.b4a.objects.ServiceHelper.StarterHelper.startFromActivity(processBA, wl, false))
		    BA.handler.postDelayed(wl, 5);

	}
	static class WaitForLayout implements Runnable {
		public void run() {
			if (afterFirstLayout)
				return;
			if (mostCurrent == null)
				return;
            
			if (mostCurrent.layout.getWidth() == 0) {
				BA.handler.postDelayed(this, 5);
				return;
			}
			mostCurrent.layout.getLayoutParams().height = mostCurrent.layout.getHeight();
			mostCurrent.layout.getLayoutParams().width = mostCurrent.layout.getWidth();
			afterFirstLayout = true;
			mostCurrent.afterFirstLayout();
		}
	}
	private void afterFirstLayout() {
        if (this != mostCurrent)
			return;
		activityBA = new BA(this, layout, processBA, "b4a.example", "b4a.example.ze_basis");
        
        processBA.sharedProcessBA.activityBA = new java.lang.ref.WeakReference<BA>(activityBA);
        anywheresoftware.b4a.objects.ViewWrapper.lastId = 0;
        _activity = new ActivityWrapper(activityBA, "activity");
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        if (BA.isShellModeRuntimeCheck(processBA)) {
			if (isFirst)
				processBA.raiseEvent2(null, true, "SHELL", false);
			processBA.raiseEvent2(null, true, "CREATE", true, "b4a.example.ze_basis", processBA, activityBA, _activity, anywheresoftware.b4a.keywords.Common.Density, mostCurrent);
			_activity.reinitializeForShell(activityBA, "activity");
		}
        initializeProcessGlobals();		
        initializeGlobals();
        
        BA.LogInfo("** Activity (ze_basis) Create, isFirst = " + isFirst + " **");
        processBA.raiseEvent2(null, true, "activity_create", false, isFirst);
		isFirst = false;
		if (this != mostCurrent)
			return;
        processBA.setActivityPaused(false);
        BA.LogInfo("** Activity (ze_basis) Resume **");
        processBA.raiseEvent(null, "activity_resume");
        if (android.os.Build.VERSION.SDK_INT >= 11) {
			try {
				android.app.Activity.class.getMethod("invalidateOptionsMenu").invoke(this,(Object[]) null);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}
	public void addMenuItem(B4AMenuItem item) {
		if (menuItems == null)
			menuItems = new java.util.ArrayList<B4AMenuItem>();
		menuItems.add(item);
	}
	@Override
	public boolean onCreateOptionsMenu(android.view.Menu menu) {
		super.onCreateOptionsMenu(menu);
        try {
            if (processBA.subExists("activity_actionbarhomeclick")) {
                Class.forName("android.app.ActionBar").getMethod("setHomeButtonEnabled", boolean.class).invoke(
                    getClass().getMethod("getActionBar").invoke(this), true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (processBA.runHook("oncreateoptionsmenu", this, new Object[] {menu}))
            return true;
		if (menuItems == null)
			return false;
		for (B4AMenuItem bmi : menuItems) {
			android.view.MenuItem mi = menu.add(bmi.title);
			if (bmi.drawable != null)
				mi.setIcon(bmi.drawable);
            if (android.os.Build.VERSION.SDK_INT >= 11) {
				try {
                    if (bmi.addToBar) {
				        android.view.MenuItem.class.getMethod("setShowAsAction", int.class).invoke(mi, 1);
                    }
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			mi.setOnMenuItemClickListener(new B4AMenuItemsClickListener(bmi.eventName.toLowerCase(BA.cul)));
		}
        
		return true;
	}   
 @Override
 public boolean onOptionsItemSelected(android.view.MenuItem item) {
    if (item.getItemId() == 16908332) {
        processBA.raiseEvent(null, "activity_actionbarhomeclick");
        return true;
    }
    else
        return super.onOptionsItemSelected(item); 
}
@Override
 public boolean onPrepareOptionsMenu(android.view.Menu menu) {
    super.onPrepareOptionsMenu(menu);
    processBA.runHook("onprepareoptionsmenu", this, new Object[] {menu});
    return true;
    
 }
 protected void onStart() {
    super.onStart();
    processBA.runHook("onstart", this, null);
}
 protected void onStop() {
    super.onStop();
    processBA.runHook("onstop", this, null);
}
    public void onWindowFocusChanged(boolean hasFocus) {
       super.onWindowFocusChanged(hasFocus);
       if (processBA.subExists("activity_windowfocuschanged"))
           processBA.raiseEvent2(null, true, "activity_windowfocuschanged", false, hasFocus);
    }
	private class B4AMenuItemsClickListener implements android.view.MenuItem.OnMenuItemClickListener {
		private final String eventName;
		public B4AMenuItemsClickListener(String eventName) {
			this.eventName = eventName;
		}
		public boolean onMenuItemClick(android.view.MenuItem item) {
			processBA.raiseEventFromUI(item.getTitle(), eventName + "_click");
			return true;
		}
	}
    public static Class<?> getObject() {
		return ze_basis.class;
	}
    private Boolean onKeySubExist = null;
    private Boolean onKeyUpSubExist = null;
	@Override
	public boolean onKeyDown(int keyCode, android.view.KeyEvent event) {
        if (processBA.runHook("onkeydown", this, new Object[] {keyCode, event}))
            return true;
		if (onKeySubExist == null)
			onKeySubExist = processBA.subExists("activity_keypress");
		if (onKeySubExist) {
			if (keyCode == anywheresoftware.b4a.keywords.constants.KeyCodes.KEYCODE_BACK &&
					android.os.Build.VERSION.SDK_INT >= 18) {
				HandleKeyDelayed hk = new HandleKeyDelayed();
				hk.kc = keyCode;
				BA.handler.post(hk);
				return true;
			}
			else {
				boolean res = new HandleKeyDelayed().runDirectly(keyCode);
				if (res)
					return true;
			}
		}
		return super.onKeyDown(keyCode, event);
	}
	private class HandleKeyDelayed implements Runnable {
		int kc;
		public void run() {
			runDirectly(kc);
		}
		public boolean runDirectly(int keyCode) {
			Boolean res =  (Boolean)processBA.raiseEvent2(_activity, false, "activity_keypress", false, keyCode);
			if (res == null || res == true) {
                return true;
            }
            else if (keyCode == anywheresoftware.b4a.keywords.constants.KeyCodes.KEYCODE_BACK) {
				finish();
				return true;
			}
            return false;
		}
		
	}
    @Override
	public boolean onKeyUp(int keyCode, android.view.KeyEvent event) {
        if (processBA.runHook("onkeyup", this, new Object[] {keyCode, event}))
            return true;
		if (onKeyUpSubExist == null)
			onKeyUpSubExist = processBA.subExists("activity_keyup");
		if (onKeyUpSubExist) {
			Boolean res =  (Boolean)processBA.raiseEvent2(_activity, false, "activity_keyup", false, keyCode);
			if (res == null || res == true)
				return true;
		}
		return super.onKeyUp(keyCode, event);
	}
	@Override
	public void onNewIntent(android.content.Intent intent) {
        super.onNewIntent(intent);
		this.setIntent(intent);
        processBA.runHook("onnewintent", this, new Object[] {intent});
	}
    @Override 
	public void onPause() {
		super.onPause();
        if (_activity == null) //workaround for emulator bug (Issue 2423)
            return;
		anywheresoftware.b4a.Msgbox.dismiss(true);
        BA.LogInfo("** Activity (ze_basis) Pause, UserClosed = " + activityBA.activity.isFinishing() + " **");
        processBA.raiseEvent2(_activity, true, "activity_pause", false, activityBA.activity.isFinishing());		
        processBA.setActivityPaused(true);
        mostCurrent = null;
        if (!activityBA.activity.isFinishing())
			previousOne = new WeakReference<Activity>(this);
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        processBA.runHook("onpause", this, null);
	}

	@Override
	public void onDestroy() {
        super.onDestroy();
		previousOne = null;
        processBA.runHook("ondestroy", this, null);
	}
    @Override 
	public void onResume() {
		super.onResume();
        mostCurrent = this;
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        if (activityBA != null) { //will be null during activity create (which waits for AfterLayout).
        	ResumeMessage rm = new ResumeMessage(mostCurrent);
        	BA.handler.post(rm);
        }
        processBA.runHook("onresume", this, null);
	}
    private static class ResumeMessage implements Runnable {
    	private final WeakReference<Activity> activity;
    	public ResumeMessage(Activity activity) {
    		this.activity = new WeakReference<Activity>(activity);
    	}
		public void run() {
			if (mostCurrent == null || mostCurrent != activity.get())
				return;
			processBA.setActivityPaused(false);
            BA.LogInfo("** Activity (ze_basis) Resume **");
		    processBA.raiseEvent(mostCurrent._activity, "activity_resume", (Object[])null);
		}
    }
	@Override
	protected void onActivityResult(int requestCode, int resultCode,
	      android.content.Intent data) {
		processBA.onActivityResult(requestCode, resultCode, data);
        processBA.runHook("onactivityresult", this, new Object[] {requestCode, resultCode});
	}
	private static void initializeGlobals() {
		processBA.raiseEvent2(null, true, "globals", false, (Object[])null);
	}
    public void onRequestPermissionsResult(int requestCode,
        String permissions[], int[] grantResults) {
        for (int i = 0;i < permissions.length;i++) {
            Object[] o = new Object[] {permissions[i], grantResults[i] == 0};
            processBA.raiseEventFromDifferentThread(null,null, 0, "activity_permissionresult", true, o);
        }
            
    }



public static void initializeProcessGlobals() {
             try {
                Class.forName(BA.applicationContext.getPackageName() + ".main").getMethod("initializeProcessGlobals").invoke(null, null);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
}
public anywheresoftware.b4a.keywords.Common __c = null;
public static b4a.example.update_sender_main _u1 = null;
public static anywheresoftware.b4a.objects.SaxParser _saxparser = null;
public static String _status = "";
public anywheresoftware.b4a.objects.ButtonWrapper _bu_zeit = null;
public anywheresoftware.b4a.samples.httputils2.httputils2service _httputils2service = null;
public b4a.example.main _main = null;
public b4a.example.starter _starter = null;
public b4a.example.scale _scale = null;
public b4a.example.setup _setup = null;
public static String  _activity_create(boolean _firsttime) throws Exception{
RDebugUtils.currentModule="ze_basis";
if (Debug.shouldDelegate(mostCurrent.activityBA, "activity_create"))
	return (String) Debug.delegate(mostCurrent.activityBA, "activity_create", new Object[] {_firsttime});
anywheresoftware.b4a.sql.SQL.CursorWrapper _c_status = null;
RDebugUtils.currentLine=5373952;
 //BA.debugLineNum = 5373952;BA.debugLine="Sub Activity_Create(FirstTime As Boolean)";
RDebugUtils.currentLine=5373956;
 //BA.debugLineNum = 5373956;BA.debugLine="Try";
try {RDebugUtils.currentLine=5373959;
 //BA.debugLineNum = 5373959;BA.debugLine="Dim c_status As Cursor";
_c_status = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
RDebugUtils.currentLine=5373960;
 //BA.debugLineNum = 5373960;BA.debugLine="c_status=Main.sql1.ExecQuery(\"select Status from";
_c_status.setObject((android.database.Cursor)(mostCurrent._main._sql1.ExecQuery("select Status from tbl_ze where Zeit =(Select Max(zeit) from tbl_ze)")));
RDebugUtils.currentLine=5373961;
 //BA.debugLineNum = 5373961;BA.debugLine="c_status.Position=0";
_c_status.setPosition((int) (0));
RDebugUtils.currentLine=5373962;
 //BA.debugLineNum = 5373962;BA.debugLine="status=c_status.GetString(\"Status\")";
mostCurrent._status = _c_status.GetString("Status");
 } 
       catch (Exception e7) {
			processBA.setLastException(e7);RDebugUtils.currentLine=5373964;
 //BA.debugLineNum = 5373964;BA.debugLine="status=\"0\"";
mostCurrent._status = "0";
 };
RDebugUtils.currentLine=5373966;
 //BA.debugLineNum = 5373966;BA.debugLine="scale.SetRate(0.3)";
mostCurrent._scale._setrate(mostCurrent.activityBA,0.3);
RDebugUtils.currentLine=5373967;
 //BA.debugLineNum = 5373967;BA.debugLine="Activity.LoadLayout(\"ze_basis1\")";
mostCurrent._activity.LoadLayout("ze_basis1",mostCurrent.activityBA);
RDebugUtils.currentLine=5373968;
 //BA.debugLineNum = 5373968;BA.debugLine="If status=\"Kommen\" Then";
if ((mostCurrent._status).equals("Kommen")) { 
RDebugUtils.currentLine=5373969;
 //BA.debugLineNum = 5373969;BA.debugLine="bu_zeit.Text=\"Gehen\"";
mostCurrent._bu_zeit.setText(BA.ObjectToCharSequence("Gehen"));
 }else {
RDebugUtils.currentLine=5373971;
 //BA.debugLineNum = 5373971;BA.debugLine="bu_zeit.text=\"Kommen\"";
mostCurrent._bu_zeit.setText(BA.ObjectToCharSequence("Kommen"));
 };
RDebugUtils.currentLine=5373974;
 //BA.debugLineNum = 5373974;BA.debugLine="End Sub";
return "";
}
public static String  _activity_pause(boolean _userclosed) throws Exception{
RDebugUtils.currentModule="ze_basis";
RDebugUtils.currentLine=5505024;
 //BA.debugLineNum = 5505024;BA.debugLine="Sub Activity_Pause (UserClosed As Boolean)";
RDebugUtils.currentLine=5505026;
 //BA.debugLineNum = 5505026;BA.debugLine="End Sub";
return "";
}
public static String  _activity_resume() throws Exception{
RDebugUtils.currentModule="ze_basis";
if (Debug.shouldDelegate(mostCurrent.activityBA, "activity_resume"))
	return (String) Debug.delegate(mostCurrent.activityBA, "activity_resume", null);
RDebugUtils.currentLine=5439488;
 //BA.debugLineNum = 5439488;BA.debugLine="Sub Activity_Resume";
RDebugUtils.currentLine=5439490;
 //BA.debugLineNum = 5439490;BA.debugLine="End Sub";
return "";
}
public static String  _bu_zeit_click() throws Exception{
RDebugUtils.currentModule="ze_basis";
if (Debug.shouldDelegate(mostCurrent.activityBA, "bu_zeit_click"))
	return (String) Debug.delegate(mostCurrent.activityBA, "bu_zeit_click", null);
anywheresoftware.b4a.keywords.StringBuilderWrapper _sb = null;
RDebugUtils.currentLine=5570560;
 //BA.debugLineNum = 5570560;BA.debugLine="Sub bu_zeit_click";
RDebugUtils.currentLine=5570561;
 //BA.debugLineNum = 5570561;BA.debugLine="DateTime.DateFormat=\"yyyy-MM-dd hh:mm:ss\"";
anywheresoftware.b4a.keywords.Common.DateTime.setDateFormat("yyyy-MM-dd hh:mm:ss");
RDebugUtils.currentLine=5570563;
 //BA.debugLineNum = 5570563;BA.debugLine="Dim sb As StringBuilder";
_sb = new anywheresoftware.b4a.keywords.StringBuilderWrapper();
RDebugUtils.currentLine=5570564;
 //BA.debugLineNum = 5570564;BA.debugLine="sb.initialize";
_sb.Initialize();
RDebugUtils.currentLine=5570565;
 //BA.debugLineNum = 5570565;BA.debugLine="sb.Append(\"insert into tbl_ze (usr,Zeit,Status) \"";
_sb.Append("insert into tbl_ze (usr,Zeit,Status) ");
RDebugUtils.currentLine=5570566;
 //BA.debugLineNum = 5570566;BA.debugLine="sb.Append(\"values ('\" & u1.usr  & \"',\")";
_sb.Append("values ('"+_u1._usr+"',");
RDebugUtils.currentLine=5570567;
 //BA.debugLineNum = 5570567;BA.debugLine="sb.Append(\"'\" & DateTime.Date(DateTime.Now)  & \"'";
_sb.Append("'"+anywheresoftware.b4a.keywords.Common.DateTime.Date(anywheresoftware.b4a.keywords.Common.DateTime.getNow())+"',");
RDebugUtils.currentLine=5570568;
 //BA.debugLineNum = 5570568;BA.debugLine="sb.Append(\"'\" & bu_zeit.Text &\"');\")";
_sb.Append("'"+mostCurrent._bu_zeit.getText()+"');");
RDebugUtils.currentLine=5570571;
 //BA.debugLineNum = 5570571;BA.debugLine="Main.sql1.ExecNonQuery(sb.ToString)";
mostCurrent._main._sql1.ExecNonQuery(_sb.ToString());
RDebugUtils.currentLine=5570573;
 //BA.debugLineNum = 5570573;BA.debugLine="send_ZE";
_send_ze();
RDebugUtils.currentLine=5570578;
 //BA.debugLineNum = 5570578;BA.debugLine="End Sub";
return "";
}
public static void  _send_ze() throws Exception{
ResumableSub_send_ZE rsub = new ResumableSub_send_ZE(null);
rsub.resume(processBA, null);
}
public static class ResumableSub_send_ZE extends BA.ResumableSub {
public ResumableSub_send_ZE(b4a.example.ze_basis parent) {
this.parent = parent;
}
b4a.example.ze_basis parent;
String _url = "";
String _user = "";
String _pw = "";
String _app_id = "";
String _zeit = "";
String _stat = "";
b4a.example.soap_up _soapparser = null;
anywheresoftware.b4a.samples.httputils2.httpjob _set_ze_pooljob = null;

@Override
public void resume(BA ba, Object[] result) throws Exception{
RDebugUtils.currentModule="ze_basis";
Debug.delegate(mostCurrent.activityBA, "send_ze", null);
if (true) return;
    while (true) {
try {

        switch (state) {
            case -1:
return;

case 0:
//C
this.state = 1;
RDebugUtils.currentLine=7667713;
 //BA.debugLineNum = 7667713;BA.debugLine="Dim url As String";
_url = "";
RDebugUtils.currentLine=7667714;
 //BA.debugLineNum = 7667714;BA.debugLine="url=Main.liveservice";
_url = parent.mostCurrent._main._liveservice;
RDebugUtils.currentLine=7667717;
 //BA.debugLineNum = 7667717;BA.debugLine="Try";
if (true) break;

case 1:
//try
this.state = 6;
this.catchState = 5;
this.state = 3;
if (true) break;

case 3:
//C
this.state = 6;
this.catchState = 5;
RDebugUtils.currentLine=7667718;
 //BA.debugLineNum = 7667718;BA.debugLine="Dim user As String = Main.sql1.ExecQuerySingleRe";
_user = parent.mostCurrent._main._sql1.ExecQuerySingleResult("Select username from tbl_usr");
RDebugUtils.currentLine=7667719;
 //BA.debugLineNum = 7667719;BA.debugLine="Dim pw As String = Main.sql1.ExecQuerySingleResu";
_pw = parent.mostCurrent._main._sql1.ExecQuerySingleResult("Select pw from tbl_usr");
RDebugUtils.currentLine=7667720;
 //BA.debugLineNum = 7667720;BA.debugLine="Dim App_id As String = Main.sql1.ExecQuerySingle";
_app_id = parent.mostCurrent._main._sql1.ExecQuerySingleResult("Select max(ID) from tbl_ze");
RDebugUtils.currentLine=7667721;
 //BA.debugLineNum = 7667721;BA.debugLine="Dim zeit As String = Main.sql1.ExecQuerySingleRe";
_zeit = parent.mostCurrent._main._sql1.ExecQuerySingleResult("Select Zeit from tbl_ze where ID =(select max(ID) from tbl_ze)");
RDebugUtils.currentLine=7667722;
 //BA.debugLineNum = 7667722;BA.debugLine="Dim stat As String = Main.sql1.ExecQuerySingleRe";
_stat = parent.mostCurrent._main._sql1.ExecQuerySingleResult("Select Status from tbl_ze where ID =(select max(ID) from tbl_ze)");
 if (true) break;

case 5:
//C
this.state = 6;
this.catchState = 0;
RDebugUtils.currentLine=7667724;
 //BA.debugLineNum = 7667724;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 if (true) break;
if (true) break;

case 6:
//C
this.state = 7;
this.catchState = 0;
;
RDebugUtils.currentLine=7667727;
 //BA.debugLineNum = 7667727;BA.debugLine="Dim SoapParser As soap_up";
_soapparser = new b4a.example.soap_up();
RDebugUtils.currentLine=7667728;
 //BA.debugLineNum = 7667728;BA.debugLine="SoapParser.initialize(url,\"set_ze_pool\")";
_soapparser._initialize(null,processBA,_url,"set_ze_pool");
RDebugUtils.currentLine=7667729;
 //BA.debugLineNum = 7667729;BA.debugLine="SoapParser.AddField(\"usr\",SoapParser.TYPE_STRING,";
_soapparser._addfield(null,"usr",_soapparser._type_string,_user);
RDebugUtils.currentLine=7667730;
 //BA.debugLineNum = 7667730;BA.debugLine="SoapParser.AddField(\"pwd\",SoapParser.TYPE_STRING,";
_soapparser._addfield(null,"pwd",_soapparser._type_string,_pw);
RDebugUtils.currentLine=7667731;
 //BA.debugLineNum = 7667731;BA.debugLine="SoapParser.AddField(\"App_ID\",SoapParser.TYPE_STRI";
_soapparser._addfield(null,"App_ID",_soapparser._type_string,_app_id);
RDebugUtils.currentLine=7667732;
 //BA.debugLineNum = 7667732;BA.debugLine="SoapParser.AddField(\"Zeit\",SoapParser.TYPE_STRING";
_soapparser._addfield(null,"Zeit",_soapparser._type_string,_zeit);
RDebugUtils.currentLine=7667733;
 //BA.debugLineNum = 7667733;BA.debugLine="SoapParser.AddField(\"Status\",SoapParser.TYPE_STRI";
_soapparser._addfield(null,"Status",_soapparser._type_string,_stat);
RDebugUtils.currentLine=7667734;
 //BA.debugLineNum = 7667734;BA.debugLine="SoapParser.footer(\"set_ze_pool\")";
_soapparser._footer(null,"set_ze_pool");
RDebugUtils.currentLine=7667736;
 //BA.debugLineNum = 7667736;BA.debugLine="Dim set_ze_poolJob As HttpJob";
_set_ze_pooljob = new anywheresoftware.b4a.samples.httputils2.httpjob();
RDebugUtils.currentLine=7667737;
 //BA.debugLineNum = 7667737;BA.debugLine="set_ze_poolJob.Initialize(\"\",Me)";
_set_ze_pooljob._initialize(processBA,"",ze_basis.getObject());
RDebugUtils.currentLine=7667738;
 //BA.debugLineNum = 7667738;BA.debugLine="set_ze_poolJob.PostString(url, SoapParser.xml_sen";
_set_ze_pooljob._poststring(_url,_soapparser._xml_sendstriing);
RDebugUtils.currentLine=7667739;
 //BA.debugLineNum = 7667739;BA.debugLine="set_ze_poolJob.GetRequest.SetHeader(\"SoapAction\",";
_set_ze_pooljob._getrequest().SetHeader("SoapAction","http://tempuri.org/set_ze_pool");
RDebugUtils.currentLine=7667740;
 //BA.debugLineNum = 7667740;BA.debugLine="Log(SoapParser.xml_sendstriing)";
anywheresoftware.b4a.keywords.Common.Log(_soapparser._xml_sendstriing);
RDebugUtils.currentLine=7667741;
 //BA.debugLineNum = 7667741;BA.debugLine="set_ze_poolJob.GetRequest.SetContentType(\"text/xm";
_set_ze_pooljob._getrequest().SetContentType("text/xml; charset=utf-8");
RDebugUtils.currentLine=7667742;
 //BA.debugLineNum = 7667742;BA.debugLine="set_ze_poolJob.GetRequest.Timeout=2000";
_set_ze_pooljob._getrequest().setTimeout((int) (2000));
RDebugUtils.currentLine=7667743;
 //BA.debugLineNum = 7667743;BA.debugLine="ProgressDialogShow(\"Daten werden hochgeladen..\")";
anywheresoftware.b4a.keywords.Common.ProgressDialogShow(mostCurrent.activityBA,BA.ObjectToCharSequence("Daten werden hochgeladen.."));
RDebugUtils.currentLine=7667745;
 //BA.debugLineNum = 7667745;BA.debugLine="Wait For (set_ze_poolJob) JobDone(set_ze_poolJob";
anywheresoftware.b4a.keywords.Common.WaitFor("jobdone", processBA, this, (Object)(_set_ze_pooljob));
this.state = 13;
return;
case 13:
//C
this.state = 7;
_set_ze_pooljob = (anywheresoftware.b4a.samples.httputils2.httpjob) result[0];
;
RDebugUtils.currentLine=7667747;
 //BA.debugLineNum = 7667747;BA.debugLine="If set_ze_poolJob.Success Then";
if (true) break;

case 7:
//if
this.state = 12;
if (_set_ze_pooljob._success) { 
this.state = 9;
}else {
this.state = 11;
}if (true) break;

case 9:
//C
this.state = 12;
RDebugUtils.currentLine=7667748;
 //BA.debugLineNum = 7667748;BA.debugLine="SaxParser.Initialize";
parent._saxparser.Initialize(processBA);
RDebugUtils.currentLine=7667749;
 //BA.debugLineNum = 7667749;BA.debugLine="SaxParser.Parse(set_ze_poolJob.GetInputStream,\"P";
parent._saxparser.Parse((java.io.InputStream)(_set_ze_pooljob._getinputstream().getObject()),"P_set_ze_pool");
RDebugUtils.currentLine=7667750;
 //BA.debugLineNum = 7667750;BA.debugLine="ProgressDialogHide";
anywheresoftware.b4a.keywords.Common.ProgressDialogHide();
 if (true) break;

case 11:
//C
this.state = 12;
RDebugUtils.currentLine=7667752;
 //BA.debugLineNum = 7667752;BA.debugLine="ProgressDialogHide";
anywheresoftware.b4a.keywords.Common.ProgressDialogHide();
RDebugUtils.currentLine=7667753;
 //BA.debugLineNum = 7667753;BA.debugLine="ToastMessageShow(\"Keine Verbindung zum Server mö";
anywheresoftware.b4a.keywords.Common.ToastMessageShow(BA.ObjectToCharSequence("Keine Verbindung zum Server möglich"),anywheresoftware.b4a.keywords.Common.False);
 if (true) break;

case 12:
//C
this.state = -1;
;
RDebugUtils.currentLine=7667755;
 //BA.debugLineNum = 7667755;BA.debugLine="set_ze_poolJob.Release";
_set_ze_pooljob._release();
RDebugUtils.currentLine=7667756;
 //BA.debugLineNum = 7667756;BA.debugLine="End Sub";
if (true) break;
}} 
       catch (Exception e0) {
			
if (catchState == 0)
    throw e0;
else {
    state = catchState;
processBA.setLastException(e0);}
            }
        }
    }
}
public static String  _p_set_ze_pool_endelement(String _uri,String _name,anywheresoftware.b4a.keywords.StringBuilderWrapper _text) throws Exception{
RDebugUtils.currentModule="ze_basis";
if (Debug.shouldDelegate(mostCurrent.activityBA, "p_set_ze_pool_endelement"))
	return (String) Debug.delegate(mostCurrent.activityBA, "p_set_ze_pool_endelement", new Object[] {_uri,_name,_text});
String _node = "";
RDebugUtils.currentLine=8716288;
 //BA.debugLineNum = 8716288;BA.debugLine="Sub P_set_ze_pool_EndElement (Uri As String, Name";
RDebugUtils.currentLine=8716290;
 //BA.debugLineNum = 8716290;BA.debugLine="Dim node As String= \"set_ze_pool\" & \"Response\"";
_node = "set_ze_pool"+"Response";
RDebugUtils.currentLine=8716291;
 //BA.debugLineNum = 8716291;BA.debugLine="If SaxParser.Parents.IndexOf(node) > -1 Then";
if (_saxparser.Parents.IndexOf((Object)(_node))>-1) { 
RDebugUtils.currentLine=8716292;
 //BA.debugLineNum = 8716292;BA.debugLine="node = \"set_ze_pool\" & \"Result\"";
_node = "set_ze_pool"+"Result";
RDebugUtils.currentLine=8716293;
 //BA.debugLineNum = 8716293;BA.debugLine="Select Case Name";
switch (BA.switchObjectToInt(_name,_node)) {
case 0: {
RDebugUtils.currentLine=8716295;
 //BA.debugLineNum = 8716295;BA.debugLine="Try";
try {RDebugUtils.currentLine=8716296;
 //BA.debugLineNum = 8716296;BA.debugLine="Main.sql1.ExecNonQuery(\"Update tbl_ze set arc";
mostCurrent._main._sql1.ExecNonQuery("Update tbl_ze set archiv=1 where id ='"+_name+"'");
 } 
       catch (Exception e9) {
			processBA.setLastException(e9);RDebugUtils.currentLine=8716300;
 //BA.debugLineNum = 8716300;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 };
 break; }
}
;
 };
RDebugUtils.currentLine=8716310;
 //BA.debugLineNum = 8716310;BA.debugLine="End Sub";
return "";
}
}