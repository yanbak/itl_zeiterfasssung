package b4a.example;


import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.B4AClass;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.debug.*;

public class soap_up extends B4AClass.ImplB4AClass implements BA.SubDelegator{
    private static java.util.HashMap<String, java.lang.reflect.Method> htSubs;
    private void innerInitialize(BA _ba) throws Exception {
        if (ba == null) {
            ba = new anywheresoftware.b4a.ShellBA(_ba, this, htSubs, "b4a.example.soap_up");
            if (htSubs == null) {
                ba.loadHtSubs(this.getClass());
                htSubs = ba.htSubs;
            }
            
        }
        if (BA.isShellModeRuntimeCheck(ba)) 
			   this.getClass().getMethod("_class_globals", b4a.example.soap_up.class).invoke(this, new Object[] {null});
        else
            ba.raiseEvent2(null, true, "class_globals", false);
    }

 
    public void  innerInitializeHelper(anywheresoftware.b4a.BA _ba) throws Exception{
        innerInitialize(_ba);
    }
    public Object callSub(String sub, Object sender, Object[] args) throws Exception {
        return BA.SubDelegator.SubNotFound;
    }
public anywheresoftware.b4a.keywords.Common __c = null;
public String _type_string = "";
public String _type_integer = "";
public String _type_float = "";
public String _type_date = "";
public String _type_double = "";
public String _type_binary = "";
public String _xml_sendstriing = "";
public anywheresoftware.b4a.samples.httputils2.httputils2service _httputils2service = null;
public b4a.example.main _main = null;
public b4a.example.starter _starter = null;
public b4a.example.scale _scale = null;
public b4a.example.setup _setup = null;
public b4a.example.ze_basis _ze_basis = null;
public String  _initialize(b4a.example.soap_up __ref,anywheresoftware.b4a.BA _ba,String _url,String _methodname) throws Exception{
__ref = this;
innerInitialize(_ba);
RDebugUtils.currentModule="soap_up";
if (Debug.shouldDelegate(ba, "initialize"))
	return (String) Debug.delegate(ba, "initialize", new Object[] {_ba,_url,_methodname});
RDebugUtils.currentLine=7798784;
 //BA.debugLineNum = 7798784;BA.debugLine="Public Sub initialize(Url As String,MethodName As";
RDebugUtils.currentLine=7798785;
 //BA.debugLineNum = 7798785;BA.debugLine="xml_sendstriing =xml_sendstriing & \"<?xml version";
__ref._xml_sendstriing = __ref._xml_sendstriing+"<?xml version="+BA.ObjectToString(__c.Chr((int) (34)))+"1.0"+BA.ObjectToString(__c.Chr((int) (34)))+" encoding="+BA.ObjectToString(__c.Chr((int) (34)))+"utf-8"+BA.ObjectToString(__c.Chr((int) (34)))+"?>";
RDebugUtils.currentLine=7798786;
 //BA.debugLineNum = 7798786;BA.debugLine="xml_sendstriing= xml_sendstriing & \"<soap:Envelop";
__ref._xml_sendstriing = __ref._xml_sendstriing+"<soap:Envelope xmlns:xsi="+BA.ObjectToString(__c.Chr((int) (34)))+"http://www.w3.org/2001/XMLSchema-instance"+BA.ObjectToString(__c.Chr((int) (34)))+" xmlns:xsd="+BA.ObjectToString(__c.Chr((int) (34)))+"http://www.w3.org/2001/XMLSchema"+BA.ObjectToString(__c.Chr((int) (34)))+" xmlns:soap="+BA.ObjectToString(__c.Chr((int) (34)))+"http://schemas.xmlsoap.org/soap/envelope/"+BA.ObjectToString(__c.Chr((int) (34)))+">";
RDebugUtils.currentLine=7798787;
 //BA.debugLineNum = 7798787;BA.debugLine="xml_sendstriing= xml_sendstriing & \"<soap:Body>\"";
__ref._xml_sendstriing = __ref._xml_sendstriing+"<soap:Body>";
RDebugUtils.currentLine=7798788;
 //BA.debugLineNum = 7798788;BA.debugLine="xml_sendstriing= xml_sendstriing & \"<\" & MethodNa";
__ref._xml_sendstriing = __ref._xml_sendstriing+"<"+_methodname+" xmlns="+BA.ObjectToString(__c.Chr((int) (34)))+"http://tempuri.org/"+BA.ObjectToString(__c.Chr((int) (34)))+">";
RDebugUtils.currentLine=7798789;
 //BA.debugLineNum = 7798789;BA.debugLine="End Sub";
return "";
}
public String  _addfield(b4a.example.soap_up __ref,String _fieldname,String _fieldtype,String _fieldvalue) throws Exception{
__ref = this;
RDebugUtils.currentModule="soap_up";
if (Debug.shouldDelegate(ba, "addfield"))
	return (String) Debug.delegate(ba, "addfield", new Object[] {_fieldname,_fieldtype,_fieldvalue});
RDebugUtils.currentLine=7864320;
 //BA.debugLineNum = 7864320;BA.debugLine="Public Sub AddField(FieldName As String,FieldType";
RDebugUtils.currentLine=7864321;
 //BA.debugLineNum = 7864321;BA.debugLine="xml_sendstriing=xml_sendstriing &CRLF & $\"<${Fiel";
__ref._xml_sendstriing = __ref._xml_sendstriing+__c.CRLF+("<"+__c.SmartStringFormatter("",(Object)(_fieldname))+" >"+__c.SmartStringFormatter("",(Object)(_fieldvalue))+"</"+__c.SmartStringFormatter("",(Object)(_fieldname))+">");
RDebugUtils.currentLine=7864322;
 //BA.debugLineNum = 7864322;BA.debugLine="End Sub";
return "";
}
public String  _footer(b4a.example.soap_up __ref,String _methodname) throws Exception{
__ref = this;
RDebugUtils.currentModule="soap_up";
if (Debug.shouldDelegate(ba, "footer"))
	return (String) Debug.delegate(ba, "footer", new Object[] {_methodname});
RDebugUtils.currentLine=7929856;
 //BA.debugLineNum = 7929856;BA.debugLine="Sub footer( MethodName As String)";
RDebugUtils.currentLine=7929857;
 //BA.debugLineNum = 7929857;BA.debugLine="xml_sendstriing= xml_sendstriing & \"</\" & MethodN";
__ref._xml_sendstriing = __ref._xml_sendstriing+"</"+_methodname+">";
RDebugUtils.currentLine=7929858;
 //BA.debugLineNum = 7929858;BA.debugLine="xml_sendstriing= xml_sendstriing & \"</soap:Body>\"";
__ref._xml_sendstriing = __ref._xml_sendstriing+"</soap:Body>";
RDebugUtils.currentLine=7929859;
 //BA.debugLineNum = 7929859;BA.debugLine="xml_sendstriing= xml_sendstriing & \"</soap:Envelo";
__ref._xml_sendstriing = __ref._xml_sendstriing+"</soap:Envelope>";
RDebugUtils.currentLine=7929860;
 //BA.debugLineNum = 7929860;BA.debugLine="End Sub";
return "";
}
public String  _class_globals(b4a.example.soap_up __ref) throws Exception{
__ref = this;
RDebugUtils.currentModule="soap_up";
RDebugUtils.currentLine=7733248;
 //BA.debugLineNum = 7733248;BA.debugLine="Sub Class_Globals";
RDebugUtils.currentLine=7733249;
 //BA.debugLineNum = 7733249;BA.debugLine="Public TYPE_STRING   As String = \"xsd:string\"";
_type_string = "xsd:string";
RDebugUtils.currentLine=7733250;
 //BA.debugLineNum = 7733250;BA.debugLine="Public TYPE_INTEGER  As String = \"xsd:int\"";
_type_integer = "xsd:int";
RDebugUtils.currentLine=7733251;
 //BA.debugLineNum = 7733251;BA.debugLine="Public TYPE_FLOAT    As String = \"xsd:float\"";
_type_float = "xsd:float";
RDebugUtils.currentLine=7733252;
 //BA.debugLineNum = 7733252;BA.debugLine="Public TYPE_DATE     As String = \"xsd:date\"";
_type_date = "xsd:date";
RDebugUtils.currentLine=7733253;
 //BA.debugLineNum = 7733253;BA.debugLine="Public TYPE_DOUBLE   As String = \"xsd:double\"";
_type_double = "xsd:double";
RDebugUtils.currentLine=7733254;
 //BA.debugLineNum = 7733254;BA.debugLine="Public TYPE_BINARY   As String = \"xsd:base64Binar";
_type_binary = "xsd:base64Binary";
RDebugUtils.currentLine=7733255;
 //BA.debugLineNum = 7733255;BA.debugLine="Public xml_sendstriing As String";
_xml_sendstriing = "";
RDebugUtils.currentLine=7733257;
 //BA.debugLineNum = 7733257;BA.debugLine="End Sub";
return "";
}
}