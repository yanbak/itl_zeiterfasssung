package b4a.example;


import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.B4AClass;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.debug.*;

public class update_sender extends B4AClass.ImplB4AClass implements BA.SubDelegator{
    private static java.util.HashMap<String, java.lang.reflect.Method> htSubs;
    private void innerInitialize(BA _ba) throws Exception {
        if (ba == null) {
            ba = new anywheresoftware.b4a.ShellBA(_ba, this, htSubs, "b4a.example.update_sender");
            if (htSubs == null) {
                ba.loadHtSubs(this.getClass());
                htSubs = ba.htSubs;
            }
            
        }
        if (BA.isShellModeRuntimeCheck(ba)) 
			   this.getClass().getMethod("_class_globals", b4a.example.update_sender.class).invoke(this, new Object[] {null});
        else
            ba.raiseEvent2(null, true, "class_globals", false);
    }

 
    public void  innerInitializeHelper(anywheresoftware.b4a.BA _ba) throws Exception{
        innerInitialize(_ba);
    }
    public Object callSub(String sub, Object sender, Object[] args) throws Exception {
        return BA.SubDelegator.SubNotFound;
    }
public anywheresoftware.b4a.keywords.Common __c = null;
public b4a.example.sp_single _s1 = null;
public String _usr = "";
public String _pwd = "";
public anywheresoftware.b4a.samples.httputils2.httputils2service _httputils2service = null;
public b4a.example.main _main = null;
public b4a.example.starter _starter = null;
public b4a.example.scale _scale = null;
public b4a.example.setup _setup = null;
public b4a.example.ze_basis _ze_basis = null;
public String  _initialize(b4a.example.update_sender __ref,anywheresoftware.b4a.BA _ba) throws Exception{
__ref = this;
innerInitialize(_ba);
RDebugUtils.currentModule="update_sender";
if (Debug.shouldDelegate(ba, "initialize"))
	return (String) Debug.delegate(ba, "initialize", new Object[] {_ba});
anywheresoftware.b4a.sql.SQL.CursorWrapper _c_user = null;
RDebugUtils.currentLine=5701632;
 //BA.debugLineNum = 5701632;BA.debugLine="Public Sub Initialize";
RDebugUtils.currentLine=5701633;
 //BA.debugLineNum = 5701633;BA.debugLine="Try";
try {RDebugUtils.currentLine=5701636;
 //BA.debugLineNum = 5701636;BA.debugLine="Dim c_user As Cursor";
_c_user = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
RDebugUtils.currentLine=5701637;
 //BA.debugLineNum = 5701637;BA.debugLine="c_user=Main.sql1.ExecQuery(\"select * from tbl_usr\"";
_c_user.setObject((android.database.Cursor)(_main._sql1.ExecQuery("select * from tbl_usr")));
RDebugUtils.currentLine=5701638;
 //BA.debugLineNum = 5701638;BA.debugLine="c_user.Position=0";
_c_user.setPosition((int) (0));
RDebugUtils.currentLine=5701639;
 //BA.debugLineNum = 5701639;BA.debugLine="usr=c_user.GetString(\"username\")";
__ref._usr = _c_user.GetString("username");
RDebugUtils.currentLine=5701640;
 //BA.debugLineNum = 5701640;BA.debugLine="pwd=c_user.GetString(\"pw\")";
__ref._pwd = _c_user.GetString("pw");
 } 
       catch (Exception e8) {
			ba.setLastException(e8);RDebugUtils.currentLine=5701642;
 //BA.debugLineNum = 5701642;BA.debugLine="Log(\"ungültige anmeldung\")";
__c.Log("ungültige anmeldung");
 };
RDebugUtils.currentLine=5701644;
 //BA.debugLineNum = 5701644;BA.debugLine="End Sub";
return "";
}
public String  _send_quit(b4a.example.update_sender __ref) throws Exception{
__ref = this;
RDebugUtils.currentModule="update_sender";
if (Debug.shouldDelegate(ba, "send_quit"))
	return (String) Debug.delegate(ba, "send_quit", null);
anywheresoftware.b4a.sql.SQL.CursorWrapper _c = null;
int _i = 0;
RDebugUtils.currentLine=5832704;
 //BA.debugLineNum = 5832704;BA.debugLine="public Sub send_quit( )";
RDebugUtils.currentLine=5832708;
 //BA.debugLineNum = 5832708;BA.debugLine="Dim c As Cursor";
_c = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
RDebugUtils.currentLine=5832710;
 //BA.debugLineNum = 5832710;BA.debugLine="c= Main.sql1.ExecQuery(\"Select * from tbl_quittun";
_c.setObject((android.database.Cursor)(_main._sql1.ExecQuery("Select * from tbl_quittung where sync is null")));
RDebugUtils.currentLine=5832712;
 //BA.debugLineNum = 5832712;BA.debugLine="For i = 0 To c.RowCount -1";
{
final int step3 = 1;
final int limit3 = (int) (_c.getRowCount()-1);
_i = (int) (0) ;
for (;(step3 > 0 && _i <= limit3) || (step3 < 0 && _i >= limit3) ;_i = ((int)(0 + _i + step3))  ) {
RDebugUtils.currentLine=5832715;
 //BA.debugLineNum = 5832715;BA.debugLine="c.Position=i";
_c.setPosition(_i);
RDebugUtils.currentLine=5832716;
 //BA.debugLineNum = 5832716;BA.debugLine="s1.Initialize(Main.use_service &\"?op=up_quit\",";
__ref._s1._initialize(null,ba,_main._use_service+"?op=up_quit","up_quit",this,"Result_quittung");
RDebugUtils.currentLine=5832717;
 //BA.debugLineNum = 5832717;BA.debugLine="s1.AddField(\"usr\",s1.TYPE_STRING,usr)";
__ref._s1._addfield(null,"usr",__ref._s1._type_string,__ref._usr);
RDebugUtils.currentLine=5832718;
 //BA.debugLineNum = 5832718;BA.debugLine="s1.AddField(\"pwd\",s1.TYPE_STRING,pwd)";
__ref._s1._addfield(null,"pwd",__ref._s1._type_string,__ref._pwd);
RDebugUtils.currentLine=5832719;
 //BA.debugLineNum = 5832719;BA.debugLine="s1.AddField(\"id\",s1.TYPE_INTEGER,c.GetInt(\"id\"))";
__ref._s1._addfield(null,"id",__ref._s1._type_integer,BA.NumberToString(_c.GetInt("id")));
RDebugUtils.currentLine=5832720;
 //BA.debugLineNum = 5832720;BA.debugLine="s1.AddField(\"Auftrag\",s1.TYPE_String,c.GetString";
__ref._s1._addfield(null,"Auftrag",__ref._s1._type_string,_c.GetString("Auftrag"));
RDebugUtils.currentLine=5832721;
 //BA.debugLineNum = 5832721;BA.debugLine="s1.AddField(\"receiver\",s1.TYPE_String,c.GetStri";
__ref._s1._addfield(null,"receiver",__ref._s1._type_string,_c.GetString("receiver"));
RDebugUtils.currentLine=5832724;
 //BA.debugLineNum = 5832724;BA.debugLine="If i = c.RowCount-1 Then";
if (_i==_c.getRowCount()-1) { 
RDebugUtils.currentLine=5832725;
 //BA.debugLineNum = 5832725;BA.debugLine="s1.SendRequest(\"up_quit\",\"send_quittung\",True)";
__ref._s1._sendrequest(null,"up_quit","send_quittung",__c.True);
 }else {
RDebugUtils.currentLine=5832727;
 //BA.debugLineNum = 5832727;BA.debugLine="s1.SendRequest(\"up_quit\",\"send_quittung\",False)";
__ref._s1._sendrequest(null,"up_quit","send_quittung",__c.False);
 };
 }
};
RDebugUtils.currentLine=5832734;
 //BA.debugLineNum = 5832734;BA.debugLine="End Sub";
return "";
}
public String  _class_globals(b4a.example.update_sender __ref) throws Exception{
__ref = this;
RDebugUtils.currentModule="update_sender";
RDebugUtils.currentLine=5636096;
 //BA.debugLineNum = 5636096;BA.debugLine="Sub Class_Globals";
RDebugUtils.currentLine=5636097;
 //BA.debugLineNum = 5636097;BA.debugLine="Dim s1 As sp_single";
_s1 = new b4a.example.sp_single();
RDebugUtils.currentLine=5636098;
 //BA.debugLineNum = 5636098;BA.debugLine="Public usr As String";
_usr = "";
RDebugUtils.currentLine=5636099;
 //BA.debugLineNum = 5636099;BA.debugLine="Public pwd As String";
_pwd = "";
RDebugUtils.currentLine=5636100;
 //BA.debugLineNum = 5636100;BA.debugLine="End Sub";
return "";
}
public String  _result_ankunft(b4a.example.update_sender __ref,Object _res) throws Exception{
__ref = this;
RDebugUtils.currentModule="update_sender";
if (Debug.shouldDelegate(ba, "result_ankunft"))
	return (String) Debug.delegate(ba, "result_ankunft", new Object[] {_res});
RDebugUtils.currentLine=6160384;
 //BA.debugLineNum = 6160384;BA.debugLine="Sub Result_ankunft(res As Object)";
RDebugUtils.currentLine=6160385;
 //BA.debugLineNum = 6160385;BA.debugLine="Log(res)";
__c.Log(BA.ObjectToString(_res));
RDebugUtils.currentLine=6160386;
 //BA.debugLineNum = 6160386;BA.debugLine="End Sub";
return "";
}
public String  _result_eingabe(b4a.example.update_sender __ref,Object _res) throws Exception{
__ref = this;
RDebugUtils.currentModule="update_sender";
if (Debug.shouldDelegate(ba, "result_eingabe"))
	return (String) Debug.delegate(ba, "result_eingabe", new Object[] {_res});
RDebugUtils.currentLine=6225920;
 //BA.debugLineNum = 6225920;BA.debugLine="Sub Result_eingabe(res As Object)";
RDebugUtils.currentLine=6225921;
 //BA.debugLineNum = 6225921;BA.debugLine="Log(res)";
__c.Log(BA.ObjectToString(_res));
RDebugUtils.currentLine=6225922;
 //BA.debugLineNum = 6225922;BA.debugLine="End Sub";
return "";
}
public String  _result_info(b4a.example.update_sender __ref,Object _res) throws Exception{
__ref = this;
RDebugUtils.currentModule="update_sender";
if (Debug.shouldDelegate(ba, "result_info"))
	return (String) Debug.delegate(ba, "result_info", new Object[] {_res});
RDebugUtils.currentLine=6094848;
 //BA.debugLineNum = 6094848;BA.debugLine="Sub Result_info(res As Object)";
RDebugUtils.currentLine=6094849;
 //BA.debugLineNum = 6094849;BA.debugLine="Log(res)";
__c.Log(BA.ObjectToString(_res));
RDebugUtils.currentLine=6094850;
 //BA.debugLineNum = 6094850;BA.debugLine="End Sub";
return "";
}
public String  _send_ankunft(b4a.example.update_sender __ref) throws Exception{
__ref = this;
RDebugUtils.currentModule="update_sender";
if (Debug.shouldDelegate(ba, "send_ankunft"))
	return (String) Debug.delegate(ba, "send_ankunft", null);
anywheresoftware.b4a.sql.SQL.CursorWrapper _c = null;
int _i = 0;
RDebugUtils.currentLine=5767168;
 //BA.debugLineNum = 5767168;BA.debugLine="public Sub send_ankunft( )";
RDebugUtils.currentLine=5767172;
 //BA.debugLineNum = 5767172;BA.debugLine="Dim c As Cursor";
_c = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
RDebugUtils.currentLine=5767174;
 //BA.debugLineNum = 5767174;BA.debugLine="c= Main.sql1.ExecQuery(\"Select * from tbl_ankunft";
_c.setObject((android.database.Cursor)(_main._sql1.ExecQuery("Select * from tbl_ankunft where sync is null")));
RDebugUtils.currentLine=5767176;
 //BA.debugLineNum = 5767176;BA.debugLine="For i = 0 To c.RowCount -1";
{
final int step3 = 1;
final int limit3 = (int) (_c.getRowCount()-1);
_i = (int) (0) ;
for (;(step3 > 0 && _i <= limit3) || (step3 < 0 && _i >= limit3) ;_i = ((int)(0 + _i + step3))  ) {
RDebugUtils.currentLine=5767179;
 //BA.debugLineNum = 5767179;BA.debugLine="c.Position=i";
_c.setPosition(_i);
RDebugUtils.currentLine=5767180;
 //BA.debugLineNum = 5767180;BA.debugLine="s1.Initialize(Main.use_service &\"?op=up_ankunf";
__ref._s1._initialize(null,ba,_main._use_service+"?op=up_ankunft","up_ankunft",this,"Result_ankunft");
RDebugUtils.currentLine=5767181;
 //BA.debugLineNum = 5767181;BA.debugLine="s1.AddField(\"usr\",s1.TYPE_STRING,usr)";
__ref._s1._addfield(null,"usr",__ref._s1._type_string,__ref._usr);
RDebugUtils.currentLine=5767182;
 //BA.debugLineNum = 5767182;BA.debugLine="s1.AddField(\"pwd\",s1.TYPE_STRING,pwd)";
__ref._s1._addfield(null,"pwd",__ref._s1._type_string,__ref._pwd);
RDebugUtils.currentLine=5767183;
 //BA.debugLineNum = 5767183;BA.debugLine="s1.AddField(\"id\",s1.TYPE_INTEGER,c.GetInt(\"id\"))";
__ref._s1._addfield(null,"id",__ref._s1._type_integer,BA.NumberToString(_c.GetInt("id")));
RDebugUtils.currentLine=5767184;
 //BA.debugLineNum = 5767184;BA.debugLine="s1.AddField(\"auftrag\",s1.TYPE_String,c.GetString";
__ref._s1._addfield(null,"auftrag",__ref._s1._type_string,_c.GetString("auftrag"));
RDebugUtils.currentLine=5767185;
 //BA.debugLineNum = 5767185;BA.debugLine="s1.AddField(\"ankunft\",s1.TYPE_String,c.GetStrin";
__ref._s1._addfield(null,"ankunft",__ref._s1._type_string,_c.GetString("ankunft"));
RDebugUtils.currentLine=5767186;
 //BA.debugLineNum = 5767186;BA.debugLine="Log (c.GetString(\"ankunft\"))";
__c.Log(_c.GetString("ankunft"));
RDebugUtils.currentLine=5767187;
 //BA.debugLineNum = 5767187;BA.debugLine="s1.AddField(\"gps\",s1.TYPE_String,c.GetString(\"g";
__ref._s1._addfield(null,"gps",__ref._s1._type_string,_c.GetString("gps"));
RDebugUtils.currentLine=5767188;
 //BA.debugLineNum = 5767188;BA.debugLine="If i = c.RowCount-1 Then";
if (_i==_c.getRowCount()-1) { 
RDebugUtils.currentLine=5767189;
 //BA.debugLineNum = 5767189;BA.debugLine="s1.SendRequest(\"up_ankunft\",\"send_ankunft\",Tru";
__ref._s1._sendrequest(null,"up_ankunft","send_ankunft",__c.True);
 }else {
RDebugUtils.currentLine=5767191;
 //BA.debugLineNum = 5767191;BA.debugLine="s1.SendRequest(\"up_ankunft\",\"send_ankunft\",Fals";
__ref._s1._sendrequest(null,"up_ankunft","send_ankunft",__c.False);
 };
 }
};
RDebugUtils.currentLine=5767201;
 //BA.debugLineNum = 5767201;BA.debugLine="End Sub";
return "";
}
public String  _send_eingabe(b4a.example.update_sender __ref) throws Exception{
__ref = this;
RDebugUtils.currentModule="update_sender";
if (Debug.shouldDelegate(ba, "send_eingabe"))
	return (String) Debug.delegate(ba, "send_eingabe", null);
anywheresoftware.b4a.sql.SQL.CursorWrapper _c = null;
int _i = 0;
RDebugUtils.currentLine=5898240;
 //BA.debugLineNum = 5898240;BA.debugLine="public Sub send_eingabe( )";
RDebugUtils.currentLine=5898244;
 //BA.debugLineNum = 5898244;BA.debugLine="Dim c As Cursor";
_c = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
RDebugUtils.currentLine=5898246;
 //BA.debugLineNum = 5898246;BA.debugLine="c= Main.sql1.ExecQuery(\"Select * from tbl_nachtra";
_c.setObject((android.database.Cursor)(_main._sql1.ExecQuery("Select * from tbl_nachtrag_log_app where sync is null")));
RDebugUtils.currentLine=5898248;
 //BA.debugLineNum = 5898248;BA.debugLine="For i = 0 To c.RowCount -1";
{
final int step3 = 1;
final int limit3 = (int) (_c.getRowCount()-1);
_i = (int) (0) ;
for (;(step3 > 0 && _i <= limit3) || (step3 < 0 && _i >= limit3) ;_i = ((int)(0 + _i + step3))  ) {
RDebugUtils.currentLine=5898249;
 //BA.debugLineNum = 5898249;BA.debugLine="c.Position=i";
_c.setPosition(_i);
RDebugUtils.currentLine=5898250;
 //BA.debugLineNum = 5898250;BA.debugLine="s1.Initialize(Main.use_service &\"?op=up_eingab";
__ref._s1._initialize(null,ba,_main._use_service+"?op=up_eingabe","up_eingabe",this,"Result_eingabe");
RDebugUtils.currentLine=5898251;
 //BA.debugLineNum = 5898251;BA.debugLine="s1.AddField(\"usr\",s1.TYPE_STRING,usr)";
__ref._s1._addfield(null,"usr",__ref._s1._type_string,__ref._usr);
RDebugUtils.currentLine=5898252;
 //BA.debugLineNum = 5898252;BA.debugLine="s1.AddField(\"pwd\",s1.TYPE_STRING,pwd)";
__ref._s1._addfield(null,"pwd",__ref._s1._type_string,__ref._pwd);
RDebugUtils.currentLine=5898253;
 //BA.debugLineNum = 5898253;BA.debugLine="s1.AddField(\"id\",s1.TYPE_INTEGER,c.GetInt(\"id\"))";
__ref._s1._addfield(null,"id",__ref._s1._type_integer,BA.NumberToString(_c.GetInt("id")));
RDebugUtils.currentLine=5898254;
 //BA.debugLineNum = 5898254;BA.debugLine="s1.AddField(\"auftrag\",s1.TYPE_String,c.GetString";
__ref._s1._addfield(null,"auftrag",__ref._s1._type_string,_c.GetString("Auftrag"));
RDebugUtils.currentLine=5898255;
 //BA.debugLineNum = 5898255;BA.debugLine="s1.AddField(\"stoff\",s1.TYPE_String,c.GetString(";
__ref._s1._addfield(null,"stoff",__ref._s1._type_string,_c.GetString("Stoff"));
RDebugUtils.currentLine=5898256;
 //BA.debugLineNum = 5898256;BA.debugLine="s1.AddField(\"Anzahl\",s1.TYPE_integer,c.Getint(\"A";
__ref._s1._addfield(null,"Anzahl",__ref._s1._type_integer,BA.NumberToString(_c.GetInt("Anzahl")));
RDebugUtils.currentLine=5898257;
 //BA.debugLineNum = 5898257;BA.debugLine="s1.AddField(\"Bemerkung\",s1.TYPE_String,c.GetStri";
__ref._s1._addfield(null,"Bemerkung",__ref._s1._type_string,_c.GetString("Bemerkung"));
RDebugUtils.currentLine=5898258;
 //BA.debugLineNum = 5898258;BA.debugLine="s1.AddField(\"Gewicht\",s1.TYPE_String,c.GetString";
__ref._s1._addfield(null,"Gewicht",__ref._s1._type_string,_c.GetString("Gewicht"));
RDebugUtils.currentLine=5898259;
 //BA.debugLineNum = 5898259;BA.debugLine="s1.AddField(\"datum\",s1.TYPE_String,c.GetString(\"";
__ref._s1._addfield(null,"datum",__ref._s1._type_string,_c.GetString("date1"));
RDebugUtils.currentLine=5898260;
 //BA.debugLineNum = 5898260;BA.debugLine="If i = c.RowCount-1 Then";
if (_i==_c.getRowCount()-1) { 
RDebugUtils.currentLine=5898261;
 //BA.debugLineNum = 5898261;BA.debugLine="s1.SendRequest(\"up_eingabe\",\"send_eingabe\",Tru";
__ref._s1._sendrequest(null,"up_eingabe","send_eingabe",__c.True);
 }else {
RDebugUtils.currentLine=5898263;
 //BA.debugLineNum = 5898263;BA.debugLine="s1.SendRequest(\"up_eingabe\",\"send_eingabe\",Fals";
__ref._s1._sendrequest(null,"up_eingabe","send_eingabe",__c.False);
 };
 }
};
RDebugUtils.currentLine=5898269;
 //BA.debugLineNum = 5898269;BA.debugLine="End Sub";
return "";
}
public String  _send_nachricht(b4a.example.update_sender __ref) throws Exception{
__ref = this;
RDebugUtils.currentModule="update_sender";
if (Debug.shouldDelegate(ba, "send_nachricht"))
	return (String) Debug.delegate(ba, "send_nachricht", null);
anywheresoftware.b4a.sql.SQL.CursorWrapper _c = null;
int _i = 0;
RDebugUtils.currentLine=5963776;
 //BA.debugLineNum = 5963776;BA.debugLine="Public Sub send_nachricht";
RDebugUtils.currentLine=5963777;
 //BA.debugLineNum = 5963777;BA.debugLine="Try";
try {RDebugUtils.currentLine=5963778;
 //BA.debugLineNum = 5963778;BA.debugLine="Dim c As Cursor";
_c = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
RDebugUtils.currentLine=5963779;
 //BA.debugLineNum = 5963779;BA.debugLine="c= Main.sql1.ExecQuery(\"Select * from tbl_antwort";
_c.setObject((android.database.Cursor)(_main._sql1.ExecQuery("Select * from tbl_antwort where sync is null")));
RDebugUtils.currentLine=5963780;
 //BA.debugLineNum = 5963780;BA.debugLine="For i = 0 To c.RowCount -1";
{
final int step4 = 1;
final int limit4 = (int) (_c.getRowCount()-1);
_i = (int) (0) ;
for (;(step4 > 0 && _i <= limit4) || (step4 < 0 && _i >= limit4) ;_i = ((int)(0 + _i + step4))  ) {
RDebugUtils.currentLine=5963781;
 //BA.debugLineNum = 5963781;BA.debugLine="c.Position=i";
_c.setPosition(_i);
RDebugUtils.currentLine=5963782;
 //BA.debugLineNum = 5963782;BA.debugLine="s1.Initialize(Main.use_service &\"?op=send_info\",";
__ref._s1._initialize(null,ba,_main._use_service+"?op=send_info","send_info",this,"Result_info");
RDebugUtils.currentLine=5963783;
 //BA.debugLineNum = 5963783;BA.debugLine="s1.AddField(\"usr\",s1.TYPE_STRING,usr)";
__ref._s1._addfield(null,"usr",__ref._s1._type_string,__ref._usr);
RDebugUtils.currentLine=5963784;
 //BA.debugLineNum = 5963784;BA.debugLine="s1.AddField(\"pwd\",s1.TYPE_STRING,pwd)";
__ref._s1._addfield(null,"pwd",__ref._s1._type_string,__ref._pwd);
RDebugUtils.currentLine=5963785;
 //BA.debugLineNum = 5963785;BA.debugLine="s1.AddField(\"id\",s1.TYPE_STRING,c.GetString(\"Info";
__ref._s1._addfield(null,"id",__ref._s1._type_string,_c.GetString("Info_id"));
RDebugUtils.currentLine=5963786;
 //BA.debugLineNum = 5963786;BA.debugLine="s1.AddField(\"Antwort\",s1.TYPE_STRING,c.GetString(";
__ref._s1._addfield(null,"Antwort",__ref._s1._type_string,_c.GetString("Antwort"));
RDebugUtils.currentLine=5963787;
 //BA.debugLineNum = 5963787;BA.debugLine="s1.AddField(\"id_tabelle\",s1.TYPE_INTEGER,c.GetInt";
__ref._s1._addfield(null,"id_tabelle",__ref._s1._type_integer,BA.NumberToString(_c.GetInt("id")));
RDebugUtils.currentLine=5963788;
 //BA.debugLineNum = 5963788;BA.debugLine="If i = c.RowCount-1 Then";
if (_i==_c.getRowCount()-1) { 
RDebugUtils.currentLine=5963789;
 //BA.debugLineNum = 5963789;BA.debugLine="s1.SendRequest(\"send_info\",\"all\",True)";
__ref._s1._sendrequest(null,"send_info","all",__c.True);
 }else {
RDebugUtils.currentLine=5963791;
 //BA.debugLineNum = 5963791;BA.debugLine="s1.SendRequest(\"send_info\",\"all\",False)";
__ref._s1._sendrequest(null,"send_info","all",__c.False);
 };
 }
};
 } 
       catch (Exception e19) {
			ba.setLastException(e19);RDebugUtils.currentLine=5963797;
 //BA.debugLineNum = 5963797;BA.debugLine="Log(LastException)";
__c.Log(BA.ObjectToString(__c.LastException(getActivityBA())));
 };
RDebugUtils.currentLine=5963799;
 //BA.debugLineNum = 5963799;BA.debugLine="End Sub";
return "";
}
public String  _send_scanliste(b4a.example.update_sender __ref) throws Exception{
__ref = this;
RDebugUtils.currentModule="update_sender";
if (Debug.shouldDelegate(ba, "send_scanliste"))
	return (String) Debug.delegate(ba, "send_scanliste", null);
anywheresoftware.b4a.sql.SQL.CursorWrapper _c = null;
int _i = 0;
RDebugUtils.currentLine=6029312;
 //BA.debugLineNum = 6029312;BA.debugLine="Public Sub send_scanliste";
RDebugUtils.currentLine=6029313;
 //BA.debugLineNum = 6029313;BA.debugLine="Dim c As Cursor";
_c = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
RDebugUtils.currentLine=6029314;
 //BA.debugLineNum = 6029314;BA.debugLine="c= Main.sql1.ExecQuery(\"Select * from tbl_scan whe";
_c.setObject((android.database.Cursor)(_main._sql1.ExecQuery("Select * from tbl_scan where sync is null")));
RDebugUtils.currentLine=6029315;
 //BA.debugLineNum = 6029315;BA.debugLine="For i = 0 To c.RowCount -1";
{
final int step3 = 1;
final int limit3 = (int) (_c.getRowCount()-1);
_i = (int) (0) ;
for (;(step3 > 0 && _i <= limit3) || (step3 < 0 && _i >= limit3) ;_i = ((int)(0 + _i + step3))  ) {
RDebugUtils.currentLine=6029316;
 //BA.debugLineNum = 6029316;BA.debugLine="c.Position=i";
_c.setPosition(_i);
RDebugUtils.currentLine=6029317;
 //BA.debugLineNum = 6029317;BA.debugLine="s1.Initialize(Main.use_service &\"?op=send_info\",";
__ref._s1._initialize(null,ba,_main._use_service+"?op=send_info","up_scan",this,"Result_scan");
RDebugUtils.currentLine=6029318;
 //BA.debugLineNum = 6029318;BA.debugLine="s1.AddField(\"usr\",s1.TYPE_STRING,usr)";
__ref._s1._addfield(null,"usr",__ref._s1._type_string,__ref._usr);
RDebugUtils.currentLine=6029319;
 //BA.debugLineNum = 6029319;BA.debugLine="s1.AddField(\"pwd\",s1.TYPE_STRING,pwd)";
__ref._s1._addfield(null,"pwd",__ref._s1._type_string,__ref._pwd);
RDebugUtils.currentLine=6029320;
 //BA.debugLineNum = 6029320;BA.debugLine="s1.AddField(\"id\",s1.TYPE_Integer,c.Getint(\"id\"))";
__ref._s1._addfield(null,"id",__ref._s1._type_integer,BA.NumberToString(_c.GetInt("id")));
RDebugUtils.currentLine=6029321;
 //BA.debugLineNum = 6029321;BA.debugLine="s1.AddField(\"Code\",s1.TYPE_STRING,c.GetString(\"";
__ref._s1._addfield(null,"Code",__ref._s1._type_string,_c.GetString("Code"));
RDebugUtils.currentLine=6029322;
 //BA.debugLineNum = 6029322;BA.debugLine="s1.AddField(\"timestamp\",s1.TYPE_STRING,c.GetStr";
__ref._s1._addfield(null,"timestamp",__ref._s1._type_string,_c.GetString("timestamp"));
RDebugUtils.currentLine=6029323;
 //BA.debugLineNum = 6029323;BA.debugLine="s1.AddField(\"Bemerkung\",s1.TYPE_STRING,c.GetStr";
__ref._s1._addfield(null,"Bemerkung",__ref._s1._type_string,_c.GetString("Bemerkung"));
RDebugUtils.currentLine=6029324;
 //BA.debugLineNum = 6029324;BA.debugLine="s1.AddField(\"Auftrag\",s1.TYPE_STRING,c.GetStrin";
__ref._s1._addfield(null,"Auftrag",__ref._s1._type_string,_c.GetString("Auftrag"));
RDebugUtils.currentLine=6029325;
 //BA.debugLineNum = 6029325;BA.debugLine="s1.AddField(\"Stoff\",s1.TYPE_STRING,c.GetString(";
__ref._s1._addfield(null,"Stoff",__ref._s1._type_string,_c.GetString("Stoff"));
RDebugUtils.currentLine=6029326;
 //BA.debugLineNum = 6029326;BA.debugLine="If i = c.RowCount-1 Then";
if (_i==_c.getRowCount()-1) { 
RDebugUtils.currentLine=6029327;
 //BA.debugLineNum = 6029327;BA.debugLine="s1.SendRequest(\"up_scan\",\"all\",True)";
__ref._s1._sendrequest(null,"up_scan","all",__c.True);
 }else {
RDebugUtils.currentLine=6029329;
 //BA.debugLineNum = 6029329;BA.debugLine="s1.SendRequest(\"up_scan\",\"all\",False)";
__ref._s1._sendrequest(null,"up_scan","all",__c.False);
 };
 }
};
RDebugUtils.currentLine=6029333;
 //BA.debugLineNum = 6029333;BA.debugLine="End Sub";
return "";
}
}