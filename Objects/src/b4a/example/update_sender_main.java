package b4a.example;


import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.B4AClass;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.debug.*;

public class update_sender_main extends B4AClass.ImplB4AClass implements BA.SubDelegator{
    private static java.util.HashMap<String, java.lang.reflect.Method> htSubs;
    private void innerInitialize(BA _ba) throws Exception {
        if (ba == null) {
            ba = new anywheresoftware.b4a.ShellBA(_ba, this, htSubs, "b4a.example.update_sender_main");
            if (htSubs == null) {
                ba.loadHtSubs(this.getClass());
                htSubs = ba.htSubs;
            }
            
        }
        if (BA.isShellModeRuntimeCheck(ba)) 
			   this.getClass().getMethod("_class_globals", b4a.example.update_sender_main.class).invoke(this, new Object[] {null});
        else
            ba.raiseEvent2(null, true, "class_globals", false);
    }

 
    public void  innerInitializeHelper(anywheresoftware.b4a.BA _ba) throws Exception{
        innerInitialize(_ba);
    }
    public Object callSub(String sub, Object sender, Object[] args) throws Exception {
        return BA.SubDelegator.SubNotFound;
    }
public anywheresoftware.b4a.keywords.Common __c = null;
public b4a.example.sp_single_main _s1 = null;
public String _usr = "";
public String _pwd = "";
public anywheresoftware.b4a.samples.httputils2.httputils2service _httputils2service = null;
public b4a.example.main _main = null;
public b4a.example.starter _starter = null;
public b4a.example.scale _scale = null;
public b4a.example.setup _setup = null;
public b4a.example.ze_basis _ze_basis = null;
public String  _initialize(b4a.example.update_sender_main __ref,anywheresoftware.b4a.BA _ba) throws Exception{
__ref = this;
innerInitialize(_ba);
RDebugUtils.currentModule="update_sender_main";
if (Debug.shouldDelegate(ba, "initialize"))
	return (String) Debug.delegate(ba, "initialize", new Object[] {_ba});
anywheresoftware.b4a.sql.SQL.CursorWrapper _c_user = null;
RDebugUtils.currentLine=4653056;
 //BA.debugLineNum = 4653056;BA.debugLine="Public Sub Initialize";
RDebugUtils.currentLine=4653057;
 //BA.debugLineNum = 4653057;BA.debugLine="Try";
try {RDebugUtils.currentLine=4653060;
 //BA.debugLineNum = 4653060;BA.debugLine="Dim c_user As Cursor";
_c_user = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
RDebugUtils.currentLine=4653061;
 //BA.debugLineNum = 4653061;BA.debugLine="c_user=Main.sql1.ExecQuery(\"select * from tbl_usr\"";
_c_user.setObject((android.database.Cursor)(_main._sql1.ExecQuery("select * from tbl_usr")));
RDebugUtils.currentLine=4653062;
 //BA.debugLineNum = 4653062;BA.debugLine="c_user.Position=0";
_c_user.setPosition((int) (0));
RDebugUtils.currentLine=4653063;
 //BA.debugLineNum = 4653063;BA.debugLine="usr=c_user.GetString(\"username\")";
__ref._usr = _c_user.GetString("username");
RDebugUtils.currentLine=4653064;
 //BA.debugLineNum = 4653064;BA.debugLine="pwd=c_user.GetString(\"pw\")";
__ref._pwd = _c_user.GetString("pw");
 } 
       catch (Exception e8) {
			ba.setLastException(e8);RDebugUtils.currentLine=4653066;
 //BA.debugLineNum = 4653066;BA.debugLine="Log(\"ungültige anmeldung\")";
__c.Log("ungültige anmeldung");
 };
RDebugUtils.currentLine=4653068;
 //BA.debugLineNum = 4653068;BA.debugLine="End Sub";
return "";
}
public String  _class_globals(b4a.example.update_sender_main __ref) throws Exception{
__ref = this;
RDebugUtils.currentModule="update_sender_main";
RDebugUtils.currentLine=4587520;
 //BA.debugLineNum = 4587520;BA.debugLine="Sub Class_Globals";
RDebugUtils.currentLine=4587521;
 //BA.debugLineNum = 4587521;BA.debugLine="Dim s1 As sp_single_main";
_s1 = new b4a.example.sp_single_main();
RDebugUtils.currentLine=4587522;
 //BA.debugLineNum = 4587522;BA.debugLine="Public usr As String";
_usr = "";
RDebugUtils.currentLine=4587523;
 //BA.debugLineNum = 4587523;BA.debugLine="Public pwd As String";
_pwd = "";
RDebugUtils.currentLine=4587524;
 //BA.debugLineNum = 4587524;BA.debugLine="End Sub";
return "";
}
public String  _result_ankunft(b4a.example.update_sender_main __ref,Object _res) throws Exception{
__ref = this;
RDebugUtils.currentModule="update_sender_main";
if (Debug.shouldDelegate(ba, "result_ankunft"))
	return (String) Debug.delegate(ba, "result_ankunft", new Object[] {_res});
RDebugUtils.currentLine=5111808;
 //BA.debugLineNum = 5111808;BA.debugLine="Sub Result_ankunft(res As Object)";
RDebugUtils.currentLine=5111809;
 //BA.debugLineNum = 5111809;BA.debugLine="Log(res)";
__c.Log(BA.ObjectToString(_res));
RDebugUtils.currentLine=5111810;
 //BA.debugLineNum = 5111810;BA.debugLine="End Sub";
return "";
}
public String  _result_eingabe(b4a.example.update_sender_main __ref,Object _res) throws Exception{
__ref = this;
RDebugUtils.currentModule="update_sender_main";
if (Debug.shouldDelegate(ba, "result_eingabe"))
	return (String) Debug.delegate(ba, "result_eingabe", new Object[] {_res});
RDebugUtils.currentLine=5177344;
 //BA.debugLineNum = 5177344;BA.debugLine="Sub Result_eingabe(res As Object)";
RDebugUtils.currentLine=5177345;
 //BA.debugLineNum = 5177345;BA.debugLine="Log(res)";
__c.Log(BA.ObjectToString(_res));
RDebugUtils.currentLine=5177346;
 //BA.debugLineNum = 5177346;BA.debugLine="End Sub";
return "";
}
public String  _result_info(b4a.example.update_sender_main __ref,Object _res) throws Exception{
__ref = this;
RDebugUtils.currentModule="update_sender_main";
if (Debug.shouldDelegate(ba, "result_info"))
	return (String) Debug.delegate(ba, "result_info", new Object[] {_res});
RDebugUtils.currentLine=5046272;
 //BA.debugLineNum = 5046272;BA.debugLine="Sub Result_info(res As Object)";
RDebugUtils.currentLine=5046273;
 //BA.debugLineNum = 5046273;BA.debugLine="Log(res)";
__c.Log(BA.ObjectToString(_res));
RDebugUtils.currentLine=5046274;
 //BA.debugLineNum = 5046274;BA.debugLine="End Sub";
return "";
}
public String  _send_ankunft(b4a.example.update_sender_main __ref) throws Exception{
__ref = this;
RDebugUtils.currentModule="update_sender_main";
if (Debug.shouldDelegate(ba, "send_ankunft"))
	return (String) Debug.delegate(ba, "send_ankunft", null);
anywheresoftware.b4a.sql.SQL.CursorWrapper _c = null;
int _i = 0;
RDebugUtils.currentLine=4718592;
 //BA.debugLineNum = 4718592;BA.debugLine="public Sub send_ankunft( )";
RDebugUtils.currentLine=4718596;
 //BA.debugLineNum = 4718596;BA.debugLine="Dim c As Cursor";
_c = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
RDebugUtils.currentLine=4718598;
 //BA.debugLineNum = 4718598;BA.debugLine="c= Main.sql1.ExecQuery(\"Select * from tbl_ankunft";
_c.setObject((android.database.Cursor)(_main._sql1.ExecQuery("Select * from tbl_ankunft where sync is null")));
RDebugUtils.currentLine=4718600;
 //BA.debugLineNum = 4718600;BA.debugLine="For i = 0 To c.RowCount -1";
{
final int step3 = 1;
final int limit3 = (int) (_c.getRowCount()-1);
_i = (int) (0) ;
for (;(step3 > 0 && _i <= limit3) || (step3 < 0 && _i >= limit3) ;_i = ((int)(0 + _i + step3))  ) {
RDebugUtils.currentLine=4718603;
 //BA.debugLineNum = 4718603;BA.debugLine="c.Position=i";
_c.setPosition(_i);
RDebugUtils.currentLine=4718604;
 //BA.debugLineNum = 4718604;BA.debugLine="s1.Initialize(Main.use_service &\"?op=up_ankunf";
__ref._s1._initialize(null,ba,_main._use_service+"?op=up_ankunft","up_ankunft",this,"Result_ankunft");
RDebugUtils.currentLine=4718605;
 //BA.debugLineNum = 4718605;BA.debugLine="s1.AddField(\"usr\",s1.TYPE_STRING,usr)";
__ref._s1._addfield(null,"usr",__ref._s1._type_string,__ref._usr);
RDebugUtils.currentLine=4718606;
 //BA.debugLineNum = 4718606;BA.debugLine="s1.AddField(\"pwd\",s1.TYPE_STRING,pwd)";
__ref._s1._addfield(null,"pwd",__ref._s1._type_string,__ref._pwd);
RDebugUtils.currentLine=4718607;
 //BA.debugLineNum = 4718607;BA.debugLine="s1.AddField(\"id\",s1.TYPE_INTEGER,c.GetInt(\"id\"))";
__ref._s1._addfield(null,"id",__ref._s1._type_integer,BA.NumberToString(_c.GetInt("id")));
RDebugUtils.currentLine=4718608;
 //BA.debugLineNum = 4718608;BA.debugLine="s1.AddField(\"auftrag\",s1.TYPE_String,c.GetString";
__ref._s1._addfield(null,"auftrag",__ref._s1._type_string,_c.GetString("auftrag"));
RDebugUtils.currentLine=4718609;
 //BA.debugLineNum = 4718609;BA.debugLine="s1.AddField(\"ankunft\",s1.TYPE_String,c.GetStrin";
__ref._s1._addfield(null,"ankunft",__ref._s1._type_string,_c.GetString("ankunft"));
RDebugUtils.currentLine=4718610;
 //BA.debugLineNum = 4718610;BA.debugLine="Log (c.GetString(\"ankunft\"))";
__c.Log(_c.GetString("ankunft"));
RDebugUtils.currentLine=4718611;
 //BA.debugLineNum = 4718611;BA.debugLine="s1.AddField(\"gps\",s1.TYPE_String,c.GetString(\"g";
__ref._s1._addfield(null,"gps",__ref._s1._type_string,_c.GetString("gps"));
RDebugUtils.currentLine=4718612;
 //BA.debugLineNum = 4718612;BA.debugLine="If i = c.RowCount-1 Then";
if (_i==_c.getRowCount()-1) { 
RDebugUtils.currentLine=4718613;
 //BA.debugLineNum = 4718613;BA.debugLine="s1.SendRequest(\"up_ankunft\",\"send_ankunft\",Tru";
__ref._s1._sendrequest(null,"up_ankunft","send_ankunft",__c.True);
 }else {
RDebugUtils.currentLine=4718615;
 //BA.debugLineNum = 4718615;BA.debugLine="s1.SendRequest(\"up_ankunft\",\"send_ankunft\",Fals";
__ref._s1._sendrequest(null,"up_ankunft","send_ankunft",__c.False);
 };
 }
};
RDebugUtils.currentLine=4718625;
 //BA.debugLineNum = 4718625;BA.debugLine="End Sub";
return "";
}
public String  _send_eingabe(b4a.example.update_sender_main __ref) throws Exception{
__ref = this;
RDebugUtils.currentModule="update_sender_main";
if (Debug.shouldDelegate(ba, "send_eingabe"))
	return (String) Debug.delegate(ba, "send_eingabe", null);
anywheresoftware.b4a.sql.SQL.CursorWrapper _c = null;
int _i = 0;
RDebugUtils.currentLine=4849664;
 //BA.debugLineNum = 4849664;BA.debugLine="public Sub send_eingabe( )";
RDebugUtils.currentLine=4849668;
 //BA.debugLineNum = 4849668;BA.debugLine="Dim c As Cursor";
_c = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
RDebugUtils.currentLine=4849670;
 //BA.debugLineNum = 4849670;BA.debugLine="c= Main.sql1.ExecQuery(\"Select * from tbl_nachtra";
_c.setObject((android.database.Cursor)(_main._sql1.ExecQuery("Select * from tbl_nachtrag_log_app where sync is null")));
RDebugUtils.currentLine=4849672;
 //BA.debugLineNum = 4849672;BA.debugLine="For i = 0 To c.RowCount -1";
{
final int step3 = 1;
final int limit3 = (int) (_c.getRowCount()-1);
_i = (int) (0) ;
for (;(step3 > 0 && _i <= limit3) || (step3 < 0 && _i >= limit3) ;_i = ((int)(0 + _i + step3))  ) {
RDebugUtils.currentLine=4849673;
 //BA.debugLineNum = 4849673;BA.debugLine="c.Position=i";
_c.setPosition(_i);
RDebugUtils.currentLine=4849674;
 //BA.debugLineNum = 4849674;BA.debugLine="s1.Initialize(Main.use_service &\"?op=up_eingab";
__ref._s1._initialize(null,ba,_main._use_service+"?op=up_eingabe","up_eingabe",this,"Result_eingabe");
RDebugUtils.currentLine=4849675;
 //BA.debugLineNum = 4849675;BA.debugLine="s1.AddField(\"usr\",s1.TYPE_STRING,usr)";
__ref._s1._addfield(null,"usr",__ref._s1._type_string,__ref._usr);
RDebugUtils.currentLine=4849676;
 //BA.debugLineNum = 4849676;BA.debugLine="s1.AddField(\"pwd\",s1.TYPE_STRING,pwd)";
__ref._s1._addfield(null,"pwd",__ref._s1._type_string,__ref._pwd);
RDebugUtils.currentLine=4849677;
 //BA.debugLineNum = 4849677;BA.debugLine="s1.AddField(\"id\",s1.TYPE_INTEGER,c.GetInt(\"id\"))";
__ref._s1._addfield(null,"id",__ref._s1._type_integer,BA.NumberToString(_c.GetInt("id")));
RDebugUtils.currentLine=4849678;
 //BA.debugLineNum = 4849678;BA.debugLine="s1.AddField(\"auftrag\",s1.TYPE_String,c.GetString";
__ref._s1._addfield(null,"auftrag",__ref._s1._type_string,_c.GetString("Auftrag"));
RDebugUtils.currentLine=4849679;
 //BA.debugLineNum = 4849679;BA.debugLine="s1.AddField(\"stoff\",s1.TYPE_String,c.GetString(";
__ref._s1._addfield(null,"stoff",__ref._s1._type_string,_c.GetString("Stoff"));
RDebugUtils.currentLine=4849680;
 //BA.debugLineNum = 4849680;BA.debugLine="s1.AddField(\"Anzahl\",s1.TYPE_integer,c.Getint(\"A";
__ref._s1._addfield(null,"Anzahl",__ref._s1._type_integer,BA.NumberToString(_c.GetInt("Anzahl")));
RDebugUtils.currentLine=4849681;
 //BA.debugLineNum = 4849681;BA.debugLine="s1.AddField(\"Bemerkung\",s1.TYPE_String,c.GetStri";
__ref._s1._addfield(null,"Bemerkung",__ref._s1._type_string,_c.GetString("Bemerkung"));
RDebugUtils.currentLine=4849682;
 //BA.debugLineNum = 4849682;BA.debugLine="s1.AddField(\"Gewicht\",s1.TYPE_String,c.GetString";
__ref._s1._addfield(null,"Gewicht",__ref._s1._type_string,_c.GetString("Gewicht"));
RDebugUtils.currentLine=4849683;
 //BA.debugLineNum = 4849683;BA.debugLine="s1.AddField(\"datum\",s1.TYPE_String,c.GetString(\"";
__ref._s1._addfield(null,"datum",__ref._s1._type_string,_c.GetString("date1"));
RDebugUtils.currentLine=4849684;
 //BA.debugLineNum = 4849684;BA.debugLine="If i = c.RowCount-1 Then";
if (_i==_c.getRowCount()-1) { 
RDebugUtils.currentLine=4849685;
 //BA.debugLineNum = 4849685;BA.debugLine="s1.SendRequest(\"up_eingabe\",\"send_eingabe\",Tru";
__ref._s1._sendrequest(null,"up_eingabe","send_eingabe",__c.True);
 }else {
RDebugUtils.currentLine=4849687;
 //BA.debugLineNum = 4849687;BA.debugLine="s1.SendRequest(\"up_eingabe\",\"send_eingabe\",Fals";
__ref._s1._sendrequest(null,"up_eingabe","send_eingabe",__c.False);
 };
 }
};
RDebugUtils.currentLine=4849693;
 //BA.debugLineNum = 4849693;BA.debugLine="End Sub";
return "";
}
public String  _send_nachricht(b4a.example.update_sender_main __ref) throws Exception{
__ref = this;
RDebugUtils.currentModule="update_sender_main";
if (Debug.shouldDelegate(ba, "send_nachricht"))
	return (String) Debug.delegate(ba, "send_nachricht", null);
anywheresoftware.b4a.sql.SQL.CursorWrapper _c = null;
int _i = 0;
RDebugUtils.currentLine=4915200;
 //BA.debugLineNum = 4915200;BA.debugLine="Public Sub send_nachricht";
RDebugUtils.currentLine=4915201;
 //BA.debugLineNum = 4915201;BA.debugLine="Try";
try {RDebugUtils.currentLine=4915202;
 //BA.debugLineNum = 4915202;BA.debugLine="Dim c As Cursor";
_c = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
RDebugUtils.currentLine=4915203;
 //BA.debugLineNum = 4915203;BA.debugLine="c= Main.sql1.ExecQuery(\"Select * from tbl_antwort";
_c.setObject((android.database.Cursor)(_main._sql1.ExecQuery("Select * from tbl_antwort where sync is null")));
RDebugUtils.currentLine=4915204;
 //BA.debugLineNum = 4915204;BA.debugLine="For i = 0 To c.RowCount -1";
{
final int step4 = 1;
final int limit4 = (int) (_c.getRowCount()-1);
_i = (int) (0) ;
for (;(step4 > 0 && _i <= limit4) || (step4 < 0 && _i >= limit4) ;_i = ((int)(0 + _i + step4))  ) {
RDebugUtils.currentLine=4915205;
 //BA.debugLineNum = 4915205;BA.debugLine="c.Position=i";
_c.setPosition(_i);
RDebugUtils.currentLine=4915206;
 //BA.debugLineNum = 4915206;BA.debugLine="s1.Initialize(main.use_service &\"?op=send_info\",";
__ref._s1._initialize(null,ba,_main._use_service+"?op=send_info","send_info",this,"Result_info");
RDebugUtils.currentLine=4915207;
 //BA.debugLineNum = 4915207;BA.debugLine="s1.AddField(\"usr\",s1.TYPE_STRING,usr)";
__ref._s1._addfield(null,"usr",__ref._s1._type_string,__ref._usr);
RDebugUtils.currentLine=4915208;
 //BA.debugLineNum = 4915208;BA.debugLine="s1.AddField(\"pwd\",s1.TYPE_STRING,pwd)";
__ref._s1._addfield(null,"pwd",__ref._s1._type_string,__ref._pwd);
RDebugUtils.currentLine=4915209;
 //BA.debugLineNum = 4915209;BA.debugLine="s1.AddField(\"id\",s1.TYPE_STRING,c.GetString(\"Info";
__ref._s1._addfield(null,"id",__ref._s1._type_string,_c.GetString("Info_id"));
RDebugUtils.currentLine=4915210;
 //BA.debugLineNum = 4915210;BA.debugLine="s1.AddField(\"Antwort\",s1.TYPE_STRING,c.GetString(";
__ref._s1._addfield(null,"Antwort",__ref._s1._type_string,_c.GetString("Antwort"));
RDebugUtils.currentLine=4915211;
 //BA.debugLineNum = 4915211;BA.debugLine="s1.AddField(\"id_tabelle\",s1.TYPE_INTEGER,c.GetInt";
__ref._s1._addfield(null,"id_tabelle",__ref._s1._type_integer,BA.NumberToString(_c.GetInt("id")));
RDebugUtils.currentLine=4915212;
 //BA.debugLineNum = 4915212;BA.debugLine="If i = c.RowCount-1 Then";
if (_i==_c.getRowCount()-1) { 
RDebugUtils.currentLine=4915213;
 //BA.debugLineNum = 4915213;BA.debugLine="s1.SendRequest(\"send_info\",\"all\",True)";
__ref._s1._sendrequest(null,"send_info","all",__c.True);
 }else {
RDebugUtils.currentLine=4915215;
 //BA.debugLineNum = 4915215;BA.debugLine="s1.SendRequest(\"send_info\",\"all\",False)";
__ref._s1._sendrequest(null,"send_info","all",__c.False);
 };
 }
};
 } 
       catch (Exception e19) {
			ba.setLastException(e19);RDebugUtils.currentLine=4915221;
 //BA.debugLineNum = 4915221;BA.debugLine="Log(LastException)";
__c.Log(BA.ObjectToString(__c.LastException(getActivityBA())));
 };
RDebugUtils.currentLine=4915223;
 //BA.debugLineNum = 4915223;BA.debugLine="End Sub";
return "";
}
public String  _send_quit(b4a.example.update_sender_main __ref) throws Exception{
__ref = this;
RDebugUtils.currentModule="update_sender_main";
if (Debug.shouldDelegate(ba, "send_quit"))
	return (String) Debug.delegate(ba, "send_quit", null);
anywheresoftware.b4a.sql.SQL.CursorWrapper _c = null;
int _i = 0;
RDebugUtils.currentLine=4784128;
 //BA.debugLineNum = 4784128;BA.debugLine="public Sub send_quit( )";
RDebugUtils.currentLine=4784132;
 //BA.debugLineNum = 4784132;BA.debugLine="Dim c As Cursor";
_c = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
RDebugUtils.currentLine=4784134;
 //BA.debugLineNum = 4784134;BA.debugLine="c= Main.sql1.ExecQuery(\"Select * from tbl_quittun";
_c.setObject((android.database.Cursor)(_main._sql1.ExecQuery("Select * from tbl_quittung where sync is null")));
RDebugUtils.currentLine=4784136;
 //BA.debugLineNum = 4784136;BA.debugLine="For i = 0 To c.RowCount -1";
{
final int step3 = 1;
final int limit3 = (int) (_c.getRowCount()-1);
_i = (int) (0) ;
for (;(step3 > 0 && _i <= limit3) || (step3 < 0 && _i >= limit3) ;_i = ((int)(0 + _i + step3))  ) {
RDebugUtils.currentLine=4784139;
 //BA.debugLineNum = 4784139;BA.debugLine="c.Position=i";
_c.setPosition(_i);
RDebugUtils.currentLine=4784140;
 //BA.debugLineNum = 4784140;BA.debugLine="s1.Initialize(Main.use_service &\"?op=up_quit\",";
__ref._s1._initialize(null,ba,_main._use_service+"?op=up_quit","up_quit",this,"Result_quittung");
RDebugUtils.currentLine=4784141;
 //BA.debugLineNum = 4784141;BA.debugLine="s1.AddField(\"usr\",s1.TYPE_STRING,usr)";
__ref._s1._addfield(null,"usr",__ref._s1._type_string,__ref._usr);
RDebugUtils.currentLine=4784142;
 //BA.debugLineNum = 4784142;BA.debugLine="s1.AddField(\"pwd\",s1.TYPE_STRING,pwd)";
__ref._s1._addfield(null,"pwd",__ref._s1._type_string,__ref._pwd);
RDebugUtils.currentLine=4784143;
 //BA.debugLineNum = 4784143;BA.debugLine="s1.AddField(\"id\",s1.TYPE_INTEGER,c.GetInt(\"id\"))";
__ref._s1._addfield(null,"id",__ref._s1._type_integer,BA.NumberToString(_c.GetInt("id")));
RDebugUtils.currentLine=4784144;
 //BA.debugLineNum = 4784144;BA.debugLine="s1.AddField(\"Auftrag\",s1.TYPE_String,c.GetString";
__ref._s1._addfield(null,"Auftrag",__ref._s1._type_string,_c.GetString("Auftrag"));
RDebugUtils.currentLine=4784145;
 //BA.debugLineNum = 4784145;BA.debugLine="s1.AddField(\"receiver\",s1.TYPE_String,c.GetStri";
__ref._s1._addfield(null,"receiver",__ref._s1._type_string,_c.GetString("receiver"));
RDebugUtils.currentLine=4784148;
 //BA.debugLineNum = 4784148;BA.debugLine="If i = c.RowCount-1 Then";
if (_i==_c.getRowCount()-1) { 
RDebugUtils.currentLine=4784149;
 //BA.debugLineNum = 4784149;BA.debugLine="s1.SendRequest(\"up_quit\",\"send_quittung\",True)";
__ref._s1._sendrequest(null,"up_quit","send_quittung",__c.True);
 }else {
RDebugUtils.currentLine=4784151;
 //BA.debugLineNum = 4784151;BA.debugLine="s1.SendRequest(\"up_quit\",\"send_quittung\",False)";
__ref._s1._sendrequest(null,"up_quit","send_quittung",__c.False);
 };
 }
};
RDebugUtils.currentLine=4784158;
 //BA.debugLineNum = 4784158;BA.debugLine="End Sub";
return "";
}
public String  _send_scanliste(b4a.example.update_sender_main __ref) throws Exception{
__ref = this;
RDebugUtils.currentModule="update_sender_main";
if (Debug.shouldDelegate(ba, "send_scanliste"))
	return (String) Debug.delegate(ba, "send_scanliste", null);
anywheresoftware.b4a.sql.SQL.CursorWrapper _c = null;
int _i = 0;
RDebugUtils.currentLine=4980736;
 //BA.debugLineNum = 4980736;BA.debugLine="Public Sub send_scanliste";
RDebugUtils.currentLine=4980737;
 //BA.debugLineNum = 4980737;BA.debugLine="Dim c As Cursor";
_c = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
RDebugUtils.currentLine=4980738;
 //BA.debugLineNum = 4980738;BA.debugLine="c= Main.sql1.ExecQuery(\"Select * from tbl_scan whe";
_c.setObject((android.database.Cursor)(_main._sql1.ExecQuery("Select * from tbl_scan where sync is null")));
RDebugUtils.currentLine=4980739;
 //BA.debugLineNum = 4980739;BA.debugLine="For i = 0 To c.RowCount -1";
{
final int step3 = 1;
final int limit3 = (int) (_c.getRowCount()-1);
_i = (int) (0) ;
for (;(step3 > 0 && _i <= limit3) || (step3 < 0 && _i >= limit3) ;_i = ((int)(0 + _i + step3))  ) {
RDebugUtils.currentLine=4980740;
 //BA.debugLineNum = 4980740;BA.debugLine="c.Position=i";
_c.setPosition(_i);
RDebugUtils.currentLine=4980741;
 //BA.debugLineNum = 4980741;BA.debugLine="s1.Initialize(main.use_service &\"?op=send_info\",";
__ref._s1._initialize(null,ba,_main._use_service+"?op=send_info","up_scan",this,"Result_scan");
RDebugUtils.currentLine=4980742;
 //BA.debugLineNum = 4980742;BA.debugLine="s1.AddField(\"usr\",s1.TYPE_STRING,usr)";
__ref._s1._addfield(null,"usr",__ref._s1._type_string,__ref._usr);
RDebugUtils.currentLine=4980743;
 //BA.debugLineNum = 4980743;BA.debugLine="s1.AddField(\"pwd\",s1.TYPE_STRING,pwd)";
__ref._s1._addfield(null,"pwd",__ref._s1._type_string,__ref._pwd);
RDebugUtils.currentLine=4980744;
 //BA.debugLineNum = 4980744;BA.debugLine="s1.AddField(\"id\",s1.TYPE_Integer,c.Getint(\"id\"))";
__ref._s1._addfield(null,"id",__ref._s1._type_integer,BA.NumberToString(_c.GetInt("id")));
RDebugUtils.currentLine=4980745;
 //BA.debugLineNum = 4980745;BA.debugLine="s1.AddField(\"Code\",s1.TYPE_STRING,c.GetString(\"";
__ref._s1._addfield(null,"Code",__ref._s1._type_string,_c.GetString("Code"));
RDebugUtils.currentLine=4980746;
 //BA.debugLineNum = 4980746;BA.debugLine="s1.AddField(\"timestamp\",s1.TYPE_STRING,c.GetStr";
__ref._s1._addfield(null,"timestamp",__ref._s1._type_string,_c.GetString("timestamp"));
RDebugUtils.currentLine=4980747;
 //BA.debugLineNum = 4980747;BA.debugLine="s1.AddField(\"Bemerkung\",s1.TYPE_STRING,c.GetStr";
__ref._s1._addfield(null,"Bemerkung",__ref._s1._type_string,_c.GetString("Bemerkung"));
RDebugUtils.currentLine=4980748;
 //BA.debugLineNum = 4980748;BA.debugLine="s1.AddField(\"Auftrag\",s1.TYPE_STRING,c.GetStrin";
__ref._s1._addfield(null,"Auftrag",__ref._s1._type_string,_c.GetString("Auftrag"));
RDebugUtils.currentLine=4980749;
 //BA.debugLineNum = 4980749;BA.debugLine="s1.AddField(\"Stoff\",s1.TYPE_STRING,c.GetString(";
__ref._s1._addfield(null,"Stoff",__ref._s1._type_string,_c.GetString("Stoff"));
RDebugUtils.currentLine=4980750;
 //BA.debugLineNum = 4980750;BA.debugLine="If i = c.RowCount-1 Then";
if (_i==_c.getRowCount()-1) { 
RDebugUtils.currentLine=4980751;
 //BA.debugLineNum = 4980751;BA.debugLine="s1.SendRequest(\"up_scan\",\"all\",True)";
__ref._s1._sendrequest(null,"up_scan","all",__c.True);
 }else {
RDebugUtils.currentLine=4980753;
 //BA.debugLineNum = 4980753;BA.debugLine="s1.SendRequest(\"up_scan\",\"all\",False)";
__ref._s1._sendrequest(null,"up_scan","all",__c.False);
 };
 }
};
RDebugUtils.currentLine=4980757;
 //BA.debugLineNum = 4980757;BA.debugLine="End Sub";
return "";
}
}