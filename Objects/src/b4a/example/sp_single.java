package b4a.example;


import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.B4AClass;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.debug.*;

public class sp_single extends B4AClass.ImplB4AClass implements BA.SubDelegator{
    private static java.util.HashMap<String, java.lang.reflect.Method> htSubs;
    private void innerInitialize(BA _ba) throws Exception {
        if (ba == null) {
            ba = new anywheresoftware.b4a.ShellBA(_ba, this, htSubs, "b4a.example.sp_single");
            if (htSubs == null) {
                ba.loadHtSubs(this.getClass());
                htSubs = ba.htSubs;
            }
            
        }
        if (BA.isShellModeRuntimeCheck(ba)) 
			   this.getClass().getMethod("_class_globals", b4a.example.sp_single.class).invoke(this, new Object[] {null});
        else
            ba.raiseEvent2(null, true, "class_globals", false);
    }

 
    public void  innerInitializeHelper(anywheresoftware.b4a.BA _ba) throws Exception{
        innerInitialize(_ba);
    }
    public Object callSub(String sub, Object sender, Object[] args) throws Exception {
        return BA.SubDelegator.SubNotFound;
    }
public anywheresoftware.b4a.keywords.Common __c = null;
public anywheresoftware.b4a.objects.SaxParser _parser = null;
public String _type_string = "";
public String _type_integer = "";
public String _type_float = "";
public String _type_date = "";
public String _type_double = "";
public String _type_binary = "";
public String _urlws = "";
public Object _modules = null;
public String _en = "";
public String _xml_sendstriing = "";
public boolean _last1 = false;
public String _method = "";
public b4a.example.update_sender _update_send = null;
public anywheresoftware.b4a.samples.httputils2.httputils2service _httputils2service = null;
public b4a.example.main _main = null;
public b4a.example.starter _starter = null;
public b4a.example.scale _scale = null;
public b4a.example.setup _setup = null;
public b4a.example.ze_basis _ze_basis = null;
public String  _addfield(b4a.example.sp_single __ref,String _fieldname,String _fieldtype,String _fieldvalue) throws Exception{
__ref = this;
RDebugUtils.currentModule="sp_single";
if (Debug.shouldDelegate(ba, "addfield"))
	return (String) Debug.delegate(ba, "addfield", new Object[] {_fieldname,_fieldtype,_fieldvalue});
RDebugUtils.currentLine=2949120;
 //BA.debugLineNum = 2949120;BA.debugLine="Public Sub AddField(FieldName As String,FieldType";
RDebugUtils.currentLine=2949122;
 //BA.debugLineNum = 2949122;BA.debugLine="xml_sendstriing=xml_sendstriing &CRLF & $\"<${Fiel";
__ref._xml_sendstriing = __ref._xml_sendstriing+__c.CRLF+("<"+__c.SmartStringFormatter("",(Object)(_fieldname))+" >"+__c.SmartStringFormatter("",(Object)(_fieldvalue))+"</"+__c.SmartStringFormatter("",(Object)(_fieldname))+">");
RDebugUtils.currentLine=2949123;
 //BA.debugLineNum = 2949123;BA.debugLine="End Sub";
return "";
}
public String  _class_globals(b4a.example.sp_single __ref) throws Exception{
__ref = this;
RDebugUtils.currentModule="sp_single";
RDebugUtils.currentLine=2686976;
 //BA.debugLineNum = 2686976;BA.debugLine="Sub Class_Globals";
RDebugUtils.currentLine=2686978;
 //BA.debugLineNum = 2686978;BA.debugLine="Dim parser As SaxParser";
_parser = new anywheresoftware.b4a.objects.SaxParser();
RDebugUtils.currentLine=2686979;
 //BA.debugLineNum = 2686979;BA.debugLine="Public TYPE_STRING   As String = \"xsd:string\"";
_type_string = "xsd:string";
RDebugUtils.currentLine=2686980;
 //BA.debugLineNum = 2686980;BA.debugLine="Public TYPE_INTEGER  As String = \"xsd:int\"";
_type_integer = "xsd:int";
RDebugUtils.currentLine=2686981;
 //BA.debugLineNum = 2686981;BA.debugLine="Public TYPE_FLOAT    As String = \"xsd:float\"";
_type_float = "xsd:float";
RDebugUtils.currentLine=2686982;
 //BA.debugLineNum = 2686982;BA.debugLine="Public TYPE_DATE     As String = \"xsd:date\"";
_type_date = "xsd:date";
RDebugUtils.currentLine=2686983;
 //BA.debugLineNum = 2686983;BA.debugLine="Public TYPE_DOUBLE   As String = \"xsd:double\"";
_type_double = "xsd:double";
RDebugUtils.currentLine=2686984;
 //BA.debugLineNum = 2686984;BA.debugLine="Public TYPE_BINARY   As String = \"xsd:base64Binar";
_type_binary = "xsd:base64Binary";
RDebugUtils.currentLine=2686986;
 //BA.debugLineNum = 2686986;BA.debugLine="Private UrlWS        As String";
_urlws = "";
RDebugUtils.currentLine=2686988;
 //BA.debugLineNum = 2686988;BA.debugLine="Private Modules      As Object";
_modules = new Object();
RDebugUtils.currentLine=2686989;
 //BA.debugLineNum = 2686989;BA.debugLine="Private EN           As String";
_en = "";
RDebugUtils.currentLine=2686990;
 //BA.debugLineNum = 2686990;BA.debugLine="Private xml_sendstriing As String";
_xml_sendstriing = "";
RDebugUtils.currentLine=2686991;
 //BA.debugLineNum = 2686991;BA.debugLine="Private last1 As Boolean";
_last1 = false;
RDebugUtils.currentLine=2686992;
 //BA.debugLineNum = 2686992;BA.debugLine="Dim method As String";
_method = "";
RDebugUtils.currentLine=2686993;
 //BA.debugLineNum = 2686993;BA.debugLine="Dim update_send As update_sender";
_update_send = new b4a.example.update_sender();
RDebugUtils.currentLine=2686994;
 //BA.debugLineNum = 2686994;BA.debugLine="End Sub";
return "";
}
public String  _initialize(b4a.example.sp_single __ref,anywheresoftware.b4a.BA _ba,String _url,String _methodname,Object _module,String _eventname) throws Exception{
__ref = this;
innerInitialize(_ba);
RDebugUtils.currentModule="sp_single";
if (Debug.shouldDelegate(ba, "initialize"))
	return (String) Debug.delegate(ba, "initialize", new Object[] {_ba,_url,_methodname,_module,_eventname});
RDebugUtils.currentLine=2883584;
 //BA.debugLineNum = 2883584;BA.debugLine="Public Sub initialize(Url As String,MethodName As";
RDebugUtils.currentLine=2883587;
 //BA.debugLineNum = 2883587;BA.debugLine="UrlWS        = Url";
__ref._urlws = _url;
RDebugUtils.currentLine=2883588;
 //BA.debugLineNum = 2883588;BA.debugLine="method=MethodName";
__ref._method = _methodname;
RDebugUtils.currentLine=2883589;
 //BA.debugLineNum = 2883589;BA.debugLine="EN           = eventname";
__ref._en = _eventname;
RDebugUtils.currentLine=2883590;
 //BA.debugLineNum = 2883590;BA.debugLine="Modules      = module";
__ref._modules = _module;
RDebugUtils.currentLine=2883593;
 //BA.debugLineNum = 2883593;BA.debugLine="xml_sendstriing =xml_sendstriing & \"<?xml version";
__ref._xml_sendstriing = __ref._xml_sendstriing+"<?xml version="+BA.ObjectToString(__c.Chr((int) (34)))+"1.0"+BA.ObjectToString(__c.Chr((int) (34)))+" encoding="+BA.ObjectToString(__c.Chr((int) (34)))+"utf-8"+BA.ObjectToString(__c.Chr((int) (34)))+"?>";
RDebugUtils.currentLine=2883594;
 //BA.debugLineNum = 2883594;BA.debugLine="xml_sendstriing= xml_sendstriing & \"<soap:Envelope";
__ref._xml_sendstriing = __ref._xml_sendstriing+"<soap:Envelope xmlns:xsi="+BA.ObjectToString(__c.Chr((int) (34)))+"http://www.w3.org/2001/XMLSchema-instance"+BA.ObjectToString(__c.Chr((int) (34)))+" xmlns:xsd="+BA.ObjectToString(__c.Chr((int) (34)))+"http://www.w3.org/2001/XMLSchema"+BA.ObjectToString(__c.Chr((int) (34)))+" xmlns:soap="+BA.ObjectToString(__c.Chr((int) (34)))+"http://schemas.xmlsoap.org/soap/envelope/"+BA.ObjectToString(__c.Chr((int) (34)))+">";
RDebugUtils.currentLine=2883595;
 //BA.debugLineNum = 2883595;BA.debugLine="xml_sendstriing= xml_sendstriing & \"<soap:Body>\"";
__ref._xml_sendstriing = __ref._xml_sendstriing+"<soap:Body>";
RDebugUtils.currentLine=2883596;
 //BA.debugLineNum = 2883596;BA.debugLine="xml_sendstriing= xml_sendstriing & \"<\" & Metho";
__ref._xml_sendstriing = __ref._xml_sendstriing+"<"+_methodname+" xmlns="+BA.ObjectToString(__c.Chr((int) (34)))+"http://tempuri.org/"+BA.ObjectToString(__c.Chr((int) (34)))+">";
RDebugUtils.currentLine=2883597;
 //BA.debugLineNum = 2883597;BA.debugLine="End Sub";
return "";
}
public String  _jobdone(b4a.example.sp_single __ref,anywheresoftware.b4a.samples.httputils2.httpjob _job) throws Exception{
__ref = this;
RDebugUtils.currentModule="sp_single";
if (Debug.shouldDelegate(ba, "jobdone"))
	return (String) Debug.delegate(ba, "jobdone", new Object[] {_job});
RDebugUtils.currentLine=3080192;
 //BA.debugLineNum = 3080192;BA.debugLine="Private Sub JobDone(Job As HttpJob)";
RDebugUtils.currentLine=3080194;
 //BA.debugLineNum = 3080194;BA.debugLine="parser.Initialize";
__ref._parser.Initialize(ba);
RDebugUtils.currentLine=3080196;
 //BA.debugLineNum = 3080196;BA.debugLine="If Job.Success Then";
if (_job._success) { 
RDebugUtils.currentLine=3080197;
 //BA.debugLineNum = 3080197;BA.debugLine="Select Case Job.JobName";
switch (BA.switchObjectToInt(_job._jobname,"logincheck","all","send_eingabe")) {
case 0: {
RDebugUtils.currentLine=3080199;
 //BA.debugLineNum = 3080199;BA.debugLine="parser.Parse(Job.GetInputStream,\"P_logincheck\"";
__ref._parser.Parse((java.io.InputStream)(_job._getinputstream().getObject()),"P_logincheck");
 break; }
case 1: {
RDebugUtils.currentLine=3080201;
 //BA.debugLineNum = 3080201;BA.debugLine="parser.Parse(Job.GetInputStream,\"P_all\")";
__ref._parser.Parse((java.io.InputStream)(_job._getinputstream().getObject()),"P_all");
RDebugUtils.currentLine=3080202;
 //BA.debugLineNum = 3080202;BA.debugLine="Main.aftersend=True";
_main._aftersend = __c.True;
RDebugUtils.currentLine=3080204;
 //BA.debugLineNum = 3080204;BA.debugLine="StartActivity(Main.ac_to_start)";
__c.StartActivity(ba,(Object)(_main._ac_to_start));
 break; }
case 2: {
RDebugUtils.currentLine=3080211;
 //BA.debugLineNum = 3080211;BA.debugLine="parser.Parse(Job.GetInputStream,\"P_eingabe\")";
__ref._parser.Parse((java.io.InputStream)(_job._getinputstream().getObject()),"P_eingabe");
RDebugUtils.currentLine=3080212;
 //BA.debugLineNum = 3080212;BA.debugLine="update_send.Initialize";
__ref._update_send._initialize(null,ba);
RDebugUtils.currentLine=3080213;
 //BA.debugLineNum = 3080213;BA.debugLine="update_send.send_quit";
__ref._update_send._send_quit(null);
RDebugUtils.currentLine=3080214;
 //BA.debugLineNum = 3080214;BA.debugLine="Main.aftersend=True";
_main._aftersend = __c.True;
RDebugUtils.currentLine=3080216;
 //BA.debugLineNum = 3080216;BA.debugLine="StartActivity(Main.ac_to_start)";
__c.StartActivity(ba,(Object)(_main._ac_to_start));
 break; }
}
;
 }else {
RDebugUtils.currentLine=3080221;
 //BA.debugLineNum = 3080221;BA.debugLine="Select Case Job.JobName";
switch (BA.switchObjectToInt(_job._jobname,"send_eingabe","logincheck")) {
case 0: {
RDebugUtils.currentLine=3080223;
 //BA.debugLineNum = 3080223;BA.debugLine="parser.Parse(Job.GetInputStream,\"P_eingabe\")";
__ref._parser.Parse((java.io.InputStream)(_job._getinputstream().getObject()),"P_eingabe");
RDebugUtils.currentLine=3080224;
 //BA.debugLineNum = 3080224;BA.debugLine="Main.aftersend=True";
_main._aftersend = __c.True;
 break; }
case 1: {
RDebugUtils.currentLine=3080227;
 //BA.debugLineNum = 3080227;BA.debugLine="If SubExists(Modules,EN) Then";
if (__c.SubExists(ba,__ref._modules,__ref._en)) { 
RDebugUtils.currentLine=3080228;
 //BA.debugLineNum = 3080228;BA.debugLine="CallSubDelayed2(Modules,EN,Job.ErrorMessage)";
__c.CallSubDelayed2(ba,__ref._modules,__ref._en,(Object)(_job._errormessage));
 };
RDebugUtils.currentLine=3080234;
 //BA.debugLineNum = 3080234;BA.debugLine="Try";
try { } 
       catch (Exception e28) {
			ba.setLastException(e28);RDebugUtils.currentLine=3080239;
 //BA.debugLineNum = 3080239;BA.debugLine="Log(LastException)";
__c.Log(BA.ObjectToString(__c.LastException(getActivityBA())));
 };
 break; }
}
;
 };
RDebugUtils.currentLine=3080250;
 //BA.debugLineNum = 3080250;BA.debugLine="End Sub";
return "";
}
public String  _p_eingabe_endelement(b4a.example.sp_single __ref,String _uri,String _name,anywheresoftware.b4a.keywords.StringBuilderWrapper _text) throws Exception{
__ref = this;
RDebugUtils.currentModule="sp_single";
if (Debug.shouldDelegate(ba, "p_eingabe_endelement"))
	return (String) Debug.delegate(ba, "p_eingabe_endelement", new Object[] {_uri,_name,_text});
String _node = "";
RDebugUtils.currentLine=2818048;
 //BA.debugLineNum = 2818048;BA.debugLine="Sub P_eingabe_EndElement (Uri As String, Name As S";
RDebugUtils.currentLine=2818049;
 //BA.debugLineNum = 2818049;BA.debugLine="Dim node As String= method & \"Response\"";
_node = __ref._method+"Response";
RDebugUtils.currentLine=2818050;
 //BA.debugLineNum = 2818050;BA.debugLine="If parser.Parents.IndexOf(node) > -1 Then";
if (__ref._parser.Parents.IndexOf((Object)(_node))>-1) { 
RDebugUtils.currentLine=2818051;
 //BA.debugLineNum = 2818051;BA.debugLine="node = method & \"Result\"";
_node = __ref._method+"Result";
RDebugUtils.currentLine=2818052;
 //BA.debugLineNum = 2818052;BA.debugLine="Select Case Name";
switch (BA.switchObjectToInt(_name,_node)) {
case 0: {
RDebugUtils.currentLine=2818054;
 //BA.debugLineNum = 2818054;BA.debugLine="Try";
try {RDebugUtils.currentLine=2818055;
 //BA.debugLineNum = 2818055;BA.debugLine="Main.sql1.ExecNonQuery(\"Update tbl_nachtrag_lo";
_main._sql1.ExecNonQuery("Update tbl_nachtrag_log_app set sync=1 where id ="+_text.ToString());
RDebugUtils.currentLine=2818056;
 //BA.debugLineNum = 2818056;BA.debugLine="Log(Main.sql1.ExecQuerySingleResult(\"Select cou";
__c.Log(_main._sql1.ExecQuerySingleResult("Select count(sync) from tbl_nachtrag_log_app where sync=1"));
 } 
       catch (Exception e10) {
			ba.setLastException(e10);RDebugUtils.currentLine=2818058;
 //BA.debugLineNum = 2818058;BA.debugLine="CallSubDelayed2(Modules,EN,Text.ToString)";
__c.CallSubDelayed2(ba,__ref._modules,__ref._en,(Object)(_text.ToString()));
RDebugUtils.currentLine=2818059;
 //BA.debugLineNum = 2818059;BA.debugLine="Log(LastException)";
__c.Log(BA.ObjectToString(__c.LastException(getActivityBA())));
 };
 break; }
}
;
 };
RDebugUtils.currentLine=2818068;
 //BA.debugLineNum = 2818068;BA.debugLine="End Sub";
return "";
}
public String  _p_logincheck_endelement(b4a.example.sp_single __ref,String _uri,String _name,anywheresoftware.b4a.keywords.StringBuilderWrapper _text) throws Exception{
__ref = this;
RDebugUtils.currentModule="sp_single";
if (Debug.shouldDelegate(ba, "p_logincheck_endelement"))
	return (String) Debug.delegate(ba, "p_logincheck_endelement", new Object[] {_uri,_name,_text});
RDebugUtils.currentLine=2752512;
 //BA.debugLineNum = 2752512;BA.debugLine="Sub P_logincheck_EndElement (Uri As String, Name A";
RDebugUtils.currentLine=2752513;
 //BA.debugLineNum = 2752513;BA.debugLine="If parser.Parents.IndexOf(\"logincheckResponse\")";
if (__ref._parser.Parents.IndexOf((Object)("logincheckResponse"))>-1) { 
RDebugUtils.currentLine=2752514;
 //BA.debugLineNum = 2752514;BA.debugLine="Select Case Name";
switch (BA.switchObjectToInt(_name,"logincheckResult")) {
case 0: {
RDebugUtils.currentLine=2752516;
 //BA.debugLineNum = 2752516;BA.debugLine="If SubExists(Modules,EN) Then";
if (__c.SubExists(ba,__ref._modules,__ref._en)) { 
RDebugUtils.currentLine=2752517;
 //BA.debugLineNum = 2752517;BA.debugLine="CallSubDelayed2(Modules,EN,Text.ToString)";
__c.CallSubDelayed2(ba,__ref._modules,__ref._en,(Object)(_text.ToString()));
 };
 break; }
}
;
 };
RDebugUtils.currentLine=2752521;
 //BA.debugLineNum = 2752521;BA.debugLine="End Sub";
return "";
}
public String  _sendrequest(b4a.example.sp_single __ref,String _methodname,String _jobname,boolean _last) throws Exception{
__ref = this;
RDebugUtils.currentModule="sp_single";
if (Debug.shouldDelegate(ba, "sendrequest"))
	return (String) Debug.delegate(ba, "sendrequest", new Object[] {_methodname,_jobname,_last});
anywheresoftware.b4a.samples.httputils2.httpjob _ht1 = null;
RDebugUtils.currentLine=3014656;
 //BA.debugLineNum = 3014656;BA.debugLine="Sub SendRequest( MethodName As String,jobname As S";
RDebugUtils.currentLine=3014658;
 //BA.debugLineNum = 3014658;BA.debugLine="last1=last";
__ref._last1 = _last;
RDebugUtils.currentLine=3014659;
 //BA.debugLineNum = 3014659;BA.debugLine="xml_sendstriing= xml_sendstriing & \"</\" & Meth";
__ref._xml_sendstriing = __ref._xml_sendstriing+"</"+_methodname+">";
RDebugUtils.currentLine=3014660;
 //BA.debugLineNum = 3014660;BA.debugLine="xml_sendstriing= xml_sendstriing & \"</soap:Body>";
__ref._xml_sendstriing = __ref._xml_sendstriing+"</soap:Body>";
RDebugUtils.currentLine=3014661;
 //BA.debugLineNum = 3014661;BA.debugLine="xml_sendstriing= xml_sendstriing & \"</soap:Envelop";
__ref._xml_sendstriing = __ref._xml_sendstriing+"</soap:Envelope>";
RDebugUtils.currentLine=3014662;
 //BA.debugLineNum = 3014662;BA.debugLine="Log(xml_sendstriing)";
__c.Log(__ref._xml_sendstriing);
RDebugUtils.currentLine=3014663;
 //BA.debugLineNum = 3014663;BA.debugLine="Log(\"string ist fertig\")";
__c.Log("string ist fertig");
RDebugUtils.currentLine=3014664;
 //BA.debugLineNum = 3014664;BA.debugLine="Dim ht1 As HttpJob";
_ht1 = new anywheresoftware.b4a.samples.httputils2.httpjob();
RDebugUtils.currentLine=3014665;
 //BA.debugLineNum = 3014665;BA.debugLine="ht1.Initialize(jobname,Me)";
_ht1._initialize(ba,_jobname,this);
RDebugUtils.currentLine=3014667;
 //BA.debugLineNum = 3014667;BA.debugLine="ht1.PostString(UrlWS,xml_sendstriing)";
_ht1._poststring(__ref._urlws,__ref._xml_sendstriing);
RDebugUtils.currentLine=3014670;
 //BA.debugLineNum = 3014670;BA.debugLine="ht1.GetRequest.SetHeader(\"SoapAction\", \"http://te";
_ht1._getrequest().SetHeader("SoapAction","http://tempuri.org/"+_methodname+"");
RDebugUtils.currentLine=3014673;
 //BA.debugLineNum = 3014673;BA.debugLine="ht1.GetRequest.SetContentType(\"text/xml; charset=";
_ht1._getrequest().SetContentType("text/xml; charset=utf-8");
RDebugUtils.currentLine=3014674;
 //BA.debugLineNum = 3014674;BA.debugLine="ht1.GetRequest.Timeout=2000";
_ht1._getrequest().setTimeout((int) (2000));
RDebugUtils.currentLine=3014676;
 //BA.debugLineNum = 3014676;BA.debugLine="End Sub";
return "";
}
}