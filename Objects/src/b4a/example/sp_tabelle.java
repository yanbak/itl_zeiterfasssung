package b4a.example;


import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.B4AClass;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.debug.*;

public class sp_tabelle extends B4AClass.ImplB4AClass implements BA.SubDelegator{
    private static java.util.HashMap<String, java.lang.reflect.Method> htSubs;
    private void innerInitialize(BA _ba) throws Exception {
        if (ba == null) {
            ba = new anywheresoftware.b4a.ShellBA(_ba, this, htSubs, "b4a.example.sp_tabelle");
            if (htSubs == null) {
                ba.loadHtSubs(this.getClass());
                htSubs = ba.htSubs;
            }
            
        }
        if (BA.isShellModeRuntimeCheck(ba)) 
			   this.getClass().getMethod("_class_globals", b4a.example.sp_tabelle.class).invoke(this, new Object[] {null});
        else
            ba.raiseEvent2(null, true, "class_globals", false);
    }

 
    public void  innerInitializeHelper(anywheresoftware.b4a.BA _ba) throws Exception{
        innerInitialize(_ba);
    }
    public Object callSub(String sub, Object sender, Object[] args) throws Exception {
        return BA.SubDelegator.SubNotFound;
    }
public anywheresoftware.b4a.keywords.Common __c = null;
public anywheresoftware.b4a.objects.SaxParser _parser = null;
public String _type_string = "";
public String _type_integer = "";
public String _type_float = "";
public String _type_date = "";
public String _type_double = "";
public String _type_binary = "";
public String _urlws = "";
public String[] _ar_auftrag = null;
public String[] _ar_auftrag_stoff = null;
public String[] _ar_stoffliste = null;
public String[] _ar_getnews = null;
public String[] _ar_erlaubbtetalons = null;
public String[] _ar_pbcodes = null;
public Object _modules = null;
public String _en = "";
public String _xml_sendstriing = "";
public double _version = 0;
public boolean _last = false;
public b4a.example.update_sender_main _u1 = null;
public anywheresoftware.b4a.samples.httputils2.httputils2service _httputils2service = null;
public b4a.example.main _main = null;
public b4a.example.starter _starter = null;
public b4a.example.scale _scale = null;
public b4a.example.setup _setup = null;
public b4a.example.ze_basis _ze_basis = null;
public String  _initialize(b4a.example.sp_tabelle __ref,anywheresoftware.b4a.BA _ba,String _url,String _methodname,Object _module,String _eventname) throws Exception{
__ref = this;
innerInitialize(_ba);
RDebugUtils.currentModule="sp_tabelle";
if (Debug.shouldDelegate(ba, "initialize"))
	return (String) Debug.delegate(ba, "initialize", new Object[] {_ba,_url,_methodname,_module,_eventname});
RDebugUtils.currentLine=3735552;
 //BA.debugLineNum = 3735552;BA.debugLine="Public Sub initialize(Url As String,MethodName As";
RDebugUtils.currentLine=3735555;
 //BA.debugLineNum = 3735555;BA.debugLine="UrlWS        = Url";
__ref._urlws = _url;
RDebugUtils.currentLine=3735557;
 //BA.debugLineNum = 3735557;BA.debugLine="EN           = eventname";
__ref._en = _eventname;
RDebugUtils.currentLine=3735558;
 //BA.debugLineNum = 3735558;BA.debugLine="Modules      = module";
__ref._modules = _module;
RDebugUtils.currentLine=3735561;
 //BA.debugLineNum = 3735561;BA.debugLine="xml_sendstriing =xml_sendstriing & \"<?xml version";
__ref._xml_sendstriing = __ref._xml_sendstriing+"<?xml version="+BA.ObjectToString(__c.Chr((int) (34)))+"1.0"+BA.ObjectToString(__c.Chr((int) (34)))+" encoding="+BA.ObjectToString(__c.Chr((int) (34)))+"utf-8"+BA.ObjectToString(__c.Chr((int) (34)))+"?>";
RDebugUtils.currentLine=3735562;
 //BA.debugLineNum = 3735562;BA.debugLine="xml_sendstriing= xml_sendstriing & \"<soap:Envelope";
__ref._xml_sendstriing = __ref._xml_sendstriing+"<soap:Envelope xmlns:xsi="+BA.ObjectToString(__c.Chr((int) (34)))+"http://www.w3.org/2001/XMLSchema-instance"+BA.ObjectToString(__c.Chr((int) (34)))+" xmlns:xsd="+BA.ObjectToString(__c.Chr((int) (34)))+"http://www.w3.org/2001/XMLSchema"+BA.ObjectToString(__c.Chr((int) (34)))+" xmlns:soap="+BA.ObjectToString(__c.Chr((int) (34)))+"http://schemas.xmlsoap.org/soap/envelope/"+BA.ObjectToString(__c.Chr((int) (34)))+">";
RDebugUtils.currentLine=3735563;
 //BA.debugLineNum = 3735563;BA.debugLine="xml_sendstriing= xml_sendstriing & \"<soap:Body>\"";
__ref._xml_sendstriing = __ref._xml_sendstriing+"<soap:Body>";
RDebugUtils.currentLine=3735564;
 //BA.debugLineNum = 3735564;BA.debugLine="xml_sendstriing= xml_sendstriing & \"<\" & Metho";
__ref._xml_sendstriing = __ref._xml_sendstriing+"<"+_methodname+" xmlns="+BA.ObjectToString(__c.Chr((int) (34)))+"http://tempuri.org/"+BA.ObjectToString(__c.Chr((int) (34)))+">";
RDebugUtils.currentLine=3735565;
 //BA.debugLineNum = 3735565;BA.debugLine="End Sub";
return "";
}
public String  _addfield(b4a.example.sp_tabelle __ref,String _fieldname,String _fieldtype,String _fieldvalue) throws Exception{
__ref = this;
RDebugUtils.currentModule="sp_tabelle";
if (Debug.shouldDelegate(ba, "addfield"))
	return (String) Debug.delegate(ba, "addfield", new Object[] {_fieldname,_fieldtype,_fieldvalue});
RDebugUtils.currentLine=3801088;
 //BA.debugLineNum = 3801088;BA.debugLine="Public Sub AddField(FieldName As String,FieldType";
RDebugUtils.currentLine=3801090;
 //BA.debugLineNum = 3801090;BA.debugLine="xml_sendstriing=xml_sendstriing &CRLF & $\"<${Fiel";
__ref._xml_sendstriing = __ref._xml_sendstriing+__c.CRLF+("<"+__c.SmartStringFormatter("",(Object)(_fieldname))+" >"+__c.SmartStringFormatter("",(Object)(_fieldvalue))+"</"+__c.SmartStringFormatter("",(Object)(_fieldname))+">");
RDebugUtils.currentLine=3801091;
 //BA.debugLineNum = 3801091;BA.debugLine="End Sub";
return "";
}
public String  _sendrequest(b4a.example.sp_tabelle __ref,String _methodname,String _jobname) throws Exception{
__ref = this;
RDebugUtils.currentModule="sp_tabelle";
if (Debug.shouldDelegate(ba, "sendrequest"))
	return (String) Debug.delegate(ba, "sendrequest", new Object[] {_methodname,_jobname});
anywheresoftware.b4a.samples.httputils2.httpjob _ht1 = null;
RDebugUtils.currentLine=3866624;
 //BA.debugLineNum = 3866624;BA.debugLine="Sub SendRequest( MethodName As String,jobname As S";
RDebugUtils.currentLine=3866628;
 //BA.debugLineNum = 3866628;BA.debugLine="xml_sendstriing= xml_sendstriing & \"</\" & Meth";
__ref._xml_sendstriing = __ref._xml_sendstriing+"</"+_methodname+">";
RDebugUtils.currentLine=3866629;
 //BA.debugLineNum = 3866629;BA.debugLine="xml_sendstriing= xml_sendstriing & \"</soap:Body>";
__ref._xml_sendstriing = __ref._xml_sendstriing+"</soap:Body>";
RDebugUtils.currentLine=3866630;
 //BA.debugLineNum = 3866630;BA.debugLine="xml_sendstriing= xml_sendstriing & \"</soap:Envelop";
__ref._xml_sendstriing = __ref._xml_sendstriing+"</soap:Envelope>";
RDebugUtils.currentLine=3866632;
 //BA.debugLineNum = 3866632;BA.debugLine="Dim ht1 As HttpJob";
_ht1 = new anywheresoftware.b4a.samples.httputils2.httpjob();
RDebugUtils.currentLine=3866633;
 //BA.debugLineNum = 3866633;BA.debugLine="ht1.Initialize(jobname,Me)";
_ht1._initialize(ba,_jobname,this);
RDebugUtils.currentLine=3866634;
 //BA.debugLineNum = 3866634;BA.debugLine="Log(\"go\")";
__c.Log("go");
RDebugUtils.currentLine=3866635;
 //BA.debugLineNum = 3866635;BA.debugLine="ht1.PostString(UrlWS,xml_sendstriing)";
_ht1._poststring(__ref._urlws,__ref._xml_sendstriing);
RDebugUtils.currentLine=3866638;
 //BA.debugLineNum = 3866638;BA.debugLine="ht1.GetRequest.SetHeader(\"SoapAction\", \"http://te";
_ht1._getrequest().SetHeader("SoapAction","http://tempuri.org/"+_methodname+"");
RDebugUtils.currentLine=3866641;
 //BA.debugLineNum = 3866641;BA.debugLine="ht1.GetRequest.SetContentType(\"text/xml; charset=";
_ht1._getrequest().SetContentType("text/xml; charset=utf-8");
RDebugUtils.currentLine=3866642;
 //BA.debugLineNum = 3866642;BA.debugLine="ht1.GetRequest.Timeout=2000";
_ht1._getrequest().setTimeout((int) (2000));
RDebugUtils.currentLine=3866644;
 //BA.debugLineNum = 3866644;BA.debugLine="End Sub";
return "";
}
public String  _class_globals(b4a.example.sp_tabelle __ref) throws Exception{
__ref = this;
RDebugUtils.currentModule="sp_tabelle";
RDebugUtils.currentLine=3473408;
 //BA.debugLineNum = 3473408;BA.debugLine="Private Sub Class_Globals";
RDebugUtils.currentLine=3473410;
 //BA.debugLineNum = 3473410;BA.debugLine="Dim parser As SaxParser";
_parser = new anywheresoftware.b4a.objects.SaxParser();
RDebugUtils.currentLine=3473411;
 //BA.debugLineNum = 3473411;BA.debugLine="Public TYPE_STRING   As String = \"xsd:string\"";
_type_string = "xsd:string";
RDebugUtils.currentLine=3473412;
 //BA.debugLineNum = 3473412;BA.debugLine="Public TYPE_INTEGER  As String = \"xsd:int\"";
_type_integer = "xsd:int";
RDebugUtils.currentLine=3473413;
 //BA.debugLineNum = 3473413;BA.debugLine="Public TYPE_FLOAT    As String = \"xsd:float\"";
_type_float = "xsd:float";
RDebugUtils.currentLine=3473414;
 //BA.debugLineNum = 3473414;BA.debugLine="Public TYPE_DATE     As String = \"xsd:date\"";
_type_date = "xsd:date";
RDebugUtils.currentLine=3473415;
 //BA.debugLineNum = 3473415;BA.debugLine="Public TYPE_DOUBLE   As String = \"xsd:double\"";
_type_double = "xsd:double";
RDebugUtils.currentLine=3473416;
 //BA.debugLineNum = 3473416;BA.debugLine="Public TYPE_BINARY   As String = \"xsd:base64Binar";
_type_binary = "xsd:base64Binary";
RDebugUtils.currentLine=3473418;
 //BA.debugLineNum = 3473418;BA.debugLine="Private UrlWS        As String";
_urlws = "";
RDebugUtils.currentLine=3473419;
 //BA.debugLineNum = 3473419;BA.debugLine="Dim ar_auftrag(13) As String";
_ar_auftrag = new String[(int) (13)];
java.util.Arrays.fill(_ar_auftrag,"");
RDebugUtils.currentLine=3473420;
 //BA.debugLineNum = 3473420;BA.debugLine="Dim ar_auftrag_stoff(4) As String";
_ar_auftrag_stoff = new String[(int) (4)];
java.util.Arrays.fill(_ar_auftrag_stoff,"");
RDebugUtils.currentLine=3473421;
 //BA.debugLineNum = 3473421;BA.debugLine="Dim ar_stoffliste(2) As String";
_ar_stoffliste = new String[(int) (2)];
java.util.Arrays.fill(_ar_stoffliste,"");
RDebugUtils.currentLine=3473422;
 //BA.debugLineNum = 3473422;BA.debugLine="Dim ar_getnews(4) As String";
_ar_getnews = new String[(int) (4)];
java.util.Arrays.fill(_ar_getnews,"");
RDebugUtils.currentLine=3473423;
 //BA.debugLineNum = 3473423;BA.debugLine="Dim ar_erlaubbtetalons(3) As String";
_ar_erlaubbtetalons = new String[(int) (3)];
java.util.Arrays.fill(_ar_erlaubbtetalons,"");
RDebugUtils.currentLine=3473424;
 //BA.debugLineNum = 3473424;BA.debugLine="Dim ar_pbcodes(2) As String";
_ar_pbcodes = new String[(int) (2)];
java.util.Arrays.fill(_ar_pbcodes,"");
RDebugUtils.currentLine=3473425;
 //BA.debugLineNum = 3473425;BA.debugLine="Private Modules      As Object";
_modules = new Object();
RDebugUtils.currentLine=3473426;
 //BA.debugLineNum = 3473426;BA.debugLine="Private EN           As String";
_en = "";
RDebugUtils.currentLine=3473427;
 //BA.debugLineNum = 3473427;BA.debugLine="Private xml_sendstriing As String";
_xml_sendstriing = "";
RDebugUtils.currentLine=3473428;
 //BA.debugLineNum = 3473428;BA.debugLine="Dim version As Double";
_version = 0;
RDebugUtils.currentLine=3473429;
 //BA.debugLineNum = 3473429;BA.debugLine="Public last As Boolean";
_last = false;
RDebugUtils.currentLine=3473430;
 //BA.debugLineNum = 3473430;BA.debugLine="Dim u1 As update_sender_main";
_u1 = new b4a.example.update_sender_main();
RDebugUtils.currentLine=3473431;
 //BA.debugLineNum = 3473431;BA.debugLine="End Sub";
return "";
}
public String  _jobdone(b4a.example.sp_tabelle __ref,anywheresoftware.b4a.samples.httputils2.httpjob _job) throws Exception{
__ref = this;
RDebugUtils.currentModule="sp_tabelle";
if (Debug.shouldDelegate(ba, "jobdone"))
	return (String) Debug.delegate(ba, "jobdone", new Object[] {_job});
anywheresoftware.b4a.objects.streams.File.OutputStreamWrapper _out3 = null;
anywheresoftware.b4a.objects.IntentWrapper _i = null;
anywheresoftware.b4a.phone.PackageManagerWrapper _pm = null;
int _res = 0;
anywheresoftware.b4a.samples.httputils2.httpjob _jobapk = null;
String _verstring = "";
anywheresoftware.b4a.keywords.StringBuilderWrapper _sb = null;
RDebugUtils.currentLine=3932160;
 //BA.debugLineNum = 3932160;BA.debugLine="Private Sub JobDone(Job As HttpJob)";
RDebugUtils.currentLine=3932162;
 //BA.debugLineNum = 3932162;BA.debugLine="parser.Initialize";
__ref._parser.Initialize(ba);
RDebugUtils.currentLine=3932164;
 //BA.debugLineNum = 3932164;BA.debugLine="If Job.Success Then";
if (_job._success) { 
RDebugUtils.currentLine=3932165;
 //BA.debugLineNum = 3932165;BA.debugLine="Select Case Job.JobName";
switch (BA.switchObjectToInt(_job._jobname,"JobApkDownload","get_version","get_news","get_erlaubtetalons","get_pbcodes")) {
case 0: {
RDebugUtils.currentLine=3932168;
 //BA.debugLineNum = 3932168;BA.debugLine="Dim out3 As OutputStream = File.OpenOutput(File";
_out3 = new anywheresoftware.b4a.objects.streams.File.OutputStreamWrapper();
_out3 = __c.File.OpenOutput(__c.File.getDirDefaultExternal(),"ISP_KML.apk",__c.False);
RDebugUtils.currentLine=3932169;
 //BA.debugLineNum = 3932169;BA.debugLine="File.Copy2(Job.GetInputStream, out3)";
__c.File.Copy2((java.io.InputStream)(_job._getinputstream().getObject()),(java.io.OutputStream)(_out3.getObject()));
RDebugUtils.currentLine=3932170;
 //BA.debugLineNum = 3932170;BA.debugLine="out3.Close";
_out3.Close();
RDebugUtils.currentLine=3932171;
 //BA.debugLineNum = 3932171;BA.debugLine="Dim i As Intent";
_i = new anywheresoftware.b4a.objects.IntentWrapper();
RDebugUtils.currentLine=3932172;
 //BA.debugLineNum = 3932172;BA.debugLine="i.Initialize(i.ACTION_VIEW, \"file://\" & File.";
_i.Initialize(_i.ACTION_VIEW,"file://"+__c.File.Combine(__c.File.getDirDefaultExternal(),"ISP_KML.apk"));
RDebugUtils.currentLine=3932173;
 //BA.debugLineNum = 3932173;BA.debugLine="i.SetType(\"application/vnd.android.package-ar";
_i.SetType("application/vnd.android.package-archive");
RDebugUtils.currentLine=3932174;
 //BA.debugLineNum = 3932174;BA.debugLine="StartActivity(i)";
__c.StartActivity(ba,(Object)(_i.getObject()));
 break; }
case 1: {
RDebugUtils.currentLine=3932177;
 //BA.debugLineNum = 3932177;BA.debugLine="Try";
try {RDebugUtils.currentLine=3932180;
 //BA.debugLineNum = 3932180;BA.debugLine="parser.Parse(Job.GetInputStream,\"P_get_version\"";
__ref._parser.Parse((java.io.InputStream)(_job._getinputstream().getObject()),"P_get_version");
RDebugUtils.currentLine=3932182;
 //BA.debugLineNum = 3932182;BA.debugLine="Log(version)";
__c.Log(BA.NumberToString(__ref._version));
RDebugUtils.currentLine=3932183;
 //BA.debugLineNum = 3932183;BA.debugLine="Dim pm As PackageManager";
_pm = new anywheresoftware.b4a.phone.PackageManagerWrapper();
RDebugUtils.currentLine=3932184;
 //BA.debugLineNum = 3932184;BA.debugLine="If pm.GetVersionName(\"com.interseroh.kml\") < ve";
if ((double)(Double.parseDouble(_pm.GetVersionName("com.interseroh.kml")))<__ref._version) { 
RDebugUtils.currentLine=3932185;
 //BA.debugLineNum = 3932185;BA.debugLine="Dim res As Int";
_res = 0;
RDebugUtils.currentLine=3932186;
 //BA.debugLineNum = 3932186;BA.debugLine="res=Msgbox2(\"Update verfügbar! Möchten sie inst";
_res = __c.Msgbox2(BA.ObjectToCharSequence("Update verfügbar! Möchten sie installieren?"),BA.ObjectToCharSequence("Update"),"Ja","Nein","",(android.graphics.Bitmap)(__c.Null),getActivityBA());
RDebugUtils.currentLine=3932187;
 //BA.debugLineNum = 3932187;BA.debugLine="If res = DialogResponse.POSITIVE Then";
if (_res==__c.DialogResponse.POSITIVE) { 
RDebugUtils.currentLine=3932188;
 //BA.debugLineNum = 3932188;BA.debugLine="Dim jobapk As HttpJob";
_jobapk = new anywheresoftware.b4a.samples.httputils2.httpjob();
RDebugUtils.currentLine=3932189;
 //BA.debugLineNum = 3932189;BA.debugLine="jobapk.Initialize(\"JobApkDownload\", Me)";
_jobapk._initialize(ba,"JobApkDownload",this);
RDebugUtils.currentLine=3932192;
 //BA.debugLineNum = 3932192;BA.debugLine="Dim verstring As String";
_verstring = "";
RDebugUtils.currentLine=3932193;
 //BA.debugLineNum = 3932193;BA.debugLine="verstring=version";
_verstring = BA.NumberToString(__ref._version);
RDebugUtils.currentLine=3932194;
 //BA.debugLineNum = 3932194;BA.debugLine="jobapk.Download(\"http://www.interseroh-pfand.";
_jobapk._download("http://www.interseroh-pfand.com/appupdate/ISP_KML.apk?Version="+_verstring.replace(".",""));
 };
 };
 } 
       catch (Exception e29) {
			ba.setLastException(e29);RDebugUtils.currentLine=3932200;
 //BA.debugLineNum = 3932200;BA.debugLine="Log(LastException)";
__c.Log(BA.ObjectToString(__c.LastException(getActivityBA())));
 };
 break; }
case 2: {
RDebugUtils.currentLine=3932205;
 //BA.debugLineNum = 3932205;BA.debugLine="Main.sql1.ExecNonQuery(\"Delete from tmp_news";
_main._sql1.ExecNonQuery("Delete from tmp_news");
RDebugUtils.currentLine=3932206;
 //BA.debugLineNum = 3932206;BA.debugLine="parser.Parse(Job.GetInputStream,\"P_getnews\")";
__ref._parser.Parse((java.io.InputStream)(_job._getinputstream().getObject()),"P_getnews");
RDebugUtils.currentLine=3932207;
 //BA.debugLineNum = 3932207;BA.debugLine="Log(Main.sql1.ExecQuerySingleResult(\"Select c";
__c.Log(_main._sql1.ExecQuerySingleResult("Select count(infoid) from tmp_news"));
RDebugUtils.currentLine=3932208;
 //BA.debugLineNum = 3932208;BA.debugLine="Main.sql1.ExecNonQuery(\"insert into tbl_news";
_main._sql1.ExecNonQuery("insert into tbl_news (infotext,Absender,timestamp, infoID) select tmp_news.infotext,tmp_news.absender,tmp_news.infoid,tmp_news.infoid from tmp_news "+" left outer join tbl_news on tmp_news.InfoID = tbl_news.InfoID where tbl_news.infoid Is null");
RDebugUtils.currentLine=3932210;
 //BA.debugLineNum = 3932210;BA.debugLine="Log(Main.sql1.ExecQuerySingleResult(\"Select count";
__c.Log(_main._sql1.ExecQuerySingleResult("Select count(infoid) from tbl_news"));
 break; }
case 3: {
RDebugUtils.currentLine=3932212;
 //BA.debugLineNum = 3932212;BA.debugLine="Main.sql1.ExecNonQuery(\"Delete from tmp_erlau";
_main._sql1.ExecNonQuery("Delete from tmp_erlaubte_codes");
RDebugUtils.currentLine=3932213;
 //BA.debugLineNum = 3932213;BA.debugLine="parser.Parse(Job.GetInputStream,\"P_erlaubteta";
__ref._parser.Parse((java.io.InputStream)(_job._getinputstream().getObject()),"P_erlaubtetalons");
RDebugUtils.currentLine=3932215;
 //BA.debugLineNum = 3932215;BA.debugLine="Dim sb As StringBuilder";
_sb = new anywheresoftware.b4a.keywords.StringBuilderWrapper();
RDebugUtils.currentLine=3932216;
 //BA.debugLineNum = 3932216;BA.debugLine="sb.Initialize";
_sb.Initialize();
RDebugUtils.currentLine=3932217;
 //BA.debugLineNum = 3932217;BA.debugLine="sb.Append(\"insert into tbl_erlaubte_codes (co";
_sb.Append("insert into tbl_erlaubte_codes (codestart,len,art) Select ");
RDebugUtils.currentLine=3932218;
 //BA.debugLineNum = 3932218;BA.debugLine="sb.Append(\" tmp_erlaubte_codes.codestart,tmp_erlau";
_sb.Append(" tmp_erlaubte_codes.codestart,tmp_erlaubte_codes.len,tmp_erlaubte_codes.art");
RDebugUtils.currentLine=3932219;
 //BA.debugLineNum = 3932219;BA.debugLine="sb.Append(\" from tmp_erlaubte_codes left outer joi";
_sb.Append(" from tmp_erlaubte_codes left outer join tbl_erlaubte_codes on tmp_erlaubte_codes.codestart=");
RDebugUtils.currentLine=3932220;
 //BA.debugLineNum = 3932220;BA.debugLine="sb.Append(\" tbl_erlaubte_codes.codestart And tmp_e";
_sb.Append(" tbl_erlaubte_codes.codestart And tmp_erlaubte_codes.art=tbl_erlaubte_codes.art");
RDebugUtils.currentLine=3932221;
 //BA.debugLineNum = 3932221;BA.debugLine="sb.Append(\" where tbl_erlaubte_codes.codestart Is";
_sb.Append(" where tbl_erlaubte_codes.codestart Is null");
RDebugUtils.currentLine=3932223;
 //BA.debugLineNum = 3932223;BA.debugLine="Main.sql1.ExecnonQuery(sb.ToString)";
_main._sql1.ExecNonQuery(_sb.ToString());
 break; }
case 4: {
RDebugUtils.currentLine=3932226;
 //BA.debugLineNum = 3932226;BA.debugLine="Main.sql1.ExecNonQuery(\"Delete from tbl_scan_t";
_main._sql1.ExecNonQuery("Delete from tbl_scan_temp");
RDebugUtils.currentLine=3932227;
 //BA.debugLineNum = 3932227;BA.debugLine="parser.Parse(Job.GetInputStream,\"P_pbscan\")";
__ref._parser.Parse((java.io.InputStream)(_job._getinputstream().getObject()),"P_pbscan");
 break; }
}
;
 }else {
 };
RDebugUtils.currentLine=3932234;
 //BA.debugLineNum = 3932234;BA.debugLine="ProgressDialogHide";
__c.ProgressDialogHide();
RDebugUtils.currentLine=3932236;
 //BA.debugLineNum = 3932236;BA.debugLine="End Sub";
return "";
}
public String  _p_get_version_endelement(b4a.example.sp_tabelle __ref,String _uri,String _name,anywheresoftware.b4a.keywords.StringBuilderWrapper _text) throws Exception{
__ref = this;
RDebugUtils.currentModule="sp_tabelle";
if (Debug.shouldDelegate(ba, "p_get_version_endelement"))
	return (String) Debug.delegate(ba, "p_get_version_endelement", new Object[] {_uri,_name,_text});
RDebugUtils.currentLine=3538944;
 //BA.debugLineNum = 3538944;BA.debugLine="Sub P_get_version_EndElement(Uri As String, Name A";
RDebugUtils.currentLine=3538947;
 //BA.debugLineNum = 3538947;BA.debugLine="Select Case Name";
switch (BA.switchObjectToInt(_name,"get_versionResult")) {
case 0: {
RDebugUtils.currentLine=3538949;
 //BA.debugLineNum = 3538949;BA.debugLine="version=Text.ToString";
__ref._version = (double)(Double.parseDouble(_text.ToString()));
 break; }
}
;
RDebugUtils.currentLine=3538955;
 //BA.debugLineNum = 3538955;BA.debugLine="End Sub";
return "";
}
public String  _p_getnews_endelement(b4a.example.sp_tabelle __ref,String _uri,String _name,anywheresoftware.b4a.keywords.StringBuilderWrapper _text) throws Exception{
__ref = this;
RDebugUtils.currentModule="sp_tabelle";
if (Debug.shouldDelegate(ba, "p_getnews_endelement"))
	return (String) Debug.delegate(ba, "p_getnews_endelement", new Object[] {_uri,_name,_text});
int _i = 0;
RDebugUtils.currentLine=3604480;
 //BA.debugLineNum = 3604480;BA.debugLine="Sub P_getnews_EndElement(Uri As String, Name As St";
RDebugUtils.currentLine=3604482;
 //BA.debugLineNum = 3604482;BA.debugLine="If parser.Parents.IndexOf(\"Table1\") > -1 Then";
if (__ref._parser.Parents.IndexOf((Object)("Table1"))>-1) { 
RDebugUtils.currentLine=3604483;
 //BA.debugLineNum = 3604483;BA.debugLine="Select Case Name";
switch (BA.switchObjectToInt(_name,"Infotext","Absender","timestamp","id")) {
case 0: {
RDebugUtils.currentLine=3604485;
 //BA.debugLineNum = 3604485;BA.debugLine="For i = 0 To 1";
{
final int step4 = 1;
final int limit4 = (int) (1);
_i = (int) (0) ;
for (;(step4 > 0 && _i <= limit4) || (step4 < 0 && _i >= limit4) ;_i = ((int)(0 + _i + step4))  ) {
RDebugUtils.currentLine=3604486;
 //BA.debugLineNum = 3604486;BA.debugLine="ar_getnews(i)=\"\"";
__ref._ar_getnews[_i] = "";
 }
};
RDebugUtils.currentLine=3604488;
 //BA.debugLineNum = 3604488;BA.debugLine="ar_getnews(0)=Text.ToString";
__ref._ar_getnews[(int) (0)] = _text.ToString();
 break; }
case 1: {
RDebugUtils.currentLine=3604490;
 //BA.debugLineNum = 3604490;BA.debugLine="ar_getnews(1)=Text.ToString";
__ref._ar_getnews[(int) (1)] = _text.ToString();
 break; }
case 2: {
RDebugUtils.currentLine=3604492;
 //BA.debugLineNum = 3604492;BA.debugLine="ar_getnews(2)=Main.sf.Left(Text.ToString.Replace";
__ref._ar_getnews[(int) (2)] = _main._sf._vvv6(_text.ToString().replace("T"," "),(long) (16));
 break; }
case 3: {
RDebugUtils.currentLine=3604494;
 //BA.debugLineNum = 3604494;BA.debugLine="ar_getnews(3)=Text.ToString";
__ref._ar_getnews[(int) (3)] = _text.ToString();
RDebugUtils.currentLine=3604495;
 //BA.debugLineNum = 3604495;BA.debugLine="Main.sql1.ExecNonQuery2(\"Insert into tmp_news";
_main._sql1.ExecNonQuery2("Insert into tmp_news (Infotext,Absender,timestamp,Infoid)"+" values(?,?,?,?)",anywheresoftware.b4a.keywords.Common.ArrayToList(__ref._ar_getnews));
RDebugUtils.currentLine=3604497;
 //BA.debugLineNum = 3604497;BA.debugLine="Log(ar_getnews(1) &\"/\" & ar_getnews(2))";
__c.Log(__ref._ar_getnews[(int) (1)]+"/"+__ref._ar_getnews[(int) (2)]);
RDebugUtils.currentLine=3604498;
 //BA.debugLineNum = 3604498;BA.debugLine="For i = 0 To 3";
{
final int step16 = 1;
final int limit16 = (int) (3);
_i = (int) (0) ;
for (;(step16 > 0 && _i <= limit16) || (step16 < 0 && _i >= limit16) ;_i = ((int)(0 + _i + step16))  ) {
RDebugUtils.currentLine=3604499;
 //BA.debugLineNum = 3604499;BA.debugLine="ar_getnews(i)=\"\"";
__ref._ar_getnews[_i] = "";
 }
};
 break; }
}
;
 };
RDebugUtils.currentLine=3604506;
 //BA.debugLineNum = 3604506;BA.debugLine="End Sub";
return "";
}
public String  _parser_startelement(b4a.example.sp_tabelle __ref,String _uri,String _name,anywheresoftware.b4a.objects.SaxParser.AttributesWrapper _attributes) throws Exception{
__ref = this;
RDebugUtils.currentModule="sp_tabelle";
if (Debug.shouldDelegate(ba, "parser_startelement"))
	return (String) Debug.delegate(ba, "parser_startelement", new Object[] {_uri,_name,_attributes});
RDebugUtils.currentLine=3670016;
 //BA.debugLineNum = 3670016;BA.debugLine="Sub Parser_StartElement (Uri As String, Name As St";
RDebugUtils.currentLine=3670018;
 //BA.debugLineNum = 3670018;BA.debugLine="End Sub";
return "";
}
}