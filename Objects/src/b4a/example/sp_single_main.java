package b4a.example;


import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.B4AClass;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.debug.*;

public class sp_single_main extends B4AClass.ImplB4AClass implements BA.SubDelegator{
    private static java.util.HashMap<String, java.lang.reflect.Method> htSubs;
    private void innerInitialize(BA _ba) throws Exception {
        if (ba == null) {
            ba = new anywheresoftware.b4a.ShellBA(_ba, this, htSubs, "b4a.example.sp_single_main");
            if (htSubs == null) {
                ba.loadHtSubs(this.getClass());
                htSubs = ba.htSubs;
            }
            
        }
        if (BA.isShellModeRuntimeCheck(ba)) 
			   this.getClass().getMethod("_class_globals", b4a.example.sp_single_main.class).invoke(this, new Object[] {null});
        else
            ba.raiseEvent2(null, true, "class_globals", false);
    }

 
    public void  innerInitializeHelper(anywheresoftware.b4a.BA _ba) throws Exception{
        innerInitialize(_ba);
    }
    public Object callSub(String sub, Object sender, Object[] args) throws Exception {
        return BA.SubDelegator.SubNotFound;
    }
public anywheresoftware.b4a.keywords.Common __c = null;
public anywheresoftware.b4a.objects.SaxParser _parser = null;
public String _type_string = "";
public String _type_integer = "";
public String _type_float = "";
public String _type_date = "";
public String _type_double = "";
public String _type_binary = "";
public String _urlws = "";
public Object _modules = null;
public String _en = "";
public String _xml_sendstriing = "";
public boolean _last1 = false;
public String _method = "";
public anywheresoftware.b4a.samples.httputils2.httputils2service _httputils2service = null;
public b4a.example.main _main = null;
public b4a.example.starter _starter = null;
public b4a.example.scale _scale = null;
public b4a.example.setup _setup = null;
public b4a.example.ze_basis _ze_basis = null;
public String  _initialize(b4a.example.sp_single_main __ref,anywheresoftware.b4a.BA _ba,String _url,String _methodname,Object _module,String _eventname) throws Exception{
__ref = this;
innerInitialize(_ba);
RDebugUtils.currentModule="sp_single_main";
if (Debug.shouldDelegate(ba, "initialize"))
	return (String) Debug.delegate(ba, "initialize", new Object[] {_ba,_url,_methodname,_module,_eventname});
RDebugUtils.currentLine=6684672;
 //BA.debugLineNum = 6684672;BA.debugLine="Public Sub initialize(Url As String,MethodName As";
RDebugUtils.currentLine=6684675;
 //BA.debugLineNum = 6684675;BA.debugLine="UrlWS        = Url";
__ref._urlws = _url;
RDebugUtils.currentLine=6684676;
 //BA.debugLineNum = 6684676;BA.debugLine="method=MethodName";
__ref._method = _methodname;
RDebugUtils.currentLine=6684677;
 //BA.debugLineNum = 6684677;BA.debugLine="EN           = eventname";
__ref._en = _eventname;
RDebugUtils.currentLine=6684678;
 //BA.debugLineNum = 6684678;BA.debugLine="Modules      = module";
__ref._modules = _module;
RDebugUtils.currentLine=6684681;
 //BA.debugLineNum = 6684681;BA.debugLine="xml_sendstriing =xml_sendstriing & \"<?xml version";
__ref._xml_sendstriing = __ref._xml_sendstriing+"<?xml version="+BA.ObjectToString(__c.Chr((int) (34)))+"1.0"+BA.ObjectToString(__c.Chr((int) (34)))+" encoding="+BA.ObjectToString(__c.Chr((int) (34)))+"utf-8"+BA.ObjectToString(__c.Chr((int) (34)))+"?>";
RDebugUtils.currentLine=6684682;
 //BA.debugLineNum = 6684682;BA.debugLine="xml_sendstriing= xml_sendstriing & \"<soap:Envelope";
__ref._xml_sendstriing = __ref._xml_sendstriing+"<soap:Envelope xmlns:xsi="+BA.ObjectToString(__c.Chr((int) (34)))+"http://www.w3.org/2001/XMLSchema-instance"+BA.ObjectToString(__c.Chr((int) (34)))+" xmlns:xsd="+BA.ObjectToString(__c.Chr((int) (34)))+"http://www.w3.org/2001/XMLSchema"+BA.ObjectToString(__c.Chr((int) (34)))+" xmlns:soap="+BA.ObjectToString(__c.Chr((int) (34)))+"http://schemas.xmlsoap.org/soap/envelope/"+BA.ObjectToString(__c.Chr((int) (34)))+">";
RDebugUtils.currentLine=6684683;
 //BA.debugLineNum = 6684683;BA.debugLine="xml_sendstriing= xml_sendstriing & \"<soap:Body>\"";
__ref._xml_sendstriing = __ref._xml_sendstriing+"<soap:Body>";
RDebugUtils.currentLine=6684684;
 //BA.debugLineNum = 6684684;BA.debugLine="xml_sendstriing= xml_sendstriing & \"<\" & Metho";
__ref._xml_sendstriing = __ref._xml_sendstriing+"<"+_methodname+" xmlns="+BA.ObjectToString(__c.Chr((int) (34)))+"http://tempuri.org/"+BA.ObjectToString(__c.Chr((int) (34)))+">";
RDebugUtils.currentLine=6684685;
 //BA.debugLineNum = 6684685;BA.debugLine="End Sub";
return "";
}
public String  _addfield(b4a.example.sp_single_main __ref,String _fieldname,String _fieldtype,String _fieldvalue) throws Exception{
__ref = this;
RDebugUtils.currentModule="sp_single_main";
if (Debug.shouldDelegate(ba, "addfield"))
	return (String) Debug.delegate(ba, "addfield", new Object[] {_fieldname,_fieldtype,_fieldvalue});
RDebugUtils.currentLine=6750208;
 //BA.debugLineNum = 6750208;BA.debugLine="Public Sub AddField(FieldName As String,FieldType";
RDebugUtils.currentLine=6750210;
 //BA.debugLineNum = 6750210;BA.debugLine="xml_sendstriing=xml_sendstriing &CRLF & $\"<${Fiel";
__ref._xml_sendstriing = __ref._xml_sendstriing+__c.CRLF+("<"+__c.SmartStringFormatter("",(Object)(_fieldname))+" >"+__c.SmartStringFormatter("",(Object)(_fieldvalue))+"</"+__c.SmartStringFormatter("",(Object)(_fieldname))+">");
RDebugUtils.currentLine=6750211;
 //BA.debugLineNum = 6750211;BA.debugLine="End Sub";
return "";
}
public String  _sendrequest(b4a.example.sp_single_main __ref,String _methodname,String _jobname,boolean _last) throws Exception{
__ref = this;
RDebugUtils.currentModule="sp_single_main";
if (Debug.shouldDelegate(ba, "sendrequest"))
	return (String) Debug.delegate(ba, "sendrequest", new Object[] {_methodname,_jobname,_last});
anywheresoftware.b4a.samples.httputils2.httpjob _ht1 = null;
RDebugUtils.currentLine=6815744;
 //BA.debugLineNum = 6815744;BA.debugLine="Sub SendRequest( MethodName As String,jobname As S";
RDebugUtils.currentLine=6815746;
 //BA.debugLineNum = 6815746;BA.debugLine="last1=last";
__ref._last1 = _last;
RDebugUtils.currentLine=6815747;
 //BA.debugLineNum = 6815747;BA.debugLine="xml_sendstriing= xml_sendstriing & \"</\" & Meth";
__ref._xml_sendstriing = __ref._xml_sendstriing+"</"+_methodname+">";
RDebugUtils.currentLine=6815748;
 //BA.debugLineNum = 6815748;BA.debugLine="xml_sendstriing= xml_sendstriing & \"</soap:Body>";
__ref._xml_sendstriing = __ref._xml_sendstriing+"</soap:Body>";
RDebugUtils.currentLine=6815749;
 //BA.debugLineNum = 6815749;BA.debugLine="xml_sendstriing= xml_sendstriing & \"</soap:Envelop";
__ref._xml_sendstriing = __ref._xml_sendstriing+"</soap:Envelope>";
RDebugUtils.currentLine=6815750;
 //BA.debugLineNum = 6815750;BA.debugLine="Log(xml_sendstriing)";
__c.Log(__ref._xml_sendstriing);
RDebugUtils.currentLine=6815751;
 //BA.debugLineNum = 6815751;BA.debugLine="Log(\"string ist fertig\")";
__c.Log("string ist fertig");
RDebugUtils.currentLine=6815752;
 //BA.debugLineNum = 6815752;BA.debugLine="Dim ht1 As HttpJob";
_ht1 = new anywheresoftware.b4a.samples.httputils2.httpjob();
RDebugUtils.currentLine=6815753;
 //BA.debugLineNum = 6815753;BA.debugLine="ht1.Initialize(jobname,Me)";
_ht1._initialize(ba,_jobname,this);
RDebugUtils.currentLine=6815755;
 //BA.debugLineNum = 6815755;BA.debugLine="ht1.PostString(UrlWS,xml_sendstriing)";
_ht1._poststring(__ref._urlws,__ref._xml_sendstriing);
RDebugUtils.currentLine=6815758;
 //BA.debugLineNum = 6815758;BA.debugLine="ht1.GetRequest.SetHeader(\"SoapAction\", \"http://te";
_ht1._getrequest().SetHeader("SoapAction","http://tempuri.org/"+_methodname+"");
RDebugUtils.currentLine=6815761;
 //BA.debugLineNum = 6815761;BA.debugLine="ht1.GetRequest.SetContentType(\"text/xml; charset=";
_ht1._getrequest().SetContentType("text/xml; charset=utf-8");
RDebugUtils.currentLine=6815762;
 //BA.debugLineNum = 6815762;BA.debugLine="ht1.GetRequest.Timeout=2000";
_ht1._getrequest().setTimeout((int) (2000));
RDebugUtils.currentLine=6815764;
 //BA.debugLineNum = 6815764;BA.debugLine="End Sub";
return "";
}
public String  _class_globals(b4a.example.sp_single_main __ref) throws Exception{
__ref = this;
RDebugUtils.currentModule="sp_single_main";
RDebugUtils.currentLine=6291456;
 //BA.debugLineNum = 6291456;BA.debugLine="Sub Class_Globals";
RDebugUtils.currentLine=6291458;
 //BA.debugLineNum = 6291458;BA.debugLine="Dim parser As SaxParser";
_parser = new anywheresoftware.b4a.objects.SaxParser();
RDebugUtils.currentLine=6291459;
 //BA.debugLineNum = 6291459;BA.debugLine="Public TYPE_STRING   As String = \"xsd:string\"";
_type_string = "xsd:string";
RDebugUtils.currentLine=6291460;
 //BA.debugLineNum = 6291460;BA.debugLine="Public TYPE_INTEGER  As String = \"xsd:int\"";
_type_integer = "xsd:int";
RDebugUtils.currentLine=6291461;
 //BA.debugLineNum = 6291461;BA.debugLine="Public TYPE_FLOAT    As String = \"xsd:float\"";
_type_float = "xsd:float";
RDebugUtils.currentLine=6291462;
 //BA.debugLineNum = 6291462;BA.debugLine="Public TYPE_DATE     As String = \"xsd:date\"";
_type_date = "xsd:date";
RDebugUtils.currentLine=6291463;
 //BA.debugLineNum = 6291463;BA.debugLine="Public TYPE_DOUBLE   As String = \"xsd:double\"";
_type_double = "xsd:double";
RDebugUtils.currentLine=6291464;
 //BA.debugLineNum = 6291464;BA.debugLine="Public TYPE_BINARY   As String = \"xsd:base64Binar";
_type_binary = "xsd:base64Binary";
RDebugUtils.currentLine=6291466;
 //BA.debugLineNum = 6291466;BA.debugLine="Private UrlWS        As String";
_urlws = "";
RDebugUtils.currentLine=6291468;
 //BA.debugLineNum = 6291468;BA.debugLine="Private Modules      As Object";
_modules = new Object();
RDebugUtils.currentLine=6291469;
 //BA.debugLineNum = 6291469;BA.debugLine="Private EN           As String";
_en = "";
RDebugUtils.currentLine=6291470;
 //BA.debugLineNum = 6291470;BA.debugLine="Private xml_sendstriing As String";
_xml_sendstriing = "";
RDebugUtils.currentLine=6291471;
 //BA.debugLineNum = 6291471;BA.debugLine="Private last1 As Boolean";
_last1 = false;
RDebugUtils.currentLine=6291472;
 //BA.debugLineNum = 6291472;BA.debugLine="Dim method As String";
_method = "";
RDebugUtils.currentLine=6291473;
 //BA.debugLineNum = 6291473;BA.debugLine="End Sub";
return "";
}
public String  _imageupload(b4a.example.sp_single_main __ref,String _img,Object _meth) throws Exception{
__ref = this;
RDebugUtils.currentModule="sp_single_main";
if (Debug.shouldDelegate(ba, "imageupload"))
	return (String) Debug.delegate(ba, "imageupload", new Object[] {_img,_meth});
simplysoftware.base64image.base64image _base64con = null;
String _b64str = "";
anywheresoftware.b4a.samples.httputils2.httpjob _job1 = null;
String _xml = "";
RDebugUtils.currentLine=6946816;
 //BA.debugLineNum = 6946816;BA.debugLine="Sub imageupload( img As String,meth As Object)";
RDebugUtils.currentLine=6946818;
 //BA.debugLineNum = 6946818;BA.debugLine="Private Base64Con As Base64Image";
_base64con = new simplysoftware.base64image.base64image();
RDebugUtils.currentLine=6946819;
 //BA.debugLineNum = 6946819;BA.debugLine="Base64Con.Initialize";
_base64con._initialize(ba);
RDebugUtils.currentLine=6946822;
 //BA.debugLineNum = 6946822;BA.debugLine="Private B64Str As String = Base64Con.EncodeFro";
_b64str = _base64con._encodefromimage(__c.File.getDirInternal(),_img);
RDebugUtils.currentLine=6946824;
 //BA.debugLineNum = 6946824;BA.debugLine="Private job1 As HttpJob";
_job1 = new anywheresoftware.b4a.samples.httputils2.httpjob();
RDebugUtils.currentLine=6946826;
 //BA.debugLineNum = 6946826;BA.debugLine="Dim XML As String";
_xml = "";
RDebugUtils.currentLine=6946827;
 //BA.debugLineNum = 6946827;BA.debugLine="XML = \"\"";
_xml = "";
RDebugUtils.currentLine=6946836;
 //BA.debugLineNum = 6946836;BA.debugLine="XML = XML & \"<?xml version='1.0' encoding='ut";
_xml = _xml+"<?xml version='1.0' encoding='utf-8'?>";
RDebugUtils.currentLine=6946837;
 //BA.debugLineNum = 6946837;BA.debugLine="XML = XML & \"<soap:Envelope xmlns:xsi='http://";
_xml = _xml+"<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>";
RDebugUtils.currentLine=6946838;
 //BA.debugLineNum = 6946838;BA.debugLine="XML = XML & \"<soap:Body>\"";
_xml = _xml+"<soap:Body>";
RDebugUtils.currentLine=6946839;
 //BA.debugLineNum = 6946839;BA.debugLine="XML = XML & \"<UploadFile xmlns='http://tempuri";
_xml = _xml+"<UploadFile xmlns='http://tempuri.org/'>";
RDebugUtils.currentLine=6946840;
 //BA.debugLineNum = 6946840;BA.debugLine="XML = XML & \"<f>\" & B64Str & \"</f>\"";
_xml = _xml+"<f>"+_b64str+"</f>";
RDebugUtils.currentLine=6946841;
 //BA.debugLineNum = 6946841;BA.debugLine="XML = XML & \"<fileName>\" & img & \"</fileName>\"";
_xml = _xml+"<fileName>"+_img+"</fileName>";
RDebugUtils.currentLine=6946842;
 //BA.debugLineNum = 6946842;BA.debugLine="XML = XML & \"</UploadFile>\"";
_xml = _xml+"</UploadFile>";
RDebugUtils.currentLine=6946843;
 //BA.debugLineNum = 6946843;BA.debugLine="XML = XML & \"</soap:Body>\"";
_xml = _xml+"</soap:Body>";
RDebugUtils.currentLine=6946844;
 //BA.debugLineNum = 6946844;BA.debugLine="XML = XML & \"</soap:Envelope>\"";
_xml = _xml+"</soap:Envelope>";
RDebugUtils.currentLine=6946845;
 //BA.debugLineNum = 6946845;BA.debugLine="XML = XML.Replace(\"'\", Chr(34))";
_xml = _xml.replace("'",BA.ObjectToString(__c.Chr((int) (34))));
RDebugUtils.currentLine=6946847;
 //BA.debugLineNum = 6946847;BA.debugLine="job1.Initialize(\"img_up\", meth)";
_job1._initialize(ba,"img_up",_meth);
RDebugUtils.currentLine=6946849;
 //BA.debugLineNum = 6946849;BA.debugLine="job1.PostString (Main.use_service &\"\", XML)";
_job1._poststring(_main._use_service+"",_xml);
RDebugUtils.currentLine=6946850;
 //BA.debugLineNum = 6946850;BA.debugLine="job1.GetRequest.SetContentType(\"text/xml\")";
_job1._getrequest().SetContentType("text/xml");
RDebugUtils.currentLine=6946851;
 //BA.debugLineNum = 6946851;BA.debugLine="job1.GetRequest.SetHeader(\"SoapAction\", \"http://t";
_job1._getrequest().SetHeader("SoapAction","http://tempuri.org/UploadFile");
RDebugUtils.currentLine=6946852;
 //BA.debugLineNum = 6946852;BA.debugLine="End Sub";
return "";
}
public String  _jobdone(b4a.example.sp_single_main __ref,anywheresoftware.b4a.samples.httputils2.httpjob _job) throws Exception{
__ref = this;
RDebugUtils.currentModule="sp_single_main";
if (Debug.shouldDelegate(ba, "jobdone"))
	return (String) Debug.delegate(ba, "jobdone", new Object[] {_job});
RDebugUtils.currentLine=6881280;
 //BA.debugLineNum = 6881280;BA.debugLine="Private Sub JobDone(Job As HttpJob)";
RDebugUtils.currentLine=6881282;
 //BA.debugLineNum = 6881282;BA.debugLine="parser.Initialize";
__ref._parser.Initialize(ba);
RDebugUtils.currentLine=6881284;
 //BA.debugLineNum = 6881284;BA.debugLine="If Job.Success Then";
if (_job._success) { 
RDebugUtils.currentLine=6881285;
 //BA.debugLineNum = 6881285;BA.debugLine="Select Case Job.JobName";
switch (BA.switchObjectToInt(_job._jobname,"logincheck","all","img_up","send_ankunft","send_quittung","send_eingabe")) {
case 0: {
RDebugUtils.currentLine=6881287;
 //BA.debugLineNum = 6881287;BA.debugLine="parser.Parse(Job.GetInputStream,\"P_logincheck\"";
__ref._parser.Parse((java.io.InputStream)(_job._getinputstream().getObject()),"P_logincheck");
 break; }
case 1: {
RDebugUtils.currentLine=6881289;
 //BA.debugLineNum = 6881289;BA.debugLine="parser.Parse(Job.GetInputStream,\"P_all\")";
__ref._parser.Parse((java.io.InputStream)(_job._getinputstream().getObject()),"P_all");
 break; }
case 2: {
 break; }
case 3: {
RDebugUtils.currentLine=6881294;
 //BA.debugLineNum = 6881294;BA.debugLine="parser.Parse(Job.GetInputStream,\"P_ankunft\")";
__ref._parser.Parse((java.io.InputStream)(_job._getinputstream().getObject()),"P_ankunft");
 break; }
case 4: {
RDebugUtils.currentLine=6881300;
 //BA.debugLineNum = 6881300;BA.debugLine="parser.Parse(Job.GetInputStream,\"P_quitt";
__ref._parser.Parse((java.io.InputStream)(_job._getinputstream().getObject()),"P_quittung");
 break; }
case 5: {
RDebugUtils.currentLine=6881306;
 //BA.debugLineNum = 6881306;BA.debugLine="parser.Parse(Job.GetInputStream,\"P_eingabe\")";
__ref._parser.Parse((java.io.InputStream)(_job._getinputstream().getObject()),"P_eingabe");
 break; }
}
;
 };
RDebugUtils.currentLine=6881313;
 //BA.debugLineNum = 6881313;BA.debugLine="End Sub";
return "";
}
public String  _p_all_endelement(b4a.example.sp_single_main __ref,String _uri,String _name,anywheresoftware.b4a.keywords.StringBuilderWrapper _text) throws Exception{
__ref = this;
RDebugUtils.currentModule="sp_single_main";
if (Debug.shouldDelegate(ba, "p_all_endelement"))
	return (String) Debug.delegate(ba, "p_all_endelement", new Object[] {_uri,_name,_text});
String _node = "";
RDebugUtils.currentLine=6488064;
 //BA.debugLineNum = 6488064;BA.debugLine="Sub P_all_EndElement (Uri As String, Name As Strin";
RDebugUtils.currentLine=6488065;
 //BA.debugLineNum = 6488065;BA.debugLine="Dim node As String= method & \"Response\"";
_node = __ref._method+"Response";
RDebugUtils.currentLine=6488066;
 //BA.debugLineNum = 6488066;BA.debugLine="If parser.Parents.IndexOf(node) > -1 Then";
if (__ref._parser.Parents.IndexOf((Object)(_node))>-1) { 
RDebugUtils.currentLine=6488068;
 //BA.debugLineNum = 6488068;BA.debugLine="Select Case Name";
switch (BA.switchObjectToInt(_name,"send_infoResult","up_scanResult")) {
case 0: {
RDebugUtils.currentLine=6488070;
 //BA.debugLineNum = 6488070;BA.debugLine="Try";
try {RDebugUtils.currentLine=6488071;
 //BA.debugLineNum = 6488071;BA.debugLine="Main.sql1.ExecNonQuery(\"Update tbl_Antwort se";
_main._sql1.ExecNonQuery("Update tbl_Antwort set sync=1 where id ="+_text.ToString());
RDebugUtils.currentLine=6488072;
 //BA.debugLineNum = 6488072;BA.debugLine="Log(Main.sql1.ExecQuerySingleResult(\"Select cou";
__c.Log(_main._sql1.ExecQuerySingleResult("Select count(sync) from tbl_Antwort where sync=1"));
 } 
       catch (Exception e9) {
			ba.setLastException(e9);RDebugUtils.currentLine=6488074;
 //BA.debugLineNum = 6488074;BA.debugLine="If SubExists(Modules,EN) Then";
if (__c.SubExists(ba,__ref._modules,__ref._en)) { 
RDebugUtils.currentLine=6488075;
 //BA.debugLineNum = 6488075;BA.debugLine="CallSubDelayed2(Modules,EN,Text.ToString)";
__c.CallSubDelayed2(ba,__ref._modules,__ref._en,(Object)(_text.ToString()));
 };
 };
 break; }
case 1: {
RDebugUtils.currentLine=6488083;
 //BA.debugLineNum = 6488083;BA.debugLine="Try";
try {RDebugUtils.currentLine=6488084;
 //BA.debugLineNum = 6488084;BA.debugLine="Main.sql1.ExecNonQuery(\"Update tbl_scan set";
_main._sql1.ExecNonQuery("Update tbl_scan set sync=1 where id ="+_text.ToString());
RDebugUtils.currentLine=6488085;
 //BA.debugLineNum = 6488085;BA.debugLine="Log(Main.sql1.ExecQuerySingleResult(\"Select cou";
__c.Log(_main._sql1.ExecQuerySingleResult("Select count(sync) from tbl_scan where sync=1"));
 } 
       catch (Exception e18) {
			ba.setLastException(e18);RDebugUtils.currentLine=6488087;
 //BA.debugLineNum = 6488087;BA.debugLine="Log(LastException)";
__c.Log(BA.ObjectToString(__c.LastException(getActivityBA())));
RDebugUtils.currentLine=6488088;
 //BA.debugLineNum = 6488088;BA.debugLine="If SubExists(Modules,EN) Then";
if (__c.SubExists(ba,__ref._modules,__ref._en)) { 
RDebugUtils.currentLine=6488089;
 //BA.debugLineNum = 6488089;BA.debugLine="CallSubDelayed2(Modules,EN,Text.ToString)";
__c.CallSubDelayed2(ba,__ref._modules,__ref._en,(Object)(_text.ToString()));
 };
 };
 break; }
}
;
 };
RDebugUtils.currentLine=6488100;
 //BA.debugLineNum = 6488100;BA.debugLine="End Sub";
return "";
}
public String  _p_ankunft_endelement(b4a.example.sp_single_main __ref,String _uri,String _name,anywheresoftware.b4a.keywords.StringBuilderWrapper _text) throws Exception{
__ref = this;
RDebugUtils.currentModule="sp_single_main";
if (Debug.shouldDelegate(ba, "p_ankunft_endelement"))
	return (String) Debug.delegate(ba, "p_ankunft_endelement", new Object[] {_uri,_name,_text});
RDebugUtils.currentLine=6553600;
 //BA.debugLineNum = 6553600;BA.debugLine="Sub P_ankunft_EndElement (Uri As String, Name As S";
RDebugUtils.currentLine=6553602;
 //BA.debugLineNum = 6553602;BA.debugLine="If parser.Parents.IndexOf(\"up_ankunftResponse\")";
if (__ref._parser.Parents.IndexOf((Object)("up_ankunftResponse"))>-1) { 
RDebugUtils.currentLine=6553604;
 //BA.debugLineNum = 6553604;BA.debugLine="Select Case Name";
switch (BA.switchObjectToInt(_name,"up_ankunftResult")) {
case 0: {
RDebugUtils.currentLine=6553606;
 //BA.debugLineNum = 6553606;BA.debugLine="Try";
try {RDebugUtils.currentLine=6553607;
 //BA.debugLineNum = 6553607;BA.debugLine="Main.sql1.ExecNonQuery(\"Update tbl_Ankunft set";
_main._sql1.ExecNonQuery("Update tbl_Ankunft set sync=1 where id ="+_text.ToString());
RDebugUtils.currentLine=6553608;
 //BA.debugLineNum = 6553608;BA.debugLine="Log(Main.sql1.ExecQuerySingleResult(\"Select cou";
__c.Log(_main._sql1.ExecQuerySingleResult("Select count(sync) from tbl_ankunft where sync=1"));
 } 
       catch (Exception e8) {
			ba.setLastException(e8);RDebugUtils.currentLine=6553610;
 //BA.debugLineNum = 6553610;BA.debugLine="Log(LastException)";
__c.Log(BA.ObjectToString(__c.LastException(getActivityBA())));
RDebugUtils.currentLine=6553611;
 //BA.debugLineNum = 6553611;BA.debugLine="CallSubDelayed2(Modules,EN,Text.ToString)";
__c.CallSubDelayed2(ba,__ref._modules,__ref._en,(Object)(_text.ToString()));
 };
 break; }
}
;
 };
RDebugUtils.currentLine=6553620;
 //BA.debugLineNum = 6553620;BA.debugLine="End Sub";
return "";
}
public String  _p_eingabe_endelement(b4a.example.sp_single_main __ref,String _uri,String _name,anywheresoftware.b4a.keywords.StringBuilderWrapper _text) throws Exception{
__ref = this;
RDebugUtils.currentModule="sp_single_main";
if (Debug.shouldDelegate(ba, "p_eingabe_endelement"))
	return (String) Debug.delegate(ba, "p_eingabe_endelement", new Object[] {_uri,_name,_text});
String _node = "";
RDebugUtils.currentLine=6619136;
 //BA.debugLineNum = 6619136;BA.debugLine="Sub P_eingabe_EndElement (Uri As String, Name As S";
RDebugUtils.currentLine=6619137;
 //BA.debugLineNum = 6619137;BA.debugLine="Dim node As String= method & \"Response\"";
_node = __ref._method+"Response";
RDebugUtils.currentLine=6619138;
 //BA.debugLineNum = 6619138;BA.debugLine="If parser.Parents.IndexOf(node) > -1 Then";
if (__ref._parser.Parents.IndexOf((Object)(_node))>-1) { 
RDebugUtils.currentLine=6619139;
 //BA.debugLineNum = 6619139;BA.debugLine="node = method & \"Result\"";
_node = __ref._method+"Result";
RDebugUtils.currentLine=6619140;
 //BA.debugLineNum = 6619140;BA.debugLine="Select Case Name";
switch (BA.switchObjectToInt(_name,_node)) {
case 0: {
RDebugUtils.currentLine=6619142;
 //BA.debugLineNum = 6619142;BA.debugLine="Try";
try {RDebugUtils.currentLine=6619143;
 //BA.debugLineNum = 6619143;BA.debugLine="Main.sql1.ExecNonQuery(\"Update tbl_nachtrag_lo";
_main._sql1.ExecNonQuery("Update tbl_nachtrag_log_app set sync=1 where id ="+_text.ToString());
RDebugUtils.currentLine=6619144;
 //BA.debugLineNum = 6619144;BA.debugLine="Log(Main.sql1.ExecQuerySingleResult(\"Select cou";
__c.Log(_main._sql1.ExecQuerySingleResult("Select count(sync) from tbl_nachtrag_log_app where sync=1"));
 } 
       catch (Exception e10) {
			ba.setLastException(e10);RDebugUtils.currentLine=6619146;
 //BA.debugLineNum = 6619146;BA.debugLine="CallSubDelayed2(Modules,EN,Text.ToString)";
__c.CallSubDelayed2(ba,__ref._modules,__ref._en,(Object)(_text.ToString()));
RDebugUtils.currentLine=6619147;
 //BA.debugLineNum = 6619147;BA.debugLine="Log(LastException)";
__c.Log(BA.ObjectToString(__c.LastException(getActivityBA())));
 };
 break; }
}
;
 };
RDebugUtils.currentLine=6619156;
 //BA.debugLineNum = 6619156;BA.debugLine="End Sub";
return "";
}
public String  _p_logincheck_endelement(b4a.example.sp_single_main __ref,String _uri,String _name,anywheresoftware.b4a.keywords.StringBuilderWrapper _text) throws Exception{
__ref = this;
RDebugUtils.currentModule="sp_single_main";
if (Debug.shouldDelegate(ba, "p_logincheck_endelement"))
	return (String) Debug.delegate(ba, "p_logincheck_endelement", new Object[] {_uri,_name,_text});
RDebugUtils.currentLine=6356992;
 //BA.debugLineNum = 6356992;BA.debugLine="Sub P_logincheck_EndElement (Uri As String, Name A";
RDebugUtils.currentLine=6356993;
 //BA.debugLineNum = 6356993;BA.debugLine="If parser.Parents.IndexOf(\"logincheckResponse\")";
if (__ref._parser.Parents.IndexOf((Object)("logincheckResponse"))>-1) { 
RDebugUtils.currentLine=6356994;
 //BA.debugLineNum = 6356994;BA.debugLine="Select Case Name";
switch (BA.switchObjectToInt(_name,"logincheckResult")) {
case 0: {
RDebugUtils.currentLine=6356996;
 //BA.debugLineNum = 6356996;BA.debugLine="If SubExists(Modules,EN) Then";
if (__c.SubExists(ba,__ref._modules,__ref._en)) { 
RDebugUtils.currentLine=6356997;
 //BA.debugLineNum = 6356997;BA.debugLine="CallSubDelayed2(Modules,EN,Text.ToString)";
__c.CallSubDelayed2(ba,__ref._modules,__ref._en,(Object)(_text.ToString()));
 };
 break; }
}
;
 };
RDebugUtils.currentLine=6357001;
 //BA.debugLineNum = 6357001;BA.debugLine="End Sub";
return "";
}
public String  _p_quittung_endelement(b4a.example.sp_single_main __ref,String _uri,String _name,anywheresoftware.b4a.keywords.StringBuilderWrapper _text) throws Exception{
__ref = this;
RDebugUtils.currentModule="sp_single_main";
if (Debug.shouldDelegate(ba, "p_quittung_endelement"))
	return (String) Debug.delegate(ba, "p_quittung_endelement", new Object[] {_uri,_name,_text});
RDebugUtils.currentLine=6422528;
 //BA.debugLineNum = 6422528;BA.debugLine="Sub P_quittung_EndElement (Uri As String, Name As";
RDebugUtils.currentLine=6422530;
 //BA.debugLineNum = 6422530;BA.debugLine="If parser.Parents.IndexOf(\"up_quitResponse\") > -";
if (__ref._parser.Parents.IndexOf((Object)("up_quitResponse"))>-1) { 
RDebugUtils.currentLine=6422531;
 //BA.debugLineNum = 6422531;BA.debugLine="Select Case Name";
switch (BA.switchObjectToInt(_name,"up_quitResult")) {
case 0: {
RDebugUtils.currentLine=6422533;
 //BA.debugLineNum = 6422533;BA.debugLine="Try";
try {RDebugUtils.currentLine=6422534;
 //BA.debugLineNum = 6422534;BA.debugLine="Main.sql1.ExecNonQuery(\"Update tbl_quittung s";
_main._sql1.ExecNonQuery("Update tbl_quittung set sync=1 where id ="+_text.ToString());
RDebugUtils.currentLine=6422535;
 //BA.debugLineNum = 6422535;BA.debugLine="Log(Main.sql1.ExecQuerySingleResult(\"Select cou";
__c.Log(_main._sql1.ExecQuerySingleResult("Select count(sync) from tbl_quittung where sync=1"));
 } 
       catch (Exception e8) {
			ba.setLastException(e8);RDebugUtils.currentLine=6422537;
 //BA.debugLineNum = 6422537;BA.debugLine="If SubExists(Modules,EN) Then";
if (__c.SubExists(ba,__ref._modules,__ref._en)) { 
RDebugUtils.currentLine=6422538;
 //BA.debugLineNum = 6422538;BA.debugLine="CallSubDelayed2(Modules,EN,Text.ToString)";
__c.CallSubDelayed2(ba,__ref._modules,__ref._en,(Object)(_text.ToString()));
 };
 };
 break; }
}
;
 };
RDebugUtils.currentLine=6422543;
 //BA.debugLineNum = 6422543;BA.debugLine="End Sub";
return "";
}
}