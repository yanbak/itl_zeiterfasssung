
package b4a.example;

import java.io.IOException;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.pc.PCBA;
import anywheresoftware.b4a.pc.RDebug;
import anywheresoftware.b4a.pc.RemoteObject;
import anywheresoftware.b4a.pc.RDebug.IRemote;
import anywheresoftware.b4a.pc.Debug;
import anywheresoftware.b4a.pc.B4XTypes.B4XClass;
import anywheresoftware.b4a.pc.B4XTypes.DeviceClass;

public class main implements IRemote{
	public static main mostCurrent;
	public static RemoteObject processBA;
    public static boolean processGlobalsRun;
    public static RemoteObject myClass;
    public static RemoteObject remoteMe;
	public main() {
		mostCurrent = this;
	}
    public RemoteObject getRemoteMe() {
        return remoteMe;    
    }
    
	public static void main (String[] args) throws Exception {
		new RDebug(args[0], Integer.parseInt(args[1]), Integer.parseInt(args[2]), args[3]);
		RDebug.INSTANCE.waitForTask();

	}
    static {
        anywheresoftware.b4a.pc.RapidSub.moduleToObject.put(new B4XClass("main"), "b4a.example.main");
	}

public boolean isSingleton() {
		return true;
	}
     public static RemoteObject getObject() {
		return myClass;
	 }

	public RemoteObject activityBA;
	public RemoteObject _activity;
    private PCBA pcBA;

	public PCBA create(Object[] args) throws ClassNotFoundException{
		processBA = (RemoteObject) args[1];
		activityBA = (RemoteObject) args[2];
		_activity = (RemoteObject) args[3];
        anywheresoftware.b4a.keywords.Common.Density = (Float)args[4];
        remoteMe = (RemoteObject) args[5];
		pcBA = new PCBA(this, main.class);
        main_subs_0.initializeProcessGlobals();
		return pcBA;
	}
public static RemoteObject __c = RemoteObject.declareNull("anywheresoftware.b4a.keywords.Common");
public static RemoteObject _sql1 = RemoteObject.declareNull("anywheresoftware.b4a.sql.SQL");
public static RemoteObject _sf = RemoteObject.declareNull("adr.stringfunctions.stringfunctions");
public static RemoteObject _update = RemoteObject.declareNull("b4a.example.update_sender_main");
public static RemoteObject _messagefrom = RemoteObject.createImmutable("");
public static RemoteObject _scanlistsrc = RemoteObject.createImmutable("");
public static RemoteObject _ac_to_start = RemoteObject.createImmutable("");
public static RemoteObject _s1 = RemoteObject.declareNull("b4a.example.sp_tabelle");
public static RemoteObject _liveservice = RemoteObject.createImmutable("");
public static RemoteObject _testservice = RemoteObject.createImmutable("");
public static RemoteObject _use_service = RemoteObject.createImmutable("");
public static RemoteObject _u1 = RemoteObject.declareNull("b4a.example.update_sender_main");
public static RemoteObject _reload = RemoteObject.createImmutable("");
public static RemoteObject _aftersend = RemoteObject.createImmutable(false);
public static RemoteObject _auftrag_fuer_detail = RemoteObject.createImmutable("");
public static RemoteObject _scrollposition_1 = RemoteObject.createImmutable(0);
public static RemoteObject _anmeldung = RemoteObject.createImmutable(false);
public static RemoteObject _mainsender = RemoteObject.createImmutable(false);
public static RemoteObject _newsshown = RemoteObject.createImmutable("");
public static RemoteObject _newstext = RemoteObject.createImmutable("");
public static RemoteObject _pm = RemoteObject.declareNull("anywheresoftware.b4a.phone.PackageManagerWrapper");
public static RemoteObject _imageview1 = RemoteObject.declareNull("anywheresoftware.b4a.objects.ImageViewWrapper");
public static RemoteObject _imageview2 = RemoteObject.declareNull("anywheresoftware.b4a.objects.ImageViewWrapper");
public static RemoteObject _imageview3 = RemoteObject.declareNull("anywheresoftware.b4a.objects.ImageViewWrapper");
public static RemoteObject _imageview4 = RemoteObject.declareNull("anywheresoftware.b4a.objects.ImageViewWrapper");
public static RemoteObject _panel1 = RemoteObject.declareNull("anywheresoftware.b4a.objects.PanelWrapper");
public static RemoteObject _lbl_newsitems = RemoteObject.declareNull("anywheresoftware.b4a.objects.LabelWrapper");
public static RemoteObject _lbl_ver = RemoteObject.declareNull("anywheresoftware.b4a.objects.LabelWrapper");
public static RemoteObject _lbl_user = RemoteObject.declareNull("anywheresoftware.b4a.objects.LabelWrapper");
public static RemoteObject _lbl_news2 = RemoteObject.declareNull("anywheresoftware.b4a.objects.LabelWrapper");
public static RemoteObject _bu_news2 = RemoteObject.declareNull("anywheresoftware.b4a.objects.ButtonWrapper");
public static RemoteObject _httputils2service = RemoteObject.declareNull("anywheresoftware.b4a.samples.httputils2.httputils2service");
public static b4a.example.starter _starter = null;
public static b4a.example.scale _scale = null;
public static b4a.example.setup _setup = null;
public static b4a.example.ze_basis _ze_basis = null;
  public Object[] GetGlobals() {
		return new Object[] {"ac_to_start",main._ac_to_start,"Activity",main.mostCurrent._activity,"aftersend",main._aftersend,"anmeldung",main._anmeldung,"auftrag_fuer_detail",main._auftrag_fuer_detail,"bu_news2",main.mostCurrent._bu_news2,"HttpUtils2Service",main.mostCurrent._httputils2service,"ImageView1",main.mostCurrent._imageview1,"ImageView2",main.mostCurrent._imageview2,"ImageView3",main.mostCurrent._imageview3,"ImageView4",main.mostCurrent._imageview4,"lbl_news2",main.mostCurrent._lbl_news2,"lbl_newsitems",main.mostCurrent._lbl_newsitems,"lbl_user",main.mostCurrent._lbl_user,"lbl_ver",main.mostCurrent._lbl_ver,"liveservice",main._liveservice,"mainsender",main._mainsender,"messagefrom",main._messagefrom,"newsshown",main._newsshown,"newstext",main._newstext,"Panel1",main.mostCurrent._panel1,"pm",main.mostCurrent._pm,"reload",main._reload,"s1",main._s1,"scale",Debug.moduleToString(b4a.example.scale.class),"scanlistsrc",main._scanlistsrc,"scrollposition_1",main._scrollposition_1,"setup",Debug.moduleToString(b4a.example.setup.class),"sf",main._sf,"sql1",main._sql1,"Starter",Debug.moduleToString(b4a.example.starter.class),"testservice",main._testservice,"u1",main._u1,"update",main._update,"use_service",main._use_service,"ze_basis",Debug.moduleToString(b4a.example.ze_basis.class)};
}
}