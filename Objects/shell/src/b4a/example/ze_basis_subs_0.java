package b4a.example;

import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.pc.*;

public class ze_basis_subs_0 {


public static RemoteObject  _activity_create(RemoteObject _firsttime) throws Exception{
try {
		Debug.PushSubsStack("Activity_Create (ze_basis) ","ze_basis",7,ze_basis.mostCurrent.activityBA,ze_basis.mostCurrent,20);
if (RapidSub.canDelegate("activity_create")) return b4a.example.ze_basis.remoteMe.runUserSub(false, "ze_basis","activity_create", _firsttime);
RemoteObject _c_status = RemoteObject.declareNull("anywheresoftware.b4a.sql.SQL.CursorWrapper");
Debug.locals.put("FirstTime", _firsttime);
 BA.debugLineNum = 20;BA.debugLine="Sub Activity_Create(FirstTime As Boolean)";
Debug.ShouldStop(524288);
 BA.debugLineNum = 24;BA.debugLine="Try";
Debug.ShouldStop(8388608);
try { BA.debugLineNum = 27;BA.debugLine="Dim c_status As Cursor";
Debug.ShouldStop(67108864);
_c_status = RemoteObject.createNew ("anywheresoftware.b4a.sql.SQL.CursorWrapper");Debug.locals.put("c_status", _c_status);
 BA.debugLineNum = 28;BA.debugLine="c_status=Main.sql1.ExecQuery(\"select Status from";
Debug.ShouldStop(134217728);
_c_status.setObject(ze_basis.mostCurrent._main._sql1.runMethod(false,"ExecQuery",(Object)(RemoteObject.createImmutable("select Status from tbl_ze where Zeit =(Select Max(zeit) from tbl_ze)"))));
 BA.debugLineNum = 29;BA.debugLine="c_status.Position=0";
Debug.ShouldStop(268435456);
_c_status.runMethod(true,"setPosition",BA.numberCast(int.class, 0));
 BA.debugLineNum = 30;BA.debugLine="status=c_status.GetString(\"Status\")";
Debug.ShouldStop(536870912);
ze_basis.mostCurrent._status = _c_status.runMethod(true,"GetString",(Object)(RemoteObject.createImmutable("Status")));
 Debug.CheckDeviceExceptions();
} 
       catch (Exception e7) {
			BA.rdebugUtils.runVoidMethod("setLastException",ze_basis.processBA, e7.toString()); BA.debugLineNum = 32;BA.debugLine="status=\"0\"";
Debug.ShouldStop(-2147483648);
ze_basis.mostCurrent._status = BA.ObjectToString("0");
 };
 BA.debugLineNum = 34;BA.debugLine="scale.SetRate(0.3)";
Debug.ShouldStop(2);
ze_basis.mostCurrent._scale.runVoidMethod ("_setrate",ze_basis.mostCurrent.activityBA,(Object)(BA.numberCast(double.class, 0.3)));
 BA.debugLineNum = 35;BA.debugLine="Activity.LoadLayout(\"ze_basis1\")";
Debug.ShouldStop(4);
ze_basis.mostCurrent._activity.runMethodAndSync(false,"LoadLayout",(Object)(RemoteObject.createImmutable("ze_basis1")),ze_basis.mostCurrent.activityBA);
 BA.debugLineNum = 36;BA.debugLine="If status=\"Kommen\" Then";
Debug.ShouldStop(8);
if (RemoteObject.solveBoolean("=",ze_basis.mostCurrent._status,BA.ObjectToString("Kommen"))) { 
 BA.debugLineNum = 37;BA.debugLine="bu_zeit.Text=\"Gehen\"";
Debug.ShouldStop(16);
ze_basis.mostCurrent._bu_zeit.runMethod(true,"setText",BA.ObjectToCharSequence("Gehen"));
 }else {
 BA.debugLineNum = 39;BA.debugLine="bu_zeit.text=\"Kommen\"";
Debug.ShouldStop(64);
ze_basis.mostCurrent._bu_zeit.runMethod(true,"setText",BA.ObjectToCharSequence("Kommen"));
 };
 BA.debugLineNum = 42;BA.debugLine="End Sub";
Debug.ShouldStop(512);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _activity_pause(RemoteObject _userclosed) throws Exception{
try {
		Debug.PushSubsStack("Activity_Pause (ze_basis) ","ze_basis",7,ze_basis.mostCurrent.activityBA,ze_basis.mostCurrent,48);
if (RapidSub.canDelegate("activity_pause")) return b4a.example.ze_basis.remoteMe.runUserSub(false, "ze_basis","activity_pause", _userclosed);
Debug.locals.put("UserClosed", _userclosed);
 BA.debugLineNum = 48;BA.debugLine="Sub Activity_Pause (UserClosed As Boolean)";
Debug.ShouldStop(32768);
 BA.debugLineNum = 50;BA.debugLine="End Sub";
Debug.ShouldStop(131072);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _activity_resume() throws Exception{
try {
		Debug.PushSubsStack("Activity_Resume (ze_basis) ","ze_basis",7,ze_basis.mostCurrent.activityBA,ze_basis.mostCurrent,44);
if (RapidSub.canDelegate("activity_resume")) return b4a.example.ze_basis.remoteMe.runUserSub(false, "ze_basis","activity_resume");
 BA.debugLineNum = 44;BA.debugLine="Sub Activity_Resume";
Debug.ShouldStop(2048);
 BA.debugLineNum = 46;BA.debugLine="End Sub";
Debug.ShouldStop(8192);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _bu_zeit_click() throws Exception{
try {
		Debug.PushSubsStack("bu_zeit_click (ze_basis) ","ze_basis",7,ze_basis.mostCurrent.activityBA,ze_basis.mostCurrent,51);
if (RapidSub.canDelegate("bu_zeit_click")) return b4a.example.ze_basis.remoteMe.runUserSub(false, "ze_basis","bu_zeit_click");
RemoteObject _sb = RemoteObject.declareNull("anywheresoftware.b4a.keywords.StringBuilderWrapper");
 BA.debugLineNum = 51;BA.debugLine="Sub bu_zeit_click";
Debug.ShouldStop(262144);
 BA.debugLineNum = 52;BA.debugLine="DateTime.DateFormat=\"yyyy-MM-dd hh:mm:ss\"";
Debug.ShouldStop(524288);
ze_basis.mostCurrent.__c.getField(false,"DateTime").runMethod(true,"setDateFormat",BA.ObjectToString("yyyy-MM-dd hh:mm:ss"));
 BA.debugLineNum = 54;BA.debugLine="Dim sb As StringBuilder";
Debug.ShouldStop(2097152);
_sb = RemoteObject.createNew ("anywheresoftware.b4a.keywords.StringBuilderWrapper");Debug.locals.put("sb", _sb);
 BA.debugLineNum = 55;BA.debugLine="sb.initialize";
Debug.ShouldStop(4194304);
_sb.runVoidMethod ("Initialize");
 BA.debugLineNum = 56;BA.debugLine="sb.Append(\"insert into tbl_ze (usr,Zeit,Status) \"";
Debug.ShouldStop(8388608);
_sb.runVoidMethod ("Append",(Object)(RemoteObject.createImmutable("insert into tbl_ze (usr,Zeit,Status) ")));
 BA.debugLineNum = 57;BA.debugLine="sb.Append(\"values ('\" & u1.usr  & \"',\")";
Debug.ShouldStop(16777216);
_sb.runVoidMethod ("Append",(Object)(RemoteObject.concat(RemoteObject.createImmutable("values ('"),ze_basis._u1.getField(true,"_usr"),RemoteObject.createImmutable("',"))));
 BA.debugLineNum = 58;BA.debugLine="sb.Append(\"'\" & DateTime.Date(DateTime.Now)  & \"'";
Debug.ShouldStop(33554432);
_sb.runVoidMethod ("Append",(Object)(RemoteObject.concat(RemoteObject.createImmutable("'"),ze_basis.mostCurrent.__c.getField(false,"DateTime").runMethod(true,"Date",(Object)(ze_basis.mostCurrent.__c.getField(false,"DateTime").runMethod(true,"getNow"))),RemoteObject.createImmutable("',"))));
 BA.debugLineNum = 59;BA.debugLine="sb.Append(\"'\" & bu_zeit.Text &\"');\")";
Debug.ShouldStop(67108864);
_sb.runVoidMethod ("Append",(Object)(RemoteObject.concat(RemoteObject.createImmutable("'"),ze_basis.mostCurrent._bu_zeit.runMethod(true,"getText"),RemoteObject.createImmutable("');"))));
 BA.debugLineNum = 62;BA.debugLine="Main.sql1.ExecNonQuery(sb.ToString)";
Debug.ShouldStop(536870912);
ze_basis.mostCurrent._main._sql1.runVoidMethod ("ExecNonQuery",(Object)(_sb.runMethod(true,"ToString")));
 BA.debugLineNum = 64;BA.debugLine="send_ZE";
Debug.ShouldStop(-2147483648);
_send_ze();
 BA.debugLineNum = 69;BA.debugLine="End Sub";
Debug.ShouldStop(16);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _globals() throws Exception{
 //BA.debugLineNum = 13;BA.debugLine="Sub Globals";
 //BA.debugLineNum = 16;BA.debugLine="Dim status As String";
ze_basis.mostCurrent._status = RemoteObject.createImmutable("");
 //BA.debugLineNum = 17;BA.debugLine="Dim bu_zeit As Button";
ze_basis.mostCurrent._bu_zeit = RemoteObject.createNew ("anywheresoftware.b4a.objects.ButtonWrapper");
 //BA.debugLineNum = 18;BA.debugLine="End Sub";
return RemoteObject.createImmutable("");
}
public static RemoteObject  _p_set_ze_pool_endelement(RemoteObject _uri,RemoteObject _name,RemoteObject _text) throws Exception{
try {
		Debug.PushSubsStack("P_set_ze_pool_EndElement (ze_basis) ","ze_basis",7,ze_basis.mostCurrent.activityBA,ze_basis.mostCurrent,118);
if (RapidSub.canDelegate("p_set_ze_pool_endelement")) return b4a.example.ze_basis.remoteMe.runUserSub(false, "ze_basis","p_set_ze_pool_endelement", _uri, _name, _text);
RemoteObject _node = RemoteObject.createImmutable("");
Debug.locals.put("Uri", _uri);
Debug.locals.put("Name", _name);
Debug.locals.put("Text", _text);
 BA.debugLineNum = 118;BA.debugLine="Sub P_set_ze_pool_EndElement (Uri As String, Name";
Debug.ShouldStop(2097152);
 BA.debugLineNum = 120;BA.debugLine="Dim node As String= \"set_ze_pool\" & \"Response\"";
Debug.ShouldStop(8388608);
_node = RemoteObject.concat(RemoteObject.createImmutable("set_ze_pool"),RemoteObject.createImmutable("Response"));Debug.locals.put("node", _node);Debug.locals.put("node", _node);
 BA.debugLineNum = 121;BA.debugLine="If SaxParser.Parents.IndexOf(node) > -1 Then";
Debug.ShouldStop(16777216);
if (RemoteObject.solveBoolean(">",ze_basis._saxparser.getField(false,"Parents").runMethod(true,"IndexOf",(Object)((_node))),BA.numberCast(double.class, -(double) (0 + 1)))) { 
 BA.debugLineNum = 122;BA.debugLine="node = \"set_ze_pool\" & \"Result\"";
Debug.ShouldStop(33554432);
_node = RemoteObject.concat(RemoteObject.createImmutable("set_ze_pool"),RemoteObject.createImmutable("Result"));Debug.locals.put("node", _node);
 BA.debugLineNum = 123;BA.debugLine="Select Case Name";
Debug.ShouldStop(67108864);
switch (BA.switchObjectToInt(_name,_node)) {
case 0: {
 BA.debugLineNum = 125;BA.debugLine="Try";
Debug.ShouldStop(268435456);
try { BA.debugLineNum = 126;BA.debugLine="Main.sql1.ExecNonQuery(\"Update tbl_ze set arc";
Debug.ShouldStop(536870912);
ze_basis.mostCurrent._main._sql1.runVoidMethod ("ExecNonQuery",(Object)(RemoteObject.concat(RemoteObject.createImmutable("Update tbl_ze set archiv=1 where id ='"),_name,RemoteObject.createImmutable("'"))));
 Debug.CheckDeviceExceptions();
} 
       catch (Exception e9) {
			BA.rdebugUtils.runVoidMethod("setLastException",ze_basis.processBA, e9.toString()); BA.debugLineNum = 130;BA.debugLine="Log(LastException)";
Debug.ShouldStop(2);
ze_basis.mostCurrent.__c.runVoidMethod ("Log",(Object)(BA.ObjectToString(ze_basis.mostCurrent.__c.runMethod(false,"LastException",ze_basis.mostCurrent.activityBA))));
 };
 break; }
}
;
 };
 BA.debugLineNum = 140;BA.debugLine="End Sub";
Debug.ShouldStop(2048);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _process_globals() throws Exception{
 //BA.debugLineNum = 6;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 9;BA.debugLine="Dim u1 As update_sender_main";
ze_basis._u1 = RemoteObject.createNew ("b4a.example.update_sender_main");
 //BA.debugLineNum = 10;BA.debugLine="Dim SaxParser As SaxParser";
ze_basis._saxparser = RemoteObject.createNew ("anywheresoftware.b4a.objects.SaxParser");
 //BA.debugLineNum = 11;BA.debugLine="End Sub";
return RemoteObject.createImmutable("");
}
public static void  _send_ze() throws Exception{
ResumableSub_send_ZE rsub = new ResumableSub_send_ZE(null);
rsub.resume(null, null);
}
public static class ResumableSub_send_ZE extends BA.ResumableSub {
public ResumableSub_send_ZE(b4a.example.ze_basis parent) {
this.parent = parent;
}
java.util.LinkedHashMap<String, Object> rsLocals = new java.util.LinkedHashMap<String, Object>();
b4a.example.ze_basis parent;
RemoteObject _url = RemoteObject.createImmutable("");
RemoteObject _user = RemoteObject.createImmutable("");
RemoteObject _pw = RemoteObject.createImmutable("");
RemoteObject _app_id = RemoteObject.createImmutable("");
RemoteObject _zeit = RemoteObject.createImmutable("");
RemoteObject _stat = RemoteObject.createImmutable("");
RemoteObject _soapparser = RemoteObject.declareNull("b4a.example.soap_up");
RemoteObject _set_ze_pooljob = RemoteObject.declareNull("anywheresoftware.b4a.samples.httputils2.httpjob");

@Override
public void resume(BA ba, RemoteObject result) throws Exception{
try {
		Debug.PushSubsStack("send_ZE (ze_basis) ","ze_basis",7,ze_basis.mostCurrent.activityBA,ze_basis.mostCurrent,72);
if (RapidSub.canDelegate("send_ze")) return ;
Debug.locals = rsLocals;Debug.currentSubFrame.locals = rsLocals;

    while (true) {
try {

        switch (state) {
            case -1:
return;

case 0:
//C
this.state = 1;
 BA.debugLineNum = 73;BA.debugLine="Dim url As String";
Debug.ShouldStop(256);
_url = RemoteObject.createImmutable("");Debug.locals.put("url", _url);
 BA.debugLineNum = 74;BA.debugLine="url=Main.liveservice";
Debug.ShouldStop(512);
_url = parent.mostCurrent._main._liveservice;Debug.locals.put("url", _url);
 BA.debugLineNum = 77;BA.debugLine="Try";
Debug.ShouldStop(4096);
if (true) break;

case 1:
//try
this.state = 6;
this.catchState = 5;
this.state = 3;
if (true) break;

case 3:
//C
this.state = 6;
this.catchState = 5;
 BA.debugLineNum = 78;BA.debugLine="Dim user As String = Main.sql1.ExecQuerySingleRe";
Debug.ShouldStop(8192);
_user = parent.mostCurrent._main._sql1.runMethod(true,"ExecQuerySingleResult",(Object)(RemoteObject.createImmutable("Select username from tbl_usr")));Debug.locals.put("user", _user);Debug.locals.put("user", _user);
 BA.debugLineNum = 79;BA.debugLine="Dim pw As String = Main.sql1.ExecQuerySingleResu";
Debug.ShouldStop(16384);
_pw = parent.mostCurrent._main._sql1.runMethod(true,"ExecQuerySingleResult",(Object)(RemoteObject.createImmutable("Select pw from tbl_usr")));Debug.locals.put("pw", _pw);Debug.locals.put("pw", _pw);
 BA.debugLineNum = 80;BA.debugLine="Dim App_id As String = Main.sql1.ExecQuerySingle";
Debug.ShouldStop(32768);
_app_id = parent.mostCurrent._main._sql1.runMethod(true,"ExecQuerySingleResult",(Object)(RemoteObject.createImmutable("Select max(ID) from tbl_ze")));Debug.locals.put("App_id", _app_id);Debug.locals.put("App_id", _app_id);
 BA.debugLineNum = 81;BA.debugLine="Dim zeit As String = Main.sql1.ExecQuerySingleRe";
Debug.ShouldStop(65536);
_zeit = parent.mostCurrent._main._sql1.runMethod(true,"ExecQuerySingleResult",(Object)(RemoteObject.createImmutable("Select Zeit from tbl_ze where ID =(select max(ID) from tbl_ze)")));Debug.locals.put("zeit", _zeit);Debug.locals.put("zeit", _zeit);
 BA.debugLineNum = 82;BA.debugLine="Dim stat As String = Main.sql1.ExecQuerySingleRe";
Debug.ShouldStop(131072);
_stat = parent.mostCurrent._main._sql1.runMethod(true,"ExecQuerySingleResult",(Object)(RemoteObject.createImmutable("Select Status from tbl_ze where ID =(select max(ID) from tbl_ze)")));Debug.locals.put("stat", _stat);Debug.locals.put("stat", _stat);
 Debug.CheckDeviceExceptions();
if (true) break;

case 5:
//C
this.state = 6;
this.catchState = 0;
 BA.debugLineNum = 84;BA.debugLine="Log(LastException)";
Debug.ShouldStop(524288);
parent.mostCurrent.__c.runVoidMethod ("Log",(Object)(BA.ObjectToString(parent.mostCurrent.__c.runMethod(false,"LastException",ze_basis.mostCurrent.activityBA))));
 if (true) break;
if (true) break;

case 6:
//C
this.state = 7;
this.catchState = 0;
;
 BA.debugLineNum = 87;BA.debugLine="Dim SoapParser As soap_up";
Debug.ShouldStop(4194304);
_soapparser = RemoteObject.createNew ("b4a.example.soap_up");Debug.locals.put("SoapParser", _soapparser);
 BA.debugLineNum = 88;BA.debugLine="SoapParser.initialize(url,\"set_ze_pool\")";
Debug.ShouldStop(8388608);
_soapparser.runClassMethod (b4a.example.soap_up.class, "_initialize",ze_basis.processBA,(Object)(_url),(Object)(RemoteObject.createImmutable("set_ze_pool")));
 BA.debugLineNum = 89;BA.debugLine="SoapParser.AddField(\"usr\",SoapParser.TYPE_STRING,";
Debug.ShouldStop(16777216);
_soapparser.runClassMethod (b4a.example.soap_up.class, "_addfield",(Object)(BA.ObjectToString("usr")),(Object)(_soapparser.getField(true,"_type_string")),(Object)(_user));
 BA.debugLineNum = 90;BA.debugLine="SoapParser.AddField(\"pwd\",SoapParser.TYPE_STRING,";
Debug.ShouldStop(33554432);
_soapparser.runClassMethod (b4a.example.soap_up.class, "_addfield",(Object)(BA.ObjectToString("pwd")),(Object)(_soapparser.getField(true,"_type_string")),(Object)(_pw));
 BA.debugLineNum = 91;BA.debugLine="SoapParser.AddField(\"App_ID\",SoapParser.TYPE_STRI";
Debug.ShouldStop(67108864);
_soapparser.runClassMethod (b4a.example.soap_up.class, "_addfield",(Object)(BA.ObjectToString("App_ID")),(Object)(_soapparser.getField(true,"_type_string")),(Object)(_app_id));
 BA.debugLineNum = 92;BA.debugLine="SoapParser.AddField(\"Zeit\",SoapParser.TYPE_STRING";
Debug.ShouldStop(134217728);
_soapparser.runClassMethod (b4a.example.soap_up.class, "_addfield",(Object)(BA.ObjectToString("Zeit")),(Object)(_soapparser.getField(true,"_type_string")),(Object)(_zeit));
 BA.debugLineNum = 93;BA.debugLine="SoapParser.AddField(\"Status\",SoapParser.TYPE_STRI";
Debug.ShouldStop(268435456);
_soapparser.runClassMethod (b4a.example.soap_up.class, "_addfield",(Object)(BA.ObjectToString("Status")),(Object)(_soapparser.getField(true,"_type_string")),(Object)(_stat));
 BA.debugLineNum = 94;BA.debugLine="SoapParser.footer(\"set_ze_pool\")";
Debug.ShouldStop(536870912);
_soapparser.runClassMethod (b4a.example.soap_up.class, "_footer",(Object)(RemoteObject.createImmutable("set_ze_pool")));
 BA.debugLineNum = 96;BA.debugLine="Dim set_ze_poolJob As HttpJob";
Debug.ShouldStop(-2147483648);
_set_ze_pooljob = RemoteObject.createNew ("anywheresoftware.b4a.samples.httputils2.httpjob");Debug.locals.put("set_ze_poolJob", _set_ze_pooljob);
 BA.debugLineNum = 97;BA.debugLine="set_ze_poolJob.Initialize(\"\",Me)";
Debug.ShouldStop(1);
_set_ze_pooljob.runVoidMethod ("_initialize",ze_basis.processBA,(Object)(BA.ObjectToString("")),(Object)(ze_basis.getObject()));
 BA.debugLineNum = 98;BA.debugLine="set_ze_poolJob.PostString(url, SoapParser.xml_sen";
Debug.ShouldStop(2);
_set_ze_pooljob.runVoidMethod ("_poststring",(Object)(_url),(Object)(_soapparser.getField(true,"_xml_sendstriing")));
 BA.debugLineNum = 99;BA.debugLine="set_ze_poolJob.GetRequest.SetHeader(\"SoapAction\",";
Debug.ShouldStop(4);
_set_ze_pooljob.runMethod(false,"_getrequest").runVoidMethod ("SetHeader",(Object)(BA.ObjectToString("SoapAction")),(Object)(RemoteObject.createImmutable("http://tempuri.org/set_ze_pool")));
 BA.debugLineNum = 100;BA.debugLine="Log(SoapParser.xml_sendstriing)";
Debug.ShouldStop(8);
parent.mostCurrent.__c.runVoidMethod ("Log",(Object)(_soapparser.getField(true,"_xml_sendstriing")));
 BA.debugLineNum = 101;BA.debugLine="set_ze_poolJob.GetRequest.SetContentType(\"text/xm";
Debug.ShouldStop(16);
_set_ze_pooljob.runMethod(false,"_getrequest").runVoidMethod ("SetContentType",(Object)(RemoteObject.createImmutable("text/xml; charset=utf-8")));
 BA.debugLineNum = 102;BA.debugLine="set_ze_poolJob.GetRequest.Timeout=2000";
Debug.ShouldStop(32);
_set_ze_pooljob.runMethod(false,"_getrequest").runVoidMethod ("setTimeout",BA.numberCast(int.class, 2000));
 BA.debugLineNum = 103;BA.debugLine="ProgressDialogShow(\"Daten werden hochgeladen..\")";
Debug.ShouldStop(64);
parent.mostCurrent.__c.runVoidMethod ("ProgressDialogShow",ze_basis.mostCurrent.activityBA,(Object)(BA.ObjectToCharSequence(RemoteObject.createImmutable("Daten werden hochgeladen.."))));
 BA.debugLineNum = 105;BA.debugLine="Wait For (set_ze_poolJob) JobDone(set_ze_poolJob";
Debug.ShouldStop(256);
parent.mostCurrent.__c.runVoidMethod ("WaitFor","jobdone", ze_basis.processBA, anywheresoftware.b4a.pc.PCResumableSub.createDebugResumeSub(this), (_set_ze_pooljob));
this.state = 13;
return;
case 13:
//C
this.state = 7;
_set_ze_pooljob = (RemoteObject) result.getArrayElement(false,RemoteObject.createImmutable(0));Debug.locals.put("set_ze_poolJob", _set_ze_pooljob);
;
 BA.debugLineNum = 107;BA.debugLine="If set_ze_poolJob.Success Then";
Debug.ShouldStop(1024);
if (true) break;

case 7:
//if
this.state = 12;
if (_set_ze_pooljob.getField(true,"_success").<Boolean>get().booleanValue()) { 
this.state = 9;
}else {
this.state = 11;
}if (true) break;

case 9:
//C
this.state = 12;
 BA.debugLineNum = 108;BA.debugLine="SaxParser.Initialize";
Debug.ShouldStop(2048);
parent._saxparser.runVoidMethod ("Initialize",ze_basis.processBA);
 BA.debugLineNum = 109;BA.debugLine="SaxParser.Parse(set_ze_poolJob.GetInputStream,\"P";
Debug.ShouldStop(4096);
parent._saxparser.runVoidMethodAndSync ("Parse",(Object)((_set_ze_pooljob.runMethod(false,"_getinputstream").getObject())),(Object)(RemoteObject.createImmutable("P_set_ze_pool")));
 BA.debugLineNum = 110;BA.debugLine="ProgressDialogHide";
Debug.ShouldStop(8192);
parent.mostCurrent.__c.runVoidMethod ("ProgressDialogHide");
 if (true) break;

case 11:
//C
this.state = 12;
 BA.debugLineNum = 112;BA.debugLine="ProgressDialogHide";
Debug.ShouldStop(32768);
parent.mostCurrent.__c.runVoidMethod ("ProgressDialogHide");
 BA.debugLineNum = 113;BA.debugLine="ToastMessageShow(\"Keine Verbindung zum Server mö";
Debug.ShouldStop(65536);
parent.mostCurrent.__c.runVoidMethod ("ToastMessageShow",(Object)(BA.ObjectToCharSequence("Keine Verbindung zum Server möglich")),(Object)(parent.mostCurrent.__c.getField(true,"False")));
 if (true) break;

case 12:
//C
this.state = -1;
;
 BA.debugLineNum = 115;BA.debugLine="set_ze_poolJob.Release";
Debug.ShouldStop(262144);
_set_ze_pooljob.runVoidMethod ("_release");
 BA.debugLineNum = 116;BA.debugLine="End Sub";
Debug.ShouldStop(524288);
if (true) break;
}} 
       catch (Exception e0) {
			
if (catchState == 0)
    throw e0;
else {
    state = catchState;
BA.rdebugUtils.runVoidMethod("setLastException",ze_basis.processBA, e0.toString());}
            }
        }
    }
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
}
public static void  _jobdone(RemoteObject _set_ze_pooljob) throws Exception{
}
}