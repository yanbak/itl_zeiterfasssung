
package b4a.example;

import anywheresoftware.b4a.pc.PCBA;
import anywheresoftware.b4a.pc.RemoteObject;

public class sp_tabelle {
    public static RemoteObject myClass;
	public sp_tabelle() {
	}
    public static PCBA staticBA = new PCBA(null, sp_tabelle.class);

public static RemoteObject __c = RemoteObject.declareNull("anywheresoftware.b4a.keywords.Common");
public static RemoteObject _parser = RemoteObject.declareNull("anywheresoftware.b4a.objects.SaxParser");
public static RemoteObject _type_string = RemoteObject.createImmutable("");
public static RemoteObject _type_integer = RemoteObject.createImmutable("");
public static RemoteObject _type_float = RemoteObject.createImmutable("");
public static RemoteObject _type_date = RemoteObject.createImmutable("");
public static RemoteObject _type_double = RemoteObject.createImmutable("");
public static RemoteObject _type_binary = RemoteObject.createImmutable("");
public static RemoteObject _urlws = RemoteObject.createImmutable("");
public static RemoteObject _ar_auftrag = null;
public static RemoteObject _ar_auftrag_stoff = null;
public static RemoteObject _ar_stoffliste = null;
public static RemoteObject _ar_getnews = null;
public static RemoteObject _ar_erlaubbtetalons = null;
public static RemoteObject _ar_pbcodes = null;
public static RemoteObject _modules = RemoteObject.declareNull("Object");
public static RemoteObject _en = RemoteObject.createImmutable("");
public static RemoteObject _xml_sendstriing = RemoteObject.createImmutable("");
public static RemoteObject _version = RemoteObject.createImmutable(0);
public static RemoteObject _last = RemoteObject.createImmutable(false);
public static RemoteObject _u1 = RemoteObject.declareNull("b4a.example.update_sender_main");
public static RemoteObject _httputils2service = RemoteObject.declareNull("anywheresoftware.b4a.samples.httputils2.httputils2service");
public static b4a.example.main _main = null;
public static b4a.example.starter _starter = null;
public static b4a.example.scale _scale = null;
public static b4a.example.setup _setup = null;
public static b4a.example.ze_basis _ze_basis = null;
public static Object[] GetGlobals(RemoteObject _ref) throws Exception {
		return new Object[] {"ar_auftrag",_ref.getField(false, "_ar_auftrag"),"ar_auftrag_stoff",_ref.getField(false, "_ar_auftrag_stoff"),"ar_erlaubbtetalons",_ref.getField(false, "_ar_erlaubbtetalons"),"ar_getnews",_ref.getField(false, "_ar_getnews"),"ar_pbcodes",_ref.getField(false, "_ar_pbcodes"),"ar_stoffliste",_ref.getField(false, "_ar_stoffliste"),"EN",_ref.getField(false, "_en"),"HttpUtils2Service",_ref.getField(false, "_httputils2service"),"last",_ref.getField(false, "_last"),"Modules",_ref.getField(false, "_modules"),"parser",_ref.getField(false, "_parser"),"TYPE_BINARY",_ref.getField(false, "_type_binary"),"TYPE_DATE",_ref.getField(false, "_type_date"),"TYPE_DOUBLE",_ref.getField(false, "_type_double"),"TYPE_FLOAT",_ref.getField(false, "_type_float"),"TYPE_INTEGER",_ref.getField(false, "_type_integer"),"TYPE_STRING",_ref.getField(false, "_type_string"),"u1",_ref.getField(false, "_u1"),"UrlWS",_ref.getField(false, "_urlws"),"version",_ref.getField(false, "_version"),"xml_sendstriing",_ref.getField(false, "_xml_sendstriing")};
}
}