package b4a.example;

import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.pc.*;

public class sp_single_subs_0 {


public static RemoteObject  _addfield(RemoteObject __ref,RemoteObject _fieldname,RemoteObject _fieldtype,RemoteObject _fieldvalue) throws Exception{
try {
		Debug.PushSubsStack("AddField (sp_single) ","sp_single",3,__ref.getField(false, "ba"),__ref,70);
if (RapidSub.canDelegate("addfield")) return __ref.runUserSub(false, "sp_single","addfield", __ref, _fieldname, _fieldtype, _fieldvalue);
Debug.locals.put("FieldName", _fieldname);
Debug.locals.put("FieldType", _fieldtype);
Debug.locals.put("FieldValue", _fieldvalue);
 BA.debugLineNum = 70;BA.debugLine="Public Sub AddField(FieldName As String,FieldType";
Debug.ShouldStop(32);
 BA.debugLineNum = 72;BA.debugLine="xml_sendstriing=xml_sendstriing &CRLF & $\"<${Fiel";
Debug.ShouldStop(128);
__ref.setField ("_xml_sendstriing",RemoteObject.concat(__ref.getField(true,"_xml_sendstriing"),sp_single.__c.getField(true,"CRLF"),(RemoteObject.concat(RemoteObject.createImmutable("<"),sp_single.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_fieldname))),RemoteObject.createImmutable(" >"),sp_single.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_fieldvalue))),RemoteObject.createImmutable("</"),sp_single.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_fieldname))),RemoteObject.createImmutable(">")))));
 BA.debugLineNum = 73;BA.debugLine="End Sub";
Debug.ShouldStop(256);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _class_globals(RemoteObject __ref) throws Exception{
 //BA.debugLineNum = 2;BA.debugLine="Sub Class_Globals";
 //BA.debugLineNum = 4;BA.debugLine="Dim parser As SaxParser";
sp_single._parser = RemoteObject.createNew ("anywheresoftware.b4a.objects.SaxParser");__ref.setField("_parser",sp_single._parser);
 //BA.debugLineNum = 5;BA.debugLine="Public TYPE_STRING   As String = \"xsd:string\"";
sp_single._type_string = BA.ObjectToString("xsd:string");__ref.setField("_type_string",sp_single._type_string);
 //BA.debugLineNum = 6;BA.debugLine="Public TYPE_INTEGER  As String = \"xsd:int\"";
sp_single._type_integer = BA.ObjectToString("xsd:int");__ref.setField("_type_integer",sp_single._type_integer);
 //BA.debugLineNum = 7;BA.debugLine="Public TYPE_FLOAT    As String = \"xsd:float\"";
sp_single._type_float = BA.ObjectToString("xsd:float");__ref.setField("_type_float",sp_single._type_float);
 //BA.debugLineNum = 8;BA.debugLine="Public TYPE_DATE     As String = \"xsd:date\"";
sp_single._type_date = BA.ObjectToString("xsd:date");__ref.setField("_type_date",sp_single._type_date);
 //BA.debugLineNum = 9;BA.debugLine="Public TYPE_DOUBLE   As String = \"xsd:double\"";
sp_single._type_double = BA.ObjectToString("xsd:double");__ref.setField("_type_double",sp_single._type_double);
 //BA.debugLineNum = 10;BA.debugLine="Public TYPE_BINARY   As String = \"xsd:base64Binar";
sp_single._type_binary = BA.ObjectToString("xsd:base64Binary");__ref.setField("_type_binary",sp_single._type_binary);
 //BA.debugLineNum = 12;BA.debugLine="Private UrlWS        As String";
sp_single._urlws = RemoteObject.createImmutable("");__ref.setField("_urlws",sp_single._urlws);
 //BA.debugLineNum = 14;BA.debugLine="Private Modules      As Object";
sp_single._modules = RemoteObject.createNew ("Object");__ref.setField("_modules",sp_single._modules);
 //BA.debugLineNum = 15;BA.debugLine="Private EN           As String";
sp_single._en = RemoteObject.createImmutable("");__ref.setField("_en",sp_single._en);
 //BA.debugLineNum = 16;BA.debugLine="Private xml_sendstriing As String";
sp_single._xml_sendstriing = RemoteObject.createImmutable("");__ref.setField("_xml_sendstriing",sp_single._xml_sendstriing);
 //BA.debugLineNum = 17;BA.debugLine="Private last1 As Boolean";
sp_single._last1 = RemoteObject.createImmutable(false);__ref.setField("_last1",sp_single._last1);
 //BA.debugLineNum = 18;BA.debugLine="Dim method As String";
sp_single._method = RemoteObject.createImmutable("");__ref.setField("_method",sp_single._method);
 //BA.debugLineNum = 19;BA.debugLine="Dim update_send As update_sender";
sp_single._update_send = RemoteObject.createNew ("b4a.example.update_sender");__ref.setField("_update_send",sp_single._update_send);
 //BA.debugLineNum = 20;BA.debugLine="End Sub";
return RemoteObject.createImmutable("");
}
public static RemoteObject  _initialize(RemoteObject __ref,RemoteObject _ba,RemoteObject _url,RemoteObject _methodname,RemoteObject _module,RemoteObject _eventname) throws Exception{
try {
		Debug.PushSubsStack("initialize (sp_single) ","sp_single",3,__ref.getField(false, "ba"),__ref,55);
if (RapidSub.canDelegate("initialize")) return __ref.runUserSub(false, "sp_single","initialize", __ref, _ba, _url, _methodname, _module, _eventname);
__ref.runVoidMethodAndSync("innerInitializeHelper", _ba);
Debug.locals.put("ba", _ba);
Debug.locals.put("Url", _url);
Debug.locals.put("MethodName", _methodname);
Debug.locals.put("module", _module);
Debug.locals.put("eventname", _eventname);
 BA.debugLineNum = 55;BA.debugLine="Public Sub initialize(Url As String,MethodName As";
Debug.ShouldStop(4194304);
 BA.debugLineNum = 58;BA.debugLine="UrlWS        = Url";
Debug.ShouldStop(33554432);
__ref.setField ("_urlws",_url);
 BA.debugLineNum = 59;BA.debugLine="method=MethodName";
Debug.ShouldStop(67108864);
__ref.setField ("_method",_methodname);
 BA.debugLineNum = 60;BA.debugLine="EN           = eventname";
Debug.ShouldStop(134217728);
__ref.setField ("_en",_eventname);
 BA.debugLineNum = 61;BA.debugLine="Modules      = module";
Debug.ShouldStop(268435456);
__ref.setField ("_modules",_module);
 BA.debugLineNum = 64;BA.debugLine="xml_sendstriing =xml_sendstriing & \"<?xml version";
Debug.ShouldStop(-2147483648);
__ref.setField ("_xml_sendstriing",RemoteObject.concat(__ref.getField(true,"_xml_sendstriing"),RemoteObject.createImmutable("<?xml version="),sp_single.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable("1.0"),sp_single.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable(" encoding="),sp_single.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable("utf-8"),sp_single.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable("?>")));
 BA.debugLineNum = 65;BA.debugLine="xml_sendstriing= xml_sendstriing & \"<soap:Envelope";
Debug.ShouldStop(1);
__ref.setField ("_xml_sendstriing",RemoteObject.concat(__ref.getField(true,"_xml_sendstriing"),RemoteObject.createImmutable("<soap:Envelope xmlns:xsi="),sp_single.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable("http://www.w3.org/2001/XMLSchema-instance"),sp_single.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable(" xmlns:xsd="),sp_single.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable("http://www.w3.org/2001/XMLSchema"),sp_single.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable(" xmlns:soap="),sp_single.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable("http://schemas.xmlsoap.org/soap/envelope/"),sp_single.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable(">")));
 BA.debugLineNum = 66;BA.debugLine="xml_sendstriing= xml_sendstriing & \"<soap:Body>\"";
Debug.ShouldStop(2);
__ref.setField ("_xml_sendstriing",RemoteObject.concat(__ref.getField(true,"_xml_sendstriing"),RemoteObject.createImmutable("<soap:Body>")));
 BA.debugLineNum = 67;BA.debugLine="xml_sendstriing= xml_sendstriing & \"<\" & Metho";
Debug.ShouldStop(4);
__ref.setField ("_xml_sendstriing",RemoteObject.concat(__ref.getField(true,"_xml_sendstriing"),RemoteObject.createImmutable("<"),_methodname,RemoteObject.createImmutable(" xmlns="),sp_single.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable("http://tempuri.org/"),sp_single.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable(">")));
 BA.debugLineNum = 68;BA.debugLine="End Sub";
Debug.ShouldStop(8);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _jobdone(RemoteObject __ref,RemoteObject _job) throws Exception{
try {
		Debug.PushSubsStack("JobDone (sp_single) ","sp_single",3,__ref.getField(false, "ba"),__ref,97);
if (RapidSub.canDelegate("jobdone")) return __ref.runUserSub(false, "sp_single","jobdone", __ref, _job);
Debug.locals.put("Job", _job);
 BA.debugLineNum = 97;BA.debugLine="Private Sub JobDone(Job As HttpJob)";
Debug.ShouldStop(1);
 BA.debugLineNum = 99;BA.debugLine="parser.Initialize";
Debug.ShouldStop(4);
__ref.getField(false,"_parser").runVoidMethod ("Initialize",__ref.getField(false, "ba"));
 BA.debugLineNum = 101;BA.debugLine="If Job.Success Then";
Debug.ShouldStop(16);
if (_job.getField(true,"_success").<Boolean>get().booleanValue()) { 
 BA.debugLineNum = 102;BA.debugLine="Select Case Job.JobName";
Debug.ShouldStop(32);
switch (BA.switchObjectToInt(_job.getField(true,"_jobname"),BA.ObjectToString("logincheck"),BA.ObjectToString("all"),BA.ObjectToString("send_eingabe"))) {
case 0: {
 BA.debugLineNum = 104;BA.debugLine="parser.Parse(Job.GetInputStream,\"P_logincheck\"";
Debug.ShouldStop(128);
__ref.getField(false,"_parser").runVoidMethodAndSync ("Parse",(Object)((_job.runMethod(false,"_getinputstream").getObject())),(Object)(RemoteObject.createImmutable("P_logincheck")));
 break; }
case 1: {
 BA.debugLineNum = 106;BA.debugLine="parser.Parse(Job.GetInputStream,\"P_all\")";
Debug.ShouldStop(512);
__ref.getField(false,"_parser").runVoidMethodAndSync ("Parse",(Object)((_job.runMethod(false,"_getinputstream").getObject())),(Object)(RemoteObject.createImmutable("P_all")));
 BA.debugLineNum = 107;BA.debugLine="Main.aftersend=True";
Debug.ShouldStop(1024);
sp_single._main._aftersend = sp_single.__c.getField(true,"True");
 BA.debugLineNum = 109;BA.debugLine="StartActivity(Main.ac_to_start)";
Debug.ShouldStop(4096);
sp_single.__c.runVoidMethod ("StartActivity",__ref.getField(false, "ba"),(Object)((sp_single._main._ac_to_start)));
 break; }
case 2: {
 BA.debugLineNum = 116;BA.debugLine="parser.Parse(Job.GetInputStream,\"P_eingabe\")";
Debug.ShouldStop(524288);
__ref.getField(false,"_parser").runVoidMethodAndSync ("Parse",(Object)((_job.runMethod(false,"_getinputstream").getObject())),(Object)(RemoteObject.createImmutable("P_eingabe")));
 BA.debugLineNum = 117;BA.debugLine="update_send.Initialize";
Debug.ShouldStop(1048576);
__ref.getField(false,"_update_send").runClassMethod (b4a.example.update_sender.class, "_initialize",__ref.getField(false, "ba"));
 BA.debugLineNum = 118;BA.debugLine="update_send.send_quit";
Debug.ShouldStop(2097152);
__ref.getField(false,"_update_send").runClassMethod (b4a.example.update_sender.class, "_send_quit");
 BA.debugLineNum = 119;BA.debugLine="Main.aftersend=True";
Debug.ShouldStop(4194304);
sp_single._main._aftersend = sp_single.__c.getField(true,"True");
 BA.debugLineNum = 121;BA.debugLine="StartActivity(Main.ac_to_start)";
Debug.ShouldStop(16777216);
sp_single.__c.runVoidMethod ("StartActivity",__ref.getField(false, "ba"),(Object)((sp_single._main._ac_to_start)));
 break; }
}
;
 }else {
 BA.debugLineNum = 126;BA.debugLine="Select Case Job.JobName";
Debug.ShouldStop(536870912);
switch (BA.switchObjectToInt(_job.getField(true,"_jobname"),BA.ObjectToString("send_eingabe"),BA.ObjectToString("logincheck"))) {
case 0: {
 BA.debugLineNum = 128;BA.debugLine="parser.Parse(Job.GetInputStream,\"P_eingabe\")";
Debug.ShouldStop(-2147483648);
__ref.getField(false,"_parser").runVoidMethodAndSync ("Parse",(Object)((_job.runMethod(false,"_getinputstream").getObject())),(Object)(RemoteObject.createImmutable("P_eingabe")));
 BA.debugLineNum = 129;BA.debugLine="Main.aftersend=True";
Debug.ShouldStop(1);
sp_single._main._aftersend = sp_single.__c.getField(true,"True");
 break; }
case 1: {
 BA.debugLineNum = 132;BA.debugLine="If SubExists(Modules,EN) Then";
Debug.ShouldStop(8);
if (sp_single.__c.runMethod(true,"SubExists",__ref.getField(false, "ba"),(Object)(__ref.getField(false,"_modules")),(Object)(__ref.getField(true,"_en"))).<Boolean>get().booleanValue()) { 
 BA.debugLineNum = 133;BA.debugLine="CallSubDelayed2(Modules,EN,Job.ErrorMessage)";
Debug.ShouldStop(16);
sp_single.__c.runVoidMethod ("CallSubDelayed2",__ref.getField(false, "ba"),(Object)(__ref.getField(false,"_modules")),(Object)(__ref.getField(true,"_en")),(Object)((_job.getField(true,"_errormessage"))));
 };
 BA.debugLineNum = 139;BA.debugLine="Try";
Debug.ShouldStop(1024);
try { Debug.CheckDeviceExceptions();
} 
       catch (Exception e28) {
			BA.rdebugUtils.runVoidMethod("setLastException",__ref.getField(false, "ba"), e28.toString()); BA.debugLineNum = 144;BA.debugLine="Log(LastException)";
Debug.ShouldStop(32768);
sp_single.__c.runVoidMethod ("Log",(Object)(BA.ObjectToString(sp_single.__c.runMethod(false,"LastException",__ref.runMethod(false,"getActivityBA")))));
 };
 break; }
}
;
 };
 BA.debugLineNum = 155;BA.debugLine="End Sub";
Debug.ShouldStop(67108864);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _p_eingabe_endelement(RemoteObject __ref,RemoteObject _uri,RemoteObject _name,RemoteObject _text) throws Exception{
try {
		Debug.PushSubsStack("P_eingabe_EndElement (sp_single) ","sp_single",3,__ref.getField(false, "ba"),__ref,33);
if (RapidSub.canDelegate("p_eingabe_endelement")) return __ref.runUserSub(false, "sp_single","p_eingabe_endelement", __ref, _uri, _name, _text);
RemoteObject _node = RemoteObject.createImmutable("");
Debug.locals.put("Uri", _uri);
Debug.locals.put("Name", _name);
Debug.locals.put("Text", _text);
 BA.debugLineNum = 33;BA.debugLine="Sub P_eingabe_EndElement (Uri As String, Name As S";
Debug.ShouldStop(1);
 BA.debugLineNum = 34;BA.debugLine="Dim node As String= method & \"Response\"";
Debug.ShouldStop(2);
_node = RemoteObject.concat(__ref.getField(true,"_method"),RemoteObject.createImmutable("Response"));Debug.locals.put("node", _node);Debug.locals.put("node", _node);
 BA.debugLineNum = 35;BA.debugLine="If parser.Parents.IndexOf(node) > -1 Then";
Debug.ShouldStop(4);
if (RemoteObject.solveBoolean(">",__ref.getField(false,"_parser").getField(false,"Parents").runMethod(true,"IndexOf",(Object)((_node))),BA.numberCast(double.class, -(double) (0 + 1)))) { 
 BA.debugLineNum = 36;BA.debugLine="node = method & \"Result\"";
Debug.ShouldStop(8);
_node = RemoteObject.concat(__ref.getField(true,"_method"),RemoteObject.createImmutable("Result"));Debug.locals.put("node", _node);
 BA.debugLineNum = 37;BA.debugLine="Select Case Name";
Debug.ShouldStop(16);
switch (BA.switchObjectToInt(_name,_node)) {
case 0: {
 BA.debugLineNum = 39;BA.debugLine="Try";
Debug.ShouldStop(64);
try { BA.debugLineNum = 40;BA.debugLine="Main.sql1.ExecNonQuery(\"Update tbl_nachtrag_lo";
Debug.ShouldStop(128);
sp_single._main._sql1.runVoidMethod ("ExecNonQuery",(Object)(RemoteObject.concat(RemoteObject.createImmutable("Update tbl_nachtrag_log_app set sync=1 where id ="),_text.runMethod(true,"ToString"))));
 BA.debugLineNum = 41;BA.debugLine="Log(Main.sql1.ExecQuerySingleResult(\"Select cou";
Debug.ShouldStop(256);
sp_single.__c.runVoidMethod ("Log",(Object)(sp_single._main._sql1.runMethod(true,"ExecQuerySingleResult",(Object)(RemoteObject.createImmutable("Select count(sync) from tbl_nachtrag_log_app where sync=1")))));
 Debug.CheckDeviceExceptions();
} 
       catch (Exception e10) {
			BA.rdebugUtils.runVoidMethod("setLastException",__ref.getField(false, "ba"), e10.toString()); BA.debugLineNum = 43;BA.debugLine="CallSubDelayed2(Modules,EN,Text.ToString)";
Debug.ShouldStop(1024);
sp_single.__c.runVoidMethod ("CallSubDelayed2",__ref.getField(false, "ba"),(Object)(__ref.getField(false,"_modules")),(Object)(__ref.getField(true,"_en")),(Object)((_text.runMethod(true,"ToString"))));
 BA.debugLineNum = 44;BA.debugLine="Log(LastException)";
Debug.ShouldStop(2048);
sp_single.__c.runVoidMethod ("Log",(Object)(BA.ObjectToString(sp_single.__c.runMethod(false,"LastException",__ref.runMethod(false,"getActivityBA")))));
 };
 break; }
}
;
 };
 BA.debugLineNum = 53;BA.debugLine="End Sub";
Debug.ShouldStop(1048576);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _p_logincheck_endelement(RemoteObject __ref,RemoteObject _uri,RemoteObject _name,RemoteObject _text) throws Exception{
try {
		Debug.PushSubsStack("P_logincheck_EndElement (sp_single) ","sp_single",3,__ref.getField(false, "ba"),__ref,22);
if (RapidSub.canDelegate("p_logincheck_endelement")) return __ref.runUserSub(false, "sp_single","p_logincheck_endelement", __ref, _uri, _name, _text);
Debug.locals.put("Uri", _uri);
Debug.locals.put("Name", _name);
Debug.locals.put("Text", _text);
 BA.debugLineNum = 22;BA.debugLine="Sub P_logincheck_EndElement (Uri As String, Name A";
Debug.ShouldStop(2097152);
 BA.debugLineNum = 23;BA.debugLine="If parser.Parents.IndexOf(\"logincheckResponse\")";
Debug.ShouldStop(4194304);
if (RemoteObject.solveBoolean(">",__ref.getField(false,"_parser").getField(false,"Parents").runMethod(true,"IndexOf",(Object)((RemoteObject.createImmutable("logincheckResponse")))),BA.numberCast(double.class, -(double) (0 + 1)))) { 
 BA.debugLineNum = 24;BA.debugLine="Select Case Name";
Debug.ShouldStop(8388608);
switch (BA.switchObjectToInt(_name,BA.ObjectToString("logincheckResult"))) {
case 0: {
 BA.debugLineNum = 26;BA.debugLine="If SubExists(Modules,EN) Then";
Debug.ShouldStop(33554432);
if (sp_single.__c.runMethod(true,"SubExists",__ref.getField(false, "ba"),(Object)(__ref.getField(false,"_modules")),(Object)(__ref.getField(true,"_en"))).<Boolean>get().booleanValue()) { 
 BA.debugLineNum = 27;BA.debugLine="CallSubDelayed2(Modules,EN,Text.ToString)";
Debug.ShouldStop(67108864);
sp_single.__c.runVoidMethod ("CallSubDelayed2",__ref.getField(false, "ba"),(Object)(__ref.getField(false,"_modules")),(Object)(__ref.getField(true,"_en")),(Object)((_text.runMethod(true,"ToString"))));
 };
 break; }
}
;
 };
 BA.debugLineNum = 31;BA.debugLine="End Sub";
Debug.ShouldStop(1073741824);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _sendrequest(RemoteObject __ref,RemoteObject _methodname,RemoteObject _jobname,RemoteObject _last) throws Exception{
try {
		Debug.PushSubsStack("SendRequest (sp_single) ","sp_single",3,__ref.getField(false, "ba"),__ref,75);
if (RapidSub.canDelegate("sendrequest")) return __ref.runUserSub(false, "sp_single","sendrequest", __ref, _methodname, _jobname, _last);
RemoteObject _ht1 = RemoteObject.declareNull("anywheresoftware.b4a.samples.httputils2.httpjob");
Debug.locals.put("MethodName", _methodname);
Debug.locals.put("jobname", _jobname);
Debug.locals.put("last", _last);
 BA.debugLineNum = 75;BA.debugLine="Sub SendRequest( MethodName As String,jobname As S";
Debug.ShouldStop(1024);
 BA.debugLineNum = 77;BA.debugLine="last1=last";
Debug.ShouldStop(4096);
__ref.setField ("_last1",_last);
 BA.debugLineNum = 78;BA.debugLine="xml_sendstriing= xml_sendstriing & \"</\" & Meth";
Debug.ShouldStop(8192);
__ref.setField ("_xml_sendstriing",RemoteObject.concat(__ref.getField(true,"_xml_sendstriing"),RemoteObject.createImmutable("</"),_methodname,RemoteObject.createImmutable(">")));
 BA.debugLineNum = 79;BA.debugLine="xml_sendstriing= xml_sendstriing & \"</soap:Body>";
Debug.ShouldStop(16384);
__ref.setField ("_xml_sendstriing",RemoteObject.concat(__ref.getField(true,"_xml_sendstriing"),RemoteObject.createImmutable("</soap:Body>")));
 BA.debugLineNum = 80;BA.debugLine="xml_sendstriing= xml_sendstriing & \"</soap:Envelop";
Debug.ShouldStop(32768);
__ref.setField ("_xml_sendstriing",RemoteObject.concat(__ref.getField(true,"_xml_sendstriing"),RemoteObject.createImmutable("</soap:Envelope>")));
 BA.debugLineNum = 81;BA.debugLine="Log(xml_sendstriing)";
Debug.ShouldStop(65536);
sp_single.__c.runVoidMethod ("Log",(Object)(__ref.getField(true,"_xml_sendstriing")));
 BA.debugLineNum = 82;BA.debugLine="Log(\"string ist fertig\")";
Debug.ShouldStop(131072);
sp_single.__c.runVoidMethod ("Log",(Object)(RemoteObject.createImmutable("string ist fertig")));
 BA.debugLineNum = 83;BA.debugLine="Dim ht1 As HttpJob";
Debug.ShouldStop(262144);
_ht1 = RemoteObject.createNew ("anywheresoftware.b4a.samples.httputils2.httpjob");Debug.locals.put("ht1", _ht1);
 BA.debugLineNum = 84;BA.debugLine="ht1.Initialize(jobname,Me)";
Debug.ShouldStop(524288);
_ht1.runVoidMethod ("_initialize",__ref.getField(false, "ba"),(Object)(_jobname),(Object)(__ref));
 BA.debugLineNum = 86;BA.debugLine="ht1.PostString(UrlWS,xml_sendstriing)";
Debug.ShouldStop(2097152);
_ht1.runVoidMethod ("_poststring",(Object)(__ref.getField(true,"_urlws")),(Object)(__ref.getField(true,"_xml_sendstriing")));
 BA.debugLineNum = 89;BA.debugLine="ht1.GetRequest.SetHeader(\"SoapAction\", \"http://te";
Debug.ShouldStop(16777216);
_ht1.runMethod(false,"_getrequest").runVoidMethod ("SetHeader",(Object)(BA.ObjectToString("SoapAction")),(Object)(RemoteObject.concat(RemoteObject.createImmutable("http://tempuri.org/"),_methodname,RemoteObject.createImmutable(""))));
 BA.debugLineNum = 92;BA.debugLine="ht1.GetRequest.SetContentType(\"text/xml; charset=";
Debug.ShouldStop(134217728);
_ht1.runMethod(false,"_getrequest").runVoidMethod ("SetContentType",(Object)(RemoteObject.createImmutable("text/xml; charset=utf-8")));
 BA.debugLineNum = 93;BA.debugLine="ht1.GetRequest.Timeout=2000";
Debug.ShouldStop(268435456);
_ht1.runMethod(false,"_getrequest").runVoidMethod ("setTimeout",BA.numberCast(int.class, 2000));
 BA.debugLineNum = 95;BA.debugLine="End Sub";
Debug.ShouldStop(1073741824);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
}