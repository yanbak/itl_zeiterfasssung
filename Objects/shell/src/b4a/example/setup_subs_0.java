package b4a.example;

import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.pc.*;

public class setup_subs_0 {


public static RemoteObject  _activity_create(RemoteObject _firsttime) throws Exception{
try {
		Debug.PushSubsStack("Activity_Create (setup) ","setup",5,setup.mostCurrent.activityBA,setup.mostCurrent,29);
if (RapidSub.canDelegate("activity_create")) return b4a.example.setup.remoteMe.runUserSub(false, "setup","activity_create", _firsttime);
RemoteObject _ph = RemoteObject.declareNull("anywheresoftware.b4a.phone.Phone");
RemoteObject _usr_db = RemoteObject.createImmutable("");
Debug.locals.put("FirstTime", _firsttime);
 BA.debugLineNum = 29;BA.debugLine="Sub Activity_Create(FirstTime As Boolean)";
Debug.ShouldStop(268435456);
 BA.debugLineNum = 33;BA.debugLine="Dim ph As Phone";
Debug.ShouldStop(1);
_ph = RemoteObject.createNew ("anywheresoftware.b4a.phone.Phone");Debug.locals.put("ph", _ph);
 BA.debugLineNum = 34;BA.debugLine="If scale.GetDevicePhysicalSize < 6 Then";
Debug.ShouldStop(2);
if (RemoteObject.solveBoolean("<",setup.mostCurrent._scale.runMethod(true,"_getdevicephysicalsize",setup.mostCurrent.activityBA),BA.numberCast(double.class, 6))) { 
 BA.debugLineNum = 35;BA.debugLine="ph.SetScreenOrientation(1)";
Debug.ShouldStop(4);
_ph.runVoidMethod ("SetScreenOrientation",setup.processBA,(Object)(BA.numberCast(int.class, 1)));
 }else {
 BA.debugLineNum = 37;BA.debugLine="ph.SetScreenOrientation(-1)";
Debug.ShouldStop(16);
_ph.runVoidMethod ("SetScreenOrientation",setup.processBA,(Object)(BA.numberCast(int.class, -(double) (0 + 1))));
 };
 BA.debugLineNum = 40;BA.debugLine="Activity.LoadLayout(\"setup\")";
Debug.ShouldStop(128);
setup.mostCurrent._activity.runMethodAndSync(false,"LoadLayout",(Object)(RemoteObject.createImmutable("setup")),setup.mostCurrent.activityBA);
 BA.debugLineNum = 41;BA.debugLine="Try";
Debug.ShouldStop(256);
try { BA.debugLineNum = 42;BA.debugLine="Dim usr_db As String";
Debug.ShouldStop(512);
_usr_db = RemoteObject.createImmutable("");Debug.locals.put("usr_db", _usr_db);
 BA.debugLineNum = 43;BA.debugLine="usr_db =Main.sql1.ExecQuerySingleResult(\"Select";
Debug.ShouldStop(1024);
_usr_db = setup.mostCurrent._main._sql1.runMethod(true,"ExecQuerySingleResult",(Object)(RemoteObject.createImmutable("Select min(username) from tbl_usr")));Debug.locals.put("usr_db", _usr_db);
 BA.debugLineNum = 44;BA.debugLine="If usr_db <> \"null\" Then";
Debug.ShouldStop(2048);
if (RemoteObject.solveBoolean("!",_usr_db,BA.ObjectToString("null"))) { 
 BA.debugLineNum = 45;BA.debugLine="EditText1.text=usr_db";
Debug.ShouldStop(4096);
setup.mostCurrent._edittext1.runMethodAndSync(true,"setText",BA.ObjectToCharSequence(_usr_db));
 };
 Debug.CheckDeviceExceptions();
} 
       catch (Exception e15) {
			BA.rdebugUtils.runVoidMethod("setLastException",setup.processBA, e15.toString()); };
 BA.debugLineNum = 52;BA.debugLine="scale.ScaleAll(Panel1,True)";
Debug.ShouldStop(524288);
setup.mostCurrent._scale.runVoidMethod ("_scaleall",setup.mostCurrent.activityBA,RemoteObject.declareNull("anywheresoftware.b4a.AbsObjectWrapper").runMethod(false, "ConvertToWrapper", RemoteObject.createNew("anywheresoftware.b4a.objects.ActivityWrapper"), setup.mostCurrent._panel1.getObject()),(Object)(setup.mostCurrent.__c.getField(true,"True")));
 BA.debugLineNum = 53;BA.debugLine="End Sub";
Debug.ShouldStop(1048576);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _activity_pause(RemoteObject _userclosed) throws Exception{
try {
		Debug.PushSubsStack("Activity_Pause (setup) ","setup",5,setup.mostCurrent.activityBA,setup.mostCurrent,59);
if (RapidSub.canDelegate("activity_pause")) return b4a.example.setup.remoteMe.runUserSub(false, "setup","activity_pause", _userclosed);
Debug.locals.put("UserClosed", _userclosed);
 BA.debugLineNum = 59;BA.debugLine="Sub Activity_Pause (UserClosed As Boolean)";
Debug.ShouldStop(67108864);
 BA.debugLineNum = 61;BA.debugLine="End Sub";
Debug.ShouldStop(268435456);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _activity_resume() throws Exception{
try {
		Debug.PushSubsStack("Activity_Resume (setup) ","setup",5,setup.mostCurrent.activityBA,setup.mostCurrent,55);
if (RapidSub.canDelegate("activity_resume")) return b4a.example.setup.remoteMe.runUserSub(false, "setup","activity_resume");
 BA.debugLineNum = 55;BA.debugLine="Sub Activity_Resume";
Debug.ShouldStop(4194304);
 BA.debugLineNum = 57;BA.debugLine="End Sub";
Debug.ShouldStop(16777216);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _bu_impressum_click() throws Exception{
try {
		Debug.PushSubsStack("bu_impressum_Click (setup) ","setup",5,setup.mostCurrent.activityBA,setup.mostCurrent,125);
if (RapidSub.canDelegate("bu_impressum_click")) return b4a.example.setup.remoteMe.runUserSub(false, "setup","bu_impressum_click");
RemoteObject _p = RemoteObject.declareNull("anywheresoftware.b4a.phone.Phone.PhoneIntents");
 BA.debugLineNum = 125;BA.debugLine="Sub bu_impressum_Click";
Debug.ShouldStop(268435456);
 BA.debugLineNum = 126;BA.debugLine="Dim p As PhoneIntents";
Debug.ShouldStop(536870912);
_p = RemoteObject.createNew ("anywheresoftware.b4a.phone.Phone.PhoneIntents");Debug.locals.put("p", _p);
 BA.debugLineNum = 127;BA.debugLine="StartActivity(p.OpenBrowser(\"https://www.klein";
Debug.ShouldStop(1073741824);
setup.mostCurrent.__c.runVoidMethod ("StartActivity",setup.processBA,(Object)((_p.runMethod(false,"OpenBrowser",(Object)(RemoteObject.createImmutable("https://www.kleinmengenlogistik.de/impressum.html"))))));
 BA.debugLineNum = 128;BA.debugLine="End Sub";
Debug.ShouldStop(-2147483648);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _bu_reset_click() throws Exception{
try {
		Debug.PushSubsStack("bu_reset_Click (setup) ","setup",5,setup.mostCurrent.activityBA,setup.mostCurrent,114);
if (RapidSub.canDelegate("bu_reset_click")) return b4a.example.setup.remoteMe.runUserSub(false, "setup","bu_reset_click");
RemoteObject _u1 = RemoteObject.declareNull("b4a.example.update_sender_main");
 BA.debugLineNum = 114;BA.debugLine="Sub bu_reset_Click";
Debug.ShouldStop(131072);
 BA.debugLineNum = 115;BA.debugLine="Dim u1 As 	update_sender_main";
Debug.ShouldStop(262144);
_u1 = RemoteObject.createNew ("b4a.example.update_sender_main");Debug.locals.put("u1", _u1);
 BA.debugLineNum = 116;BA.debugLine="u1.Initialize";
Debug.ShouldStop(524288);
_u1.runClassMethod (b4a.example.update_sender_main.class, "_initialize",setup.processBA);
 BA.debugLineNum = 118;BA.debugLine="s2.Initialize(Main.use_service &\"?op=get_version\"";
Debug.ShouldStop(2097152);
setup._s2.runClassMethod (b4a.example.sp_tabelle.class, "_initialize",setup.processBA,(Object)(RemoteObject.concat(setup.mostCurrent._main._use_service,RemoteObject.createImmutable("?op=get_version"))),(Object)(BA.ObjectToString("get_version")),(Object)(setup.getObject()),(Object)(RemoteObject.createImmutable("xx")));
 BA.debugLineNum = 119;BA.debugLine="s2.AddField(\"usr\",s1.TYPE_STRING,u1.usr)";
Debug.ShouldStop(4194304);
setup._s2.runClassMethod (b4a.example.sp_tabelle.class, "_addfield",(Object)(BA.ObjectToString("usr")),(Object)(setup._s1.getField(true,"_type_string")),(Object)(_u1.getField(true,"_usr")));
 BA.debugLineNum = 120;BA.debugLine="s2.AddField(\"pwd\",s1.TYPE_STRING,u1.pwd)";
Debug.ShouldStop(8388608);
setup._s2.runClassMethod (b4a.example.sp_tabelle.class, "_addfield",(Object)(BA.ObjectToString("pwd")),(Object)(setup._s1.getField(true,"_type_string")),(Object)(_u1.getField(true,"_pwd")));
 BA.debugLineNum = 121;BA.debugLine="s2.SendRequest(\"get_version\",\"get_version\")";
Debug.ShouldStop(16777216);
setup._s2.runClassMethod (b4a.example.sp_tabelle.class, "_sendrequest",(Object)(BA.ObjectToString("get_version")),(Object)(RemoteObject.createImmutable("get_version")));
 BA.debugLineNum = 122;BA.debugLine="End Sub";
Debug.ShouldStop(33554432);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _button1_click() throws Exception{
try {
		Debug.PushSubsStack("Button1_Click (setup) ","setup",5,setup.mostCurrent.activityBA,setup.mostCurrent,64);
if (RapidSub.canDelegate("button1_click")) return b4a.example.setup.remoteMe.runUserSub(false, "setup","button1_click");
 BA.debugLineNum = 64;BA.debugLine="Sub Button1_Click";
Debug.ShouldStop(-2147483648);
 BA.debugLineNum = 67;BA.debugLine="Main.sf.Initialize";
Debug.ShouldStop(4);
setup.mostCurrent._main._sf.runVoidMethod ("_initialize",setup.processBA);
 BA.debugLineNum = 69;BA.debugLine="If Main.sf.len(EditText1.text) > 1 Then";
Debug.ShouldStop(16);
if (RemoteObject.solveBoolean(">",setup.mostCurrent._main._sf.runMethod(true,"_vvv7",(Object)(setup.mostCurrent._edittext1.runMethod(true,"getText"))),BA.numberCast(double.class, 1))) { 
 BA.debugLineNum = 71;BA.debugLine="If Main.sf.len(EditText2.Text) >1 Then";
Debug.ShouldStop(64);
if (RemoteObject.solveBoolean(">",setup.mostCurrent._main._sf.runMethod(true,"_vvv7",(Object)(setup.mostCurrent._edittext2.runMethod(true,"getText"))),BA.numberCast(double.class, 1))) { 
 BA.debugLineNum = 73;BA.debugLine="Try";
Debug.ShouldStop(256);
try { BA.debugLineNum = 76;BA.debugLine="s1.Initialize(Main.use_service &\"?op=loginchec";
Debug.ShouldStop(2048);
setup._s1.runClassMethod (b4a.example.sp_single.class, "_initialize",setup.processBA,(Object)(RemoteObject.concat(setup.mostCurrent._main._use_service,RemoteObject.createImmutable("?op=logincheck"))),(Object)(BA.ObjectToString("logincheck")),(Object)(setup.getObject()),(Object)(RemoteObject.createImmutable("Result")));
 BA.debugLineNum = 77;BA.debugLine="s1.AddField(\"usr\",s1.TYPE_STRING,EditText1.Text)";
Debug.ShouldStop(4096);
setup._s1.runClassMethod (b4a.example.sp_single.class, "_addfield",(Object)(BA.ObjectToString("usr")),(Object)(setup._s1.getField(true,"_type_string")),(Object)(setup.mostCurrent._edittext1.runMethod(true,"getText")));
 BA.debugLineNum = 78;BA.debugLine="s1.AddField(\"pwd\",s1.TYPE_STRING,EditText2.Text)";
Debug.ShouldStop(8192);
setup._s1.runClassMethod (b4a.example.sp_single.class, "_addfield",(Object)(BA.ObjectToString("pwd")),(Object)(setup._s1.getField(true,"_type_string")),(Object)(setup.mostCurrent._edittext2.runMethod(true,"getText")));
 BA.debugLineNum = 79;BA.debugLine="s1.SendRequest(\"logincheck\",\"logincheck\",False)";
Debug.ShouldStop(16384);
setup._s1.runClassMethod (b4a.example.sp_single.class, "_sendrequest",(Object)(BA.ObjectToString("logincheck")),(Object)(BA.ObjectToString("logincheck")),(Object)(setup.mostCurrent.__c.getField(true,"False")));
 Debug.CheckDeviceExceptions();
} 
       catch (Exception e10) {
			BA.rdebugUtils.runVoidMethod("setLastException",setup.processBA, e10.toString()); BA.debugLineNum = 81;BA.debugLine="Msgbox(LastException,\"\")";
Debug.ShouldStop(65536);
setup.mostCurrent.__c.runVoidMethodAndSync ("Msgbox",(Object)(BA.ObjectToCharSequence(setup.mostCurrent.__c.runMethod(false,"LastException",setup.mostCurrent.activityBA).getObject())),(Object)(BA.ObjectToCharSequence(RemoteObject.createImmutable(""))),setup.mostCurrent.activityBA);
 };
 }else {
 BA.debugLineNum = 84;BA.debugLine="Msgbox(\"Passwort zu kurz\",\"Fehler\")";
Debug.ShouldStop(524288);
setup.mostCurrent.__c.runVoidMethodAndSync ("Msgbox",(Object)(BA.ObjectToCharSequence("Passwort zu kurz")),(Object)(BA.ObjectToCharSequence(RemoteObject.createImmutable("Fehler"))),setup.mostCurrent.activityBA);
 };
 }else {
 BA.debugLineNum = 89;BA.debugLine="Msgbox(\"Benutzername zu kurz\",\"Fehler\")";
Debug.ShouldStop(16777216);
setup.mostCurrent.__c.runVoidMethodAndSync ("Msgbox",(Object)(BA.ObjectToCharSequence("Benutzername zu kurz")),(Object)(BA.ObjectToCharSequence(RemoteObject.createImmutable("Fehler"))),setup.mostCurrent.activityBA);
 };
 BA.debugLineNum = 95;BA.debugLine="End Sub";
Debug.ShouldStop(1073741824);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _globals() throws Exception{
 //BA.debugLineNum = 13;BA.debugLine="Sub Globals";
 //BA.debugLineNum = 17;BA.debugLine="Private EditText1 As EditText";
setup.mostCurrent._edittext1 = RemoteObject.createNew ("anywheresoftware.b4a.objects.EditTextWrapper");
 //BA.debugLineNum = 18;BA.debugLine="Private EditText2 As EditText";
setup.mostCurrent._edittext2 = RemoteObject.createNew ("anywheresoftware.b4a.objects.EditTextWrapper");
 //BA.debugLineNum = 19;BA.debugLine="Private Label1 As Label";
setup.mostCurrent._label1 = RemoteObject.createNew ("anywheresoftware.b4a.objects.LabelWrapper");
 //BA.debugLineNum = 20;BA.debugLine="Private Label2 As Label";
setup.mostCurrent._label2 = RemoteObject.createNew ("anywheresoftware.b4a.objects.LabelWrapper");
 //BA.debugLineNum = 21;BA.debugLine="Private Label3 As Label";
setup.mostCurrent._label3 = RemoteObject.createNew ("anywheresoftware.b4a.objects.LabelWrapper");
 //BA.debugLineNum = 22;BA.debugLine="Private Panel1 As Panel";
setup.mostCurrent._panel1 = RemoteObject.createNew ("anywheresoftware.b4a.objects.PanelWrapper");
 //BA.debugLineNum = 23;BA.debugLine="Private Button1 As Button";
setup.mostCurrent._button1 = RemoteObject.createNew ("anywheresoftware.b4a.objects.ButtonWrapper");
 //BA.debugLineNum = 24;BA.debugLine="Private bu_reset As Button";
setup.mostCurrent._bu_reset = RemoteObject.createNew ("anywheresoftware.b4a.objects.ButtonWrapper");
 //BA.debugLineNum = 27;BA.debugLine="End Sub";
return RemoteObject.createImmutable("");
}
public static RemoteObject  _process_globals() throws Exception{
 //BA.debugLineNum = 6;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 9;BA.debugLine="Dim s1 As sp_single";
setup._s1 = RemoteObject.createNew ("b4a.example.sp_single");
 //BA.debugLineNum = 10;BA.debugLine="Dim s2 As sp_tabelle";
setup._s2 = RemoteObject.createNew ("b4a.example.sp_tabelle");
 //BA.debugLineNum = 11;BA.debugLine="End Sub";
return RemoteObject.createImmutable("");
}
public static RemoteObject  _result(RemoteObject _res) throws Exception{
try {
		Debug.PushSubsStack("result (setup) ","setup",5,setup.mostCurrent.activityBA,setup.mostCurrent,96);
if (RapidSub.canDelegate("result")) return b4a.example.setup.remoteMe.runUserSub(false, "setup","result", _res);
RemoteObject _sb = RemoteObject.declareNull("anywheresoftware.b4a.keywords.StringBuilderWrapper");
Debug.locals.put("res", _res);
 BA.debugLineNum = 96;BA.debugLine="Sub result (res As Object)";
Debug.ShouldStop(-2147483648);
 BA.debugLineNum = 97;BA.debugLine="If res=\"Anmeldung erfolgreich\" Then";
Debug.ShouldStop(1);
if (RemoteObject.solveBoolean("=",_res,RemoteObject.createImmutable(("Anmeldung erfolgreich")))) { 
 BA.debugLineNum = 99;BA.debugLine="Msgbox(res,\"Erfolg\")";
Debug.ShouldStop(4);
setup.mostCurrent.__c.runVoidMethodAndSync ("Msgbox",(Object)(BA.ObjectToCharSequence(_res)),(Object)(BA.ObjectToCharSequence(RemoteObject.createImmutable("Erfolg"))),setup.mostCurrent.activityBA);
 BA.debugLineNum = 100;BA.debugLine="Dim sb As StringBuilder";
Debug.ShouldStop(8);
_sb = RemoteObject.createNew ("anywheresoftware.b4a.keywords.StringBuilderWrapper");Debug.locals.put("sb", _sb);
 BA.debugLineNum = 101;BA.debugLine="sb.initialize";
Debug.ShouldStop(16);
_sb.runVoidMethod ("Initialize");
 BA.debugLineNum = 102;BA.debugLine="sb.Append(\"insert into tbl_usr (username,pw) \")";
Debug.ShouldStop(32);
_sb.runVoidMethod ("Append",(Object)(RemoteObject.createImmutable("insert into tbl_usr (username,pw) ")));
 BA.debugLineNum = 103;BA.debugLine="sb.Append(\"values ('\" & EditText1.Text & \"',\")";
Debug.ShouldStop(64);
_sb.runVoidMethod ("Append",(Object)(RemoteObject.concat(RemoteObject.createImmutable("values ('"),setup.mostCurrent._edittext1.runMethod(true,"getText"),RemoteObject.createImmutable("',"))));
 BA.debugLineNum = 104;BA.debugLine="sb.Append(\"'\" & EditText2.Text & \"');\")";
Debug.ShouldStop(128);
_sb.runVoidMethod ("Append",(Object)(RemoteObject.concat(RemoteObject.createImmutable("'"),setup.mostCurrent._edittext2.runMethod(true,"getText"),RemoteObject.createImmutable("');"))));
 BA.debugLineNum = 105;BA.debugLine="Main.sql1.ExecNonQuery(\"Delete from tbl_usr\")";
Debug.ShouldStop(256);
setup.mostCurrent._main._sql1.runVoidMethod ("ExecNonQuery",(Object)(RemoteObject.createImmutable("Delete from tbl_usr")));
 BA.debugLineNum = 106;BA.debugLine="Main.sql1.ExecNonQuery(sb.ToString)";
Debug.ShouldStop(512);
setup.mostCurrent._main._sql1.runVoidMethod ("ExecNonQuery",(Object)(_sb.runMethod(true,"ToString")));
 BA.debugLineNum = 107;BA.debugLine="Main.anmeldung=True";
Debug.ShouldStop(1024);
setup.mostCurrent._main._anmeldung = setup.mostCurrent.__c.getField(true,"True");
 }else {
 BA.debugLineNum = 110;BA.debugLine="Msgbox(res,\"Fehler\")";
Debug.ShouldStop(8192);
setup.mostCurrent.__c.runVoidMethodAndSync ("Msgbox",(Object)(BA.ObjectToCharSequence(_res)),(Object)(BA.ObjectToCharSequence(RemoteObject.createImmutable("Fehler"))),setup.mostCurrent.activityBA);
 };
 BA.debugLineNum = 113;BA.debugLine="End Sub";
Debug.ShouldStop(65536);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
}