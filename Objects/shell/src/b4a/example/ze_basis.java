
package b4a.example;

import java.io.IOException;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.pc.PCBA;
import anywheresoftware.b4a.pc.RDebug;
import anywheresoftware.b4a.pc.RemoteObject;
import anywheresoftware.b4a.pc.RDebug.IRemote;
import anywheresoftware.b4a.pc.Debug;
import anywheresoftware.b4a.pc.B4XTypes.B4XClass;
import anywheresoftware.b4a.pc.B4XTypes.DeviceClass;

public class ze_basis implements IRemote{
	public static ze_basis mostCurrent;
	public static RemoteObject processBA;
    public static boolean processGlobalsRun;
    public static RemoteObject myClass;
    public static RemoteObject remoteMe;
	public ze_basis() {
		mostCurrent = this;
	}
    public RemoteObject getRemoteMe() {
        return remoteMe;    
    }
    
	public static void main (String[] args) throws Exception {
		new RDebug(args[0], Integer.parseInt(args[1]), Integer.parseInt(args[2]), args[3]);
		RDebug.INSTANCE.waitForTask();

	}
    static {
        anywheresoftware.b4a.pc.RapidSub.moduleToObject.put(new B4XClass("ze_basis"), "b4a.example.ze_basis");
	}

public boolean isSingleton() {
		return true;
	}
     public static RemoteObject getObject() {
		return myClass;
	 }

	public RemoteObject activityBA;
	public RemoteObject _activity;
    private PCBA pcBA;

	public PCBA create(Object[] args) throws ClassNotFoundException{
		processBA = (RemoteObject) args[1];
		activityBA = (RemoteObject) args[2];
		_activity = (RemoteObject) args[3];
        anywheresoftware.b4a.keywords.Common.Density = (Float)args[4];
        remoteMe = (RemoteObject) args[5];
		pcBA = new PCBA(this, ze_basis.class);
        main_subs_0.initializeProcessGlobals();
		return pcBA;
	}
public static RemoteObject __c = RemoteObject.declareNull("anywheresoftware.b4a.keywords.Common");
public static RemoteObject _u1 = RemoteObject.declareNull("b4a.example.update_sender_main");
public static RemoteObject _saxparser = RemoteObject.declareNull("anywheresoftware.b4a.objects.SaxParser");
public static RemoteObject _status = RemoteObject.createImmutable("");
public static RemoteObject _bu_zeit = RemoteObject.declareNull("anywheresoftware.b4a.objects.ButtonWrapper");
public static RemoteObject _httputils2service = RemoteObject.declareNull("anywheresoftware.b4a.samples.httputils2.httputils2service");
public static b4a.example.main _main = null;
public static b4a.example.starter _starter = null;
public static b4a.example.scale _scale = null;
public static b4a.example.setup _setup = null;
  public Object[] GetGlobals() {
		return new Object[] {"Activity",ze_basis.mostCurrent._activity,"bu_zeit",ze_basis.mostCurrent._bu_zeit,"HttpUtils2Service",ze_basis.mostCurrent._httputils2service,"Main",Debug.moduleToString(b4a.example.main.class),"SaxParser",ze_basis._saxparser,"scale",Debug.moduleToString(b4a.example.scale.class),"setup",Debug.moduleToString(b4a.example.setup.class),"Starter",Debug.moduleToString(b4a.example.starter.class),"status",ze_basis.mostCurrent._status,"u1",ze_basis._u1};
}
}