package b4a.example;

import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.pc.*;

public class sp_tabelle_subs_0 {


public static RemoteObject  _addfield(RemoteObject __ref,RemoteObject _fieldname,RemoteObject _fieldtype,RemoteObject _fieldvalue) throws Exception{
try {
		Debug.PushSubsStack("AddField (sp_tabelle) ","sp_tabelle",4,__ref.getField(false, "ba"),__ref,97);
if (RapidSub.canDelegate("addfield")) return __ref.runUserSub(false, "sp_tabelle","addfield", __ref, _fieldname, _fieldtype, _fieldvalue);
Debug.locals.put("FieldName", _fieldname);
Debug.locals.put("FieldType", _fieldtype);
Debug.locals.put("FieldValue", _fieldvalue);
 BA.debugLineNum = 97;BA.debugLine="Public Sub AddField(FieldName As String,FieldType";
Debug.ShouldStop(1);
 BA.debugLineNum = 99;BA.debugLine="xml_sendstriing=xml_sendstriing &CRLF & $\"<${Fiel";
Debug.ShouldStop(4);
__ref.setField ("_xml_sendstriing",RemoteObject.concat(__ref.getField(true,"_xml_sendstriing"),sp_tabelle.__c.getField(true,"CRLF"),(RemoteObject.concat(RemoteObject.createImmutable("<"),sp_tabelle.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_fieldname))),RemoteObject.createImmutable(" >"),sp_tabelle.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_fieldvalue))),RemoteObject.createImmutable("</"),sp_tabelle.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_fieldname))),RemoteObject.createImmutable(">")))));
 BA.debugLineNum = 100;BA.debugLine="End Sub";
Debug.ShouldStop(8);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _class_globals(RemoteObject __ref) throws Exception{
 //BA.debugLineNum = 3;BA.debugLine="Private Sub Class_Globals";
 //BA.debugLineNum = 5;BA.debugLine="Dim parser As SaxParser";
sp_tabelle._parser = RemoteObject.createNew ("anywheresoftware.b4a.objects.SaxParser");__ref.setField("_parser",sp_tabelle._parser);
 //BA.debugLineNum = 6;BA.debugLine="Public TYPE_STRING   As String = \"xsd:string\"";
sp_tabelle._type_string = BA.ObjectToString("xsd:string");__ref.setField("_type_string",sp_tabelle._type_string);
 //BA.debugLineNum = 7;BA.debugLine="Public TYPE_INTEGER  As String = \"xsd:int\"";
sp_tabelle._type_integer = BA.ObjectToString("xsd:int");__ref.setField("_type_integer",sp_tabelle._type_integer);
 //BA.debugLineNum = 8;BA.debugLine="Public TYPE_FLOAT    As String = \"xsd:float\"";
sp_tabelle._type_float = BA.ObjectToString("xsd:float");__ref.setField("_type_float",sp_tabelle._type_float);
 //BA.debugLineNum = 9;BA.debugLine="Public TYPE_DATE     As String = \"xsd:date\"";
sp_tabelle._type_date = BA.ObjectToString("xsd:date");__ref.setField("_type_date",sp_tabelle._type_date);
 //BA.debugLineNum = 10;BA.debugLine="Public TYPE_DOUBLE   As String = \"xsd:double\"";
sp_tabelle._type_double = BA.ObjectToString("xsd:double");__ref.setField("_type_double",sp_tabelle._type_double);
 //BA.debugLineNum = 11;BA.debugLine="Public TYPE_BINARY   As String = \"xsd:base64Binar";
sp_tabelle._type_binary = BA.ObjectToString("xsd:base64Binary");__ref.setField("_type_binary",sp_tabelle._type_binary);
 //BA.debugLineNum = 13;BA.debugLine="Private UrlWS        As String";
sp_tabelle._urlws = RemoteObject.createImmutable("");__ref.setField("_urlws",sp_tabelle._urlws);
 //BA.debugLineNum = 14;BA.debugLine="Dim ar_auftrag(13) As String";
sp_tabelle._ar_auftrag = RemoteObject.createNewArray ("String", new int[] {13}, new Object[]{});__ref.setField("_ar_auftrag",sp_tabelle._ar_auftrag);
 //BA.debugLineNum = 15;BA.debugLine="Dim ar_auftrag_stoff(4) As String";
sp_tabelle._ar_auftrag_stoff = RemoteObject.createNewArray ("String", new int[] {4}, new Object[]{});__ref.setField("_ar_auftrag_stoff",sp_tabelle._ar_auftrag_stoff);
 //BA.debugLineNum = 16;BA.debugLine="Dim ar_stoffliste(2) As String";
sp_tabelle._ar_stoffliste = RemoteObject.createNewArray ("String", new int[] {2}, new Object[]{});__ref.setField("_ar_stoffliste",sp_tabelle._ar_stoffliste);
 //BA.debugLineNum = 17;BA.debugLine="Dim ar_getnews(4) As String";
sp_tabelle._ar_getnews = RemoteObject.createNewArray ("String", new int[] {4}, new Object[]{});__ref.setField("_ar_getnews",sp_tabelle._ar_getnews);
 //BA.debugLineNum = 18;BA.debugLine="Dim ar_erlaubbtetalons(3) As String";
sp_tabelle._ar_erlaubbtetalons = RemoteObject.createNewArray ("String", new int[] {3}, new Object[]{});__ref.setField("_ar_erlaubbtetalons",sp_tabelle._ar_erlaubbtetalons);
 //BA.debugLineNum = 19;BA.debugLine="Dim ar_pbcodes(2) As String";
sp_tabelle._ar_pbcodes = RemoteObject.createNewArray ("String", new int[] {2}, new Object[]{});__ref.setField("_ar_pbcodes",sp_tabelle._ar_pbcodes);
 //BA.debugLineNum = 20;BA.debugLine="Private Modules      As Object";
sp_tabelle._modules = RemoteObject.createNew ("Object");__ref.setField("_modules",sp_tabelle._modules);
 //BA.debugLineNum = 21;BA.debugLine="Private EN           As String";
sp_tabelle._en = RemoteObject.createImmutable("");__ref.setField("_en",sp_tabelle._en);
 //BA.debugLineNum = 22;BA.debugLine="Private xml_sendstriing As String";
sp_tabelle._xml_sendstriing = RemoteObject.createImmutable("");__ref.setField("_xml_sendstriing",sp_tabelle._xml_sendstriing);
 //BA.debugLineNum = 23;BA.debugLine="Dim version As Double";
sp_tabelle._version = RemoteObject.createImmutable(0);__ref.setField("_version",sp_tabelle._version);
 //BA.debugLineNum = 24;BA.debugLine="Public last As Boolean";
sp_tabelle._last = RemoteObject.createImmutable(false);__ref.setField("_last",sp_tabelle._last);
 //BA.debugLineNum = 25;BA.debugLine="Dim u1 As update_sender_main";
sp_tabelle._u1 = RemoteObject.createNew ("b4a.example.update_sender_main");__ref.setField("_u1",sp_tabelle._u1);
 //BA.debugLineNum = 26;BA.debugLine="End Sub";
return RemoteObject.createImmutable("");
}
public static RemoteObject  _initialize(RemoteObject __ref,RemoteObject _ba,RemoteObject _url,RemoteObject _methodname,RemoteObject _module,RemoteObject _eventname) throws Exception{
try {
		Debug.PushSubsStack("initialize (sp_tabelle) ","sp_tabelle",4,__ref.getField(false, "ba"),__ref,82);
if (RapidSub.canDelegate("initialize")) return __ref.runUserSub(false, "sp_tabelle","initialize", __ref, _ba, _url, _methodname, _module, _eventname);
__ref.runVoidMethodAndSync("innerInitializeHelper", _ba);
Debug.locals.put("ba", _ba);
Debug.locals.put("Url", _url);
Debug.locals.put("MethodName", _methodname);
Debug.locals.put("module", _module);
Debug.locals.put("eventname", _eventname);
 BA.debugLineNum = 82;BA.debugLine="Public Sub initialize(Url As String,MethodName As";
Debug.ShouldStop(131072);
 BA.debugLineNum = 85;BA.debugLine="UrlWS        = Url";
Debug.ShouldStop(1048576);
__ref.setField ("_urlws",_url);
 BA.debugLineNum = 87;BA.debugLine="EN           = eventname";
Debug.ShouldStop(4194304);
__ref.setField ("_en",_eventname);
 BA.debugLineNum = 88;BA.debugLine="Modules      = module";
Debug.ShouldStop(8388608);
__ref.setField ("_modules",_module);
 BA.debugLineNum = 91;BA.debugLine="xml_sendstriing =xml_sendstriing & \"<?xml version";
Debug.ShouldStop(67108864);
__ref.setField ("_xml_sendstriing",RemoteObject.concat(__ref.getField(true,"_xml_sendstriing"),RemoteObject.createImmutable("<?xml version="),sp_tabelle.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable("1.0"),sp_tabelle.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable(" encoding="),sp_tabelle.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable("utf-8"),sp_tabelle.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable("?>")));
 BA.debugLineNum = 92;BA.debugLine="xml_sendstriing= xml_sendstriing & \"<soap:Envelope";
Debug.ShouldStop(134217728);
__ref.setField ("_xml_sendstriing",RemoteObject.concat(__ref.getField(true,"_xml_sendstriing"),RemoteObject.createImmutable("<soap:Envelope xmlns:xsi="),sp_tabelle.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable("http://www.w3.org/2001/XMLSchema-instance"),sp_tabelle.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable(" xmlns:xsd="),sp_tabelle.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable("http://www.w3.org/2001/XMLSchema"),sp_tabelle.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable(" xmlns:soap="),sp_tabelle.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable("http://schemas.xmlsoap.org/soap/envelope/"),sp_tabelle.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable(">")));
 BA.debugLineNum = 93;BA.debugLine="xml_sendstriing= xml_sendstriing & \"<soap:Body>\"";
Debug.ShouldStop(268435456);
__ref.setField ("_xml_sendstriing",RemoteObject.concat(__ref.getField(true,"_xml_sendstriing"),RemoteObject.createImmutable("<soap:Body>")));
 BA.debugLineNum = 94;BA.debugLine="xml_sendstriing= xml_sendstriing & \"<\" & Metho";
Debug.ShouldStop(536870912);
__ref.setField ("_xml_sendstriing",RemoteObject.concat(__ref.getField(true,"_xml_sendstriing"),RemoteObject.createImmutable("<"),_methodname,RemoteObject.createImmutable(" xmlns="),sp_tabelle.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable("http://tempuri.org/"),sp_tabelle.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable(">")));
 BA.debugLineNum = 95;BA.debugLine="End Sub";
Debug.ShouldStop(1073741824);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _jobdone(RemoteObject __ref,RemoteObject _job) throws Exception{
try {
		Debug.PushSubsStack("JobDone (sp_tabelle) ","sp_tabelle",4,__ref.getField(false, "ba"),__ref,124);
if (RapidSub.canDelegate("jobdone")) return __ref.runUserSub(false, "sp_tabelle","jobdone", __ref, _job);
RemoteObject _out3 = RemoteObject.declareNull("anywheresoftware.b4a.objects.streams.File.OutputStreamWrapper");
RemoteObject _i = RemoteObject.declareNull("anywheresoftware.b4a.objects.IntentWrapper");
RemoteObject _pm = RemoteObject.declareNull("anywheresoftware.b4a.phone.PackageManagerWrapper");
RemoteObject _res = RemoteObject.createImmutable(0);
RemoteObject _jobapk = RemoteObject.declareNull("anywheresoftware.b4a.samples.httputils2.httpjob");
RemoteObject _verstring = RemoteObject.createImmutable("");
RemoteObject _sb = RemoteObject.declareNull("anywheresoftware.b4a.keywords.StringBuilderWrapper");
Debug.locals.put("Job", _job);
 BA.debugLineNum = 124;BA.debugLine="Private Sub JobDone(Job As HttpJob)";
Debug.ShouldStop(134217728);
 BA.debugLineNum = 126;BA.debugLine="parser.Initialize";
Debug.ShouldStop(536870912);
__ref.getField(false,"_parser").runVoidMethod ("Initialize",__ref.getField(false, "ba"));
 BA.debugLineNum = 128;BA.debugLine="If Job.Success Then";
Debug.ShouldStop(-2147483648);
if (_job.getField(true,"_success").<Boolean>get().booleanValue()) { 
 BA.debugLineNum = 129;BA.debugLine="Select Case Job.JobName";
Debug.ShouldStop(1);
switch (BA.switchObjectToInt(_job.getField(true,"_jobname"),BA.ObjectToString("JobApkDownload"),BA.ObjectToString("get_version"),BA.ObjectToString("get_news"),BA.ObjectToString("get_erlaubtetalons"),BA.ObjectToString("get_pbcodes"))) {
case 0: {
 BA.debugLineNum = 132;BA.debugLine="Dim out3 As OutputStream = File.OpenOutput(File";
Debug.ShouldStop(8);
_out3 = RemoteObject.createNew ("anywheresoftware.b4a.objects.streams.File.OutputStreamWrapper");
_out3 = sp_tabelle.__c.getField(false,"File").runMethod(false,"OpenOutput",(Object)(sp_tabelle.__c.getField(false,"File").runMethod(true,"getDirDefaultExternal")),(Object)(BA.ObjectToString("ISP_KML.apk")),(Object)(sp_tabelle.__c.getField(true,"False")));Debug.locals.put("out3", _out3);Debug.locals.put("out3", _out3);
 BA.debugLineNum = 133;BA.debugLine="File.Copy2(Job.GetInputStream, out3)";
Debug.ShouldStop(16);
sp_tabelle.__c.getField(false,"File").runVoidMethod ("Copy2",(Object)((_job.runMethod(false,"_getinputstream").getObject())),(Object)((_out3.getObject())));
 BA.debugLineNum = 134;BA.debugLine="out3.Close";
Debug.ShouldStop(32);
_out3.runVoidMethod ("Close");
 BA.debugLineNum = 135;BA.debugLine="Dim i As Intent";
Debug.ShouldStop(64);
_i = RemoteObject.createNew ("anywheresoftware.b4a.objects.IntentWrapper");Debug.locals.put("i", _i);
 BA.debugLineNum = 136;BA.debugLine="i.Initialize(i.ACTION_VIEW, \"file://\" & File.";
Debug.ShouldStop(128);
_i.runVoidMethod ("Initialize",(Object)(_i.getField(true,"ACTION_VIEW")),(Object)(RemoteObject.concat(RemoteObject.createImmutable("file://"),sp_tabelle.__c.getField(false,"File").runMethod(true,"Combine",(Object)(sp_tabelle.__c.getField(false,"File").runMethod(true,"getDirDefaultExternal")),(Object)(RemoteObject.createImmutable("ISP_KML.apk"))))));
 BA.debugLineNum = 137;BA.debugLine="i.SetType(\"application/vnd.android.package-ar";
Debug.ShouldStop(256);
_i.runVoidMethod ("SetType",(Object)(RemoteObject.createImmutable("application/vnd.android.package-archive")));
 BA.debugLineNum = 138;BA.debugLine="StartActivity(i)";
Debug.ShouldStop(512);
sp_tabelle.__c.runVoidMethod ("StartActivity",__ref.getField(false, "ba"),(Object)((_i.getObject())));
 break; }
case 1: {
 BA.debugLineNum = 141;BA.debugLine="Try";
Debug.ShouldStop(4096);
try { BA.debugLineNum = 144;BA.debugLine="parser.Parse(Job.GetInputStream,\"P_get_version\"";
Debug.ShouldStop(32768);
__ref.getField(false,"_parser").runVoidMethodAndSync ("Parse",(Object)((_job.runMethod(false,"_getinputstream").getObject())),(Object)(RemoteObject.createImmutable("P_get_version")));
 BA.debugLineNum = 146;BA.debugLine="Log(version)";
Debug.ShouldStop(131072);
sp_tabelle.__c.runVoidMethod ("Log",(Object)(BA.NumberToString(__ref.getField(true,"_version"))));
 BA.debugLineNum = 147;BA.debugLine="Dim pm As PackageManager";
Debug.ShouldStop(262144);
_pm = RemoteObject.createNew ("anywheresoftware.b4a.phone.PackageManagerWrapper");Debug.locals.put("pm", _pm);
 BA.debugLineNum = 148;BA.debugLine="If pm.GetVersionName(\"com.interseroh.kml\") < ve";
Debug.ShouldStop(524288);
if (RemoteObject.solveBoolean("<",BA.numberCast(double.class, _pm.runMethod(true,"GetVersionName",(Object)(RemoteObject.createImmutable("com.interseroh.kml")))),__ref.getField(true,"_version"))) { 
 BA.debugLineNum = 149;BA.debugLine="Dim res As Int";
Debug.ShouldStop(1048576);
_res = RemoteObject.createImmutable(0);Debug.locals.put("res", _res);
 BA.debugLineNum = 150;BA.debugLine="res=Msgbox2(\"Update verfügbar! Möchten sie inst";
Debug.ShouldStop(2097152);
_res = sp_tabelle.__c.runMethodAndSync(true,"Msgbox2",(Object)(BA.ObjectToCharSequence("Update verfügbar! Möchten sie installieren?")),(Object)(BA.ObjectToCharSequence("Update")),(Object)(BA.ObjectToString("Ja")),(Object)(BA.ObjectToString("Nein")),(Object)(BA.ObjectToString("")),(Object)((sp_tabelle.__c.getField(false,"Null"))),__ref.runMethod(false,"getActivityBA"));Debug.locals.put("res", _res);
 BA.debugLineNum = 151;BA.debugLine="If res = DialogResponse.POSITIVE Then";
Debug.ShouldStop(4194304);
if (RemoteObject.solveBoolean("=",_res,BA.numberCast(double.class, sp_tabelle.__c.getField(false,"DialogResponse").getField(true,"POSITIVE")))) { 
 BA.debugLineNum = 152;BA.debugLine="Dim jobapk As HttpJob";
Debug.ShouldStop(8388608);
_jobapk = RemoteObject.createNew ("anywheresoftware.b4a.samples.httputils2.httpjob");Debug.locals.put("jobapk", _jobapk);
 BA.debugLineNum = 153;BA.debugLine="jobapk.Initialize(\"JobApkDownload\", Me)";
Debug.ShouldStop(16777216);
_jobapk.runVoidMethod ("_initialize",__ref.getField(false, "ba"),(Object)(BA.ObjectToString("JobApkDownload")),(Object)(__ref));
 BA.debugLineNum = 156;BA.debugLine="Dim verstring As String";
Debug.ShouldStop(134217728);
_verstring = RemoteObject.createImmutable("");Debug.locals.put("verstring", _verstring);
 BA.debugLineNum = 157;BA.debugLine="verstring=version";
Debug.ShouldStop(268435456);
_verstring = BA.NumberToString(__ref.getField(true,"_version"));Debug.locals.put("verstring", _verstring);
 BA.debugLineNum = 158;BA.debugLine="jobapk.Download(\"http://www.interseroh-pfand.";
Debug.ShouldStop(536870912);
_jobapk.runVoidMethod ("_download",(Object)(RemoteObject.concat(RemoteObject.createImmutable("http://www.interseroh-pfand.com/appupdate/ISP_KML.apk?Version="),_verstring.runMethod(true,"replace",(Object)(BA.ObjectToString(".")),(Object)(RemoteObject.createImmutable(""))))));
 };
 };
 Debug.CheckDeviceExceptions();
} 
       catch (Exception e29) {
			BA.rdebugUtils.runVoidMethod("setLastException",__ref.getField(false, "ba"), e29.toString()); BA.debugLineNum = 164;BA.debugLine="Log(LastException)";
Debug.ShouldStop(8);
sp_tabelle.__c.runVoidMethod ("Log",(Object)(BA.ObjectToString(sp_tabelle.__c.runMethod(false,"LastException",__ref.runMethod(false,"getActivityBA")))));
 };
 break; }
case 2: {
 BA.debugLineNum = 169;BA.debugLine="Main.sql1.ExecNonQuery(\"Delete from tmp_news";
Debug.ShouldStop(256);
sp_tabelle._main._sql1.runVoidMethod ("ExecNonQuery",(Object)(RemoteObject.createImmutable("Delete from tmp_news")));
 BA.debugLineNum = 170;BA.debugLine="parser.Parse(Job.GetInputStream,\"P_getnews\")";
Debug.ShouldStop(512);
__ref.getField(false,"_parser").runVoidMethodAndSync ("Parse",(Object)((_job.runMethod(false,"_getinputstream").getObject())),(Object)(RemoteObject.createImmutable("P_getnews")));
 BA.debugLineNum = 171;BA.debugLine="Log(Main.sql1.ExecQuerySingleResult(\"Select c";
Debug.ShouldStop(1024);
sp_tabelle.__c.runVoidMethod ("Log",(Object)(sp_tabelle._main._sql1.runMethod(true,"ExecQuerySingleResult",(Object)(RemoteObject.createImmutable("Select count(infoid) from tmp_news")))));
 BA.debugLineNum = 172;BA.debugLine="Main.sql1.ExecNonQuery(\"insert into tbl_news";
Debug.ShouldStop(2048);
sp_tabelle._main._sql1.runVoidMethod ("ExecNonQuery",(Object)(RemoteObject.concat(RemoteObject.createImmutable("insert into tbl_news (infotext,Absender,timestamp, infoID) select tmp_news.infotext,tmp_news.absender,tmp_news.infoid,tmp_news.infoid from tmp_news "),RemoteObject.createImmutable(" left outer join tbl_news on tmp_news.InfoID = tbl_news.InfoID where tbl_news.infoid Is null"))));
 BA.debugLineNum = 174;BA.debugLine="Log(Main.sql1.ExecQuerySingleResult(\"Select count";
Debug.ShouldStop(8192);
sp_tabelle.__c.runVoidMethod ("Log",(Object)(sp_tabelle._main._sql1.runMethod(true,"ExecQuerySingleResult",(Object)(RemoteObject.createImmutable("Select count(infoid) from tbl_news")))));
 break; }
case 3: {
 BA.debugLineNum = 176;BA.debugLine="Main.sql1.ExecNonQuery(\"Delete from tmp_erlau";
Debug.ShouldStop(32768);
sp_tabelle._main._sql1.runVoidMethod ("ExecNonQuery",(Object)(RemoteObject.createImmutable("Delete from tmp_erlaubte_codes")));
 BA.debugLineNum = 177;BA.debugLine="parser.Parse(Job.GetInputStream,\"P_erlaubteta";
Debug.ShouldStop(65536);
__ref.getField(false,"_parser").runVoidMethodAndSync ("Parse",(Object)((_job.runMethod(false,"_getinputstream").getObject())),(Object)(RemoteObject.createImmutable("P_erlaubtetalons")));
 BA.debugLineNum = 179;BA.debugLine="Dim sb As StringBuilder";
Debug.ShouldStop(262144);
_sb = RemoteObject.createNew ("anywheresoftware.b4a.keywords.StringBuilderWrapper");Debug.locals.put("sb", _sb);
 BA.debugLineNum = 180;BA.debugLine="sb.Initialize";
Debug.ShouldStop(524288);
_sb.runVoidMethod ("Initialize");
 BA.debugLineNum = 181;BA.debugLine="sb.Append(\"insert into tbl_erlaubte_codes (co";
Debug.ShouldStop(1048576);
_sb.runVoidMethod ("Append",(Object)(RemoteObject.createImmutable("insert into tbl_erlaubte_codes (codestart,len,art) Select ")));
 BA.debugLineNum = 182;BA.debugLine="sb.Append(\" tmp_erlaubte_codes.codestart,tmp_erlau";
Debug.ShouldStop(2097152);
_sb.runVoidMethod ("Append",(Object)(RemoteObject.createImmutable(" tmp_erlaubte_codes.codestart,tmp_erlaubte_codes.len,tmp_erlaubte_codes.art")));
 BA.debugLineNum = 183;BA.debugLine="sb.Append(\" from tmp_erlaubte_codes left outer joi";
Debug.ShouldStop(4194304);
_sb.runVoidMethod ("Append",(Object)(RemoteObject.createImmutable(" from tmp_erlaubte_codes left outer join tbl_erlaubte_codes on tmp_erlaubte_codes.codestart=")));
 BA.debugLineNum = 184;BA.debugLine="sb.Append(\" tbl_erlaubte_codes.codestart And tmp_e";
Debug.ShouldStop(8388608);
_sb.runVoidMethod ("Append",(Object)(RemoteObject.createImmutable(" tbl_erlaubte_codes.codestart And tmp_erlaubte_codes.art=tbl_erlaubte_codes.art")));
 BA.debugLineNum = 185;BA.debugLine="sb.Append(\" where tbl_erlaubte_codes.codestart Is";
Debug.ShouldStop(16777216);
_sb.runVoidMethod ("Append",(Object)(RemoteObject.createImmutable(" where tbl_erlaubte_codes.codestart Is null")));
 BA.debugLineNum = 187;BA.debugLine="Main.sql1.ExecnonQuery(sb.ToString)";
Debug.ShouldStop(67108864);
sp_tabelle._main._sql1.runVoidMethod ("ExecNonQuery",(Object)(_sb.runMethod(true,"ToString")));
 break; }
case 4: {
 BA.debugLineNum = 190;BA.debugLine="Main.sql1.ExecNonQuery(\"Delete from tbl_scan_t";
Debug.ShouldStop(536870912);
sp_tabelle._main._sql1.runVoidMethod ("ExecNonQuery",(Object)(RemoteObject.createImmutable("Delete from tbl_scan_temp")));
 BA.debugLineNum = 191;BA.debugLine="parser.Parse(Job.GetInputStream,\"P_pbscan\")";
Debug.ShouldStop(1073741824);
__ref.getField(false,"_parser").runVoidMethodAndSync ("Parse",(Object)((_job.runMethod(false,"_getinputstream").getObject())),(Object)(RemoteObject.createImmutable("P_pbscan")));
 break; }
}
;
 }else {
 };
 BA.debugLineNum = 198;BA.debugLine="ProgressDialogHide";
Debug.ShouldStop(32);
sp_tabelle.__c.runVoidMethod ("ProgressDialogHide");
 BA.debugLineNum = 200;BA.debugLine="End Sub";
Debug.ShouldStop(128);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _p_get_version_endelement(RemoteObject __ref,RemoteObject _uri,RemoteObject _name,RemoteObject _text) throws Exception{
try {
		Debug.PushSubsStack("P_get_version_EndElement (sp_tabelle) ","sp_tabelle",4,__ref.getField(false, "ba"),__ref,28);
if (RapidSub.canDelegate("p_get_version_endelement")) return __ref.runUserSub(false, "sp_tabelle","p_get_version_endelement", __ref, _uri, _name, _text);
Debug.locals.put("Uri", _uri);
Debug.locals.put("Name", _name);
Debug.locals.put("Text", _text);
 BA.debugLineNum = 28;BA.debugLine="Sub P_get_version_EndElement(Uri As String, Name A";
Debug.ShouldStop(134217728);
 BA.debugLineNum = 31;BA.debugLine="Select Case Name";
Debug.ShouldStop(1073741824);
switch (BA.switchObjectToInt(_name,BA.ObjectToString("get_versionResult"))) {
case 0: {
 BA.debugLineNum = 33;BA.debugLine="version=Text.ToString";
Debug.ShouldStop(1);
__ref.setField ("_version",BA.numberCast(double.class, _text.runMethod(true,"ToString")));
 break; }
}
;
 BA.debugLineNum = 39;BA.debugLine="End Sub";
Debug.ShouldStop(64);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _p_getnews_endelement(RemoteObject __ref,RemoteObject _uri,RemoteObject _name,RemoteObject _text) throws Exception{
try {
		Debug.PushSubsStack("P_getnews_EndElement (sp_tabelle) ","sp_tabelle",4,__ref.getField(false, "ba"),__ref,40);
if (RapidSub.canDelegate("p_getnews_endelement")) return __ref.runUserSub(false, "sp_tabelle","p_getnews_endelement", __ref, _uri, _name, _text);
int _i = 0;
Debug.locals.put("Uri", _uri);
Debug.locals.put("Name", _name);
Debug.locals.put("Text", _text);
 BA.debugLineNum = 40;BA.debugLine="Sub P_getnews_EndElement(Uri As String, Name As St";
Debug.ShouldStop(128);
 BA.debugLineNum = 42;BA.debugLine="If parser.Parents.IndexOf(\"Table1\") > -1 Then";
Debug.ShouldStop(512);
if (RemoteObject.solveBoolean(">",__ref.getField(false,"_parser").getField(false,"Parents").runMethod(true,"IndexOf",(Object)((RemoteObject.createImmutable("Table1")))),BA.numberCast(double.class, -(double) (0 + 1)))) { 
 BA.debugLineNum = 43;BA.debugLine="Select Case Name";
Debug.ShouldStop(1024);
switch (BA.switchObjectToInt(_name,BA.ObjectToString("Infotext"),BA.ObjectToString("Absender"),BA.ObjectToString("timestamp"),BA.ObjectToString("id"))) {
case 0: {
 BA.debugLineNum = 45;BA.debugLine="For i = 0 To 1";
Debug.ShouldStop(4096);
{
final int step4 = 1;
final int limit4 = 1;
_i = 0 ;
for (;(step4 > 0 && _i <= limit4) || (step4 < 0 && _i >= limit4) ;_i = ((int)(0 + _i + step4))  ) {
Debug.locals.put("i", _i);
 BA.debugLineNum = 46;BA.debugLine="ar_getnews(i)=\"\"";
Debug.ShouldStop(8192);
__ref.getField(false,"_ar_getnews").setArrayElement (BA.ObjectToString(""),BA.numberCast(int.class, _i));
 }
}Debug.locals.put("i", _i);
;
 BA.debugLineNum = 48;BA.debugLine="ar_getnews(0)=Text.ToString";
Debug.ShouldStop(32768);
__ref.getField(false,"_ar_getnews").setArrayElement (_text.runMethod(true,"ToString"),BA.numberCast(int.class, 0));
 break; }
case 1: {
 BA.debugLineNum = 50;BA.debugLine="ar_getnews(1)=Text.ToString";
Debug.ShouldStop(131072);
__ref.getField(false,"_ar_getnews").setArrayElement (_text.runMethod(true,"ToString"),BA.numberCast(int.class, 1));
 break; }
case 2: {
 BA.debugLineNum = 52;BA.debugLine="ar_getnews(2)=Main.sf.Left(Text.ToString.Replace";
Debug.ShouldStop(524288);
__ref.getField(false,"_ar_getnews").setArrayElement (sp_tabelle._main._sf.runMethod(true,"_vvv6",(Object)(_text.runMethod(true,"ToString").runMethod(true,"replace",(Object)(BA.ObjectToString("T")),(Object)(RemoteObject.createImmutable(" ")))),(Object)(BA.numberCast(long.class, 16))),BA.numberCast(int.class, 2));
 break; }
case 3: {
 BA.debugLineNum = 54;BA.debugLine="ar_getnews(3)=Text.ToString";
Debug.ShouldStop(2097152);
__ref.getField(false,"_ar_getnews").setArrayElement (_text.runMethod(true,"ToString"),BA.numberCast(int.class, 3));
 BA.debugLineNum = 55;BA.debugLine="Main.sql1.ExecNonQuery2(\"Insert into tmp_news";
Debug.ShouldStop(4194304);
sp_tabelle._main._sql1.runVoidMethod ("ExecNonQuery2",(Object)(RemoteObject.concat(RemoteObject.createImmutable("Insert into tmp_news (Infotext,Absender,timestamp,Infoid)"),RemoteObject.createImmutable(" values(?,?,?,?)"))),(Object)(sp_tabelle.__c.runMethod(false, "ArrayToList", (Object)(__ref.getField(false,"_ar_getnews")))));
 BA.debugLineNum = 57;BA.debugLine="Log(ar_getnews(1) &\"/\" & ar_getnews(2))";
Debug.ShouldStop(16777216);
sp_tabelle.__c.runVoidMethod ("Log",(Object)(RemoteObject.concat(__ref.getField(false,"_ar_getnews").getArrayElement(true,BA.numberCast(int.class, 1)),RemoteObject.createImmutable("/"),__ref.getField(false,"_ar_getnews").getArrayElement(true,BA.numberCast(int.class, 2)))));
 BA.debugLineNum = 58;BA.debugLine="For i = 0 To 3";
Debug.ShouldStop(33554432);
{
final int step16 = 1;
final int limit16 = 3;
_i = 0 ;
for (;(step16 > 0 && _i <= limit16) || (step16 < 0 && _i >= limit16) ;_i = ((int)(0 + _i + step16))  ) {
Debug.locals.put("i", _i);
 BA.debugLineNum = 59;BA.debugLine="ar_getnews(i)=\"\"";
Debug.ShouldStop(67108864);
__ref.getField(false,"_ar_getnews").setArrayElement (BA.ObjectToString(""),BA.numberCast(int.class, _i));
 }
}Debug.locals.put("i", _i);
;
 break; }
}
;
 };
 BA.debugLineNum = 66;BA.debugLine="End Sub";
Debug.ShouldStop(2);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _parser_startelement(RemoteObject __ref,RemoteObject _uri,RemoteObject _name,RemoteObject _attributes) throws Exception{
try {
		Debug.PushSubsStack("Parser_StartElement (sp_tabelle) ","sp_tabelle",4,__ref.getField(false, "ba"),__ref,77);
if (RapidSub.canDelegate("parser_startelement")) return __ref.runUserSub(false, "sp_tabelle","parser_startelement", __ref, _uri, _name, _attributes);
Debug.locals.put("Uri", _uri);
Debug.locals.put("Name", _name);
Debug.locals.put("Attributes", _attributes);
 BA.debugLineNum = 77;BA.debugLine="Sub Parser_StartElement (Uri As String, Name As St";
Debug.ShouldStop(4096);
 BA.debugLineNum = 79;BA.debugLine="End Sub";
Debug.ShouldStop(16384);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _sendrequest(RemoteObject __ref,RemoteObject _methodname,RemoteObject _jobname) throws Exception{
try {
		Debug.PushSubsStack("SendRequest (sp_tabelle) ","sp_tabelle",4,__ref.getField(false, "ba"),__ref,102);
if (RapidSub.canDelegate("sendrequest")) return __ref.runUserSub(false, "sp_tabelle","sendrequest", __ref, _methodname, _jobname);
RemoteObject _ht1 = RemoteObject.declareNull("anywheresoftware.b4a.samples.httputils2.httpjob");
Debug.locals.put("MethodName", _methodname);
Debug.locals.put("jobname", _jobname);
 BA.debugLineNum = 102;BA.debugLine="Sub SendRequest( MethodName As String,jobname As S";
Debug.ShouldStop(32);
 BA.debugLineNum = 106;BA.debugLine="xml_sendstriing= xml_sendstriing & \"</\" & Meth";
Debug.ShouldStop(512);
__ref.setField ("_xml_sendstriing",RemoteObject.concat(__ref.getField(true,"_xml_sendstriing"),RemoteObject.createImmutable("</"),_methodname,RemoteObject.createImmutable(">")));
 BA.debugLineNum = 107;BA.debugLine="xml_sendstriing= xml_sendstriing & \"</soap:Body>";
Debug.ShouldStop(1024);
__ref.setField ("_xml_sendstriing",RemoteObject.concat(__ref.getField(true,"_xml_sendstriing"),RemoteObject.createImmutable("</soap:Body>")));
 BA.debugLineNum = 108;BA.debugLine="xml_sendstriing= xml_sendstriing & \"</soap:Envelop";
Debug.ShouldStop(2048);
__ref.setField ("_xml_sendstriing",RemoteObject.concat(__ref.getField(true,"_xml_sendstriing"),RemoteObject.createImmutable("</soap:Envelope>")));
 BA.debugLineNum = 110;BA.debugLine="Dim ht1 As HttpJob";
Debug.ShouldStop(8192);
_ht1 = RemoteObject.createNew ("anywheresoftware.b4a.samples.httputils2.httpjob");Debug.locals.put("ht1", _ht1);
 BA.debugLineNum = 111;BA.debugLine="ht1.Initialize(jobname,Me)";
Debug.ShouldStop(16384);
_ht1.runVoidMethod ("_initialize",__ref.getField(false, "ba"),(Object)(_jobname),(Object)(__ref));
 BA.debugLineNum = 112;BA.debugLine="Log(\"go\")";
Debug.ShouldStop(32768);
sp_tabelle.__c.runVoidMethod ("Log",(Object)(RemoteObject.createImmutable("go")));
 BA.debugLineNum = 113;BA.debugLine="ht1.PostString(UrlWS,xml_sendstriing)";
Debug.ShouldStop(65536);
_ht1.runVoidMethod ("_poststring",(Object)(__ref.getField(true,"_urlws")),(Object)(__ref.getField(true,"_xml_sendstriing")));
 BA.debugLineNum = 116;BA.debugLine="ht1.GetRequest.SetHeader(\"SoapAction\", \"http://te";
Debug.ShouldStop(524288);
_ht1.runMethod(false,"_getrequest").runVoidMethod ("SetHeader",(Object)(BA.ObjectToString("SoapAction")),(Object)(RemoteObject.concat(RemoteObject.createImmutable("http://tempuri.org/"),_methodname,RemoteObject.createImmutable(""))));
 BA.debugLineNum = 119;BA.debugLine="ht1.GetRequest.SetContentType(\"text/xml; charset=";
Debug.ShouldStop(4194304);
_ht1.runMethod(false,"_getrequest").runVoidMethod ("SetContentType",(Object)(RemoteObject.createImmutable("text/xml; charset=utf-8")));
 BA.debugLineNum = 120;BA.debugLine="ht1.GetRequest.Timeout=2000";
Debug.ShouldStop(8388608);
_ht1.runMethod(false,"_getrequest").runVoidMethod ("setTimeout",BA.numberCast(int.class, 2000));
 BA.debugLineNum = 122;BA.debugLine="End Sub";
Debug.ShouldStop(33554432);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
}