package b4a.example;

import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.pc.*;

public class update_sender_subs_0 {


public static RemoteObject  _class_globals(RemoteObject __ref) throws Exception{
 //BA.debugLineNum = 2;BA.debugLine="Sub Class_Globals";
 //BA.debugLineNum = 3;BA.debugLine="Dim s1 As sp_single";
update_sender._s1 = RemoteObject.createNew ("b4a.example.sp_single");__ref.setField("_s1",update_sender._s1);
 //BA.debugLineNum = 4;BA.debugLine="Public usr As String";
update_sender._usr = RemoteObject.createImmutable("");__ref.setField("_usr",update_sender._usr);
 //BA.debugLineNum = 5;BA.debugLine="Public pwd As String";
update_sender._pwd = RemoteObject.createImmutable("");__ref.setField("_pwd",update_sender._pwd);
 //BA.debugLineNum = 6;BA.debugLine="End Sub";
return RemoteObject.createImmutable("");
}
public static RemoteObject  _initialize(RemoteObject __ref,RemoteObject _ba) throws Exception{
try {
		Debug.PushSubsStack("Initialize (update_sender) ","update_sender",8,__ref.getField(false, "ba"),__ref,9);
if (RapidSub.canDelegate("initialize")) return __ref.runUserSub(false, "update_sender","initialize", __ref, _ba);
__ref.runVoidMethodAndSync("innerInitializeHelper", _ba);
RemoteObject _c_user = RemoteObject.declareNull("anywheresoftware.b4a.sql.SQL.CursorWrapper");
Debug.locals.put("ba", _ba);
 BA.debugLineNum = 9;BA.debugLine="Public Sub Initialize";
Debug.ShouldStop(256);
 BA.debugLineNum = 10;BA.debugLine="Try";
Debug.ShouldStop(512);
try { BA.debugLineNum = 13;BA.debugLine="Dim c_user As Cursor";
Debug.ShouldStop(4096);
_c_user = RemoteObject.createNew ("anywheresoftware.b4a.sql.SQL.CursorWrapper");Debug.locals.put("c_user", _c_user);
 BA.debugLineNum = 14;BA.debugLine="c_user=Main.sql1.ExecQuery(\"select * from tbl_usr\"";
Debug.ShouldStop(8192);
_c_user.setObject(update_sender._main._sql1.runMethod(false,"ExecQuery",(Object)(RemoteObject.createImmutable("select * from tbl_usr"))));
 BA.debugLineNum = 15;BA.debugLine="c_user.Position=0";
Debug.ShouldStop(16384);
_c_user.runMethod(true,"setPosition",BA.numberCast(int.class, 0));
 BA.debugLineNum = 16;BA.debugLine="usr=c_user.GetString(\"username\")";
Debug.ShouldStop(32768);
__ref.setField ("_usr",_c_user.runMethod(true,"GetString",(Object)(RemoteObject.createImmutable("username"))));
 BA.debugLineNum = 17;BA.debugLine="pwd=c_user.GetString(\"pw\")";
Debug.ShouldStop(65536);
__ref.setField ("_pwd",_c_user.runMethod(true,"GetString",(Object)(RemoteObject.createImmutable("pw"))));
 Debug.CheckDeviceExceptions();
} 
       catch (Exception e8) {
			BA.rdebugUtils.runVoidMethod("setLastException",__ref.getField(false, "ba"), e8.toString()); BA.debugLineNum = 19;BA.debugLine="Log(\"ungültige anmeldung\")";
Debug.ShouldStop(262144);
update_sender.__c.runVoidMethod ("Log",(Object)(RemoteObject.createImmutable("ungültige anmeldung")));
 };
 BA.debugLineNum = 21;BA.debugLine="End Sub";
Debug.ShouldStop(1048576);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _result_ankunft(RemoteObject __ref,RemoteObject _res) throws Exception{
try {
		Debug.PushSubsStack("Result_ankunft (update_sender) ","update_sender",8,__ref.getField(false, "ba"),__ref,169);
if (RapidSub.canDelegate("result_ankunft")) return __ref.runUserSub(false, "update_sender","result_ankunft", __ref, _res);
Debug.locals.put("res", _res);
 BA.debugLineNum = 169;BA.debugLine="Sub Result_ankunft(res As Object)";
Debug.ShouldStop(256);
 BA.debugLineNum = 170;BA.debugLine="Log(res)";
Debug.ShouldStop(512);
update_sender.__c.runVoidMethod ("Log",(Object)(BA.ObjectToString(_res)));
 BA.debugLineNum = 171;BA.debugLine="End Sub";
Debug.ShouldStop(1024);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _result_eingabe(RemoteObject __ref,RemoteObject _res) throws Exception{
try {
		Debug.PushSubsStack("Result_eingabe (update_sender) ","update_sender",8,__ref.getField(false, "ba"),__ref,172);
if (RapidSub.canDelegate("result_eingabe")) return __ref.runUserSub(false, "update_sender","result_eingabe", __ref, _res);
Debug.locals.put("res", _res);
 BA.debugLineNum = 172;BA.debugLine="Sub Result_eingabe(res As Object)";
Debug.ShouldStop(2048);
 BA.debugLineNum = 173;BA.debugLine="Log(res)";
Debug.ShouldStop(4096);
update_sender.__c.runVoidMethod ("Log",(Object)(BA.ObjectToString(_res)));
 BA.debugLineNum = 174;BA.debugLine="End Sub";
Debug.ShouldStop(8192);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _result_info(RemoteObject __ref,RemoteObject _res) throws Exception{
try {
		Debug.PushSubsStack("Result_info (update_sender) ","update_sender",8,__ref.getField(false, "ba"),__ref,165);
if (RapidSub.canDelegate("result_info")) return __ref.runUserSub(false, "update_sender","result_info", __ref, _res);
Debug.locals.put("res", _res);
 BA.debugLineNum = 165;BA.debugLine="Sub Result_info(res As Object)";
Debug.ShouldStop(16);
 BA.debugLineNum = 166;BA.debugLine="Log(res)";
Debug.ShouldStop(32);
update_sender.__c.runVoidMethod ("Log",(Object)(BA.ObjectToString(_res)));
 BA.debugLineNum = 167;BA.debugLine="End Sub";
Debug.ShouldStop(64);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _send_ankunft(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("send_ankunft (update_sender) ","update_sender",8,__ref.getField(false, "ba"),__ref,24);
if (RapidSub.canDelegate("send_ankunft")) return __ref.runUserSub(false, "update_sender","send_ankunft", __ref);
RemoteObject _c = RemoteObject.declareNull("anywheresoftware.b4a.sql.SQL.CursorWrapper");
int _i = 0;
 BA.debugLineNum = 24;BA.debugLine="public Sub send_ankunft( )";
Debug.ShouldStop(8388608);
 BA.debugLineNum = 28;BA.debugLine="Dim c As Cursor";
Debug.ShouldStop(134217728);
_c = RemoteObject.createNew ("anywheresoftware.b4a.sql.SQL.CursorWrapper");Debug.locals.put("c", _c);
 BA.debugLineNum = 30;BA.debugLine="c= Main.sql1.ExecQuery(\"Select * from tbl_ankunft";
Debug.ShouldStop(536870912);
_c.setObject(update_sender._main._sql1.runMethod(false,"ExecQuery",(Object)(RemoteObject.createImmutable("Select * from tbl_ankunft where sync is null"))));
 BA.debugLineNum = 32;BA.debugLine="For i = 0 To c.RowCount -1";
Debug.ShouldStop(-2147483648);
{
final int step3 = 1;
final int limit3 = RemoteObject.solve(new RemoteObject[] {_c.runMethod(true,"getRowCount"),RemoteObject.createImmutable(1)}, "-",1, 1).<Integer>get().intValue();
_i = 0 ;
for (;(step3 > 0 && _i <= limit3) || (step3 < 0 && _i >= limit3) ;_i = ((int)(0 + _i + step3))  ) {
Debug.locals.put("i", _i);
 BA.debugLineNum = 35;BA.debugLine="c.Position=i";
Debug.ShouldStop(4);
_c.runMethod(true,"setPosition",BA.numberCast(int.class, _i));
 BA.debugLineNum = 36;BA.debugLine="s1.Initialize(Main.use_service &\"?op=up_ankunf";
Debug.ShouldStop(8);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_initialize",__ref.getField(false, "ba"),(Object)(RemoteObject.concat(update_sender._main._use_service,RemoteObject.createImmutable("?op=up_ankunft"))),(Object)(BA.ObjectToString("up_ankunft")),(Object)(__ref),(Object)(RemoteObject.createImmutable("Result_ankunft")));
 BA.debugLineNum = 37;BA.debugLine="s1.AddField(\"usr\",s1.TYPE_STRING,usr)";
Debug.ShouldStop(16);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_addfield",(Object)(BA.ObjectToString("usr")),(Object)(__ref.getField(false,"_s1").getField(true,"_type_string")),(Object)(__ref.getField(true,"_usr")));
 BA.debugLineNum = 38;BA.debugLine="s1.AddField(\"pwd\",s1.TYPE_STRING,pwd)";
Debug.ShouldStop(32);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_addfield",(Object)(BA.ObjectToString("pwd")),(Object)(__ref.getField(false,"_s1").getField(true,"_type_string")),(Object)(__ref.getField(true,"_pwd")));
 BA.debugLineNum = 39;BA.debugLine="s1.AddField(\"id\",s1.TYPE_INTEGER,c.GetInt(\"id\"))";
Debug.ShouldStop(64);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_addfield",(Object)(BA.ObjectToString("id")),(Object)(__ref.getField(false,"_s1").getField(true,"_type_integer")),(Object)(BA.NumberToString(_c.runMethod(true,"GetInt",(Object)(RemoteObject.createImmutable("id"))))));
 BA.debugLineNum = 40;BA.debugLine="s1.AddField(\"auftrag\",s1.TYPE_String,c.GetString";
Debug.ShouldStop(128);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_addfield",(Object)(BA.ObjectToString("auftrag")),(Object)(__ref.getField(false,"_s1").getField(true,"_type_string")),(Object)(_c.runMethod(true,"GetString",(Object)(RemoteObject.createImmutable("auftrag")))));
 BA.debugLineNum = 41;BA.debugLine="s1.AddField(\"ankunft\",s1.TYPE_String,c.GetStrin";
Debug.ShouldStop(256);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_addfield",(Object)(BA.ObjectToString("ankunft")),(Object)(__ref.getField(false,"_s1").getField(true,"_type_string")),(Object)(_c.runMethod(true,"GetString",(Object)(RemoteObject.createImmutable("ankunft")))));
 BA.debugLineNum = 42;BA.debugLine="Log (c.GetString(\"ankunft\"))";
Debug.ShouldStop(512);
update_sender.__c.runVoidMethod ("Log",(Object)(_c.runMethod(true,"GetString",(Object)(RemoteObject.createImmutable("ankunft")))));
 BA.debugLineNum = 43;BA.debugLine="s1.AddField(\"gps\",s1.TYPE_String,c.GetString(\"g";
Debug.ShouldStop(1024);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_addfield",(Object)(BA.ObjectToString("gps")),(Object)(__ref.getField(false,"_s1").getField(true,"_type_string")),(Object)(_c.runMethod(true,"GetString",(Object)(RemoteObject.createImmutable("gps")))));
 BA.debugLineNum = 44;BA.debugLine="If i = c.RowCount-1 Then";
Debug.ShouldStop(2048);
if (RemoteObject.solveBoolean("=",RemoteObject.createImmutable(_i),BA.numberCast(double.class, RemoteObject.solve(new RemoteObject[] {_c.runMethod(true,"getRowCount"),RemoteObject.createImmutable(1)}, "-",1, 1)))) { 
 BA.debugLineNum = 45;BA.debugLine="s1.SendRequest(\"up_ankunft\",\"send_ankunft\",Tru";
Debug.ShouldStop(4096);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_sendrequest",(Object)(BA.ObjectToString("up_ankunft")),(Object)(BA.ObjectToString("send_ankunft")),(Object)(update_sender.__c.getField(true,"True")));
 }else {
 BA.debugLineNum = 47;BA.debugLine="s1.SendRequest(\"up_ankunft\",\"send_ankunft\",Fals";
Debug.ShouldStop(16384);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_sendrequest",(Object)(BA.ObjectToString("up_ankunft")),(Object)(BA.ObjectToString("send_ankunft")),(Object)(update_sender.__c.getField(true,"False")));
 };
 }
}Debug.locals.put("i", _i);
;
 BA.debugLineNum = 57;BA.debugLine="End Sub";
Debug.ShouldStop(16777216);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _send_eingabe(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("send_eingabe (update_sender) ","update_sender",8,__ref.getField(false, "ba"),__ref,89);
if (RapidSub.canDelegate("send_eingabe")) return __ref.runUserSub(false, "update_sender","send_eingabe", __ref);
RemoteObject _c = RemoteObject.declareNull("anywheresoftware.b4a.sql.SQL.CursorWrapper");
int _i = 0;
 BA.debugLineNum = 89;BA.debugLine="public Sub send_eingabe( )";
Debug.ShouldStop(16777216);
 BA.debugLineNum = 93;BA.debugLine="Dim c As Cursor";
Debug.ShouldStop(268435456);
_c = RemoteObject.createNew ("anywheresoftware.b4a.sql.SQL.CursorWrapper");Debug.locals.put("c", _c);
 BA.debugLineNum = 95;BA.debugLine="c= Main.sql1.ExecQuery(\"Select * from tbl_nachtra";
Debug.ShouldStop(1073741824);
_c.setObject(update_sender._main._sql1.runMethod(false,"ExecQuery",(Object)(RemoteObject.createImmutable("Select * from tbl_nachtrag_log_app where sync is null"))));
 BA.debugLineNum = 97;BA.debugLine="For i = 0 To c.RowCount -1";
Debug.ShouldStop(1);
{
final int step3 = 1;
final int limit3 = RemoteObject.solve(new RemoteObject[] {_c.runMethod(true,"getRowCount"),RemoteObject.createImmutable(1)}, "-",1, 1).<Integer>get().intValue();
_i = 0 ;
for (;(step3 > 0 && _i <= limit3) || (step3 < 0 && _i >= limit3) ;_i = ((int)(0 + _i + step3))  ) {
Debug.locals.put("i", _i);
 BA.debugLineNum = 98;BA.debugLine="c.Position=i";
Debug.ShouldStop(2);
_c.runMethod(true,"setPosition",BA.numberCast(int.class, _i));
 BA.debugLineNum = 99;BA.debugLine="s1.Initialize(Main.use_service &\"?op=up_eingab";
Debug.ShouldStop(4);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_initialize",__ref.getField(false, "ba"),(Object)(RemoteObject.concat(update_sender._main._use_service,RemoteObject.createImmutable("?op=up_eingabe"))),(Object)(BA.ObjectToString("up_eingabe")),(Object)(__ref),(Object)(RemoteObject.createImmutable("Result_eingabe")));
 BA.debugLineNum = 100;BA.debugLine="s1.AddField(\"usr\",s1.TYPE_STRING,usr)";
Debug.ShouldStop(8);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_addfield",(Object)(BA.ObjectToString("usr")),(Object)(__ref.getField(false,"_s1").getField(true,"_type_string")),(Object)(__ref.getField(true,"_usr")));
 BA.debugLineNum = 101;BA.debugLine="s1.AddField(\"pwd\",s1.TYPE_STRING,pwd)";
Debug.ShouldStop(16);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_addfield",(Object)(BA.ObjectToString("pwd")),(Object)(__ref.getField(false,"_s1").getField(true,"_type_string")),(Object)(__ref.getField(true,"_pwd")));
 BA.debugLineNum = 102;BA.debugLine="s1.AddField(\"id\",s1.TYPE_INTEGER,c.GetInt(\"id\"))";
Debug.ShouldStop(32);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_addfield",(Object)(BA.ObjectToString("id")),(Object)(__ref.getField(false,"_s1").getField(true,"_type_integer")),(Object)(BA.NumberToString(_c.runMethod(true,"GetInt",(Object)(RemoteObject.createImmutable("id"))))));
 BA.debugLineNum = 103;BA.debugLine="s1.AddField(\"auftrag\",s1.TYPE_String,c.GetString";
Debug.ShouldStop(64);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_addfield",(Object)(BA.ObjectToString("auftrag")),(Object)(__ref.getField(false,"_s1").getField(true,"_type_string")),(Object)(_c.runMethod(true,"GetString",(Object)(RemoteObject.createImmutable("Auftrag")))));
 BA.debugLineNum = 104;BA.debugLine="s1.AddField(\"stoff\",s1.TYPE_String,c.GetString(";
Debug.ShouldStop(128);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_addfield",(Object)(BA.ObjectToString("stoff")),(Object)(__ref.getField(false,"_s1").getField(true,"_type_string")),(Object)(_c.runMethod(true,"GetString",(Object)(RemoteObject.createImmutable("Stoff")))));
 BA.debugLineNum = 105;BA.debugLine="s1.AddField(\"Anzahl\",s1.TYPE_integer,c.Getint(\"A";
Debug.ShouldStop(256);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_addfield",(Object)(BA.ObjectToString("Anzahl")),(Object)(__ref.getField(false,"_s1").getField(true,"_type_integer")),(Object)(BA.NumberToString(_c.runMethod(true,"GetInt",(Object)(RemoteObject.createImmutable("Anzahl"))))));
 BA.debugLineNum = 106;BA.debugLine="s1.AddField(\"Bemerkung\",s1.TYPE_String,c.GetStri";
Debug.ShouldStop(512);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_addfield",(Object)(BA.ObjectToString("Bemerkung")),(Object)(__ref.getField(false,"_s1").getField(true,"_type_string")),(Object)(_c.runMethod(true,"GetString",(Object)(RemoteObject.createImmutable("Bemerkung")))));
 BA.debugLineNum = 107;BA.debugLine="s1.AddField(\"Gewicht\",s1.TYPE_String,c.GetString";
Debug.ShouldStop(1024);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_addfield",(Object)(BA.ObjectToString("Gewicht")),(Object)(__ref.getField(false,"_s1").getField(true,"_type_string")),(Object)(_c.runMethod(true,"GetString",(Object)(RemoteObject.createImmutable("Gewicht")))));
 BA.debugLineNum = 108;BA.debugLine="s1.AddField(\"datum\",s1.TYPE_String,c.GetString(\"";
Debug.ShouldStop(2048);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_addfield",(Object)(BA.ObjectToString("datum")),(Object)(__ref.getField(false,"_s1").getField(true,"_type_string")),(Object)(_c.runMethod(true,"GetString",(Object)(RemoteObject.createImmutable("date1")))));
 BA.debugLineNum = 109;BA.debugLine="If i = c.RowCount-1 Then";
Debug.ShouldStop(4096);
if (RemoteObject.solveBoolean("=",RemoteObject.createImmutable(_i),BA.numberCast(double.class, RemoteObject.solve(new RemoteObject[] {_c.runMethod(true,"getRowCount"),RemoteObject.createImmutable(1)}, "-",1, 1)))) { 
 BA.debugLineNum = 110;BA.debugLine="s1.SendRequest(\"up_eingabe\",\"send_eingabe\",Tru";
Debug.ShouldStop(8192);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_sendrequest",(Object)(BA.ObjectToString("up_eingabe")),(Object)(BA.ObjectToString("send_eingabe")),(Object)(update_sender.__c.getField(true,"True")));
 }else {
 BA.debugLineNum = 112;BA.debugLine="s1.SendRequest(\"up_eingabe\",\"send_eingabe\",Fals";
Debug.ShouldStop(32768);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_sendrequest",(Object)(BA.ObjectToString("up_eingabe")),(Object)(BA.ObjectToString("send_eingabe")),(Object)(update_sender.__c.getField(true,"False")));
 };
 }
}Debug.locals.put("i", _i);
;
 BA.debugLineNum = 118;BA.debugLine="End Sub";
Debug.ShouldStop(2097152);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _send_nachricht(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("send_nachricht (update_sender) ","update_sender",8,__ref.getField(false, "ba"),__ref,119);
if (RapidSub.canDelegate("send_nachricht")) return __ref.runUserSub(false, "update_sender","send_nachricht", __ref);
RemoteObject _c = RemoteObject.declareNull("anywheresoftware.b4a.sql.SQL.CursorWrapper");
int _i = 0;
 BA.debugLineNum = 119;BA.debugLine="Public Sub send_nachricht";
Debug.ShouldStop(4194304);
 BA.debugLineNum = 120;BA.debugLine="Try";
Debug.ShouldStop(8388608);
try { BA.debugLineNum = 121;BA.debugLine="Dim c As Cursor";
Debug.ShouldStop(16777216);
_c = RemoteObject.createNew ("anywheresoftware.b4a.sql.SQL.CursorWrapper");Debug.locals.put("c", _c);
 BA.debugLineNum = 122;BA.debugLine="c= Main.sql1.ExecQuery(\"Select * from tbl_antwort";
Debug.ShouldStop(33554432);
_c.setObject(update_sender._main._sql1.runMethod(false,"ExecQuery",(Object)(RemoteObject.createImmutable("Select * from tbl_antwort where sync is null"))));
 BA.debugLineNum = 123;BA.debugLine="For i = 0 To c.RowCount -1";
Debug.ShouldStop(67108864);
{
final int step4 = 1;
final int limit4 = RemoteObject.solve(new RemoteObject[] {_c.runMethod(true,"getRowCount"),RemoteObject.createImmutable(1)}, "-",1, 1).<Integer>get().intValue();
_i = 0 ;
for (;(step4 > 0 && _i <= limit4) || (step4 < 0 && _i >= limit4) ;_i = ((int)(0 + _i + step4))  ) {
Debug.locals.put("i", _i);
 BA.debugLineNum = 124;BA.debugLine="c.Position=i";
Debug.ShouldStop(134217728);
_c.runMethod(true,"setPosition",BA.numberCast(int.class, _i));
 BA.debugLineNum = 125;BA.debugLine="s1.Initialize(Main.use_service &\"?op=send_info\",";
Debug.ShouldStop(268435456);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_initialize",__ref.getField(false, "ba"),(Object)(RemoteObject.concat(update_sender._main._use_service,RemoteObject.createImmutable("?op=send_info"))),(Object)(BA.ObjectToString("send_info")),(Object)(__ref),(Object)(RemoteObject.createImmutable("Result_info")));
 BA.debugLineNum = 126;BA.debugLine="s1.AddField(\"usr\",s1.TYPE_STRING,usr)";
Debug.ShouldStop(536870912);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_addfield",(Object)(BA.ObjectToString("usr")),(Object)(__ref.getField(false,"_s1").getField(true,"_type_string")),(Object)(__ref.getField(true,"_usr")));
 BA.debugLineNum = 127;BA.debugLine="s1.AddField(\"pwd\",s1.TYPE_STRING,pwd)";
Debug.ShouldStop(1073741824);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_addfield",(Object)(BA.ObjectToString("pwd")),(Object)(__ref.getField(false,"_s1").getField(true,"_type_string")),(Object)(__ref.getField(true,"_pwd")));
 BA.debugLineNum = 128;BA.debugLine="s1.AddField(\"id\",s1.TYPE_STRING,c.GetString(\"Info";
Debug.ShouldStop(-2147483648);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_addfield",(Object)(BA.ObjectToString("id")),(Object)(__ref.getField(false,"_s1").getField(true,"_type_string")),(Object)(_c.runMethod(true,"GetString",(Object)(RemoteObject.createImmutable("Info_id")))));
 BA.debugLineNum = 129;BA.debugLine="s1.AddField(\"Antwort\",s1.TYPE_STRING,c.GetString(";
Debug.ShouldStop(1);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_addfield",(Object)(BA.ObjectToString("Antwort")),(Object)(__ref.getField(false,"_s1").getField(true,"_type_string")),(Object)(_c.runMethod(true,"GetString",(Object)(RemoteObject.createImmutable("Antwort")))));
 BA.debugLineNum = 130;BA.debugLine="s1.AddField(\"id_tabelle\",s1.TYPE_INTEGER,c.GetInt";
Debug.ShouldStop(2);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_addfield",(Object)(BA.ObjectToString("id_tabelle")),(Object)(__ref.getField(false,"_s1").getField(true,"_type_integer")),(Object)(BA.NumberToString(_c.runMethod(true,"GetInt",(Object)(RemoteObject.createImmutable("id"))))));
 BA.debugLineNum = 131;BA.debugLine="If i = c.RowCount-1 Then";
Debug.ShouldStop(4);
if (RemoteObject.solveBoolean("=",RemoteObject.createImmutable(_i),BA.numberCast(double.class, RemoteObject.solve(new RemoteObject[] {_c.runMethod(true,"getRowCount"),RemoteObject.createImmutable(1)}, "-",1, 1)))) { 
 BA.debugLineNum = 132;BA.debugLine="s1.SendRequest(\"send_info\",\"all\",True)";
Debug.ShouldStop(8);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_sendrequest",(Object)(BA.ObjectToString("send_info")),(Object)(BA.ObjectToString("all")),(Object)(update_sender.__c.getField(true,"True")));
 }else {
 BA.debugLineNum = 134;BA.debugLine="s1.SendRequest(\"send_info\",\"all\",False)";
Debug.ShouldStop(32);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_sendrequest",(Object)(BA.ObjectToString("send_info")),(Object)(BA.ObjectToString("all")),(Object)(update_sender.__c.getField(true,"False")));
 };
 }
}Debug.locals.put("i", _i);
;
 Debug.CheckDeviceExceptions();
} 
       catch (Exception e19) {
			BA.rdebugUtils.runVoidMethod("setLastException",__ref.getField(false, "ba"), e19.toString()); BA.debugLineNum = 140;BA.debugLine="Log(LastException)";
Debug.ShouldStop(2048);
update_sender.__c.runVoidMethod ("Log",(Object)(BA.ObjectToString(update_sender.__c.runMethod(false,"LastException",__ref.runMethod(false,"getActivityBA")))));
 };
 BA.debugLineNum = 142;BA.debugLine="End Sub";
Debug.ShouldStop(8192);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _send_quit(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("send_quit (update_sender) ","update_sender",8,__ref.getField(false, "ba"),__ref,58);
if (RapidSub.canDelegate("send_quit")) return __ref.runUserSub(false, "update_sender","send_quit", __ref);
RemoteObject _c = RemoteObject.declareNull("anywheresoftware.b4a.sql.SQL.CursorWrapper");
int _i = 0;
 BA.debugLineNum = 58;BA.debugLine="public Sub send_quit( )";
Debug.ShouldStop(33554432);
 BA.debugLineNum = 62;BA.debugLine="Dim c As Cursor";
Debug.ShouldStop(536870912);
_c = RemoteObject.createNew ("anywheresoftware.b4a.sql.SQL.CursorWrapper");Debug.locals.put("c", _c);
 BA.debugLineNum = 64;BA.debugLine="c= Main.sql1.ExecQuery(\"Select * from tbl_quittun";
Debug.ShouldStop(-2147483648);
_c.setObject(update_sender._main._sql1.runMethod(false,"ExecQuery",(Object)(RemoteObject.createImmutable("Select * from tbl_quittung where sync is null"))));
 BA.debugLineNum = 66;BA.debugLine="For i = 0 To c.RowCount -1";
Debug.ShouldStop(2);
{
final int step3 = 1;
final int limit3 = RemoteObject.solve(new RemoteObject[] {_c.runMethod(true,"getRowCount"),RemoteObject.createImmutable(1)}, "-",1, 1).<Integer>get().intValue();
_i = 0 ;
for (;(step3 > 0 && _i <= limit3) || (step3 < 0 && _i >= limit3) ;_i = ((int)(0 + _i + step3))  ) {
Debug.locals.put("i", _i);
 BA.debugLineNum = 69;BA.debugLine="c.Position=i";
Debug.ShouldStop(16);
_c.runMethod(true,"setPosition",BA.numberCast(int.class, _i));
 BA.debugLineNum = 70;BA.debugLine="s1.Initialize(Main.use_service &\"?op=up_quit\",";
Debug.ShouldStop(32);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_initialize",__ref.getField(false, "ba"),(Object)(RemoteObject.concat(update_sender._main._use_service,RemoteObject.createImmutable("?op=up_quit"))),(Object)(BA.ObjectToString("up_quit")),(Object)(__ref),(Object)(RemoteObject.createImmutable("Result_quittung")));
 BA.debugLineNum = 71;BA.debugLine="s1.AddField(\"usr\",s1.TYPE_STRING,usr)";
Debug.ShouldStop(64);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_addfield",(Object)(BA.ObjectToString("usr")),(Object)(__ref.getField(false,"_s1").getField(true,"_type_string")),(Object)(__ref.getField(true,"_usr")));
 BA.debugLineNum = 72;BA.debugLine="s1.AddField(\"pwd\",s1.TYPE_STRING,pwd)";
Debug.ShouldStop(128);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_addfield",(Object)(BA.ObjectToString("pwd")),(Object)(__ref.getField(false,"_s1").getField(true,"_type_string")),(Object)(__ref.getField(true,"_pwd")));
 BA.debugLineNum = 73;BA.debugLine="s1.AddField(\"id\",s1.TYPE_INTEGER,c.GetInt(\"id\"))";
Debug.ShouldStop(256);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_addfield",(Object)(BA.ObjectToString("id")),(Object)(__ref.getField(false,"_s1").getField(true,"_type_integer")),(Object)(BA.NumberToString(_c.runMethod(true,"GetInt",(Object)(RemoteObject.createImmutable("id"))))));
 BA.debugLineNum = 74;BA.debugLine="s1.AddField(\"Auftrag\",s1.TYPE_String,c.GetString";
Debug.ShouldStop(512);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_addfield",(Object)(BA.ObjectToString("Auftrag")),(Object)(__ref.getField(false,"_s1").getField(true,"_type_string")),(Object)(_c.runMethod(true,"GetString",(Object)(RemoteObject.createImmutable("Auftrag")))));
 BA.debugLineNum = 75;BA.debugLine="s1.AddField(\"receiver\",s1.TYPE_String,c.GetStri";
Debug.ShouldStop(1024);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_addfield",(Object)(BA.ObjectToString("receiver")),(Object)(__ref.getField(false,"_s1").getField(true,"_type_string")),(Object)(_c.runMethod(true,"GetString",(Object)(RemoteObject.createImmutable("receiver")))));
 BA.debugLineNum = 78;BA.debugLine="If i = c.RowCount-1 Then";
Debug.ShouldStop(8192);
if (RemoteObject.solveBoolean("=",RemoteObject.createImmutable(_i),BA.numberCast(double.class, RemoteObject.solve(new RemoteObject[] {_c.runMethod(true,"getRowCount"),RemoteObject.createImmutable(1)}, "-",1, 1)))) { 
 BA.debugLineNum = 79;BA.debugLine="s1.SendRequest(\"up_quit\",\"send_quittung\",True)";
Debug.ShouldStop(16384);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_sendrequest",(Object)(BA.ObjectToString("up_quit")),(Object)(BA.ObjectToString("send_quittung")),(Object)(update_sender.__c.getField(true,"True")));
 }else {
 BA.debugLineNum = 81;BA.debugLine="s1.SendRequest(\"up_quit\",\"send_quittung\",False)";
Debug.ShouldStop(65536);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_sendrequest",(Object)(BA.ObjectToString("up_quit")),(Object)(BA.ObjectToString("send_quittung")),(Object)(update_sender.__c.getField(true,"False")));
 };
 }
}Debug.locals.put("i", _i);
;
 BA.debugLineNum = 88;BA.debugLine="End Sub";
Debug.ShouldStop(8388608);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _send_scanliste(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("send_scanliste (update_sender) ","update_sender",8,__ref.getField(false, "ba"),__ref,143);
if (RapidSub.canDelegate("send_scanliste")) return __ref.runUserSub(false, "update_sender","send_scanliste", __ref);
RemoteObject _c = RemoteObject.declareNull("anywheresoftware.b4a.sql.SQL.CursorWrapper");
int _i = 0;
 BA.debugLineNum = 143;BA.debugLine="Public Sub send_scanliste";
Debug.ShouldStop(16384);
 BA.debugLineNum = 144;BA.debugLine="Dim c As Cursor";
Debug.ShouldStop(32768);
_c = RemoteObject.createNew ("anywheresoftware.b4a.sql.SQL.CursorWrapper");Debug.locals.put("c", _c);
 BA.debugLineNum = 145;BA.debugLine="c= Main.sql1.ExecQuery(\"Select * from tbl_scan whe";
Debug.ShouldStop(65536);
_c.setObject(update_sender._main._sql1.runMethod(false,"ExecQuery",(Object)(RemoteObject.createImmutable("Select * from tbl_scan where sync is null"))));
 BA.debugLineNum = 146;BA.debugLine="For i = 0 To c.RowCount -1";
Debug.ShouldStop(131072);
{
final int step3 = 1;
final int limit3 = RemoteObject.solve(new RemoteObject[] {_c.runMethod(true,"getRowCount"),RemoteObject.createImmutable(1)}, "-",1, 1).<Integer>get().intValue();
_i = 0 ;
for (;(step3 > 0 && _i <= limit3) || (step3 < 0 && _i >= limit3) ;_i = ((int)(0 + _i + step3))  ) {
Debug.locals.put("i", _i);
 BA.debugLineNum = 147;BA.debugLine="c.Position=i";
Debug.ShouldStop(262144);
_c.runMethod(true,"setPosition",BA.numberCast(int.class, _i));
 BA.debugLineNum = 148;BA.debugLine="s1.Initialize(Main.use_service &\"?op=send_info\",";
Debug.ShouldStop(524288);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_initialize",__ref.getField(false, "ba"),(Object)(RemoteObject.concat(update_sender._main._use_service,RemoteObject.createImmutable("?op=send_info"))),(Object)(BA.ObjectToString("up_scan")),(Object)(__ref),(Object)(RemoteObject.createImmutable("Result_scan")));
 BA.debugLineNum = 149;BA.debugLine="s1.AddField(\"usr\",s1.TYPE_STRING,usr)";
Debug.ShouldStop(1048576);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_addfield",(Object)(BA.ObjectToString("usr")),(Object)(__ref.getField(false,"_s1").getField(true,"_type_string")),(Object)(__ref.getField(true,"_usr")));
 BA.debugLineNum = 150;BA.debugLine="s1.AddField(\"pwd\",s1.TYPE_STRING,pwd)";
Debug.ShouldStop(2097152);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_addfield",(Object)(BA.ObjectToString("pwd")),(Object)(__ref.getField(false,"_s1").getField(true,"_type_string")),(Object)(__ref.getField(true,"_pwd")));
 BA.debugLineNum = 151;BA.debugLine="s1.AddField(\"id\",s1.TYPE_Integer,c.Getint(\"id\"))";
Debug.ShouldStop(4194304);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_addfield",(Object)(BA.ObjectToString("id")),(Object)(__ref.getField(false,"_s1").getField(true,"_type_integer")),(Object)(BA.NumberToString(_c.runMethod(true,"GetInt",(Object)(RemoteObject.createImmutable("id"))))));
 BA.debugLineNum = 152;BA.debugLine="s1.AddField(\"Code\",s1.TYPE_STRING,c.GetString(\"";
Debug.ShouldStop(8388608);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_addfield",(Object)(BA.ObjectToString("Code")),(Object)(__ref.getField(false,"_s1").getField(true,"_type_string")),(Object)(_c.runMethod(true,"GetString",(Object)(RemoteObject.createImmutable("Code")))));
 BA.debugLineNum = 153;BA.debugLine="s1.AddField(\"timestamp\",s1.TYPE_STRING,c.GetStr";
Debug.ShouldStop(16777216);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_addfield",(Object)(BA.ObjectToString("timestamp")),(Object)(__ref.getField(false,"_s1").getField(true,"_type_string")),(Object)(_c.runMethod(true,"GetString",(Object)(RemoteObject.createImmutable("timestamp")))));
 BA.debugLineNum = 154;BA.debugLine="s1.AddField(\"Bemerkung\",s1.TYPE_STRING,c.GetStr";
Debug.ShouldStop(33554432);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_addfield",(Object)(BA.ObjectToString("Bemerkung")),(Object)(__ref.getField(false,"_s1").getField(true,"_type_string")),(Object)(_c.runMethod(true,"GetString",(Object)(RemoteObject.createImmutable("Bemerkung")))));
 BA.debugLineNum = 155;BA.debugLine="s1.AddField(\"Auftrag\",s1.TYPE_STRING,c.GetStrin";
Debug.ShouldStop(67108864);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_addfield",(Object)(BA.ObjectToString("Auftrag")),(Object)(__ref.getField(false,"_s1").getField(true,"_type_string")),(Object)(_c.runMethod(true,"GetString",(Object)(RemoteObject.createImmutable("Auftrag")))));
 BA.debugLineNum = 156;BA.debugLine="s1.AddField(\"Stoff\",s1.TYPE_STRING,c.GetString(";
Debug.ShouldStop(134217728);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_addfield",(Object)(BA.ObjectToString("Stoff")),(Object)(__ref.getField(false,"_s1").getField(true,"_type_string")),(Object)(_c.runMethod(true,"GetString",(Object)(RemoteObject.createImmutable("Stoff")))));
 BA.debugLineNum = 157;BA.debugLine="If i = c.RowCount-1 Then";
Debug.ShouldStop(268435456);
if (RemoteObject.solveBoolean("=",RemoteObject.createImmutable(_i),BA.numberCast(double.class, RemoteObject.solve(new RemoteObject[] {_c.runMethod(true,"getRowCount"),RemoteObject.createImmutable(1)}, "-",1, 1)))) { 
 BA.debugLineNum = 158;BA.debugLine="s1.SendRequest(\"up_scan\",\"all\",True)";
Debug.ShouldStop(536870912);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_sendrequest",(Object)(BA.ObjectToString("up_scan")),(Object)(BA.ObjectToString("all")),(Object)(update_sender.__c.getField(true,"True")));
 }else {
 BA.debugLineNum = 160;BA.debugLine="s1.SendRequest(\"up_scan\",\"all\",False)";
Debug.ShouldStop(-2147483648);
__ref.getField(false,"_s1").runClassMethod (b4a.example.sp_single.class, "_sendrequest",(Object)(BA.ObjectToString("up_scan")),(Object)(BA.ObjectToString("all")),(Object)(update_sender.__c.getField(true,"False")));
 };
 }
}Debug.locals.put("i", _i);
;
 BA.debugLineNum = 164;BA.debugLine="End Sub";
Debug.ShouldStop(8);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
}