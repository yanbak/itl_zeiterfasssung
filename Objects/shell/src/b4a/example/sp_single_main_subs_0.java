package b4a.example;

import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.pc.*;

public class sp_single_main_subs_0 {


public static RemoteObject  _addfield(RemoteObject __ref,RemoteObject _fieldname,RemoteObject _fieldtype,RemoteObject _fieldvalue) throws Exception{
try {
		Debug.PushSubsStack("AddField (sp_single_main) ","sp_single_main",9,__ref.getField(false, "ba"),__ref,143);
if (RapidSub.canDelegate("addfield")) return __ref.runUserSub(false, "sp_single_main","addfield", __ref, _fieldname, _fieldtype, _fieldvalue);
Debug.locals.put("FieldName", _fieldname);
Debug.locals.put("FieldType", _fieldtype);
Debug.locals.put("FieldValue", _fieldvalue);
 BA.debugLineNum = 143;BA.debugLine="Public Sub AddField(FieldName As String,FieldType";
Debug.ShouldStop(16384);
 BA.debugLineNum = 145;BA.debugLine="xml_sendstriing=xml_sendstriing &CRLF & $\"<${Fiel";
Debug.ShouldStop(65536);
__ref.setField ("_xml_sendstriing",RemoteObject.concat(__ref.getField(true,"_xml_sendstriing"),sp_single_main.__c.getField(true,"CRLF"),(RemoteObject.concat(RemoteObject.createImmutable("<"),sp_single_main.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_fieldname))),RemoteObject.createImmutable(" >"),sp_single_main.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_fieldvalue))),RemoteObject.createImmutable("</"),sp_single_main.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_fieldname))),RemoteObject.createImmutable(">")))));
 BA.debugLineNum = 146;BA.debugLine="End Sub";
Debug.ShouldStop(131072);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _class_globals(RemoteObject __ref) throws Exception{
 //BA.debugLineNum = 2;BA.debugLine="Sub Class_Globals";
 //BA.debugLineNum = 4;BA.debugLine="Dim parser As SaxParser";
sp_single_main._parser = RemoteObject.createNew ("anywheresoftware.b4a.objects.SaxParser");__ref.setField("_parser",sp_single_main._parser);
 //BA.debugLineNum = 5;BA.debugLine="Public TYPE_STRING   As String = \"xsd:string\"";
sp_single_main._type_string = BA.ObjectToString("xsd:string");__ref.setField("_type_string",sp_single_main._type_string);
 //BA.debugLineNum = 6;BA.debugLine="Public TYPE_INTEGER  As String = \"xsd:int\"";
sp_single_main._type_integer = BA.ObjectToString("xsd:int");__ref.setField("_type_integer",sp_single_main._type_integer);
 //BA.debugLineNum = 7;BA.debugLine="Public TYPE_FLOAT    As String = \"xsd:float\"";
sp_single_main._type_float = BA.ObjectToString("xsd:float");__ref.setField("_type_float",sp_single_main._type_float);
 //BA.debugLineNum = 8;BA.debugLine="Public TYPE_DATE     As String = \"xsd:date\"";
sp_single_main._type_date = BA.ObjectToString("xsd:date");__ref.setField("_type_date",sp_single_main._type_date);
 //BA.debugLineNum = 9;BA.debugLine="Public TYPE_DOUBLE   As String = \"xsd:double\"";
sp_single_main._type_double = BA.ObjectToString("xsd:double");__ref.setField("_type_double",sp_single_main._type_double);
 //BA.debugLineNum = 10;BA.debugLine="Public TYPE_BINARY   As String = \"xsd:base64Binar";
sp_single_main._type_binary = BA.ObjectToString("xsd:base64Binary");__ref.setField("_type_binary",sp_single_main._type_binary);
 //BA.debugLineNum = 12;BA.debugLine="Private UrlWS        As String";
sp_single_main._urlws = RemoteObject.createImmutable("");__ref.setField("_urlws",sp_single_main._urlws);
 //BA.debugLineNum = 14;BA.debugLine="Private Modules      As Object";
sp_single_main._modules = RemoteObject.createNew ("Object");__ref.setField("_modules",sp_single_main._modules);
 //BA.debugLineNum = 15;BA.debugLine="Private EN           As String";
sp_single_main._en = RemoteObject.createImmutable("");__ref.setField("_en",sp_single_main._en);
 //BA.debugLineNum = 16;BA.debugLine="Private xml_sendstriing As String";
sp_single_main._xml_sendstriing = RemoteObject.createImmutable("");__ref.setField("_xml_sendstriing",sp_single_main._xml_sendstriing);
 //BA.debugLineNum = 17;BA.debugLine="Private last1 As Boolean";
sp_single_main._last1 = RemoteObject.createImmutable(false);__ref.setField("_last1",sp_single_main._last1);
 //BA.debugLineNum = 18;BA.debugLine="Dim method As String";
sp_single_main._method = RemoteObject.createImmutable("");__ref.setField("_method",sp_single_main._method);
 //BA.debugLineNum = 19;BA.debugLine="End Sub";
return RemoteObject.createImmutable("");
}
public static RemoteObject  _imageupload(RemoteObject __ref,RemoteObject _img,RemoteObject _meth) throws Exception{
try {
		Debug.PushSubsStack("imageupload (sp_single_main) ","sp_single_main",9,__ref.getField(false, "ba"),__ref,204);
if (RapidSub.canDelegate("imageupload")) return __ref.runUserSub(false, "sp_single_main","imageupload", __ref, _img, _meth);
RemoteObject _base64con = RemoteObject.declareNull("simplysoftware.base64image.base64image");
RemoteObject _b64str = RemoteObject.createImmutable("");
RemoteObject _job1 = RemoteObject.declareNull("anywheresoftware.b4a.samples.httputils2.httpjob");
RemoteObject _xml = RemoteObject.createImmutable("");
Debug.locals.put("img", _img);
Debug.locals.put("meth", _meth);
 BA.debugLineNum = 204;BA.debugLine="Sub imageupload( img As String,meth As Object)";
Debug.ShouldStop(2048);
 BA.debugLineNum = 206;BA.debugLine="Private Base64Con As Base64Image";
Debug.ShouldStop(8192);
_base64con = RemoteObject.createNew ("simplysoftware.base64image.base64image");Debug.locals.put("Base64Con", _base64con);
 BA.debugLineNum = 207;BA.debugLine="Base64Con.Initialize";
Debug.ShouldStop(16384);
_base64con.runVoidMethod ("_initialize",__ref.getField(false, "ba"));
 BA.debugLineNum = 210;BA.debugLine="Private B64Str As String = Base64Con.EncodeFro";
Debug.ShouldStop(131072);
_b64str = _base64con.runMethod(true,"_encodefromimage",(Object)(sp_single_main.__c.getField(false,"File").runMethod(true,"getDirInternal")),(Object)(_img));Debug.locals.put("B64Str", _b64str);Debug.locals.put("B64Str", _b64str);
 BA.debugLineNum = 212;BA.debugLine="Private job1 As HttpJob";
Debug.ShouldStop(524288);
_job1 = RemoteObject.createNew ("anywheresoftware.b4a.samples.httputils2.httpjob");Debug.locals.put("job1", _job1);
 BA.debugLineNum = 214;BA.debugLine="Dim XML As String";
Debug.ShouldStop(2097152);
_xml = RemoteObject.createImmutable("");Debug.locals.put("XML", _xml);
 BA.debugLineNum = 215;BA.debugLine="XML = \"\"";
Debug.ShouldStop(4194304);
_xml = BA.ObjectToString("");Debug.locals.put("XML", _xml);
 BA.debugLineNum = 224;BA.debugLine="XML = XML & \"<?xml version='1.0' encoding='ut";
Debug.ShouldStop(-2147483648);
_xml = RemoteObject.concat(_xml,RemoteObject.createImmutable("<?xml version='1.0' encoding='utf-8'?>"));Debug.locals.put("XML", _xml);
 BA.debugLineNum = 225;BA.debugLine="XML = XML & \"<soap:Envelope xmlns:xsi='http://";
Debug.ShouldStop(1);
_xml = RemoteObject.concat(_xml,RemoteObject.createImmutable("<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>"));Debug.locals.put("XML", _xml);
 BA.debugLineNum = 226;BA.debugLine="XML = XML & \"<soap:Body>\"";
Debug.ShouldStop(2);
_xml = RemoteObject.concat(_xml,RemoteObject.createImmutable("<soap:Body>"));Debug.locals.put("XML", _xml);
 BA.debugLineNum = 227;BA.debugLine="XML = XML & \"<UploadFile xmlns='http://tempuri";
Debug.ShouldStop(4);
_xml = RemoteObject.concat(_xml,RemoteObject.createImmutable("<UploadFile xmlns='http://tempuri.org/'>"));Debug.locals.put("XML", _xml);
 BA.debugLineNum = 228;BA.debugLine="XML = XML & \"<f>\" & B64Str & \"</f>\"";
Debug.ShouldStop(8);
_xml = RemoteObject.concat(_xml,RemoteObject.createImmutable("<f>"),_b64str,RemoteObject.createImmutable("</f>"));Debug.locals.put("XML", _xml);
 BA.debugLineNum = 229;BA.debugLine="XML = XML & \"<fileName>\" & img & \"</fileName>\"";
Debug.ShouldStop(16);
_xml = RemoteObject.concat(_xml,RemoteObject.createImmutable("<fileName>"),_img,RemoteObject.createImmutable("</fileName>"));Debug.locals.put("XML", _xml);
 BA.debugLineNum = 230;BA.debugLine="XML = XML & \"</UploadFile>\"";
Debug.ShouldStop(32);
_xml = RemoteObject.concat(_xml,RemoteObject.createImmutable("</UploadFile>"));Debug.locals.put("XML", _xml);
 BA.debugLineNum = 231;BA.debugLine="XML = XML & \"</soap:Body>\"";
Debug.ShouldStop(64);
_xml = RemoteObject.concat(_xml,RemoteObject.createImmutable("</soap:Body>"));Debug.locals.put("XML", _xml);
 BA.debugLineNum = 232;BA.debugLine="XML = XML & \"</soap:Envelope>\"";
Debug.ShouldStop(128);
_xml = RemoteObject.concat(_xml,RemoteObject.createImmutable("</soap:Envelope>"));Debug.locals.put("XML", _xml);
 BA.debugLineNum = 233;BA.debugLine="XML = XML.Replace(\"'\", Chr(34))";
Debug.ShouldStop(256);
_xml = _xml.runMethod(true,"replace",(Object)(BA.ObjectToString("'")),(Object)(BA.ObjectToString(sp_single_main.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))))));Debug.locals.put("XML", _xml);
 BA.debugLineNum = 235;BA.debugLine="job1.Initialize(\"img_up\", meth)";
Debug.ShouldStop(1024);
_job1.runVoidMethod ("_initialize",__ref.getField(false, "ba"),(Object)(BA.ObjectToString("img_up")),(Object)(_meth));
 BA.debugLineNum = 237;BA.debugLine="job1.PostString (Main.use_service &\"\", XML)";
Debug.ShouldStop(4096);
_job1.runVoidMethod ("_poststring",(Object)(RemoteObject.concat(sp_single_main._main._use_service,RemoteObject.createImmutable(""))),(Object)(_xml));
 BA.debugLineNum = 238;BA.debugLine="job1.GetRequest.SetContentType(\"text/xml\")";
Debug.ShouldStop(8192);
_job1.runMethod(false,"_getrequest").runVoidMethod ("SetContentType",(Object)(RemoteObject.createImmutable("text/xml")));
 BA.debugLineNum = 239;BA.debugLine="job1.GetRequest.SetHeader(\"SoapAction\", \"http://t";
Debug.ShouldStop(16384);
_job1.runMethod(false,"_getrequest").runVoidMethod ("SetHeader",(Object)(BA.ObjectToString("SoapAction")),(Object)(RemoteObject.createImmutable("http://tempuri.org/UploadFile")));
 BA.debugLineNum = 240;BA.debugLine="End Sub";
Debug.ShouldStop(32768);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _initialize(RemoteObject __ref,RemoteObject _ba,RemoteObject _url,RemoteObject _methodname,RemoteObject _module,RemoteObject _eventname) throws Exception{
try {
		Debug.PushSubsStack("initialize (sp_single_main) ","sp_single_main",9,__ref.getField(false, "ba"),__ref,128);
if (RapidSub.canDelegate("initialize")) return __ref.runUserSub(false, "sp_single_main","initialize", __ref, _ba, _url, _methodname, _module, _eventname);
__ref.runVoidMethodAndSync("innerInitializeHelper", _ba);
Debug.locals.put("ba", _ba);
Debug.locals.put("Url", _url);
Debug.locals.put("MethodName", _methodname);
Debug.locals.put("module", _module);
Debug.locals.put("eventname", _eventname);
 BA.debugLineNum = 128;BA.debugLine="Public Sub initialize(Url As String,MethodName As";
Debug.ShouldStop(-2147483648);
 BA.debugLineNum = 131;BA.debugLine="UrlWS        = Url";
Debug.ShouldStop(4);
__ref.setField ("_urlws",_url);
 BA.debugLineNum = 132;BA.debugLine="method=MethodName";
Debug.ShouldStop(8);
__ref.setField ("_method",_methodname);
 BA.debugLineNum = 133;BA.debugLine="EN           = eventname";
Debug.ShouldStop(16);
__ref.setField ("_en",_eventname);
 BA.debugLineNum = 134;BA.debugLine="Modules      = module";
Debug.ShouldStop(32);
__ref.setField ("_modules",_module);
 BA.debugLineNum = 137;BA.debugLine="xml_sendstriing =xml_sendstriing & \"<?xml version";
Debug.ShouldStop(256);
__ref.setField ("_xml_sendstriing",RemoteObject.concat(__ref.getField(true,"_xml_sendstriing"),RemoteObject.createImmutable("<?xml version="),sp_single_main.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable("1.0"),sp_single_main.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable(" encoding="),sp_single_main.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable("utf-8"),sp_single_main.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable("?>")));
 BA.debugLineNum = 138;BA.debugLine="xml_sendstriing= xml_sendstriing & \"<soap:Envelope";
Debug.ShouldStop(512);
__ref.setField ("_xml_sendstriing",RemoteObject.concat(__ref.getField(true,"_xml_sendstriing"),RemoteObject.createImmutable("<soap:Envelope xmlns:xsi="),sp_single_main.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable("http://www.w3.org/2001/XMLSchema-instance"),sp_single_main.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable(" xmlns:xsd="),sp_single_main.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable("http://www.w3.org/2001/XMLSchema"),sp_single_main.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable(" xmlns:soap="),sp_single_main.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable("http://schemas.xmlsoap.org/soap/envelope/"),sp_single_main.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable(">")));
 BA.debugLineNum = 139;BA.debugLine="xml_sendstriing= xml_sendstriing & \"<soap:Body>\"";
Debug.ShouldStop(1024);
__ref.setField ("_xml_sendstriing",RemoteObject.concat(__ref.getField(true,"_xml_sendstriing"),RemoteObject.createImmutable("<soap:Body>")));
 BA.debugLineNum = 140;BA.debugLine="xml_sendstriing= xml_sendstriing & \"<\" & Metho";
Debug.ShouldStop(2048);
__ref.setField ("_xml_sendstriing",RemoteObject.concat(__ref.getField(true,"_xml_sendstriing"),RemoteObject.createImmutable("<"),_methodname,RemoteObject.createImmutable(" xmlns="),sp_single_main.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable("http://tempuri.org/"),sp_single_main.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable(">")));
 BA.debugLineNum = 141;BA.debugLine="End Sub";
Debug.ShouldStop(4096);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _jobdone(RemoteObject __ref,RemoteObject _job) throws Exception{
try {
		Debug.PushSubsStack("JobDone (sp_single_main) ","sp_single_main",9,__ref.getField(false, "ba"),__ref,170);
if (RapidSub.canDelegate("jobdone")) return __ref.runUserSub(false, "sp_single_main","jobdone", __ref, _job);
Debug.locals.put("Job", _job);
 BA.debugLineNum = 170;BA.debugLine="Private Sub JobDone(Job As HttpJob)";
Debug.ShouldStop(512);
 BA.debugLineNum = 172;BA.debugLine="parser.Initialize";
Debug.ShouldStop(2048);
__ref.getField(false,"_parser").runVoidMethod ("Initialize",__ref.getField(false, "ba"));
 BA.debugLineNum = 174;BA.debugLine="If Job.Success Then";
Debug.ShouldStop(8192);
if (_job.getField(true,"_success").<Boolean>get().booleanValue()) { 
 BA.debugLineNum = 175;BA.debugLine="Select Case Job.JobName";
Debug.ShouldStop(16384);
switch (BA.switchObjectToInt(_job.getField(true,"_jobname"),BA.ObjectToString("logincheck"),BA.ObjectToString("all"),BA.ObjectToString("img_up"),BA.ObjectToString("send_ankunft"),BA.ObjectToString("send_quittung"),BA.ObjectToString("send_eingabe"))) {
case 0: {
 BA.debugLineNum = 177;BA.debugLine="parser.Parse(Job.GetInputStream,\"P_logincheck\"";
Debug.ShouldStop(65536);
__ref.getField(false,"_parser").runVoidMethodAndSync ("Parse",(Object)((_job.runMethod(false,"_getinputstream").getObject())),(Object)(RemoteObject.createImmutable("P_logincheck")));
 break; }
case 1: {
 BA.debugLineNum = 179;BA.debugLine="parser.Parse(Job.GetInputStream,\"P_all\")";
Debug.ShouldStop(262144);
__ref.getField(false,"_parser").runVoidMethodAndSync ("Parse",(Object)((_job.runMethod(false,"_getinputstream").getObject())),(Object)(RemoteObject.createImmutable("P_all")));
 break; }
case 2: {
 break; }
case 3: {
 BA.debugLineNum = 184;BA.debugLine="parser.Parse(Job.GetInputStream,\"P_ankunft\")";
Debug.ShouldStop(8388608);
__ref.getField(false,"_parser").runVoidMethodAndSync ("Parse",(Object)((_job.runMethod(false,"_getinputstream").getObject())),(Object)(RemoteObject.createImmutable("P_ankunft")));
 break; }
case 4: {
 BA.debugLineNum = 190;BA.debugLine="parser.Parse(Job.GetInputStream,\"P_quitt";
Debug.ShouldStop(536870912);
__ref.getField(false,"_parser").runVoidMethodAndSync ("Parse",(Object)((_job.runMethod(false,"_getinputstream").getObject())),(Object)(RemoteObject.createImmutable("P_quittung")));
 break; }
case 5: {
 BA.debugLineNum = 196;BA.debugLine="parser.Parse(Job.GetInputStream,\"P_eingabe\")";
Debug.ShouldStop(8);
__ref.getField(false,"_parser").runVoidMethodAndSync ("Parse",(Object)((_job.runMethod(false,"_getinputstream").getObject())),(Object)(RemoteObject.createImmutable("P_eingabe")));
 break; }
}
;
 };
 BA.debugLineNum = 203;BA.debugLine="End Sub";
Debug.ShouldStop(1024);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _p_all_endelement(RemoteObject __ref,RemoteObject _uri,RemoteObject _name,RemoteObject _text) throws Exception{
try {
		Debug.PushSubsStack("P_all_EndElement (sp_single_main) ","sp_single_main",9,__ref.getField(false, "ba"),__ref,48);
if (RapidSub.canDelegate("p_all_endelement")) return __ref.runUserSub(false, "sp_single_main","p_all_endelement", __ref, _uri, _name, _text);
RemoteObject _node = RemoteObject.createImmutable("");
Debug.locals.put("Uri", _uri);
Debug.locals.put("Name", _name);
Debug.locals.put("Text", _text);
 BA.debugLineNum = 48;BA.debugLine="Sub P_all_EndElement (Uri As String, Name As Strin";
Debug.ShouldStop(32768);
 BA.debugLineNum = 49;BA.debugLine="Dim node As String= method & \"Response\"";
Debug.ShouldStop(65536);
_node = RemoteObject.concat(__ref.getField(true,"_method"),RemoteObject.createImmutable("Response"));Debug.locals.put("node", _node);Debug.locals.put("node", _node);
 BA.debugLineNum = 50;BA.debugLine="If parser.Parents.IndexOf(node) > -1 Then";
Debug.ShouldStop(131072);
if (RemoteObject.solveBoolean(">",__ref.getField(false,"_parser").getField(false,"Parents").runMethod(true,"IndexOf",(Object)((_node))),BA.numberCast(double.class, -(double) (0 + 1)))) { 
 BA.debugLineNum = 52;BA.debugLine="Select Case Name";
Debug.ShouldStop(524288);
switch (BA.switchObjectToInt(_name,BA.ObjectToString("send_infoResult"),BA.ObjectToString("up_scanResult"))) {
case 0: {
 BA.debugLineNum = 54;BA.debugLine="Try";
Debug.ShouldStop(2097152);
try { BA.debugLineNum = 55;BA.debugLine="Main.sql1.ExecNonQuery(\"Update tbl_Antwort se";
Debug.ShouldStop(4194304);
sp_single_main._main._sql1.runVoidMethod ("ExecNonQuery",(Object)(RemoteObject.concat(RemoteObject.createImmutable("Update tbl_Antwort set sync=1 where id ="),_text.runMethod(true,"ToString"))));
 BA.debugLineNum = 56;BA.debugLine="Log(Main.sql1.ExecQuerySingleResult(\"Select cou";
Debug.ShouldStop(8388608);
sp_single_main.__c.runVoidMethod ("Log",(Object)(sp_single_main._main._sql1.runMethod(true,"ExecQuerySingleResult",(Object)(RemoteObject.createImmutable("Select count(sync) from tbl_Antwort where sync=1")))));
 Debug.CheckDeviceExceptions();
} 
       catch (Exception e9) {
			BA.rdebugUtils.runVoidMethod("setLastException",__ref.getField(false, "ba"), e9.toString()); BA.debugLineNum = 58;BA.debugLine="If SubExists(Modules,EN) Then";
Debug.ShouldStop(33554432);
if (sp_single_main.__c.runMethod(true,"SubExists",__ref.getField(false, "ba"),(Object)(__ref.getField(false,"_modules")),(Object)(__ref.getField(true,"_en"))).<Boolean>get().booleanValue()) { 
 BA.debugLineNum = 59;BA.debugLine="CallSubDelayed2(Modules,EN,Text.ToString)";
Debug.ShouldStop(67108864);
sp_single_main.__c.runVoidMethod ("CallSubDelayed2",__ref.getField(false, "ba"),(Object)(__ref.getField(false,"_modules")),(Object)(__ref.getField(true,"_en")),(Object)((_text.runMethod(true,"ToString"))));
 };
 };
 break; }
case 1: {
 BA.debugLineNum = 67;BA.debugLine="Try";
Debug.ShouldStop(4);
try { BA.debugLineNum = 68;BA.debugLine="Main.sql1.ExecNonQuery(\"Update tbl_scan set";
Debug.ShouldStop(8);
sp_single_main._main._sql1.runVoidMethod ("ExecNonQuery",(Object)(RemoteObject.concat(RemoteObject.createImmutable("Update tbl_scan set sync=1 where id ="),_text.runMethod(true,"ToString"))));
 BA.debugLineNum = 69;BA.debugLine="Log(Main.sql1.ExecQuerySingleResult(\"Select cou";
Debug.ShouldStop(16);
sp_single_main.__c.runVoidMethod ("Log",(Object)(sp_single_main._main._sql1.runMethod(true,"ExecQuerySingleResult",(Object)(RemoteObject.createImmutable("Select count(sync) from tbl_scan where sync=1")))));
 Debug.CheckDeviceExceptions();
} 
       catch (Exception e18) {
			BA.rdebugUtils.runVoidMethod("setLastException",__ref.getField(false, "ba"), e18.toString()); BA.debugLineNum = 71;BA.debugLine="Log(LastException)";
Debug.ShouldStop(64);
sp_single_main.__c.runVoidMethod ("Log",(Object)(BA.ObjectToString(sp_single_main.__c.runMethod(false,"LastException",__ref.runMethod(false,"getActivityBA")))));
 BA.debugLineNum = 72;BA.debugLine="If SubExists(Modules,EN) Then";
Debug.ShouldStop(128);
if (sp_single_main.__c.runMethod(true,"SubExists",__ref.getField(false, "ba"),(Object)(__ref.getField(false,"_modules")),(Object)(__ref.getField(true,"_en"))).<Boolean>get().booleanValue()) { 
 BA.debugLineNum = 73;BA.debugLine="CallSubDelayed2(Modules,EN,Text.ToString)";
Debug.ShouldStop(256);
sp_single_main.__c.runVoidMethod ("CallSubDelayed2",__ref.getField(false, "ba"),(Object)(__ref.getField(false,"_modules")),(Object)(__ref.getField(true,"_en")),(Object)((_text.runMethod(true,"ToString"))));
 };
 };
 break; }
}
;
 };
 BA.debugLineNum = 84;BA.debugLine="End Sub";
Debug.ShouldStop(524288);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _p_ankunft_endelement(RemoteObject __ref,RemoteObject _uri,RemoteObject _name,RemoteObject _text) throws Exception{
try {
		Debug.PushSubsStack("P_ankunft_EndElement (sp_single_main) ","sp_single_main",9,__ref.getField(false, "ba"),__ref,85);
if (RapidSub.canDelegate("p_ankunft_endelement")) return __ref.runUserSub(false, "sp_single_main","p_ankunft_endelement", __ref, _uri, _name, _text);
Debug.locals.put("Uri", _uri);
Debug.locals.put("Name", _name);
Debug.locals.put("Text", _text);
 BA.debugLineNum = 85;BA.debugLine="Sub P_ankunft_EndElement (Uri As String, Name As S";
Debug.ShouldStop(1048576);
 BA.debugLineNum = 87;BA.debugLine="If parser.Parents.IndexOf(\"up_ankunftResponse\")";
Debug.ShouldStop(4194304);
if (RemoteObject.solveBoolean(">",__ref.getField(false,"_parser").getField(false,"Parents").runMethod(true,"IndexOf",(Object)((RemoteObject.createImmutable("up_ankunftResponse")))),BA.numberCast(double.class, -(double) (0 + 1)))) { 
 BA.debugLineNum = 89;BA.debugLine="Select Case Name";
Debug.ShouldStop(16777216);
switch (BA.switchObjectToInt(_name,BA.ObjectToString("up_ankunftResult"))) {
case 0: {
 BA.debugLineNum = 91;BA.debugLine="Try";
Debug.ShouldStop(67108864);
try { BA.debugLineNum = 92;BA.debugLine="Main.sql1.ExecNonQuery(\"Update tbl_Ankunft set";
Debug.ShouldStop(134217728);
sp_single_main._main._sql1.runVoidMethod ("ExecNonQuery",(Object)(RemoteObject.concat(RemoteObject.createImmutable("Update tbl_Ankunft set sync=1 where id ="),_text.runMethod(true,"ToString"))));
 BA.debugLineNum = 93;BA.debugLine="Log(Main.sql1.ExecQuerySingleResult(\"Select cou";
Debug.ShouldStop(268435456);
sp_single_main.__c.runVoidMethod ("Log",(Object)(sp_single_main._main._sql1.runMethod(true,"ExecQuerySingleResult",(Object)(RemoteObject.createImmutable("Select count(sync) from tbl_ankunft where sync=1")))));
 Debug.CheckDeviceExceptions();
} 
       catch (Exception e8) {
			BA.rdebugUtils.runVoidMethod("setLastException",__ref.getField(false, "ba"), e8.toString()); BA.debugLineNum = 95;BA.debugLine="Log(LastException)";
Debug.ShouldStop(1073741824);
sp_single_main.__c.runVoidMethod ("Log",(Object)(BA.ObjectToString(sp_single_main.__c.runMethod(false,"LastException",__ref.runMethod(false,"getActivityBA")))));
 BA.debugLineNum = 96;BA.debugLine="CallSubDelayed2(Modules,EN,Text.ToString)";
Debug.ShouldStop(-2147483648);
sp_single_main.__c.runVoidMethod ("CallSubDelayed2",__ref.getField(false, "ba"),(Object)(__ref.getField(false,"_modules")),(Object)(__ref.getField(true,"_en")),(Object)((_text.runMethod(true,"ToString"))));
 };
 break; }
}
;
 };
 BA.debugLineNum = 105;BA.debugLine="End Sub";
Debug.ShouldStop(256);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _p_eingabe_endelement(RemoteObject __ref,RemoteObject _uri,RemoteObject _name,RemoteObject _text) throws Exception{
try {
		Debug.PushSubsStack("P_eingabe_EndElement (sp_single_main) ","sp_single_main",9,__ref.getField(false, "ba"),__ref,106);
if (RapidSub.canDelegate("p_eingabe_endelement")) return __ref.runUserSub(false, "sp_single_main","p_eingabe_endelement", __ref, _uri, _name, _text);
RemoteObject _node = RemoteObject.createImmutable("");
Debug.locals.put("Uri", _uri);
Debug.locals.put("Name", _name);
Debug.locals.put("Text", _text);
 BA.debugLineNum = 106;BA.debugLine="Sub P_eingabe_EndElement (Uri As String, Name As S";
Debug.ShouldStop(512);
 BA.debugLineNum = 107;BA.debugLine="Dim node As String= method & \"Response\"";
Debug.ShouldStop(1024);
_node = RemoteObject.concat(__ref.getField(true,"_method"),RemoteObject.createImmutable("Response"));Debug.locals.put("node", _node);Debug.locals.put("node", _node);
 BA.debugLineNum = 108;BA.debugLine="If parser.Parents.IndexOf(node) > -1 Then";
Debug.ShouldStop(2048);
if (RemoteObject.solveBoolean(">",__ref.getField(false,"_parser").getField(false,"Parents").runMethod(true,"IndexOf",(Object)((_node))),BA.numberCast(double.class, -(double) (0 + 1)))) { 
 BA.debugLineNum = 109;BA.debugLine="node = method & \"Result\"";
Debug.ShouldStop(4096);
_node = RemoteObject.concat(__ref.getField(true,"_method"),RemoteObject.createImmutable("Result"));Debug.locals.put("node", _node);
 BA.debugLineNum = 110;BA.debugLine="Select Case Name";
Debug.ShouldStop(8192);
switch (BA.switchObjectToInt(_name,_node)) {
case 0: {
 BA.debugLineNum = 112;BA.debugLine="Try";
Debug.ShouldStop(32768);
try { BA.debugLineNum = 113;BA.debugLine="Main.sql1.ExecNonQuery(\"Update tbl_nachtrag_lo";
Debug.ShouldStop(65536);
sp_single_main._main._sql1.runVoidMethod ("ExecNonQuery",(Object)(RemoteObject.concat(RemoteObject.createImmutable("Update tbl_nachtrag_log_app set sync=1 where id ="),_text.runMethod(true,"ToString"))));
 BA.debugLineNum = 114;BA.debugLine="Log(Main.sql1.ExecQuerySingleResult(\"Select cou";
Debug.ShouldStop(131072);
sp_single_main.__c.runVoidMethod ("Log",(Object)(sp_single_main._main._sql1.runMethod(true,"ExecQuerySingleResult",(Object)(RemoteObject.createImmutable("Select count(sync) from tbl_nachtrag_log_app where sync=1")))));
 Debug.CheckDeviceExceptions();
} 
       catch (Exception e10) {
			BA.rdebugUtils.runVoidMethod("setLastException",__ref.getField(false, "ba"), e10.toString()); BA.debugLineNum = 116;BA.debugLine="CallSubDelayed2(Modules,EN,Text.ToString)";
Debug.ShouldStop(524288);
sp_single_main.__c.runVoidMethod ("CallSubDelayed2",__ref.getField(false, "ba"),(Object)(__ref.getField(false,"_modules")),(Object)(__ref.getField(true,"_en")),(Object)((_text.runMethod(true,"ToString"))));
 BA.debugLineNum = 117;BA.debugLine="Log(LastException)";
Debug.ShouldStop(1048576);
sp_single_main.__c.runVoidMethod ("Log",(Object)(BA.ObjectToString(sp_single_main.__c.runMethod(false,"LastException",__ref.runMethod(false,"getActivityBA")))));
 };
 break; }
}
;
 };
 BA.debugLineNum = 126;BA.debugLine="End Sub";
Debug.ShouldStop(536870912);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _p_logincheck_endelement(RemoteObject __ref,RemoteObject _uri,RemoteObject _name,RemoteObject _text) throws Exception{
try {
		Debug.PushSubsStack("P_logincheck_EndElement (sp_single_main) ","sp_single_main",9,__ref.getField(false, "ba"),__ref,21);
if (RapidSub.canDelegate("p_logincheck_endelement")) return __ref.runUserSub(false, "sp_single_main","p_logincheck_endelement", __ref, _uri, _name, _text);
Debug.locals.put("Uri", _uri);
Debug.locals.put("Name", _name);
Debug.locals.put("Text", _text);
 BA.debugLineNum = 21;BA.debugLine="Sub P_logincheck_EndElement (Uri As String, Name A";
Debug.ShouldStop(1048576);
 BA.debugLineNum = 22;BA.debugLine="If parser.Parents.IndexOf(\"logincheckResponse\")";
Debug.ShouldStop(2097152);
if (RemoteObject.solveBoolean(">",__ref.getField(false,"_parser").getField(false,"Parents").runMethod(true,"IndexOf",(Object)((RemoteObject.createImmutable("logincheckResponse")))),BA.numberCast(double.class, -(double) (0 + 1)))) { 
 BA.debugLineNum = 23;BA.debugLine="Select Case Name";
Debug.ShouldStop(4194304);
switch (BA.switchObjectToInt(_name,BA.ObjectToString("logincheckResult"))) {
case 0: {
 BA.debugLineNum = 25;BA.debugLine="If SubExists(Modules,EN) Then";
Debug.ShouldStop(16777216);
if (sp_single_main.__c.runMethod(true,"SubExists",__ref.getField(false, "ba"),(Object)(__ref.getField(false,"_modules")),(Object)(__ref.getField(true,"_en"))).<Boolean>get().booleanValue()) { 
 BA.debugLineNum = 26;BA.debugLine="CallSubDelayed2(Modules,EN,Text.ToString)";
Debug.ShouldStop(33554432);
sp_single_main.__c.runVoidMethod ("CallSubDelayed2",__ref.getField(false, "ba"),(Object)(__ref.getField(false,"_modules")),(Object)(__ref.getField(true,"_en")),(Object)((_text.runMethod(true,"ToString"))));
 };
 break; }
}
;
 };
 BA.debugLineNum = 30;BA.debugLine="End Sub";
Debug.ShouldStop(536870912);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _p_quittung_endelement(RemoteObject __ref,RemoteObject _uri,RemoteObject _name,RemoteObject _text) throws Exception{
try {
		Debug.PushSubsStack("P_quittung_EndElement (sp_single_main) ","sp_single_main",9,__ref.getField(false, "ba"),__ref,31);
if (RapidSub.canDelegate("p_quittung_endelement")) return __ref.runUserSub(false, "sp_single_main","p_quittung_endelement", __ref, _uri, _name, _text);
Debug.locals.put("Uri", _uri);
Debug.locals.put("Name", _name);
Debug.locals.put("Text", _text);
 BA.debugLineNum = 31;BA.debugLine="Sub P_quittung_EndElement (Uri As String, Name As";
Debug.ShouldStop(1073741824);
 BA.debugLineNum = 33;BA.debugLine="If parser.Parents.IndexOf(\"up_quitResponse\") > -";
Debug.ShouldStop(1);
if (RemoteObject.solveBoolean(">",__ref.getField(false,"_parser").getField(false,"Parents").runMethod(true,"IndexOf",(Object)((RemoteObject.createImmutable("up_quitResponse")))),BA.numberCast(double.class, -(double) (0 + 1)))) { 
 BA.debugLineNum = 34;BA.debugLine="Select Case Name";
Debug.ShouldStop(2);
switch (BA.switchObjectToInt(_name,BA.ObjectToString("up_quitResult"))) {
case 0: {
 BA.debugLineNum = 36;BA.debugLine="Try";
Debug.ShouldStop(8);
try { BA.debugLineNum = 37;BA.debugLine="Main.sql1.ExecNonQuery(\"Update tbl_quittung s";
Debug.ShouldStop(16);
sp_single_main._main._sql1.runVoidMethod ("ExecNonQuery",(Object)(RemoteObject.concat(RemoteObject.createImmutable("Update tbl_quittung set sync=1 where id ="),_text.runMethod(true,"ToString"))));
 BA.debugLineNum = 38;BA.debugLine="Log(Main.sql1.ExecQuerySingleResult(\"Select cou";
Debug.ShouldStop(32);
sp_single_main.__c.runVoidMethod ("Log",(Object)(sp_single_main._main._sql1.runMethod(true,"ExecQuerySingleResult",(Object)(RemoteObject.createImmutable("Select count(sync) from tbl_quittung where sync=1")))));
 Debug.CheckDeviceExceptions();
} 
       catch (Exception e8) {
			BA.rdebugUtils.runVoidMethod("setLastException",__ref.getField(false, "ba"), e8.toString()); BA.debugLineNum = 40;BA.debugLine="If SubExists(Modules,EN) Then";
Debug.ShouldStop(128);
if (sp_single_main.__c.runMethod(true,"SubExists",__ref.getField(false, "ba"),(Object)(__ref.getField(false,"_modules")),(Object)(__ref.getField(true,"_en"))).<Boolean>get().booleanValue()) { 
 BA.debugLineNum = 41;BA.debugLine="CallSubDelayed2(Modules,EN,Text.ToString)";
Debug.ShouldStop(256);
sp_single_main.__c.runVoidMethod ("CallSubDelayed2",__ref.getField(false, "ba"),(Object)(__ref.getField(false,"_modules")),(Object)(__ref.getField(true,"_en")),(Object)((_text.runMethod(true,"ToString"))));
 };
 };
 break; }
}
;
 };
 BA.debugLineNum = 46;BA.debugLine="End Sub";
Debug.ShouldStop(8192);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _sendrequest(RemoteObject __ref,RemoteObject _methodname,RemoteObject _jobname,RemoteObject _last) throws Exception{
try {
		Debug.PushSubsStack("SendRequest (sp_single_main) ","sp_single_main",9,__ref.getField(false, "ba"),__ref,148);
if (RapidSub.canDelegate("sendrequest")) return __ref.runUserSub(false, "sp_single_main","sendrequest", __ref, _methodname, _jobname, _last);
RemoteObject _ht1 = RemoteObject.declareNull("anywheresoftware.b4a.samples.httputils2.httpjob");
Debug.locals.put("MethodName", _methodname);
Debug.locals.put("jobname", _jobname);
Debug.locals.put("last", _last);
 BA.debugLineNum = 148;BA.debugLine="Sub SendRequest( MethodName As String,jobname As S";
Debug.ShouldStop(524288);
 BA.debugLineNum = 150;BA.debugLine="last1=last";
Debug.ShouldStop(2097152);
__ref.setField ("_last1",_last);
 BA.debugLineNum = 151;BA.debugLine="xml_sendstriing= xml_sendstriing & \"</\" & Meth";
Debug.ShouldStop(4194304);
__ref.setField ("_xml_sendstriing",RemoteObject.concat(__ref.getField(true,"_xml_sendstriing"),RemoteObject.createImmutable("</"),_methodname,RemoteObject.createImmutable(">")));
 BA.debugLineNum = 152;BA.debugLine="xml_sendstriing= xml_sendstriing & \"</soap:Body>";
Debug.ShouldStop(8388608);
__ref.setField ("_xml_sendstriing",RemoteObject.concat(__ref.getField(true,"_xml_sendstriing"),RemoteObject.createImmutable("</soap:Body>")));
 BA.debugLineNum = 153;BA.debugLine="xml_sendstriing= xml_sendstriing & \"</soap:Envelop";
Debug.ShouldStop(16777216);
__ref.setField ("_xml_sendstriing",RemoteObject.concat(__ref.getField(true,"_xml_sendstriing"),RemoteObject.createImmutable("</soap:Envelope>")));
 BA.debugLineNum = 154;BA.debugLine="Log(xml_sendstriing)";
Debug.ShouldStop(33554432);
sp_single_main.__c.runVoidMethod ("Log",(Object)(__ref.getField(true,"_xml_sendstriing")));
 BA.debugLineNum = 155;BA.debugLine="Log(\"string ist fertig\")";
Debug.ShouldStop(67108864);
sp_single_main.__c.runVoidMethod ("Log",(Object)(RemoteObject.createImmutable("string ist fertig")));
 BA.debugLineNum = 156;BA.debugLine="Dim ht1 As HttpJob";
Debug.ShouldStop(134217728);
_ht1 = RemoteObject.createNew ("anywheresoftware.b4a.samples.httputils2.httpjob");Debug.locals.put("ht1", _ht1);
 BA.debugLineNum = 157;BA.debugLine="ht1.Initialize(jobname,Me)";
Debug.ShouldStop(268435456);
_ht1.runVoidMethod ("_initialize",__ref.getField(false, "ba"),(Object)(_jobname),(Object)(__ref));
 BA.debugLineNum = 159;BA.debugLine="ht1.PostString(UrlWS,xml_sendstriing)";
Debug.ShouldStop(1073741824);
_ht1.runVoidMethod ("_poststring",(Object)(__ref.getField(true,"_urlws")),(Object)(__ref.getField(true,"_xml_sendstriing")));
 BA.debugLineNum = 162;BA.debugLine="ht1.GetRequest.SetHeader(\"SoapAction\", \"http://te";
Debug.ShouldStop(2);
_ht1.runMethod(false,"_getrequest").runVoidMethod ("SetHeader",(Object)(BA.ObjectToString("SoapAction")),(Object)(RemoteObject.concat(RemoteObject.createImmutable("http://tempuri.org/"),_methodname,RemoteObject.createImmutable(""))));
 BA.debugLineNum = 165;BA.debugLine="ht1.GetRequest.SetContentType(\"text/xml; charset=";
Debug.ShouldStop(16);
_ht1.runMethod(false,"_getrequest").runVoidMethod ("SetContentType",(Object)(RemoteObject.createImmutable("text/xml; charset=utf-8")));
 BA.debugLineNum = 166;BA.debugLine="ht1.GetRequest.Timeout=2000";
Debug.ShouldStop(32);
_ht1.runMethod(false,"_getrequest").runVoidMethod ("setTimeout",BA.numberCast(int.class, 2000));
 BA.debugLineNum = 168;BA.debugLine="End Sub";
Debug.ShouldStop(128);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
}