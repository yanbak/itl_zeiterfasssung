package b4a.example;

import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.pc.*;

public class soap_up_subs_0 {


public static RemoteObject  _addfield(RemoteObject __ref,RemoteObject _fieldname,RemoteObject _fieldtype,RemoteObject _fieldvalue) throws Exception{
try {
		Debug.PushSubsStack("AddField (soap_up) ","soap_up",10,__ref.getField(false, "ba"),__ref,20);
if (RapidSub.canDelegate("addfield")) return __ref.runUserSub(false, "soap_up","addfield", __ref, _fieldname, _fieldtype, _fieldvalue);
Debug.locals.put("FieldName", _fieldname);
Debug.locals.put("FieldType", _fieldtype);
Debug.locals.put("FieldValue", _fieldvalue);
 BA.debugLineNum = 20;BA.debugLine="Public Sub AddField(FieldName As String,FieldType";
Debug.ShouldStop(524288);
 BA.debugLineNum = 21;BA.debugLine="xml_sendstriing=xml_sendstriing &CRLF & $\"<${Fiel";
Debug.ShouldStop(1048576);
__ref.setField ("_xml_sendstriing",RemoteObject.concat(__ref.getField(true,"_xml_sendstriing"),soap_up.__c.getField(true,"CRLF"),(RemoteObject.concat(RemoteObject.createImmutable("<"),soap_up.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_fieldname))),RemoteObject.createImmutable(" >"),soap_up.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_fieldvalue))),RemoteObject.createImmutable("</"),soap_up.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_fieldname))),RemoteObject.createImmutable(">")))));
 BA.debugLineNum = 22;BA.debugLine="End Sub";
Debug.ShouldStop(2097152);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _class_globals(RemoteObject __ref) throws Exception{
 //BA.debugLineNum = 1;BA.debugLine="Sub Class_Globals";
 //BA.debugLineNum = 2;BA.debugLine="Public TYPE_STRING   As String = \"xsd:string\"";
soap_up._type_string = BA.ObjectToString("xsd:string");__ref.setField("_type_string",soap_up._type_string);
 //BA.debugLineNum = 3;BA.debugLine="Public TYPE_INTEGER  As String = \"xsd:int\"";
soap_up._type_integer = BA.ObjectToString("xsd:int");__ref.setField("_type_integer",soap_up._type_integer);
 //BA.debugLineNum = 4;BA.debugLine="Public TYPE_FLOAT    As String = \"xsd:float\"";
soap_up._type_float = BA.ObjectToString("xsd:float");__ref.setField("_type_float",soap_up._type_float);
 //BA.debugLineNum = 5;BA.debugLine="Public TYPE_DATE     As String = \"xsd:date\"";
soap_up._type_date = BA.ObjectToString("xsd:date");__ref.setField("_type_date",soap_up._type_date);
 //BA.debugLineNum = 6;BA.debugLine="Public TYPE_DOUBLE   As String = \"xsd:double\"";
soap_up._type_double = BA.ObjectToString("xsd:double");__ref.setField("_type_double",soap_up._type_double);
 //BA.debugLineNum = 7;BA.debugLine="Public TYPE_BINARY   As String = \"xsd:base64Binar";
soap_up._type_binary = BA.ObjectToString("xsd:base64Binary");__ref.setField("_type_binary",soap_up._type_binary);
 //BA.debugLineNum = 8;BA.debugLine="Public xml_sendstriing As String";
soap_up._xml_sendstriing = RemoteObject.createImmutable("");__ref.setField("_xml_sendstriing",soap_up._xml_sendstriing);
 //BA.debugLineNum = 10;BA.debugLine="End Sub";
return RemoteObject.createImmutable("");
}
public static RemoteObject  _footer(RemoteObject __ref,RemoteObject _methodname) throws Exception{
try {
		Debug.PushSubsStack("footer (soap_up) ","soap_up",10,__ref.getField(false, "ba"),__ref,24);
if (RapidSub.canDelegate("footer")) return __ref.runUserSub(false, "soap_up","footer", __ref, _methodname);
Debug.locals.put("MethodName", _methodname);
 BA.debugLineNum = 24;BA.debugLine="Sub footer( MethodName As String)";
Debug.ShouldStop(8388608);
 BA.debugLineNum = 25;BA.debugLine="xml_sendstriing= xml_sendstriing & \"</\" & MethodN";
Debug.ShouldStop(16777216);
__ref.setField ("_xml_sendstriing",RemoteObject.concat(__ref.getField(true,"_xml_sendstriing"),RemoteObject.createImmutable("</"),_methodname,RemoteObject.createImmutable(">")));
 BA.debugLineNum = 26;BA.debugLine="xml_sendstriing= xml_sendstriing & \"</soap:Body>\"";
Debug.ShouldStop(33554432);
__ref.setField ("_xml_sendstriing",RemoteObject.concat(__ref.getField(true,"_xml_sendstriing"),RemoteObject.createImmutable("</soap:Body>")));
 BA.debugLineNum = 27;BA.debugLine="xml_sendstriing= xml_sendstriing & \"</soap:Envelo";
Debug.ShouldStop(67108864);
__ref.setField ("_xml_sendstriing",RemoteObject.concat(__ref.getField(true,"_xml_sendstriing"),RemoteObject.createImmutable("</soap:Envelope>")));
 BA.debugLineNum = 28;BA.debugLine="End Sub";
Debug.ShouldStop(134217728);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _initialize(RemoteObject __ref,RemoteObject _ba,RemoteObject _url,RemoteObject _methodname) throws Exception{
try {
		Debug.PushSubsStack("initialize (soap_up) ","soap_up",10,__ref.getField(false, "ba"),__ref,13);
if (RapidSub.canDelegate("initialize")) return __ref.runUserSub(false, "soap_up","initialize", __ref, _ba, _url, _methodname);
__ref.runVoidMethodAndSync("innerInitializeHelper", _ba);
Debug.locals.put("ba", _ba);
Debug.locals.put("Url", _url);
Debug.locals.put("MethodName", _methodname);
 BA.debugLineNum = 13;BA.debugLine="Public Sub initialize(Url As String,MethodName As";
Debug.ShouldStop(4096);
 BA.debugLineNum = 14;BA.debugLine="xml_sendstriing =xml_sendstriing & \"<?xml version";
Debug.ShouldStop(8192);
__ref.setField ("_xml_sendstriing",RemoteObject.concat(__ref.getField(true,"_xml_sendstriing"),RemoteObject.createImmutable("<?xml version="),soap_up.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable("1.0"),soap_up.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable(" encoding="),soap_up.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable("utf-8"),soap_up.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable("?>")));
 BA.debugLineNum = 15;BA.debugLine="xml_sendstriing= xml_sendstriing & \"<soap:Envelop";
Debug.ShouldStop(16384);
__ref.setField ("_xml_sendstriing",RemoteObject.concat(__ref.getField(true,"_xml_sendstriing"),RemoteObject.createImmutable("<soap:Envelope xmlns:xsi="),soap_up.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable("http://www.w3.org/2001/XMLSchema-instance"),soap_up.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable(" xmlns:xsd="),soap_up.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable("http://www.w3.org/2001/XMLSchema"),soap_up.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable(" xmlns:soap="),soap_up.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable("http://schemas.xmlsoap.org/soap/envelope/"),soap_up.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable(">")));
 BA.debugLineNum = 16;BA.debugLine="xml_sendstriing= xml_sendstriing & \"<soap:Body>\"";
Debug.ShouldStop(32768);
__ref.setField ("_xml_sendstriing",RemoteObject.concat(__ref.getField(true,"_xml_sendstriing"),RemoteObject.createImmutable("<soap:Body>")));
 BA.debugLineNum = 17;BA.debugLine="xml_sendstriing= xml_sendstriing & \"<\" & MethodNa";
Debug.ShouldStop(65536);
__ref.setField ("_xml_sendstriing",RemoteObject.concat(__ref.getField(true,"_xml_sendstriing"),RemoteObject.createImmutable("<"),_methodname,RemoteObject.createImmutable(" xmlns="),soap_up.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable("http://tempuri.org/"),soap_up.__c.runMethod(true,"Chr",(Object)(BA.numberCast(int.class, 34))),RemoteObject.createImmutable(">")));
 BA.debugLineNum = 18;BA.debugLine="End Sub";
Debug.ShouldStop(131072);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
}