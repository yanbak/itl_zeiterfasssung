package b4a.example;

import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.pc.*;

public class main_subs_0 {


public static RemoteObject  _activity_create(RemoteObject _firsttime) throws Exception{
try {
		Debug.PushSubsStack("Activity_Create (main) ","main",0,main.mostCurrent.activityBA,main.mostCurrent,59);
if (RapidSub.canDelegate("activity_create")) return b4a.example.main.remoteMe.runUserSub(false, "main","activity_create", _firsttime);
RemoteObject _dbupdatever = RemoteObject.createImmutable(0);
RemoteObject _usr = RemoteObject.createImmutable("");
RemoteObject _pwd = RemoteObject.createImmutable("");
RemoteObject _c_user = RemoteObject.declareNull("anywheresoftware.b4a.sql.SQL.CursorWrapper");
Debug.locals.put("FirstTime", _firsttime);
 BA.debugLineNum = 59;BA.debugLine="Sub Activity_Create(FirstTime As Boolean)";
Debug.ShouldStop(67108864);
 BA.debugLineNum = 62;BA.debugLine="If File.Exists(File.DirInternal,\"db.db\") = False";
Debug.ShouldStop(536870912);
if (RemoteObject.solveBoolean("=",main.mostCurrent.__c.getField(false,"File").runMethod(true,"Exists",(Object)(main.mostCurrent.__c.getField(false,"File").runMethod(true,"getDirInternal")),(Object)(RemoteObject.createImmutable("db.db"))),main.mostCurrent.__c.getField(true,"False"))) { 
 BA.debugLineNum = 63;BA.debugLine="File.Copy(File.DirAssets,\"db.db\",File.DirInterna";
Debug.ShouldStop(1073741824);
main.mostCurrent.__c.getField(false,"File").runVoidMethod ("Copy",(Object)(main.mostCurrent.__c.getField(false,"File").runMethod(true,"getDirAssets")),(Object)(BA.ObjectToString("db.db")),(Object)(main.mostCurrent.__c.getField(false,"File").runMethod(true,"getDirInternal")),(Object)(RemoteObject.createImmutable("db.db")));
 };
 BA.debugLineNum = 65;BA.debugLine="File.copy(File.DirInternal,\"db.db\",File.DirDefaul";
Debug.ShouldStop(1);
main.mostCurrent.__c.getField(false,"File").runVoidMethod ("Copy",(Object)(main.mostCurrent.__c.getField(false,"File").runMethod(true,"getDirInternal")),(Object)(BA.ObjectToString("db.db")),(Object)(main.mostCurrent.__c.getField(false,"File").runMethod(true,"getDirDefaultExternal")),(Object)(RemoteObject.createImmutable("db.db")));
 BA.debugLineNum = 66;BA.debugLine="If FirstTime  Then";
Debug.ShouldStop(2);
if (_firsttime.<Boolean>get().booleanValue()) { 
 BA.debugLineNum = 67;BA.debugLine="Try";
Debug.ShouldStop(4);
try { BA.debugLineNum = 68;BA.debugLine="sql1.Initialize(File.DirInternal, \"db.db\", False";
Debug.ShouldStop(8);
main._sql1.runVoidMethod ("Initialize",(Object)(main.mostCurrent.__c.getField(false,"File").runMethod(true,"getDirInternal")),(Object)(BA.ObjectToString("db.db")),(Object)(main.mostCurrent.__c.getField(true,"False")));
 BA.debugLineNum = 71;BA.debugLine="If File.Exists(File.DirInternal,\"ver\")  Then";
Debug.ShouldStop(64);
if (main.mostCurrent.__c.getField(false,"File").runMethod(true,"Exists",(Object)(main.mostCurrent.__c.getField(false,"File").runMethod(true,"getDirInternal")),(Object)(RemoteObject.createImmutable("ver"))).<Boolean>get().booleanValue()) { 
 BA.debugLineNum = 72;BA.debugLine="Dim dbupdatever As Double";
Debug.ShouldStop(128);
_dbupdatever = RemoteObject.createImmutable(0);Debug.locals.put("dbupdatever", _dbupdatever);
 BA.debugLineNum = 73;BA.debugLine="dbupdatever = 1.45";
Debug.ShouldStop(256);
_dbupdatever = BA.numberCast(double.class, 1.45);Debug.locals.put("dbupdatever", _dbupdatever);
 BA.debugLineNum = 74;BA.debugLine="If File.ReadString(File.DirInternal,\"ver\") <dbu";
Debug.ShouldStop(512);
if (RemoteObject.solveBoolean("<",BA.numberCast(double.class, main.mostCurrent.__c.getField(false,"File").runMethod(true,"ReadString",(Object)(main.mostCurrent.__c.getField(false,"File").runMethod(true,"getDirInternal")),(Object)(RemoteObject.createImmutable("ver")))),_dbupdatever)) { 
 BA.debugLineNum = 75;BA.debugLine="Msgbox(\"Datenbank wird neu initialisiert, bitt";
Debug.ShouldStop(1024);
main.mostCurrent.__c.runVoidMethodAndSync ("Msgbox",(Object)(BA.ObjectToCharSequence("Datenbank wird neu initialisiert, bitte starten sie die Anwendung neu")),(Object)(BA.ObjectToCharSequence(RemoteObject.createImmutable("Achtung"))),main.mostCurrent.activityBA);
 BA.debugLineNum = 76;BA.debugLine="File.Copy(File.DirAssets,\"db.db\",File.DirInter";
Debug.ShouldStop(2048);
main.mostCurrent.__c.getField(false,"File").runVoidMethod ("Copy",(Object)(main.mostCurrent.__c.getField(false,"File").runMethod(true,"getDirAssets")),(Object)(BA.ObjectToString("db.db")),(Object)(main.mostCurrent.__c.getField(false,"File").runMethod(true,"getDirInternal")),(Object)(RemoteObject.createImmutable("db.db")));
 BA.debugLineNum = 77;BA.debugLine="File.Delete(File.DirInternal,\"ver\")";
Debug.ShouldStop(4096);
main.mostCurrent.__c.getField(false,"File").runVoidMethod ("Delete",(Object)(main.mostCurrent.__c.getField(false,"File").runMethod(true,"getDirInternal")),(Object)(RemoteObject.createImmutable("ver")));
 BA.debugLineNum = 78;BA.debugLine="File.WriteString(File.DirInternal,\"ver\",pm.Get";
Debug.ShouldStop(8192);
main.mostCurrent.__c.getField(false,"File").runVoidMethod ("WriteString",(Object)(main.mostCurrent.__c.getField(false,"File").runMethod(true,"getDirInternal")),(Object)(BA.ObjectToString("ver")),(Object)(main.mostCurrent._pm.runMethod(true,"GetVersionName",(Object)((RemoteObject.createImmutable("com.interseroh.kml"))))));
 BA.debugLineNum = 79;BA.debugLine="ExitApplication";
Debug.ShouldStop(16384);
main.mostCurrent.__c.runVoidMethod ("ExitApplication");
 };
 }else {
 BA.debugLineNum = 83;BA.debugLine="File.WriteString(File.DirInternal,\"ver\",pm.GetV";
Debug.ShouldStop(262144);
main.mostCurrent.__c.getField(false,"File").runVoidMethod ("WriteString",(Object)(main.mostCurrent.__c.getField(false,"File").runMethod(true,"getDirInternal")),(Object)(BA.ObjectToString("ver")),(Object)(main.mostCurrent._pm.runMethod(true,"GetVersionName",(Object)((RemoteObject.createImmutable("com.interseroh.kml"))))));
 };
 BA.debugLineNum = 85;BA.debugLine="Dim usr As String";
Debug.ShouldStop(1048576);
_usr = RemoteObject.createImmutable("");Debug.locals.put("usr", _usr);
 BA.debugLineNum = 86;BA.debugLine="Dim pwd As String";
Debug.ShouldStop(2097152);
_pwd = RemoteObject.createImmutable("");Debug.locals.put("pwd", _pwd);
 BA.debugLineNum = 87;BA.debugLine="Dim c_user As Cursor";
Debug.ShouldStop(4194304);
_c_user = RemoteObject.createNew ("anywheresoftware.b4a.sql.SQL.CursorWrapper");Debug.locals.put("c_user", _c_user);
 BA.debugLineNum = 88;BA.debugLine="c_user=sql1.ExecQuery(\"select * from tbl_usr\")";
Debug.ShouldStop(8388608);
_c_user.setObject(main._sql1.runMethod(false,"ExecQuery",(Object)(RemoteObject.createImmutable("select * from tbl_usr"))));
 BA.debugLineNum = 89;BA.debugLine="c_user.Position=0";
Debug.ShouldStop(16777216);
_c_user.runMethod(true,"setPosition",BA.numberCast(int.class, 0));
 BA.debugLineNum = 90;BA.debugLine="usr=c_user.GetString(\"username\")";
Debug.ShouldStop(33554432);
_usr = _c_user.runMethod(true,"GetString",(Object)(RemoteObject.createImmutable("username")));Debug.locals.put("usr", _usr);
 BA.debugLineNum = 91;BA.debugLine="anmeldung=True";
Debug.ShouldStop(67108864);
main._anmeldung = main.mostCurrent.__c.getField(true,"True");
 Debug.CheckDeviceExceptions();
} 
       catch (Exception e29) {
			BA.rdebugUtils.runVoidMethod("setLastException",main.processBA, e29.toString()); BA.debugLineNum = 96;BA.debugLine="anmeldung=False";
Debug.ShouldStop(-2147483648);
main._anmeldung = main.mostCurrent.__c.getField(true,"False");
 BA.debugLineNum = 97;BA.debugLine="Msgbox(\"Bitte geben sie ihre Zugangsdaten ein\",\"";
Debug.ShouldStop(1);
main.mostCurrent.__c.runVoidMethodAndSync ("Msgbox",(Object)(BA.ObjectToCharSequence("Bitte geben sie ihre Zugangsdaten ein")),(Object)(BA.ObjectToCharSequence(RemoteObject.createImmutable("Achtung"))),main.mostCurrent.activityBA);
 };
 };
 BA.debugLineNum = 100;BA.debugLine="scale.SetRate(0.3)";
Debug.ShouldStop(8);
main.mostCurrent._scale.runVoidMethod ("_setrate",main.mostCurrent.activityBA,(Object)(BA.numberCast(double.class, 0.3)));
 BA.debugLineNum = 101;BA.debugLine="Activity.LoadLayout(\"menu1\")";
Debug.ShouldStop(16);
main.mostCurrent._activity.runMethodAndSync(false,"LoadLayout",(Object)(RemoteObject.createImmutable("menu1")),main.mostCurrent.activityBA);
 BA.debugLineNum = 103;BA.debugLine="scale.ScaleAll(Panel1,True)";
Debug.ShouldStop(64);
main.mostCurrent._scale.runVoidMethod ("_scaleall",main.mostCurrent.activityBA,RemoteObject.declareNull("anywheresoftware.b4a.AbsObjectWrapper").runMethod(false, "ConvertToWrapper", RemoteObject.createNew("anywheresoftware.b4a.objects.ActivityWrapper"), main.mostCurrent._panel1.getObject()),(Object)(main.mostCurrent.__c.getField(true,"True")));
 BA.debugLineNum = 104;BA.debugLine="If File.Exists(File.DirInternal,\"db.db\") = False";
Debug.ShouldStop(128);
if (RemoteObject.solveBoolean("=",main.mostCurrent.__c.getField(false,"File").runMethod(true,"Exists",(Object)(main.mostCurrent.__c.getField(false,"File").runMethod(true,"getDirInternal")),(Object)(RemoteObject.createImmutable("db.db"))),main.mostCurrent.__c.getField(true,"False"))) { 
 BA.debugLineNum = 105;BA.debugLine="File.Copy(File.DirAssets,\"db.db\",File.DirInterna";
Debug.ShouldStop(256);
main.mostCurrent.__c.getField(false,"File").runVoidMethod ("Copy",(Object)(main.mostCurrent.__c.getField(false,"File").runMethod(true,"getDirAssets")),(Object)(BA.ObjectToString("db.db")),(Object)(main.mostCurrent.__c.getField(false,"File").runMethod(true,"getDirInternal")),(Object)(RemoteObject.createImmutable("db.db")));
 };
 BA.debugLineNum = 108;BA.debugLine="If anmeldung= True Then";
Debug.ShouldStop(2048);
if (RemoteObject.solveBoolean("=",main._anmeldung,main.mostCurrent.__c.getField(true,"True"))) { 
 BA.debugLineNum = 109;BA.debugLine="Try";
Debug.ShouldStop(4096);
try { BA.debugLineNum = 110;BA.debugLine="u1.Initialize";
Debug.ShouldStop(8192);
main._u1.runClassMethod (b4a.example.update_sender_main.class, "_initialize",main.processBA);
 BA.debugLineNum = 111;BA.debugLine="get_version";
Debug.ShouldStop(16384);
_get_version();
 Debug.CheckDeviceExceptions();
} 
       catch (Exception e44) {
			BA.rdebugUtils.runVoidMethod("setLastException",main.processBA, e44.toString()); BA.debugLineNum = 113;BA.debugLine="Log(LastException)";
Debug.ShouldStop(65536);
main.mostCurrent.__c.runVoidMethod ("Log",(Object)(BA.ObjectToString(main.mostCurrent.__c.runMethod(false,"LastException",main.mostCurrent.activityBA))));
 };
 }else {
 BA.debugLineNum = 116;BA.debugLine="StartActivity(\"setup\")";
Debug.ShouldStop(524288);
main.mostCurrent.__c.runVoidMethod ("StartActivity",main.processBA,(Object)((RemoteObject.createImmutable("setup"))));
 };
 BA.debugLineNum = 118;BA.debugLine="End Sub";
Debug.ShouldStop(2097152);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _activity_pause(RemoteObject _userclosed) throws Exception{
try {
		Debug.PushSubsStack("Activity_Pause (main) ","main",0,main.mostCurrent.activityBA,main.mostCurrent,144);
if (RapidSub.canDelegate("activity_pause")) return b4a.example.main.remoteMe.runUserSub(false, "main","activity_pause", _userclosed);
Debug.locals.put("UserClosed", _userclosed);
 BA.debugLineNum = 144;BA.debugLine="Sub Activity_Pause (UserClosed As Boolean)";
Debug.ShouldStop(32768);
 BA.debugLineNum = 146;BA.debugLine="End Sub";
Debug.ShouldStop(131072);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _activity_resume() throws Exception{
try {
		Debug.PushSubsStack("Activity_Resume (main) ","main",0,main.mostCurrent.activityBA,main.mostCurrent,125);
if (RapidSub.canDelegate("activity_resume")) return b4a.example.main.remoteMe.runUserSub(false, "main","activity_resume");
 BA.debugLineNum = 125;BA.debugLine="Sub Activity_Resume";
Debug.ShouldStop(268435456);
 BA.debugLineNum = 127;BA.debugLine="End Sub";
Debug.ShouldStop(1073741824);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _get_version() throws Exception{
try {
		Debug.PushSubsStack("get_version (main) ","main",0,main.mostCurrent.activityBA,main.mostCurrent,119);
if (RapidSub.canDelegate("get_version")) return b4a.example.main.remoteMe.runUserSub(false, "main","get_version");
 BA.debugLineNum = 119;BA.debugLine="Sub get_version";
Debug.ShouldStop(4194304);
 BA.debugLineNum = 120;BA.debugLine="s1.Initialize(use_service &\"?op=get_version\",\"get";
Debug.ShouldStop(8388608);
main._s1.runClassMethod (b4a.example.sp_tabelle.class, "_initialize",main.processBA,(Object)(RemoteObject.concat(main._use_service,RemoteObject.createImmutable("?op=get_version"))),(Object)(BA.ObjectToString("get_version")),(Object)(main.getObject()),(Object)(RemoteObject.createImmutable("xx")));
 BA.debugLineNum = 121;BA.debugLine="s1.AddField(\"usr\",s1.TYPE_STRING,u1.usr)";
Debug.ShouldStop(16777216);
main._s1.runClassMethod (b4a.example.sp_tabelle.class, "_addfield",(Object)(BA.ObjectToString("usr")),(Object)(main._s1.getField(true,"_type_string")),(Object)(main._u1.getField(true,"_usr")));
 BA.debugLineNum = 122;BA.debugLine="s1.AddField(\"pwd\",s1.TYPE_STRING,u1.pwd)";
Debug.ShouldStop(33554432);
main._s1.runClassMethod (b4a.example.sp_tabelle.class, "_addfield",(Object)(BA.ObjectToString("pwd")),(Object)(main._s1.getField(true,"_type_string")),(Object)(main._u1.getField(true,"_pwd")));
 BA.debugLineNum = 123;BA.debugLine="s1.SendRequest(\"get_version\",\"get_version\")";
Debug.ShouldStop(67108864);
main._s1.runClassMethod (b4a.example.sp_tabelle.class, "_sendrequest",(Object)(BA.ObjectToString("get_version")),(Object)(RemoteObject.createImmutable("get_version")));
 BA.debugLineNum = 124;BA.debugLine="End Sub";
Debug.ShouldStop(134217728);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _globals() throws Exception{
 //BA.debugLineNum = 42;BA.debugLine="Sub Globals";
 //BA.debugLineNum = 45;BA.debugLine="Dim pm As PackageManager";
main.mostCurrent._pm = RemoteObject.createNew ("anywheresoftware.b4a.phone.PackageManagerWrapper");
 //BA.debugLineNum = 46;BA.debugLine="Private ImageView1 As ImageView";
main.mostCurrent._imageview1 = RemoteObject.createNew ("anywheresoftware.b4a.objects.ImageViewWrapper");
 //BA.debugLineNum = 47;BA.debugLine="Private ImageView2 As ImageView";
main.mostCurrent._imageview2 = RemoteObject.createNew ("anywheresoftware.b4a.objects.ImageViewWrapper");
 //BA.debugLineNum = 48;BA.debugLine="Private ImageView3 As ImageView";
main.mostCurrent._imageview3 = RemoteObject.createNew ("anywheresoftware.b4a.objects.ImageViewWrapper");
 //BA.debugLineNum = 49;BA.debugLine="Private ImageView4 As ImageView";
main.mostCurrent._imageview4 = RemoteObject.createNew ("anywheresoftware.b4a.objects.ImageViewWrapper");
 //BA.debugLineNum = 50;BA.debugLine="Private Panel1 As Panel";
main.mostCurrent._panel1 = RemoteObject.createNew ("anywheresoftware.b4a.objects.PanelWrapper");
 //BA.debugLineNum = 51;BA.debugLine="Dim lbl_newsitems As Label";
main.mostCurrent._lbl_newsitems = RemoteObject.createNew ("anywheresoftware.b4a.objects.LabelWrapper");
 //BA.debugLineNum = 52;BA.debugLine="Private lbl_ver As Label";
main.mostCurrent._lbl_ver = RemoteObject.createNew ("anywheresoftware.b4a.objects.LabelWrapper");
 //BA.debugLineNum = 53;BA.debugLine="Dim lbl_user As Label";
main.mostCurrent._lbl_user = RemoteObject.createNew ("anywheresoftware.b4a.objects.LabelWrapper");
 //BA.debugLineNum = 54;BA.debugLine="Dim lbl_news2 As Label";
main.mostCurrent._lbl_news2 = RemoteObject.createNew ("anywheresoftware.b4a.objects.LabelWrapper");
 //BA.debugLineNum = 55;BA.debugLine="Dim bu_news2 As Button";
main.mostCurrent._bu_news2 = RemoteObject.createNew ("anywheresoftware.b4a.objects.ButtonWrapper");
 //BA.debugLineNum = 57;BA.debugLine="End Sub";
return RemoteObject.createImmutable("");
}
public static RemoteObject  _imageview1_click() throws Exception{
try {
		Debug.PushSubsStack("ImageView1_Click (main) ","main",0,main.mostCurrent.activityBA,main.mostCurrent,132);
if (RapidSub.canDelegate("imageview1_click")) return b4a.example.main.remoteMe.runUserSub(false, "main","imageview1_click");
 BA.debugLineNum = 132;BA.debugLine="Sub ImageView1_Click";
Debug.ShouldStop(8);
 BA.debugLineNum = 134;BA.debugLine="StartActivity(\"ze_basis\")";
Debug.ShouldStop(32);
main.mostCurrent.__c.runVoidMethod ("StartActivity",main.processBA,(Object)((RemoteObject.createImmutable("ze_basis"))));
 BA.debugLineNum = 135;BA.debugLine="End Sub";
Debug.ShouldStop(64);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _imageview2_click() throws Exception{
try {
		Debug.PushSubsStack("ImageView2_Click (main) ","main",0,main.mostCurrent.activityBA,main.mostCurrent,136);
if (RapidSub.canDelegate("imageview2_click")) return b4a.example.main.remoteMe.runUserSub(false, "main","imageview2_click");
 BA.debugLineNum = 136;BA.debugLine="Sub ImageView2_Click";
Debug.ShouldStop(128);
 BA.debugLineNum = 138;BA.debugLine="ToastMessageShow(\"Diese Funktion ist noch nicht i";
Debug.ShouldStop(512);
main.mostCurrent.__c.runVoidMethod ("ToastMessageShow",(Object)(BA.ObjectToCharSequence("Diese Funktion ist noch nicht implementiert")),(Object)(main.mostCurrent.__c.getField(true,"True")));
 BA.debugLineNum = 139;BA.debugLine="End Sub";
Debug.ShouldStop(1024);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _imageview3_click() throws Exception{
try {
		Debug.PushSubsStack("ImageView3_Click (main) ","main",0,main.mostCurrent.activityBA,main.mostCurrent,128);
if (RapidSub.canDelegate("imageview3_click")) return b4a.example.main.remoteMe.runUserSub(false, "main","imageview3_click");
 BA.debugLineNum = 128;BA.debugLine="Sub ImageView3_Click";
Debug.ShouldStop(-2147483648);
 BA.debugLineNum = 130;BA.debugLine="StartActivity(\"setup\")";
Debug.ShouldStop(2);
main.mostCurrent.__c.runVoidMethod ("StartActivity",main.processBA,(Object)((RemoteObject.createImmutable("setup"))));
 BA.debugLineNum = 131;BA.debugLine="End Sub";
Debug.ShouldStop(4);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _imageview4_click() throws Exception{
try {
		Debug.PushSubsStack("ImageView4_Click (main) ","main",0,main.mostCurrent.activityBA,main.mostCurrent,140);
if (RapidSub.canDelegate("imageview4_click")) return b4a.example.main.remoteMe.runUserSub(false, "main","imageview4_click");
 BA.debugLineNum = 140;BA.debugLine="Sub ImageView4_Click";
Debug.ShouldStop(2048);
 BA.debugLineNum = 142;BA.debugLine="ToastMessageShow(\"Diese Funktion ist noch nicht i";
Debug.ShouldStop(8192);
main.mostCurrent.__c.runVoidMethod ("ToastMessageShow",(Object)(BA.ObjectToCharSequence("Diese Funktion ist noch nicht implementiert")),(Object)(main.mostCurrent.__c.getField(true,"True")));
 BA.debugLineNum = 143;BA.debugLine="End Sub";
Debug.ShouldStop(16384);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}

public static void initializeProcessGlobals() {
    
    if (main.processGlobalsRun == false) {
	    main.processGlobalsRun = true;
		try {
		        main_subs_0._process_globals();
starter_subs_0._process_globals();
scale_subs_0._process_globals();
setup_subs_0._process_globals();
ze_basis_subs_0._process_globals();
main.myClass = BA.getDeviceClass ("b4a.example.main");
starter.myClass = BA.getDeviceClass ("b4a.example.starter");
scale.myClass = BA.getDeviceClass ("b4a.example.scale");
sp_single.myClass = BA.getDeviceClass ("b4a.example.sp_single");
sp_tabelle.myClass = BA.getDeviceClass ("b4a.example.sp_tabelle");
setup.myClass = BA.getDeviceClass ("b4a.example.setup");
update_sender_main.myClass = BA.getDeviceClass ("b4a.example.update_sender_main");
ze_basis.myClass = BA.getDeviceClass ("b4a.example.ze_basis");
update_sender.myClass = BA.getDeviceClass ("b4a.example.update_sender");
sp_single_main.myClass = BA.getDeviceClass ("b4a.example.sp_single_main");
soap_up.myClass = BA.getDeviceClass ("b4a.example.soap_up");
		
        } catch (Exception e) {
			throw new RuntimeException(e);
		}
    }
}public static RemoteObject  _process_globals() throws Exception{
 //BA.debugLineNum = 15;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 18;BA.debugLine="Dim sql1 As SQL";
main._sql1 = RemoteObject.createNew ("anywheresoftware.b4a.sql.SQL");
 //BA.debugLineNum = 19;BA.debugLine="Dim sf As StringFunctions";
main._sf = RemoteObject.createNew ("adr.stringfunctions.stringfunctions");
 //BA.debugLineNum = 20;BA.debugLine="Dim update As update_sender_main";
main._update = RemoteObject.createNew ("b4a.example.update_sender_main");
 //BA.debugLineNum = 21;BA.debugLine="Dim messagefrom As String";
main._messagefrom = RemoteObject.createImmutable("");
 //BA.debugLineNum = 22;BA.debugLine="Dim scanlistsrc As String";
main._scanlistsrc = RemoteObject.createImmutable("");
 //BA.debugLineNum = 23;BA.debugLine="Dim ac_to_start As String";
main._ac_to_start = RemoteObject.createImmutable("");
 //BA.debugLineNum = 24;BA.debugLine="Dim s1 As sp_tabelle";
main._s1 = RemoteObject.createNew ("b4a.example.sp_tabelle");
 //BA.debugLineNum = 26;BA.debugLine="Dim liveservice As String =\"https://www.intersero";
main._liveservice = BA.ObjectToString("https://www.interseroh-pfand.com/appservice1.asmx");
 //BA.debugLineNum = 27;BA.debugLine="Dim testservice As String=\"https://www.interseroh";
main._testservice = BA.ObjectToString("https://www.interseroh-pfand.com/logintest.asmx");
 //BA.debugLineNum = 28;BA.debugLine="Dim use_service As String";
main._use_service = RemoteObject.createImmutable("");
 //BA.debugLineNum = 29;BA.debugLine="use_service=testservice";
main._use_service = main._testservice;
 //BA.debugLineNum = 31;BA.debugLine="Dim u1 As update_sender_main";
main._u1 = RemoteObject.createNew ("b4a.example.update_sender_main");
 //BA.debugLineNum = 32;BA.debugLine="Dim reload As String";
main._reload = RemoteObject.createImmutable("");
 //BA.debugLineNum = 33;BA.debugLine="Dim aftersend As Boolean";
main._aftersend = RemoteObject.createImmutable(false);
 //BA.debugLineNum = 34;BA.debugLine="Dim auftrag_fuer_detail As String";
main._auftrag_fuer_detail = RemoteObject.createImmutable("");
 //BA.debugLineNum = 35;BA.debugLine="Dim scrollposition_1 As Int";
main._scrollposition_1 = RemoteObject.createImmutable(0);
 //BA.debugLineNum = 36;BA.debugLine="Dim anmeldung As Boolean";
main._anmeldung = RemoteObject.createImmutable(false);
 //BA.debugLineNum = 37;BA.debugLine="Dim mainsender As Boolean";
main._mainsender = RemoteObject.createImmutable(false);
 //BA.debugLineNum = 38;BA.debugLine="Dim newsshown As String";
main._newsshown = RemoteObject.createImmutable("");
 //BA.debugLineNum = 39;BA.debugLine="Dim newstext As String";
main._newstext = RemoteObject.createImmutable("");
 //BA.debugLineNum = 40;BA.debugLine="End Sub";
return RemoteObject.createImmutable("");
}
}