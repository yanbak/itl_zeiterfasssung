package b4a.example;

import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.pc.*;

public class scale_subs_0 {


public static RemoteObject  _bottom(RemoteObject _ba,RemoteObject _v) throws Exception{
try {
		Debug.PushSubsStack("Bottom (scale) ","scale",2,_ba,scale.mostCurrent,525);
if (RapidSub.canDelegate("bottom")) return b4a.example.scale.remoteMe.runUserSub(false, "scale","bottom", _ba, _v);
;
Debug.locals.put("v", _v);
 BA.debugLineNum = 525;BA.debugLine="Public Sub Bottom(v As View) As Int";
Debug.ShouldStop(4096);
 BA.debugLineNum = 526;BA.debugLine="Return v.Top + v.Height";
Debug.ShouldStop(8192);
if (true) return RemoteObject.solve(new RemoteObject[] {_v.runMethod(true,"getTop"),_v.runMethod(true,"getHeight")}, "+",1, 1);
 BA.debugLineNum = 527;BA.debugLine="End Sub";
Debug.ShouldStop(16384);
return RemoteObject.createImmutable(0);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _getdevicephysicalsize(RemoteObject _ba) throws Exception{
try {
		Debug.PushSubsStack("GetDevicePhysicalSize (scale) ","scale",2,_ba,scale.mostCurrent,146);
if (RapidSub.canDelegate("getdevicephysicalsize")) return b4a.example.scale.remoteMe.runUserSub(false, "scale","getdevicephysicalsize", _ba);
RemoteObject _lv = RemoteObject.declareNull("anywheresoftware.b4a.keywords.LayoutValues");
;
 BA.debugLineNum = 146;BA.debugLine="Public Sub GetDevicePhysicalSize As Float";
Debug.ShouldStop(131072);
 BA.debugLineNum = 147;BA.debugLine="Dim lv As LayoutValues";
Debug.ShouldStop(262144);
_lv = RemoteObject.createNew ("anywheresoftware.b4a.keywords.LayoutValues");Debug.locals.put("lv", _lv);
 BA.debugLineNum = 149;BA.debugLine="lv = GetDeviceLayoutValues";
Debug.ShouldStop(1048576);
_lv = scale.mostCurrent.__c.runMethod(false,"GetDeviceLayoutValues",_ba);Debug.locals.put("lv", _lv);
 BA.debugLineNum = 150;BA.debugLine="Return Sqrt(Power(lv.Height / lv.Scale / 160, 2)";
Debug.ShouldStop(2097152);
if (true) return BA.numberCast(float.class, scale.mostCurrent.__c.runMethod(true,"Sqrt",(Object)(RemoteObject.solve(new RemoteObject[] {scale.mostCurrent.__c.runMethod(true,"Power",(Object)(RemoteObject.solve(new RemoteObject[] {_lv.getField(true,"Height"),_lv.getField(true,"Scale"),RemoteObject.createImmutable(160)}, "//",0, 0)),(Object)(BA.numberCast(double.class, 2))),scale.mostCurrent.__c.runMethod(true,"Power",(Object)(RemoteObject.solve(new RemoteObject[] {_lv.getField(true,"Width"),_lv.getField(true,"Scale"),RemoteObject.createImmutable(160)}, "//",0, 0)),(Object)(BA.numberCast(double.class, 2)))}, "+",1, 0))));
 BA.debugLineNum = 151;BA.debugLine="End Sub";
Debug.ShouldStop(4194304);
return RemoteObject.createImmutable(0f);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _getscaleds(RemoteObject _ba) throws Exception{
try {
		Debug.PushSubsStack("GetScaleDS (scale) ","scale",2,_ba,scale.mostCurrent,130);
if (RapidSub.canDelegate("getscaleds")) return b4a.example.scale.remoteMe.runUserSub(false, "scale","getscaleds", _ba);
;
 BA.debugLineNum = 130;BA.debugLine="Public Sub GetScaleDS As Double";
Debug.ShouldStop(2);
 BA.debugLineNum = 131;BA.debugLine="Return cScaleDS";
Debug.ShouldStop(4);
if (true) return scale._cscaleds;
 BA.debugLineNum = 132;BA.debugLine="End Sub";
Debug.ShouldStop(8);
return RemoteObject.createImmutable(0);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _getscalex(RemoteObject _ba) throws Exception{
try {
		Debug.PushSubsStack("GetScaleX (scale) ","scale",2,_ba,scale.mostCurrent,69);
if (RapidSub.canDelegate("getscalex")) return b4a.example.scale.remoteMe.runUserSub(false, "scale","getscalex", _ba);
;
 BA.debugLineNum = 69;BA.debugLine="Public Sub GetScaleX As Double";
Debug.ShouldStop(16);
 BA.debugLineNum = 70;BA.debugLine="Return cScaleX";
Debug.ShouldStop(32);
if (true) return scale._cscalex;
 BA.debugLineNum = 71;BA.debugLine="End Sub";
Debug.ShouldStop(64);
return RemoteObject.createImmutable(0);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _getscalex_l(RemoteObject _ba) throws Exception{
try {
		Debug.PushSubsStack("GetScaleX_L (scale) ","scale",2,_ba,scale.mostCurrent,107);
if (RapidSub.canDelegate("getscalex_l")) return b4a.example.scale.remoteMe.runUserSub(false, "scale","getscalex_l", _ba);
;
 BA.debugLineNum = 107;BA.debugLine="Public Sub GetScaleX_L As Double";
Debug.ShouldStop(1024);
 BA.debugLineNum = 108;BA.debugLine="If GetDevicePhysicalSize < 6 Then";
Debug.ShouldStop(2048);
if (RemoteObject.solveBoolean("<",_getdevicephysicalsize(_ba),BA.numberCast(double.class, 6))) { 
 BA.debugLineNum = 109;BA.debugLine="Return (100%y / (cRefWidth - 50dip) / cRefScale)";
Debug.ShouldStop(4096);
if (true) return (RemoteObject.solve(new RemoteObject[] {scale.mostCurrent.__c.runMethod(true,"PerYToCurrent",(Object)(BA.numberCast(float.class, 100)),_ba),(RemoteObject.solve(new RemoteObject[] {scale._crefwidth,scale.mostCurrent.__c.runMethod(true,"DipToCurrent",(Object)(BA.numberCast(int.class, 50)))}, "-",1, 1)),scale._crefscale}, "//",0, 0));
 }else {
 BA.debugLineNum = 111;BA.debugLine="Return (1 + Rate * (100%y / (cRefWidth - 50dip)";
Debug.ShouldStop(16384);
if (true) return (RemoteObject.solve(new RemoteObject[] {RemoteObject.createImmutable(1),scale._rate,(RemoteObject.solve(new RemoteObject[] {scale.mostCurrent.__c.runMethod(true,"PerYToCurrent",(Object)(BA.numberCast(float.class, 100)),_ba),(RemoteObject.solve(new RemoteObject[] {scale._crefwidth,scale.mostCurrent.__c.runMethod(true,"DipToCurrent",(Object)(BA.numberCast(int.class, 50)))}, "-",1, 1)),scale._crefscale,RemoteObject.createImmutable(1)}, "//-",1, 0))}, "+*",1, 0));
 };
 BA.debugLineNum = 113;BA.debugLine="End Sub";
Debug.ShouldStop(65536);
return RemoteObject.createImmutable(0);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _getscalex_p(RemoteObject _ba) throws Exception{
try {
		Debug.PushSubsStack("GetScaleX_P (scale) ","scale",2,_ba,scale.mostCurrent,119);
if (RapidSub.canDelegate("getscalex_p")) return b4a.example.scale.remoteMe.runUserSub(false, "scale","getscalex_p", _ba);
;
 BA.debugLineNum = 119;BA.debugLine="Public Sub GetScaleX_P As Double";
Debug.ShouldStop(4194304);
 BA.debugLineNum = 120;BA.debugLine="If GetDevicePhysicalSize < 6 Then";
Debug.ShouldStop(8388608);
if (RemoteObject.solveBoolean("<",_getdevicephysicalsize(_ba),BA.numberCast(double.class, 6))) { 
 BA.debugLineNum = 121;BA.debugLine="Return (100%y / cRefWidth / cRefScale)";
Debug.ShouldStop(16777216);
if (true) return (RemoteObject.solve(new RemoteObject[] {scale.mostCurrent.__c.runMethod(true,"PerYToCurrent",(Object)(BA.numberCast(float.class, 100)),_ba),scale._crefwidth,scale._crefscale}, "//",0, 0));
 }else {
 BA.debugLineNum = 123;BA.debugLine="Return (1 + Rate * (100%y / (cRefWidth - 50dip)";
Debug.ShouldStop(67108864);
if (true) return (RemoteObject.solve(new RemoteObject[] {RemoteObject.createImmutable(1),scale._rate,(RemoteObject.solve(new RemoteObject[] {scale.mostCurrent.__c.runMethod(true,"PerYToCurrent",(Object)(BA.numberCast(float.class, 100)),_ba),(RemoteObject.solve(new RemoteObject[] {scale._crefwidth,scale.mostCurrent.__c.runMethod(true,"DipToCurrent",(Object)(BA.numberCast(int.class, 50)))}, "-",1, 1)),scale._crefscale,RemoteObject.createImmutable(1)}, "//-",1, 0))}, "+*",1, 0));
 };
 BA.debugLineNum = 125;BA.debugLine="End Sub";
Debug.ShouldStop(268435456);
return RemoteObject.createImmutable(0);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _getscaley(RemoteObject _ba) throws Exception{
try {
		Debug.PushSubsStack("GetScaleY (scale) ","scale",2,_ba,scale.mostCurrent,75);
if (RapidSub.canDelegate("getscaley")) return b4a.example.scale.remoteMe.runUserSub(false, "scale","getscaley", _ba);
;
 BA.debugLineNum = 75;BA.debugLine="Public Sub GetScaleY As Double";
Debug.ShouldStop(1024);
 BA.debugLineNum = 76;BA.debugLine="Return cScaleY";
Debug.ShouldStop(2048);
if (true) return scale._cscaley;
 BA.debugLineNum = 77;BA.debugLine="End Sub";
Debug.ShouldStop(4096);
return RemoteObject.createImmutable(0);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _getscaley_l(RemoteObject _ba) throws Exception{
try {
		Debug.PushSubsStack("GetScaleY_L (scale) ","scale",2,_ba,scale.mostCurrent,83);
if (RapidSub.canDelegate("getscaley_l")) return b4a.example.scale.remoteMe.runUserSub(false, "scale","getscaley_l", _ba);
;
 BA.debugLineNum = 83;BA.debugLine="Public Sub GetScaleY_L As Double";
Debug.ShouldStop(262144);
 BA.debugLineNum = 84;BA.debugLine="If GetDevicePhysicalSize < 6 Then";
Debug.ShouldStop(524288);
if (RemoteObject.solveBoolean("<",_getdevicephysicalsize(_ba),BA.numberCast(double.class, 6))) { 
 BA.debugLineNum = 85;BA.debugLine="Return (100%y / (cRefWidth - 50dip) / cRefScale)";
Debug.ShouldStop(1048576);
if (true) return (RemoteObject.solve(new RemoteObject[] {scale.mostCurrent.__c.runMethod(true,"PerYToCurrent",(Object)(BA.numberCast(float.class, 100)),_ba),(RemoteObject.solve(new RemoteObject[] {scale._crefwidth,scale.mostCurrent.__c.runMethod(true,"DipToCurrent",(Object)(BA.numberCast(int.class, 50)))}, "-",1, 1)),scale._crefscale}, "//",0, 0));
 }else {
 BA.debugLineNum = 87;BA.debugLine="Return (1 + Rate * (100%y / (cRefWidth - 50dip)";
Debug.ShouldStop(4194304);
if (true) return (RemoteObject.solve(new RemoteObject[] {RemoteObject.createImmutable(1),scale._rate,(RemoteObject.solve(new RemoteObject[] {scale.mostCurrent.__c.runMethod(true,"PerYToCurrent",(Object)(BA.numberCast(float.class, 100)),_ba),(RemoteObject.solve(new RemoteObject[] {scale._crefwidth,scale.mostCurrent.__c.runMethod(true,"DipToCurrent",(Object)(BA.numberCast(int.class, 50)))}, "-",1, 1)),scale._crefscale,RemoteObject.createImmutable(1)}, "//-",1, 0))}, "+*",1, 0));
 };
 BA.debugLineNum = 89;BA.debugLine="End Sub";
Debug.ShouldStop(16777216);
return RemoteObject.createImmutable(0);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _getscaley_p(RemoteObject _ba) throws Exception{
try {
		Debug.PushSubsStack("GetScaleY_P (scale) ","scale",2,_ba,scale.mostCurrent,95);
if (RapidSub.canDelegate("getscaley_p")) return b4a.example.scale.remoteMe.runUserSub(false, "scale","getscaley_p", _ba);
;
 BA.debugLineNum = 95;BA.debugLine="Public Sub GetScaleY_P As Double";
Debug.ShouldStop(1073741824);
 BA.debugLineNum = 96;BA.debugLine="If GetDevicePhysicalSize < 6 Then";
Debug.ShouldStop(-2147483648);
if (RemoteObject.solveBoolean("<",_getdevicephysicalsize(_ba),BA.numberCast(double.class, 6))) { 
 BA.debugLineNum = 97;BA.debugLine="Return (100%y / (cRefHeight - 50dip) / cRefScale";
Debug.ShouldStop(1);
if (true) return (RemoteObject.solve(new RemoteObject[] {scale.mostCurrent.__c.runMethod(true,"PerYToCurrent",(Object)(BA.numberCast(float.class, 100)),_ba),(RemoteObject.solve(new RemoteObject[] {scale._crefheight,scale.mostCurrent.__c.runMethod(true,"DipToCurrent",(Object)(BA.numberCast(int.class, 50)))}, "-",1, 1)),scale._crefscale}, "//",0, 0));
 }else {
 BA.debugLineNum = 99;BA.debugLine="Return (1 + Rate * (100%y / (cRefHeight - 50dip)";
Debug.ShouldStop(4);
if (true) return (RemoteObject.solve(new RemoteObject[] {RemoteObject.createImmutable(1),scale._rate,(RemoteObject.solve(new RemoteObject[] {scale.mostCurrent.__c.runMethod(true,"PerYToCurrent",(Object)(BA.numberCast(float.class, 100)),_ba),(RemoteObject.solve(new RemoteObject[] {scale._crefheight,scale.mostCurrent.__c.runMethod(true,"DipToCurrent",(Object)(BA.numberCast(int.class, 50)))}, "-",1, 1)),scale._crefscale,RemoteObject.createImmutable(1)}, "//-",1, 0))}, "+*",1, 0));
 };
 BA.debugLineNum = 101;BA.debugLine="End Sub";
Debug.ShouldStop(16);
return RemoteObject.createImmutable(0);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _horizontalcenter(RemoteObject _ba,RemoteObject _v) throws Exception{
try {
		Debug.PushSubsStack("HorizontalCenter (scale) ","scale",2,_ba,scale.mostCurrent,359);
if (RapidSub.canDelegate("horizontalcenter")) return b4a.example.scale.remoteMe.runUserSub(false, "scale","horizontalcenter", _ba, _v);
;
Debug.locals.put("v", _v);
 BA.debugLineNum = 359;BA.debugLine="Public Sub HorizontalCenter(v As View)";
Debug.ShouldStop(64);
 BA.debugLineNum = 360;BA.debugLine="v.Left = (100%x - v.Width) / 2";
Debug.ShouldStop(128);
_v.runMethod(true,"setLeft",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {(RemoteObject.solve(new RemoteObject[] {scale.mostCurrent.__c.runMethod(true,"PerXToCurrent",(Object)(BA.numberCast(float.class, 100)),_ba),_v.runMethod(true,"getWidth")}, "-",1, 1)),RemoteObject.createImmutable(2)}, "/",0, 0)));
 BA.debugLineNum = 361;BA.debugLine="End Sub";
Debug.ShouldStop(256);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _horizontalcenter2(RemoteObject _ba,RemoteObject _v,RemoteObject _vleft,RemoteObject _vright) throws Exception{
try {
		Debug.PushSubsStack("HorizontalCenter2 (scale) ","scale",2,_ba,scale.mostCurrent,365);
if (RapidSub.canDelegate("horizontalcenter2")) return b4a.example.scale.remoteMe.runUserSub(false, "scale","horizontalcenter2", _ba, _v, _vleft, _vright);
;
Debug.locals.put("v", _v);
Debug.locals.put("vLeft", _vleft);
Debug.locals.put("vRight", _vright);
 BA.debugLineNum = 365;BA.debugLine="Public Sub HorizontalCenter2(v As View, vLeft As V";
Debug.ShouldStop(4096);
 BA.debugLineNum = 366;BA.debugLine="v.Left = vLeft.Left + vLeft.Width + (vRight.Left";
Debug.ShouldStop(8192);
_v.runMethod(true,"setLeft",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {_vleft.runMethod(true,"getLeft"),_vleft.runMethod(true,"getWidth"),(RemoteObject.solve(new RemoteObject[] {_vright.runMethod(true,"getLeft"),(RemoteObject.solve(new RemoteObject[] {_vleft.runMethod(true,"getLeft"),_vleft.runMethod(true,"getWidth")}, "+",1, 1)),_v.runMethod(true,"getWidth")}, "--",2, 1)),RemoteObject.createImmutable(2)}, "++/",2, 0)));
 BA.debugLineNum = 367;BA.debugLine="If IsActivity(v) Then";
Debug.ShouldStop(16384);
if (_isactivity(_ba,_v).<Boolean>get().booleanValue()) { 
 BA.debugLineNum = 368;BA.debugLine="ToastMessageShow(\"The view is an Activity !\", Fa";
Debug.ShouldStop(32768);
scale.mostCurrent.__c.runVoidMethod ("ToastMessageShow",(Object)(BA.ObjectToCharSequence("The view is an Activity !")),(Object)(scale.mostCurrent.__c.getField(true,"False")));
 BA.debugLineNum = 369;BA.debugLine="Return";
Debug.ShouldStop(65536);
if (true) return RemoteObject.createImmutable("");
 }else {
 BA.debugLineNum = 371;BA.debugLine="If IsActivity(vLeft) Then";
Debug.ShouldStop(262144);
if (_isactivity(_ba,_vleft).<Boolean>get().booleanValue()) { 
 BA.debugLineNum = 372;BA.debugLine="If IsActivity(vRight) Then";
Debug.ShouldStop(524288);
if (_isactivity(_ba,_vright).<Boolean>get().booleanValue()) { 
 BA.debugLineNum = 373;BA.debugLine="v.Left = (100%x - v.Width) / 2";
Debug.ShouldStop(1048576);
_v.runMethod(true,"setLeft",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {(RemoteObject.solve(new RemoteObject[] {scale.mostCurrent.__c.runMethod(true,"PerXToCurrent",(Object)(BA.numberCast(float.class, 100)),_ba),_v.runMethod(true,"getWidth")}, "-",1, 1)),RemoteObject.createImmutable(2)}, "/",0, 0)));
 }else {
 BA.debugLineNum = 375;BA.debugLine="v.Left = (vRight.Left - v.Width) / 2";
Debug.ShouldStop(4194304);
_v.runMethod(true,"setLeft",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {(RemoteObject.solve(new RemoteObject[] {_vright.runMethod(true,"getLeft"),_v.runMethod(true,"getWidth")}, "-",1, 1)),RemoteObject.createImmutable(2)}, "/",0, 0)));
 };
 }else {
 BA.debugLineNum = 378;BA.debugLine="If IsActivity(vRight) Then";
Debug.ShouldStop(33554432);
if (_isactivity(_ba,_vright).<Boolean>get().booleanValue()) { 
 BA.debugLineNum = 379;BA.debugLine="v.Left = vLeft.Left + vLeft.Width + (100%x - (";
Debug.ShouldStop(67108864);
_v.runMethod(true,"setLeft",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {_vleft.runMethod(true,"getLeft"),_vleft.runMethod(true,"getWidth"),(RemoteObject.solve(new RemoteObject[] {scale.mostCurrent.__c.runMethod(true,"PerXToCurrent",(Object)(BA.numberCast(float.class, 100)),_ba),(RemoteObject.solve(new RemoteObject[] {_vleft.runMethod(true,"getLeft"),_vleft.runMethod(true,"getWidth")}, "+",1, 1)),_v.runMethod(true,"getWidth")}, "--",2, 1)),RemoteObject.createImmutable(2)}, "++/",2, 0)));
 }else {
 BA.debugLineNum = 381;BA.debugLine="v.Left = vLeft.Left + vLeft.Width + (vRight.Le";
Debug.ShouldStop(268435456);
_v.runMethod(true,"setLeft",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {_vleft.runMethod(true,"getLeft"),_vleft.runMethod(true,"getWidth"),(RemoteObject.solve(new RemoteObject[] {_vright.runMethod(true,"getLeft"),(RemoteObject.solve(new RemoteObject[] {_vleft.runMethod(true,"getLeft"),_vleft.runMethod(true,"getWidth")}, "+",1, 1)),_v.runMethod(true,"getWidth")}, "--",2, 1)),RemoteObject.createImmutable(2)}, "++/",2, 0)));
 };
 };
 };
 BA.debugLineNum = 385;BA.debugLine="End Sub";
Debug.ShouldStop(1);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _initialize(RemoteObject _ba) throws Exception{
try {
		Debug.PushSubsStack("Initialize (scale) ","scale",2,_ba,scale.mostCurrent,27);
if (RapidSub.canDelegate("initialize")) return b4a.example.scale.remoteMe.runUserSub(false, "scale","initialize", _ba);
RemoteObject _devicescale = RemoteObject.createImmutable(0);
;
 BA.debugLineNum = 27;BA.debugLine="Public Sub Initialize";
Debug.ShouldStop(67108864);
 BA.debugLineNum = 28;BA.debugLine="Dim DeviceScale As Double";
Debug.ShouldStop(134217728);
_devicescale = RemoteObject.createImmutable(0);Debug.locals.put("DeviceScale", _devicescale);
 BA.debugLineNum = 29;BA.debugLine="DeviceScale = 100dip / 100";
Debug.ShouldStop(268435456);
_devicescale = RemoteObject.solve(new RemoteObject[] {scale.mostCurrent.__c.runMethod(true,"DipToCurrent",(Object)(BA.numberCast(int.class, 100))),RemoteObject.createImmutable(100)}, "/",0, 0);Debug.locals.put("DeviceScale", _devicescale);
 BA.debugLineNum = 31;BA.debugLine="If cRefHeight <> 480 Or cRefWidth <> 320 Or cRefS";
Debug.ShouldStop(1073741824);
if (RemoteObject.solveBoolean("!",scale._crefheight,BA.numberCast(double.class, 480)) || RemoteObject.solveBoolean("!",scale._crefwidth,BA.numberCast(double.class, 320)) || RemoteObject.solveBoolean("!",scale._crefscale,BA.numberCast(double.class, 1))) { 
 BA.debugLineNum = 32;BA.debugLine="If 100%x > 100%y Then";
Debug.ShouldStop(-2147483648);
if (RemoteObject.solveBoolean(">",scale.mostCurrent.__c.runMethod(true,"PerXToCurrent",(Object)(BA.numberCast(float.class, 100)),_ba),BA.numberCast(double.class, scale.mostCurrent.__c.runMethod(true,"PerYToCurrent",(Object)(BA.numberCast(float.class, 100)),_ba)))) { 
 BA.debugLineNum = 34;BA.debugLine="cScaleX = 100%x / cRefHeight / cRefScale / Devi";
Debug.ShouldStop(2);
scale._cscalex = RemoteObject.solve(new RemoteObject[] {scale.mostCurrent.__c.runMethod(true,"PerXToCurrent",(Object)(BA.numberCast(float.class, 100)),_ba),scale._crefheight,scale._crefscale,_devicescale}, "///",0, 0);
 BA.debugLineNum = 35;BA.debugLine="cScaleY = 100%y / (cRefWidth - 50 * cRefScale)";
Debug.ShouldStop(4);
scale._cscaley = RemoteObject.solve(new RemoteObject[] {scale.mostCurrent.__c.runMethod(true,"PerYToCurrent",(Object)(BA.numberCast(float.class, 100)),_ba),(RemoteObject.solve(new RemoteObject[] {scale._crefwidth,RemoteObject.createImmutable(50),scale._crefscale}, "-*",1, 0)),_devicescale}, "//",0, 0);
 }else {
 BA.debugLineNum = 38;BA.debugLine="cScaleX = 100%x / cRefWidth / cRefScale / Devic";
Debug.ShouldStop(32);
scale._cscalex = RemoteObject.solve(new RemoteObject[] {scale.mostCurrent.__c.runMethod(true,"PerXToCurrent",(Object)(BA.numberCast(float.class, 100)),_ba),scale._crefwidth,scale._crefscale,_devicescale}, "///",0, 0);
 BA.debugLineNum = 39;BA.debugLine="cScaleY = 100%y / (cRefHeight - 50dip * cRefSca";
Debug.ShouldStop(64);
scale._cscaley = RemoteObject.solve(new RemoteObject[] {scale.mostCurrent.__c.runMethod(true,"PerYToCurrent",(Object)(BA.numberCast(float.class, 100)),_ba),(RemoteObject.solve(new RemoteObject[] {scale._crefheight,scale.mostCurrent.__c.runMethod(true,"DipToCurrent",(Object)(BA.numberCast(int.class, 50))),scale._crefscale}, "-*",1, 0)),_devicescale}, "//",0, 0);
 };
 }else {
 BA.debugLineNum = 42;BA.debugLine="If GetDevicePhysicalSize < 6 Then";
Debug.ShouldStop(512);
if (RemoteObject.solveBoolean("<",_getdevicephysicalsize(_ba),BA.numberCast(double.class, 6))) { 
 BA.debugLineNum = 43;BA.debugLine="If 100%x > 100%y Then";
Debug.ShouldStop(1024);
if (RemoteObject.solveBoolean(">",scale.mostCurrent.__c.runMethod(true,"PerXToCurrent",(Object)(BA.numberCast(float.class, 100)),_ba),BA.numberCast(double.class, scale.mostCurrent.__c.runMethod(true,"PerYToCurrent",(Object)(BA.numberCast(float.class, 100)),_ba)))) { 
 BA.debugLineNum = 45;BA.debugLine="cScaleX = 100%x / cRefHeight / cRefScale / Dev";
Debug.ShouldStop(4096);
scale._cscalex = RemoteObject.solve(new RemoteObject[] {scale.mostCurrent.__c.runMethod(true,"PerXToCurrent",(Object)(BA.numberCast(float.class, 100)),_ba),scale._crefheight,scale._crefscale,_devicescale}, "///",0, 0);
 BA.debugLineNum = 46;BA.debugLine="cScaleY = 100%y / (cRefWidth - 50 * cRefScale)";
Debug.ShouldStop(8192);
scale._cscaley = RemoteObject.solve(new RemoteObject[] {scale.mostCurrent.__c.runMethod(true,"PerYToCurrent",(Object)(BA.numberCast(float.class, 100)),_ba),(RemoteObject.solve(new RemoteObject[] {scale._crefwidth,RemoteObject.createImmutable(50),scale._crefscale}, "-*",1, 0)),_devicescale}, "//",0, 0);
 }else {
 BA.debugLineNum = 49;BA.debugLine="cScaleX = 100%x / cRefWidth / cRefScale / Devi";
Debug.ShouldStop(65536);
scale._cscalex = RemoteObject.solve(new RemoteObject[] {scale.mostCurrent.__c.runMethod(true,"PerXToCurrent",(Object)(BA.numberCast(float.class, 100)),_ba),scale._crefwidth,scale._crefscale,_devicescale}, "///",0, 0);
 BA.debugLineNum = 50;BA.debugLine="cScaleY = 100%y / (cRefHeight - 50 * cRefScale";
Debug.ShouldStop(131072);
scale._cscaley = RemoteObject.solve(new RemoteObject[] {scale.mostCurrent.__c.runMethod(true,"PerYToCurrent",(Object)(BA.numberCast(float.class, 100)),_ba),(RemoteObject.solve(new RemoteObject[] {scale._crefheight,RemoteObject.createImmutable(50),scale._crefscale}, "-*",1, 0)),_devicescale}, "//",0, 0);
 };
 }else {
 BA.debugLineNum = 53;BA.debugLine="If 100%x > 100%y Then";
Debug.ShouldStop(1048576);
if (RemoteObject.solveBoolean(">",scale.mostCurrent.__c.runMethod(true,"PerXToCurrent",(Object)(BA.numberCast(float.class, 100)),_ba),BA.numberCast(double.class, scale.mostCurrent.__c.runMethod(true,"PerYToCurrent",(Object)(BA.numberCast(float.class, 100)),_ba)))) { 
 BA.debugLineNum = 55;BA.debugLine="cScaleX = 1 + Rate * (100%x / cRefHeight / cRe";
Debug.ShouldStop(4194304);
scale._cscalex = RemoteObject.solve(new RemoteObject[] {RemoteObject.createImmutable(1),scale._rate,(RemoteObject.solve(new RemoteObject[] {scale.mostCurrent.__c.runMethod(true,"PerXToCurrent",(Object)(BA.numberCast(float.class, 100)),_ba),scale._crefheight,scale._crefscale,RemoteObject.createImmutable(1)}, "//-",1, 0)),_devicescale}, "+*/",1, 0);
 BA.debugLineNum = 56;BA.debugLine="cScaleY = 1 + Rate * (100%y / (cRefWidth - 50";
Debug.ShouldStop(8388608);
scale._cscaley = RemoteObject.solve(new RemoteObject[] {RemoteObject.createImmutable(1),scale._rate,(RemoteObject.solve(new RemoteObject[] {scale.mostCurrent.__c.runMethod(true,"PerYToCurrent",(Object)(BA.numberCast(float.class, 100)),_ba),(RemoteObject.solve(new RemoteObject[] {scale._crefwidth,RemoteObject.createImmutable(50),scale._crefscale}, "-*",1, 0)),RemoteObject.createImmutable(1)}, "/-",1, 0)),_devicescale}, "+*/",1, 0);
 }else {
 BA.debugLineNum = 59;BA.debugLine="cScaleX = 1 + Rate * (100%x / cRefWidth / cRef";
Debug.ShouldStop(67108864);
scale._cscalex = RemoteObject.solve(new RemoteObject[] {RemoteObject.createImmutable(1),scale._rate,(RemoteObject.solve(new RemoteObject[] {scale.mostCurrent.__c.runMethod(true,"PerXToCurrent",(Object)(BA.numberCast(float.class, 100)),_ba),scale._crefwidth,scale._crefscale,RemoteObject.createImmutable(1)}, "//-",1, 0)),_devicescale}, "+*/",1, 0);
 BA.debugLineNum = 60;BA.debugLine="cScaleY = 1 + Rate * (100%y / (cRefHeight - 50";
Debug.ShouldStop(134217728);
scale._cscaley = RemoteObject.solve(new RemoteObject[] {RemoteObject.createImmutable(1),scale._rate,(RemoteObject.solve(new RemoteObject[] {scale.mostCurrent.__c.runMethod(true,"PerYToCurrent",(Object)(BA.numberCast(float.class, 100)),_ba),(RemoteObject.solve(new RemoteObject[] {scale._crefheight,scale.mostCurrent.__c.runMethod(true,"DipToCurrent",(Object)(BA.numberCast(int.class, 50))),scale._crefscale}, "-*",1, 0)),RemoteObject.createImmutable(1)}, "/-",1, 0)),_devicescale}, "+*/",1, 0);
 };
 };
 BA.debugLineNum = 63;BA.debugLine="cScaleDS = 1 + Rate * ((100%x + 100%y) / (cRefWi";
Debug.ShouldStop(1073741824);
scale._cscaleds = RemoteObject.solve(new RemoteObject[] {RemoteObject.createImmutable(1),scale._rate,(RemoteObject.solve(new RemoteObject[] {(RemoteObject.solve(new RemoteObject[] {scale.mostCurrent.__c.runMethod(true,"PerXToCurrent",(Object)(BA.numberCast(float.class, 100)),_ba),scale.mostCurrent.__c.runMethod(true,"PerYToCurrent",(Object)(BA.numberCast(float.class, 100)),_ba)}, "+",1, 1)),(RemoteObject.solve(new RemoteObject[] {scale._crefwidth,scale._crefheight,RemoteObject.createImmutable(50),scale._crefscale}, "+-*",2, 0)),RemoteObject.createImmutable(1)}, "/-",1, 0)),_devicescale}, "+*/",1, 0);
 };
 BA.debugLineNum = 65;BA.debugLine="End Sub";
Debug.ShouldStop(1);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _isactivity(RemoteObject _ba,RemoteObject _v) throws Exception{
try {
		Debug.PushSubsStack("IsActivity (scale) ","scale",2,_ba,scale.mostCurrent,546);
if (RapidSub.canDelegate("isactivity")) return b4a.example.scale.remoteMe.runUserSub(false, "scale","isactivity", _ba, _v);
;
Debug.locals.put("v", _v);
 BA.debugLineNum = 546;BA.debugLine="Public Sub IsActivity(v As View) As Boolean";
Debug.ShouldStop(2);
 BA.debugLineNum = 547;BA.debugLine="Try";
Debug.ShouldStop(4);
try { BA.debugLineNum = 548;BA.debugLine="v.Left = v.Left";
Debug.ShouldStop(8);
_v.runMethod(true,"setLeft",_v.runMethod(true,"getLeft"));
 BA.debugLineNum = 549;BA.debugLine="Return False";
Debug.ShouldStop(16);
Debug.CheckDeviceExceptions();if (true) return scale.mostCurrent.__c.getField(true,"False");
 Debug.CheckDeviceExceptions();
} 
       catch (Exception e5) {
			BA.rdebugUtils.runVoidMethod("setLastException",BA.rdebugUtils.runMethod(false, "processBAFromBA", _ba), e5.toString()); BA.debugLineNum = 551;BA.debugLine="Return True";
Debug.ShouldStop(64);
if (true) return scale.mostCurrent.__c.getField(true,"True");
 };
 BA.debugLineNum = 553;BA.debugLine="End Sub";
Debug.ShouldStop(256);
return RemoteObject.createImmutable(false);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _ispanel(RemoteObject _ba,RemoteObject _v) throws Exception{
try {
		Debug.PushSubsStack("IsPanel (scale) ","scale",2,_ba,scale.mostCurrent,531);
if (RapidSub.canDelegate("ispanel")) return b4a.example.scale.remoteMe.runUserSub(false, "scale","ispanel", _ba, _v);
;
Debug.locals.put("v", _v);
 BA.debugLineNum = 531;BA.debugLine="Public Sub IsPanel(v As View) As Boolean";
Debug.ShouldStop(262144);
 BA.debugLineNum = 532;BA.debugLine="If GetType(v) = \"anywheresoftware.b4a.BALayout\" T";
Debug.ShouldStop(524288);
if (RemoteObject.solveBoolean("=",scale.mostCurrent.__c.runMethod(true,"GetType",(Object)((_v.getObject()))),BA.ObjectToString("anywheresoftware.b4a.BALayout"))) { 
 BA.debugLineNum = 533;BA.debugLine="Try";
Debug.ShouldStop(1048576);
try { BA.debugLineNum = 534;BA.debugLine="v.Left = v.Left";
Debug.ShouldStop(2097152);
_v.runMethod(true,"setLeft",_v.runMethod(true,"getLeft"));
 BA.debugLineNum = 535;BA.debugLine="Return True";
Debug.ShouldStop(4194304);
Debug.CheckDeviceExceptions();if (true) return scale.mostCurrent.__c.getField(true,"True");
 Debug.CheckDeviceExceptions();
} 
       catch (Exception e6) {
			BA.rdebugUtils.runVoidMethod("setLastException",BA.rdebugUtils.runMethod(false, "processBAFromBA", _ba), e6.toString()); BA.debugLineNum = 537;BA.debugLine="Return False";
Debug.ShouldStop(16777216);
if (true) return scale.mostCurrent.__c.getField(true,"False");
 };
 }else {
 BA.debugLineNum = 540;BA.debugLine="Return False";
Debug.ShouldStop(134217728);
if (true) return scale.mostCurrent.__c.getField(true,"False");
 };
 BA.debugLineNum = 542;BA.debugLine="End Sub";
Debug.ShouldStop(536870912);
return RemoteObject.createImmutable(false);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _process_globals() throws Exception{
 //BA.debugLineNum = 12;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 18;BA.debugLine="Public Rate As Double";
scale._rate = RemoteObject.createImmutable(0);
 //BA.debugLineNum = 19;BA.debugLine="Rate = 0.3 'value between 0 to 1.";
scale._rate = BA.numberCast(double.class, 0.3);
 //BA.debugLineNum = 20;BA.debugLine="Private cScaleX, cScaleY, cScaleDS As Double";
scale._cscalex = RemoteObject.createImmutable(0);
scale._cscaley = RemoteObject.createImmutable(0);
scale._cscaleds = RemoteObject.createImmutable(0);
 //BA.debugLineNum = 21;BA.debugLine="Private cRefWidth = 320 As Int";
scale._crefwidth = BA.numberCast(int.class, 320);
 //BA.debugLineNum = 22;BA.debugLine="Private cRefHeight = 480 As Int";
scale._crefheight = BA.numberCast(int.class, 480);
 //BA.debugLineNum = 23;BA.debugLine="Private cRefScale = 1 As Double";
scale._crefscale = BA.numberCast(double.class, 1);
 //BA.debugLineNum = 24;BA.debugLine="End Sub";
return RemoteObject.createImmutable("");
}
public static RemoteObject  _right(RemoteObject _ba,RemoteObject _v) throws Exception{
try {
		Debug.PushSubsStack("Right (scale) ","scale",2,_ba,scale.mostCurrent,520);
if (RapidSub.canDelegate("right")) return b4a.example.scale.remoteMe.runUserSub(false, "scale","right", _ba, _v);
;
Debug.locals.put("v", _v);
 BA.debugLineNum = 520;BA.debugLine="Public Sub Right(v As View) As Int";
Debug.ShouldStop(128);
 BA.debugLineNum = 521;BA.debugLine="Return v.Left + v.Width";
Debug.ShouldStop(256);
if (true) return RemoteObject.solve(new RemoteObject[] {_v.runMethod(true,"getLeft"),_v.runMethod(true,"getWidth")}, "+",1, 1);
 BA.debugLineNum = 522;BA.debugLine="End Sub";
Debug.ShouldStop(512);
return RemoteObject.createImmutable(0);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _scaleall(RemoteObject _ba,RemoteObject _act,RemoteObject _firsttime) throws Exception{
try {
		Debug.PushSubsStack("ScaleAll (scale) ","scale",2,_ba,scale.mostCurrent,246);
if (RapidSub.canDelegate("scaleall")) return b4a.example.scale.remoteMe.runUserSub(false, "scale","scaleall", _ba, _act, _firsttime);
RemoteObject _i = RemoteObject.createImmutable(0);
RemoteObject _v = RemoteObject.declareNull("anywheresoftware.b4a.objects.ConcreteViewWrapper");
;
Debug.locals.put("act", _act);
Debug.locals.put("FirstTime", _firsttime);
 BA.debugLineNum = 246;BA.debugLine="Public Sub ScaleAll(act As Activity, FirstTime As";
Debug.ShouldStop(2097152);
 BA.debugLineNum = 247;BA.debugLine="Dim I As Int";
Debug.ShouldStop(4194304);
_i = RemoteObject.createImmutable(0);Debug.locals.put("I", _i);
 BA.debugLineNum = 250;BA.debugLine="If IsPanel(act) And FirstTime = True Then";
Debug.ShouldStop(33554432);
if (RemoteObject.solveBoolean(".",_ispanel(_ba,RemoteObject.declareNull("anywheresoftware.b4a.AbsObjectWrapper").runMethod(false, "ConvertToWrapper", RemoteObject.createNew("anywheresoftware.b4a.objects.ConcreteViewWrapper"), _act.getObject()))) && RemoteObject.solveBoolean("=",_firsttime,scale.mostCurrent.__c.getField(true,"True"))) { 
 BA.debugLineNum = 252;BA.debugLine="ScaleView(act)";
Debug.ShouldStop(134217728);
_scaleview(_ba,RemoteObject.declareNull("anywheresoftware.b4a.AbsObjectWrapper").runMethod(false, "ConvertToWrapper", RemoteObject.createNew("anywheresoftware.b4a.objects.ConcreteViewWrapper"), _act.getObject()));
 }else {
 BA.debugLineNum = 254;BA.debugLine="For I = 0 To act.NumberOfViews - 1";
Debug.ShouldStop(536870912);
{
final int step5 = 1;
final int limit5 = RemoteObject.solve(new RemoteObject[] {_act.runMethod(true,"getNumberOfViews"),RemoteObject.createImmutable(1)}, "-",1, 1).<Integer>get().intValue();
_i = BA.numberCast(int.class, 0) ;
for (;(step5 > 0 && _i.<Integer>get().intValue() <= limit5) || (step5 < 0 && _i.<Integer>get().intValue() >= limit5) ;_i = RemoteObject.createImmutable((int)(0 + _i.<Integer>get().intValue() + step5))  ) {
Debug.locals.put("I", _i);
 BA.debugLineNum = 255;BA.debugLine="Dim v As View";
Debug.ShouldStop(1073741824);
_v = RemoteObject.createNew ("anywheresoftware.b4a.objects.ConcreteViewWrapper");Debug.locals.put("v", _v);
 BA.debugLineNum = 256;BA.debugLine="v = act.GetView(I)";
Debug.ShouldStop(-2147483648);
_v = _act.runMethod(false,"GetView",(Object)(_i));Debug.locals.put("v", _v);
 BA.debugLineNum = 257;BA.debugLine="ScaleView(v)";
Debug.ShouldStop(1);
_scaleview(_ba,_v);
 }
}Debug.locals.put("I", _i);
;
 };
 BA.debugLineNum = 260;BA.debugLine="End Sub";
Debug.ShouldStop(8);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _scaleallds(RemoteObject _ba,RemoteObject _act,RemoteObject _firsttime) throws Exception{
try {
		Debug.PushSubsStack("ScaleAllDS (scale) ","scale",2,_ba,scale.mostCurrent,342);
if (RapidSub.canDelegate("scaleallds")) return b4a.example.scale.remoteMe.runUserSub(false, "scale","scaleallds", _ba, _act, _firsttime);
RemoteObject _i = RemoteObject.createImmutable(0);
RemoteObject _v = RemoteObject.declareNull("anywheresoftware.b4a.objects.ConcreteViewWrapper");
;
Debug.locals.put("act", _act);
Debug.locals.put("FirstTime", _firsttime);
 BA.debugLineNum = 342;BA.debugLine="Public Sub ScaleAllDS(act As Activity, FirstTime A";
Debug.ShouldStop(2097152);
 BA.debugLineNum = 343;BA.debugLine="Dim I As Int";
Debug.ShouldStop(4194304);
_i = RemoteObject.createImmutable(0);Debug.locals.put("I", _i);
 BA.debugLineNum = 346;BA.debugLine="If IsPanel(act) AND FirstTime = True Then";
Debug.ShouldStop(33554432);
if (RemoteObject.solveBoolean(".",_ispanel(_ba,RemoteObject.declareNull("anywheresoftware.b4a.AbsObjectWrapper").runMethod(false, "ConvertToWrapper", RemoteObject.createNew("anywheresoftware.b4a.objects.ConcreteViewWrapper"), _act.getObject()))) && RemoteObject.solveBoolean("=",_firsttime,scale.mostCurrent.__c.getField(true,"True"))) { 
 BA.debugLineNum = 348;BA.debugLine="ScaleViewDS(act)";
Debug.ShouldStop(134217728);
_scaleviewds(_ba,RemoteObject.declareNull("anywheresoftware.b4a.AbsObjectWrapper").runMethod(false, "ConvertToWrapper", RemoteObject.createNew("anywheresoftware.b4a.objects.ConcreteViewWrapper"), _act.getObject()));
 }else {
 BA.debugLineNum = 350;BA.debugLine="For I = 0 To act.NumberOfViews - 1";
Debug.ShouldStop(536870912);
{
final int step5 = 1;
final int limit5 = RemoteObject.solve(new RemoteObject[] {_act.runMethod(true,"getNumberOfViews"),RemoteObject.createImmutable(1)}, "-",1, 1).<Integer>get().intValue();
_i = BA.numberCast(int.class, 0) ;
for (;(step5 > 0 && _i.<Integer>get().intValue() <= limit5) || (step5 < 0 && _i.<Integer>get().intValue() >= limit5) ;_i = RemoteObject.createImmutable((int)(0 + _i.<Integer>get().intValue() + step5))  ) {
Debug.locals.put("I", _i);
 BA.debugLineNum = 351;BA.debugLine="Dim v As View";
Debug.ShouldStop(1073741824);
_v = RemoteObject.createNew ("anywheresoftware.b4a.objects.ConcreteViewWrapper");Debug.locals.put("v", _v);
 BA.debugLineNum = 352;BA.debugLine="v = act.GetView(I)";
Debug.ShouldStop(-2147483648);
_v = _act.runMethod(false,"GetView",(Object)(_i));Debug.locals.put("v", _v);
 BA.debugLineNum = 353;BA.debugLine="ScaleViewDS(v)";
Debug.ShouldStop(1);
_scaleviewds(_ba,_v);
 }
}Debug.locals.put("I", _i);
;
 };
 BA.debugLineNum = 356;BA.debugLine="End Sub";
Debug.ShouldStop(8);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _scaleview(RemoteObject _ba,RemoteObject _v) throws Exception{
try {
		Debug.PushSubsStack("ScaleView (scale) ","scale",2,_ba,scale.mostCurrent,156);
if (RapidSub.canDelegate("scaleview")) return b4a.example.scale.remoteMe.runUserSub(false, "scale","scaleview", _ba, _v);
RemoteObject _pnl = RemoteObject.declareNull("anywheresoftware.b4a.objects.PanelWrapper");
RemoteObject _lbl = RemoteObject.declareNull("anywheresoftware.b4a.objects.LabelWrapper");
RemoteObject _scv = RemoteObject.declareNull("anywheresoftware.b4a.objects.ScrollViewWrapper");
RemoteObject _hcv = RemoteObject.declareNull("anywheresoftware.b4a.objects.HorizontalScrollViewWrapper");
RemoteObject _scv2d = RemoteObject.declareNull("flm.b4a.scrollview2d.ScrollView2DWrapper");
RemoteObject _ltv = RemoteObject.declareNull("anywheresoftware.b4a.objects.ListViewWrapper");
RemoteObject _spn = RemoteObject.declareNull("anywheresoftware.b4a.objects.SpinnerWrapper");
;
Debug.locals.put("v", _v);
 BA.debugLineNum = 156;BA.debugLine="Public Sub ScaleView(v As View)";
Debug.ShouldStop(134217728);
 BA.debugLineNum = 157;BA.debugLine="If IsActivity(v) Then";
Debug.ShouldStop(268435456);
if (_isactivity(_ba,_v).<Boolean>get().booleanValue()) { 
 BA.debugLineNum = 158;BA.debugLine="Return";
Debug.ShouldStop(536870912);
if (true) return RemoteObject.createImmutable("");
 };
 BA.debugLineNum = 161;BA.debugLine="v.Left = v.Left * cScaleX";
Debug.ShouldStop(1);
_v.runMethod(true,"setLeft",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {_v.runMethod(true,"getLeft"),scale._cscalex}, "*",0, 0)));
 BA.debugLineNum = 162;BA.debugLine="v.Top = v.Top * cScaleY";
Debug.ShouldStop(2);
_v.runMethod(true,"setTop",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {_v.runMethod(true,"getTop"),scale._cscaley}, "*",0, 0)));
 BA.debugLineNum = 163;BA.debugLine="If IsPanel(v) Then";
Debug.ShouldStop(4);
if (_ispanel(_ba,_v).<Boolean>get().booleanValue()) { 
 BA.debugLineNum = 164;BA.debugLine="Dim pnl As Panel";
Debug.ShouldStop(8);
_pnl = RemoteObject.createNew ("anywheresoftware.b4a.objects.PanelWrapper");Debug.locals.put("pnl", _pnl);
 BA.debugLineNum = 165;BA.debugLine="pnl = v";
Debug.ShouldStop(16);
_pnl.setObject(_v.getObject());
 BA.debugLineNum = 166;BA.debugLine="If pnl.Background Is BitmapDrawable Then";
Debug.ShouldStop(32);
if (RemoteObject.solveBoolean("i",_pnl.runMethod(false,"getBackground"), RemoteObject.createImmutable("android.graphics.drawable.BitmapDrawable"))) { 
 BA.debugLineNum = 169;BA.debugLine="v.Width = v.Width * Min(cScaleX, cScaleY)";
Debug.ShouldStop(256);
_v.runMethod(true,"setWidth",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {_v.runMethod(true,"getWidth"),scale.mostCurrent.__c.runMethod(true,"Min",(Object)(scale._cscalex),(Object)(scale._cscaley))}, "*",0, 0)));
 BA.debugLineNum = 170;BA.debugLine="v.Height = v.Height * Min(cScaleX, cScaleY)";
Debug.ShouldStop(512);
_v.runMethod(true,"setHeight",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {_v.runMethod(true,"getHeight"),scale.mostCurrent.__c.runMethod(true,"Min",(Object)(scale._cscalex),(Object)(scale._cscaley))}, "*",0, 0)));
 }else {
 BA.debugLineNum = 172;BA.debugLine="v.Width = v.Width * cScaleX";
Debug.ShouldStop(2048);
_v.runMethod(true,"setWidth",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {_v.runMethod(true,"getWidth"),scale._cscalex}, "*",0, 0)));
 BA.debugLineNum = 173;BA.debugLine="v.Height = v.Height * cScaleY";
Debug.ShouldStop(4096);
_v.runMethod(true,"setHeight",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {_v.runMethod(true,"getHeight"),scale._cscaley}, "*",0, 0)));
 };
 BA.debugLineNum = 175;BA.debugLine="ScaleAll(pnl, False)";
Debug.ShouldStop(16384);
_scaleall(_ba,RemoteObject.declareNull("anywheresoftware.b4a.AbsObjectWrapper").runMethod(false, "ConvertToWrapper", RemoteObject.createNew("anywheresoftware.b4a.objects.ActivityWrapper"), _pnl.getObject()),scale.mostCurrent.__c.getField(true,"False"));
 }else 
{ BA.debugLineNum = 176;BA.debugLine="Else If v Is ImageView Then";
Debug.ShouldStop(32768);
if (RemoteObject.solveBoolean("i",_v.getObjectOrNull(), RemoteObject.createImmutable("android.widget.ImageView"))) { 
 BA.debugLineNum = 179;BA.debugLine="v.Width = v.Width * Min(cScaleX, cScaleY)";
Debug.ShouldStop(262144);
_v.runMethod(true,"setWidth",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {_v.runMethod(true,"getWidth"),scale.mostCurrent.__c.runMethod(true,"Min",(Object)(scale._cscalex),(Object)(scale._cscaley))}, "*",0, 0)));
 BA.debugLineNum = 180;BA.debugLine="v.Height = v.Height * Min(cScaleX, cScaleY)";
Debug.ShouldStop(524288);
_v.runMethod(true,"setHeight",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {_v.runMethod(true,"getHeight"),scale.mostCurrent.__c.runMethod(true,"Min",(Object)(scale._cscalex),(Object)(scale._cscaley))}, "*",0, 0)));
 }else {
 BA.debugLineNum = 182;BA.debugLine="v.Width = v.Width * cScaleX";
Debug.ShouldStop(2097152);
_v.runMethod(true,"setWidth",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {_v.runMethod(true,"getWidth"),scale._cscalex}, "*",0, 0)));
 BA.debugLineNum = 183;BA.debugLine="v.Height = v.Height * cScaleY";
Debug.ShouldStop(4194304);
_v.runMethod(true,"setHeight",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {_v.runMethod(true,"getHeight"),scale._cscaley}, "*",0, 0)));
 }}
;
 BA.debugLineNum = 186;BA.debugLine="If v Is Label Then 'this will catch ALL views wit";
Debug.ShouldStop(33554432);
if (RemoteObject.solveBoolean("i",_v.getObjectOrNull(), RemoteObject.createImmutable("android.widget.TextView"))) { 
 BA.debugLineNum = 187;BA.debugLine="Dim lbl As Label = v";
Debug.ShouldStop(67108864);
_lbl = RemoteObject.createNew ("anywheresoftware.b4a.objects.LabelWrapper");
_lbl.setObject(_v.getObject());Debug.locals.put("lbl", _lbl);
 BA.debugLineNum = 188;BA.debugLine="lbl.TextSize = lbl.TextSize * cScaleX";
Debug.ShouldStop(134217728);
_lbl.runMethod(true,"setTextSize",BA.numberCast(float.class, RemoteObject.solve(new RemoteObject[] {_lbl.runMethod(true,"getTextSize"),scale._cscalex}, "*",0, 0)));
 };
 BA.debugLineNum = 191;BA.debugLine="If GetType(v) = \"anywheresoftware.b4a.objects.Scr";
Debug.ShouldStop(1073741824);
if (RemoteObject.solveBoolean("=",scale.mostCurrent.__c.runMethod(true,"GetType",(Object)((_v.getObject()))),BA.ObjectToString("anywheresoftware.b4a.objects.ScrollViewWrapper$MyScrollView"))) { 
 BA.debugLineNum = 194;BA.debugLine="Dim scv As ScrollView";
Debug.ShouldStop(2);
_scv = RemoteObject.createNew ("anywheresoftware.b4a.objects.ScrollViewWrapper");Debug.locals.put("scv", _scv);
 BA.debugLineNum = 195;BA.debugLine="scv = v";
Debug.ShouldStop(4);
_scv.setObject(_v.getObject());
 BA.debugLineNum = 196;BA.debugLine="ScaleAll(scv.Panel, False)";
Debug.ShouldStop(8);
_scaleall(_ba,RemoteObject.declareNull("anywheresoftware.b4a.AbsObjectWrapper").runMethod(false, "ConvertToWrapper", RemoteObject.createNew("anywheresoftware.b4a.objects.ActivityWrapper"), _scv.runMethod(false,"getPanel").getObject()),scale.mostCurrent.__c.getField(true,"False"));
 BA.debugLineNum = 197;BA.debugLine="scv.Panel.Height = scv.Panel.Height * cScaleY";
Debug.ShouldStop(16);
_scv.runMethod(false,"getPanel").runMethod(true,"setHeight",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {_scv.runMethod(false,"getPanel").runMethod(true,"getHeight"),scale._cscaley}, "*",0, 0)));
 }else 
{ BA.debugLineNum = 198;BA.debugLine="Else If GetType(v) = \"anywheresoftware.b4a.object";
Debug.ShouldStop(32);
if (RemoteObject.solveBoolean("=",scale.mostCurrent.__c.runMethod(true,"GetType",(Object)((_v.getObject()))),BA.ObjectToString("anywheresoftware.b4a.objects.HorizontalScrollViewWrapper$MyHScrollView"))) { 
 BA.debugLineNum = 201;BA.debugLine="Dim hcv As HorizontalScrollView";
Debug.ShouldStop(256);
_hcv = RemoteObject.createNew ("anywheresoftware.b4a.objects.HorizontalScrollViewWrapper");Debug.locals.put("hcv", _hcv);
 BA.debugLineNum = 202;BA.debugLine="hcv = v";
Debug.ShouldStop(512);
_hcv.setObject(_v.getObject());
 BA.debugLineNum = 203;BA.debugLine="ScaleAll(hcv.Panel, False)";
Debug.ShouldStop(1024);
_scaleall(_ba,RemoteObject.declareNull("anywheresoftware.b4a.AbsObjectWrapper").runMethod(false, "ConvertToWrapper", RemoteObject.createNew("anywheresoftware.b4a.objects.ActivityWrapper"), _hcv.runMethod(false,"getPanel").getObject()),scale.mostCurrent.__c.getField(true,"False"));
 BA.debugLineNum = 204;BA.debugLine="hcv.Panel.Width = hcv.Panel.Width * cScaleX";
Debug.ShouldStop(2048);
_hcv.runMethod(false,"getPanel").runMethod(true,"setWidth",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {_hcv.runMethod(false,"getPanel").runMethod(true,"getWidth"),scale._cscalex}, "*",0, 0)));
 }else 
{ BA.debugLineNum = 205;BA.debugLine="Else If GetType(v) = \"flm.b4a.scrollview2d.Scroll";
Debug.ShouldStop(4096);
if (RemoteObject.solveBoolean("=",scale.mostCurrent.__c.runMethod(true,"GetType",(Object)((_v.getObject()))),BA.ObjectToString("flm.b4a.scrollview2d.ScrollView2DWrapper$MyScrollView"))) { 
 BA.debugLineNum = 208;BA.debugLine="Dim scv2d As ScrollView2D";
Debug.ShouldStop(32768);
_scv2d = RemoteObject.createNew ("flm.b4a.scrollview2d.ScrollView2DWrapper");Debug.locals.put("scv2d", _scv2d);
 BA.debugLineNum = 209;BA.debugLine="scv2d = v";
Debug.ShouldStop(65536);
_scv2d.setObject(_v.getObject());
 BA.debugLineNum = 210;BA.debugLine="ScaleAll(scv2d.Panel, False)";
Debug.ShouldStop(131072);
_scaleall(_ba,RemoteObject.declareNull("anywheresoftware.b4a.AbsObjectWrapper").runMethod(false, "ConvertToWrapper", RemoteObject.createNew("anywheresoftware.b4a.objects.ActivityWrapper"), _scv2d.runMethod(false,"getPanel").getObject()),scale.mostCurrent.__c.getField(true,"False"));
 BA.debugLineNum = 211;BA.debugLine="scv2d.Panel.Width = scv2d.Panel.Width * cScaleX";
Debug.ShouldStop(262144);
_scv2d.runMethod(false,"getPanel").runMethod(true,"setWidth",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {_scv2d.runMethod(false,"getPanel").runMethod(true,"getWidth"),scale._cscalex}, "*",0, 0)));
 BA.debugLineNum = 212;BA.debugLine="scv2d.Panel.Height = scv2d.Panel.Height * cScale";
Debug.ShouldStop(524288);
_scv2d.runMethod(false,"getPanel").runMethod(true,"setHeight",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {_scv2d.runMethod(false,"getPanel").runMethod(true,"getHeight"),scale._cscaley}, "*",0, 0)));
 }else 
{ BA.debugLineNum = 213;BA.debugLine="Else If GetType(v) = \"anywheresoftware.b4a.object";
Debug.ShouldStop(1048576);
if (RemoteObject.solveBoolean("=",scale.mostCurrent.__c.runMethod(true,"GetType",(Object)((_v.getObject()))),BA.ObjectToString("anywheresoftware.b4a.objects.ListViewWrapper$SimpleListView"))) { 
 BA.debugLineNum = 216;BA.debugLine="Dim ltv As ListView";
Debug.ShouldStop(8388608);
_ltv = RemoteObject.createNew ("anywheresoftware.b4a.objects.ListViewWrapper");Debug.locals.put("ltv", _ltv);
 BA.debugLineNum = 217;BA.debugLine="ltv = v";
Debug.ShouldStop(16777216);
_ltv.setObject(_v.getObject());
 BA.debugLineNum = 218;BA.debugLine="ScaleView(ltv.SingleLineLayout.Label)";
Debug.ShouldStop(33554432);
_scaleview(_ba,RemoteObject.declareNull("anywheresoftware.b4a.AbsObjectWrapper").runMethod(false, "ConvertToWrapper", RemoteObject.createNew("anywheresoftware.b4a.objects.ConcreteViewWrapper"), _ltv.runMethod(false,"getSingleLineLayout").getField(false,"Label").getObject()));
 BA.debugLineNum = 219;BA.debugLine="ltv.SingleLineLayout.ItemHeight = ltv.SingleLine";
Debug.ShouldStop(67108864);
_ltv.runMethod(false,"getSingleLineLayout").runMethod(true,"setItemHeight",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {_ltv.runMethod(false,"getSingleLineLayout").runMethod(true,"getItemHeight"),scale._cscaley}, "*",0, 0)));
 BA.debugLineNum = 221;BA.debugLine="ScaleView(ltv.TwoLinesLayout.Label)";
Debug.ShouldStop(268435456);
_scaleview(_ba,RemoteObject.declareNull("anywheresoftware.b4a.AbsObjectWrapper").runMethod(false, "ConvertToWrapper", RemoteObject.createNew("anywheresoftware.b4a.objects.ConcreteViewWrapper"), _ltv.runMethod(false,"getTwoLinesLayout").getField(false,"Label").getObject()));
 BA.debugLineNum = 222;BA.debugLine="ScaleView(ltv.TwoLinesLayout.SecondLabel)";
Debug.ShouldStop(536870912);
_scaleview(_ba,RemoteObject.declareNull("anywheresoftware.b4a.AbsObjectWrapper").runMethod(false, "ConvertToWrapper", RemoteObject.createNew("anywheresoftware.b4a.objects.ConcreteViewWrapper"), _ltv.runMethod(false,"getTwoLinesLayout").getField(false,"SecondLabel").getObject()));
 BA.debugLineNum = 223;BA.debugLine="ltv.TwoLinesLayout.ItemHeight = ltv.TwoLinesLayo";
Debug.ShouldStop(1073741824);
_ltv.runMethod(false,"getTwoLinesLayout").runMethod(true,"setItemHeight",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {_ltv.runMethod(false,"getTwoLinesLayout").runMethod(true,"getItemHeight"),scale._cscaley}, "*",0, 0)));
 BA.debugLineNum = 225;BA.debugLine="ScaleView(ltv.TwoLinesAndBitmap.Label)";
Debug.ShouldStop(1);
_scaleview(_ba,RemoteObject.declareNull("anywheresoftware.b4a.AbsObjectWrapper").runMethod(false, "ConvertToWrapper", RemoteObject.createNew("anywheresoftware.b4a.objects.ConcreteViewWrapper"), _ltv.runMethod(false,"getTwoLinesAndBitmap").getField(false,"Label").getObject()));
 BA.debugLineNum = 226;BA.debugLine="ScaleView(ltv.TwoLinesAndBitmap.SecondLabel)";
Debug.ShouldStop(2);
_scaleview(_ba,RemoteObject.declareNull("anywheresoftware.b4a.AbsObjectWrapper").runMethod(false, "ConvertToWrapper", RemoteObject.createNew("anywheresoftware.b4a.objects.ConcreteViewWrapper"), _ltv.runMethod(false,"getTwoLinesAndBitmap").getField(false,"SecondLabel").getObject()));
 BA.debugLineNum = 227;BA.debugLine="ScaleView(ltv.TwoLinesAndBitmap.ImageView)";
Debug.ShouldStop(4);
_scaleview(_ba,RemoteObject.declareNull("anywheresoftware.b4a.AbsObjectWrapper").runMethod(false, "ConvertToWrapper", RemoteObject.createNew("anywheresoftware.b4a.objects.ConcreteViewWrapper"), _ltv.runMethod(false,"getTwoLinesAndBitmap").getField(false,"ImageView").getObject()));
 BA.debugLineNum = 228;BA.debugLine="ltv.TwoLinesAndBitmap.ItemHeight = ltv.TwoLinesA";
Debug.ShouldStop(8);
_ltv.runMethod(false,"getTwoLinesAndBitmap").runMethod(true,"setItemHeight",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {_ltv.runMethod(false,"getTwoLinesAndBitmap").runMethod(true,"getItemHeight"),scale._cscaley}, "*",0, 0)));
 BA.debugLineNum = 230;BA.debugLine="ltv.TwoLinesAndBitmap.ImageView.Top = (ltv.TwoLi";
Debug.ShouldStop(32);
_ltv.runMethod(false,"getTwoLinesAndBitmap").getField(false,"ImageView").runMethod(true,"setTop",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {(RemoteObject.solve(new RemoteObject[] {_ltv.runMethod(false,"getTwoLinesAndBitmap").runMethod(true,"getItemHeight"),_ltv.runMethod(false,"getTwoLinesAndBitmap").getField(false,"ImageView").runMethod(true,"getHeight")}, "-",1, 1)),RemoteObject.createImmutable(2)}, "/",0, 0)));
 }else 
{ BA.debugLineNum = 231;BA.debugLine="Else If GetType(v) = \"anywheresoftware.b4a.object";
Debug.ShouldStop(64);
if (RemoteObject.solveBoolean("=",scale.mostCurrent.__c.runMethod(true,"GetType",(Object)((_v.getObject()))),BA.ObjectToString("anywheresoftware.b4a.objects.SpinnerWrapper$B4ASpinner"))) { 
 BA.debugLineNum = 234;BA.debugLine="Dim spn As Spinner";
Debug.ShouldStop(512);
_spn = RemoteObject.createNew ("anywheresoftware.b4a.objects.SpinnerWrapper");Debug.locals.put("spn", _spn);
 BA.debugLineNum = 235;BA.debugLine="spn = v";
Debug.ShouldStop(1024);
_spn.setObject(_v.getObject());
 BA.debugLineNum = 236;BA.debugLine="spn.TextSize = spn.TextSize * cScaleX";
Debug.ShouldStop(2048);
_spn.runMethod(true,"setTextSize",BA.numberCast(float.class, RemoteObject.solve(new RemoteObject[] {_spn.runMethod(true,"getTextSize"),scale._cscalex}, "*",0, 0)));
 }}}}}
;
 BA.debugLineNum = 238;BA.debugLine="End Sub";
Debug.ShouldStop(8192);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _scaleviewds(RemoteObject _ba,RemoteObject _v) throws Exception{
try {
		Debug.PushSubsStack("ScaleViewDS (scale) ","scale",2,_ba,scale.mostCurrent,265);
if (RapidSub.canDelegate("scaleviewds")) return b4a.example.scale.remoteMe.runUserSub(false, "scale","scaleviewds", _ba, _v);
RemoteObject _pnl = RemoteObject.declareNull("anywheresoftware.b4a.objects.PanelWrapper");
RemoteObject _lbl = RemoteObject.declareNull("anywheresoftware.b4a.objects.LabelWrapper");
RemoteObject _scv = RemoteObject.declareNull("anywheresoftware.b4a.objects.ScrollViewWrapper");
RemoteObject _hcv = RemoteObject.declareNull("anywheresoftware.b4a.objects.HorizontalScrollViewWrapper");
RemoteObject _scv2d = RemoteObject.declareNull("flm.b4a.scrollview2d.ScrollView2DWrapper");
RemoteObject _ltv = RemoteObject.declareNull("anywheresoftware.b4a.objects.ListViewWrapper");
RemoteObject _spn = RemoteObject.declareNull("anywheresoftware.b4a.objects.SpinnerWrapper");
;
Debug.locals.put("v", _v);
 BA.debugLineNum = 265;BA.debugLine="Public Sub ScaleViewDS(v As View)";
Debug.ShouldStop(256);
 BA.debugLineNum = 266;BA.debugLine="If IsActivity(v) Then";
Debug.ShouldStop(512);
if (_isactivity(_ba,_v).<Boolean>get().booleanValue()) { 
 BA.debugLineNum = 267;BA.debugLine="Return";
Debug.ShouldStop(1024);
if (true) return RemoteObject.createImmutable("");
 };
 BA.debugLineNum = 270;BA.debugLine="v.Left = v.Left * cScaleDS";
Debug.ShouldStop(8192);
_v.runMethod(true,"setLeft",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {_v.runMethod(true,"getLeft"),scale._cscaleds}, "*",0, 0)));
 BA.debugLineNum = 271;BA.debugLine="v.Top = v.Top * cScaleDS";
Debug.ShouldStop(16384);
_v.runMethod(true,"setTop",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {_v.runMethod(true,"getTop"),scale._cscaleds}, "*",0, 0)));
 BA.debugLineNum = 272;BA.debugLine="v.Width = v.Width * cScaleDS";
Debug.ShouldStop(32768);
_v.runMethod(true,"setWidth",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {_v.runMethod(true,"getWidth"),scale._cscaleds}, "*",0, 0)));
 BA.debugLineNum = 273;BA.debugLine="v.Height = v.Height * cScaleDS";
Debug.ShouldStop(65536);
_v.runMethod(true,"setHeight",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {_v.runMethod(true,"getHeight"),scale._cscaleds}, "*",0, 0)));
 BA.debugLineNum = 275;BA.debugLine="If IsPanel(v) Then";
Debug.ShouldStop(262144);
if (_ispanel(_ba,_v).<Boolean>get().booleanValue()) { 
 BA.debugLineNum = 276;BA.debugLine="Dim pnl As Panel";
Debug.ShouldStop(524288);
_pnl = RemoteObject.createNew ("anywheresoftware.b4a.objects.PanelWrapper");Debug.locals.put("pnl", _pnl);
 BA.debugLineNum = 277;BA.debugLine="pnl = v";
Debug.ShouldStop(1048576);
_pnl.setObject(_v.getObject());
 BA.debugLineNum = 278;BA.debugLine="ScaleAllDS(pnl, False)";
Debug.ShouldStop(2097152);
_scaleallds(_ba,RemoteObject.declareNull("anywheresoftware.b4a.AbsObjectWrapper").runMethod(false, "ConvertToWrapper", RemoteObject.createNew("anywheresoftware.b4a.objects.ActivityWrapper"), _pnl.getObject()),scale.mostCurrent.__c.getField(true,"False"));
 };
 BA.debugLineNum = 281;BA.debugLine="If v Is Label Then 'this will catch ALL views wit";
Debug.ShouldStop(16777216);
if (RemoteObject.solveBoolean("i",_v.getObjectOrNull(), RemoteObject.createImmutable("android.widget.TextView"))) { 
 BA.debugLineNum = 282;BA.debugLine="Dim lbl As Label = v";
Debug.ShouldStop(33554432);
_lbl = RemoteObject.createNew ("anywheresoftware.b4a.objects.LabelWrapper");
_lbl.setObject(_v.getObject());Debug.locals.put("lbl", _lbl);
 BA.debugLineNum = 283;BA.debugLine="lbl.TextSize = lbl.TextSize * cScaleDS";
Debug.ShouldStop(67108864);
_lbl.runMethod(true,"setTextSize",BA.numberCast(float.class, RemoteObject.solve(new RemoteObject[] {_lbl.runMethod(true,"getTextSize"),scale._cscaleds}, "*",0, 0)));
 };
 BA.debugLineNum = 286;BA.debugLine="If GetType(v) = \"anywheresoftware.b4a.objects.Scr";
Debug.ShouldStop(536870912);
if (RemoteObject.solveBoolean("=",scale.mostCurrent.__c.runMethod(true,"GetType",(Object)((_v.getObject()))),BA.ObjectToString("anywheresoftware.b4a.objects.ScrollViewWrapper$MyScrollView"))) { 
 BA.debugLineNum = 289;BA.debugLine="Dim scv As ScrollView";
Debug.ShouldStop(1);
_scv = RemoteObject.createNew ("anywheresoftware.b4a.objects.ScrollViewWrapper");Debug.locals.put("scv", _scv);
 BA.debugLineNum = 290;BA.debugLine="scv = v";
Debug.ShouldStop(2);
_scv.setObject(_v.getObject());
 BA.debugLineNum = 291;BA.debugLine="ScaleAllDS(scv.Panel, False)";
Debug.ShouldStop(4);
_scaleallds(_ba,RemoteObject.declareNull("anywheresoftware.b4a.AbsObjectWrapper").runMethod(false, "ConvertToWrapper", RemoteObject.createNew("anywheresoftware.b4a.objects.ActivityWrapper"), _scv.runMethod(false,"getPanel").getObject()),scale.mostCurrent.__c.getField(true,"False"));
 BA.debugLineNum = 292;BA.debugLine="scv.Panel.Height = scv.Panel.Height * cScaleDS";
Debug.ShouldStop(8);
_scv.runMethod(false,"getPanel").runMethod(true,"setHeight",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {_scv.runMethod(false,"getPanel").runMethod(true,"getHeight"),scale._cscaleds}, "*",0, 0)));
 }else 
{ BA.debugLineNum = 293;BA.debugLine="Else If GetType(v) = \"anywheresoftware.b4a.object";
Debug.ShouldStop(16);
if (RemoteObject.solveBoolean("=",scale.mostCurrent.__c.runMethod(true,"GetType",(Object)((_v.getObject()))),BA.ObjectToString("anywheresoftware.b4a.objects.HorizontalScrollViewWrapper$MyHScrollView"))) { 
 BA.debugLineNum = 296;BA.debugLine="Dim hcv As HorizontalScrollView";
Debug.ShouldStop(128);
_hcv = RemoteObject.createNew ("anywheresoftware.b4a.objects.HorizontalScrollViewWrapper");Debug.locals.put("hcv", _hcv);
 BA.debugLineNum = 297;BA.debugLine="hcv = v";
Debug.ShouldStop(256);
_hcv.setObject(_v.getObject());
 BA.debugLineNum = 298;BA.debugLine="ScaleAllDS(hcv.Panel, False)";
Debug.ShouldStop(512);
_scaleallds(_ba,RemoteObject.declareNull("anywheresoftware.b4a.AbsObjectWrapper").runMethod(false, "ConvertToWrapper", RemoteObject.createNew("anywheresoftware.b4a.objects.ActivityWrapper"), _hcv.runMethod(false,"getPanel").getObject()),scale.mostCurrent.__c.getField(true,"False"));
 BA.debugLineNum = 299;BA.debugLine="hcv.Panel.Width = hcv.Panel.Width * cScaleDS";
Debug.ShouldStop(1024);
_hcv.runMethod(false,"getPanel").runMethod(true,"setWidth",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {_hcv.runMethod(false,"getPanel").runMethod(true,"getWidth"),scale._cscaleds}, "*",0, 0)));
 }else 
{ BA.debugLineNum = 300;BA.debugLine="Else If GetType(v) = \"flm.b4a.scrollview2d.Scroll";
Debug.ShouldStop(2048);
if (RemoteObject.solveBoolean("=",scale.mostCurrent.__c.runMethod(true,"GetType",(Object)((_v.getObject()))),BA.ObjectToString("flm.b4a.scrollview2d.ScrollView2DWrapper$MyScrollView"))) { 
 BA.debugLineNum = 303;BA.debugLine="Dim scv2d As ScrollView2D";
Debug.ShouldStop(16384);
_scv2d = RemoteObject.createNew ("flm.b4a.scrollview2d.ScrollView2DWrapper");Debug.locals.put("scv2d", _scv2d);
 BA.debugLineNum = 304;BA.debugLine="scv2d = v";
Debug.ShouldStop(32768);
_scv2d.setObject(_v.getObject());
 BA.debugLineNum = 305;BA.debugLine="ScaleAllDS(scv2d.Panel, False)";
Debug.ShouldStop(65536);
_scaleallds(_ba,RemoteObject.declareNull("anywheresoftware.b4a.AbsObjectWrapper").runMethod(false, "ConvertToWrapper", RemoteObject.createNew("anywheresoftware.b4a.objects.ActivityWrapper"), _scv2d.runMethod(false,"getPanel").getObject()),scale.mostCurrent.__c.getField(true,"False"));
 BA.debugLineNum = 306;BA.debugLine="scv2d.Panel.Width = scv2d.Panel.Width * cScaleDS";
Debug.ShouldStop(131072);
_scv2d.runMethod(false,"getPanel").runMethod(true,"setWidth",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {_scv2d.runMethod(false,"getPanel").runMethod(true,"getWidth"),scale._cscaleds}, "*",0, 0)));
 BA.debugLineNum = 307;BA.debugLine="scv2d.Panel.Height = scv2d.Panel.Height * cScale";
Debug.ShouldStop(262144);
_scv2d.runMethod(false,"getPanel").runMethod(true,"setHeight",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {_scv2d.runMethod(false,"getPanel").runMethod(true,"getHeight"),scale._cscaleds}, "*",0, 0)));
 }else 
{ BA.debugLineNum = 308;BA.debugLine="Else If GetType(v) = \"anywheresoftware.b4a.object";
Debug.ShouldStop(524288);
if (RemoteObject.solveBoolean("=",scale.mostCurrent.__c.runMethod(true,"GetType",(Object)((_v.getObject()))),BA.ObjectToString("anywheresoftware.b4a.objects.ListViewWrapper$SimpleListView"))) { 
 BA.debugLineNum = 311;BA.debugLine="Dim ltv As ListView";
Debug.ShouldStop(4194304);
_ltv = RemoteObject.createNew ("anywheresoftware.b4a.objects.ListViewWrapper");Debug.locals.put("ltv", _ltv);
 BA.debugLineNum = 312;BA.debugLine="ltv = v";
Debug.ShouldStop(8388608);
_ltv.setObject(_v.getObject());
 BA.debugLineNum = 313;BA.debugLine="ScaleViewDS(ltv.SingleLineLayout.Label)";
Debug.ShouldStop(16777216);
_scaleviewds(_ba,RemoteObject.declareNull("anywheresoftware.b4a.AbsObjectWrapper").runMethod(false, "ConvertToWrapper", RemoteObject.createNew("anywheresoftware.b4a.objects.ConcreteViewWrapper"), _ltv.runMethod(false,"getSingleLineLayout").getField(false,"Label").getObject()));
 BA.debugLineNum = 314;BA.debugLine="ltv.SingleLineLayout.ItemHeight = ltv.SingleLine";
Debug.ShouldStop(33554432);
_ltv.runMethod(false,"getSingleLineLayout").runMethod(true,"setItemHeight",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {_ltv.runMethod(false,"getSingleLineLayout").runMethod(true,"getItemHeight"),scale._cscaleds}, "*",0, 0)));
 BA.debugLineNum = 316;BA.debugLine="ScaleViewDS(ltv.TwoLinesLayout.Label)";
Debug.ShouldStop(134217728);
_scaleviewds(_ba,RemoteObject.declareNull("anywheresoftware.b4a.AbsObjectWrapper").runMethod(false, "ConvertToWrapper", RemoteObject.createNew("anywheresoftware.b4a.objects.ConcreteViewWrapper"), _ltv.runMethod(false,"getTwoLinesLayout").getField(false,"Label").getObject()));
 BA.debugLineNum = 317;BA.debugLine="ScaleViewDS(ltv.TwoLinesLayout.SecondLabel)";
Debug.ShouldStop(268435456);
_scaleviewds(_ba,RemoteObject.declareNull("anywheresoftware.b4a.AbsObjectWrapper").runMethod(false, "ConvertToWrapper", RemoteObject.createNew("anywheresoftware.b4a.objects.ConcreteViewWrapper"), _ltv.runMethod(false,"getTwoLinesLayout").getField(false,"SecondLabel").getObject()));
 BA.debugLineNum = 318;BA.debugLine="ltv.TwoLinesLayout.ItemHeight = ltv.TwoLinesLayo";
Debug.ShouldStop(536870912);
_ltv.runMethod(false,"getTwoLinesLayout").runMethod(true,"setItemHeight",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {_ltv.runMethod(false,"getTwoLinesLayout").runMethod(true,"getItemHeight"),scale._cscaleds}, "*",0, 0)));
 BA.debugLineNum = 320;BA.debugLine="ScaleViewDS(ltv.TwoLinesAndBitmap.Label)";
Debug.ShouldStop(-2147483648);
_scaleviewds(_ba,RemoteObject.declareNull("anywheresoftware.b4a.AbsObjectWrapper").runMethod(false, "ConvertToWrapper", RemoteObject.createNew("anywheresoftware.b4a.objects.ConcreteViewWrapper"), _ltv.runMethod(false,"getTwoLinesAndBitmap").getField(false,"Label").getObject()));
 BA.debugLineNum = 321;BA.debugLine="ScaleViewDS(ltv.TwoLinesAndBitmap.SecondLabel)";
Debug.ShouldStop(1);
_scaleviewds(_ba,RemoteObject.declareNull("anywheresoftware.b4a.AbsObjectWrapper").runMethod(false, "ConvertToWrapper", RemoteObject.createNew("anywheresoftware.b4a.objects.ConcreteViewWrapper"), _ltv.runMethod(false,"getTwoLinesAndBitmap").getField(false,"SecondLabel").getObject()));
 BA.debugLineNum = 322;BA.debugLine="ScaleViewDS(ltv.TwoLinesAndBitmap.ImageView)";
Debug.ShouldStop(2);
_scaleviewds(_ba,RemoteObject.declareNull("anywheresoftware.b4a.AbsObjectWrapper").runMethod(false, "ConvertToWrapper", RemoteObject.createNew("anywheresoftware.b4a.objects.ConcreteViewWrapper"), _ltv.runMethod(false,"getTwoLinesAndBitmap").getField(false,"ImageView").getObject()));
 BA.debugLineNum = 323;BA.debugLine="ltv.TwoLinesAndBitmap.ItemHeight = ltv.TwoLinesA";
Debug.ShouldStop(4);
_ltv.runMethod(false,"getTwoLinesAndBitmap").runMethod(true,"setItemHeight",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {_ltv.runMethod(false,"getTwoLinesAndBitmap").runMethod(true,"getItemHeight"),scale._cscaleds}, "*",0, 0)));
 BA.debugLineNum = 325;BA.debugLine="ltv.TwoLinesAndBitmap.ImageView.Top = (ltv.TwoLi";
Debug.ShouldStop(16);
_ltv.runMethod(false,"getTwoLinesAndBitmap").getField(false,"ImageView").runMethod(true,"setTop",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {(RemoteObject.solve(new RemoteObject[] {_ltv.runMethod(false,"getTwoLinesAndBitmap").runMethod(true,"getItemHeight"),_ltv.runMethod(false,"getTwoLinesAndBitmap").getField(false,"ImageView").runMethod(true,"getHeight")}, "-",1, 1)),RemoteObject.createImmutable(2)}, "/",0, 0)));
 }else 
{ BA.debugLineNum = 326;BA.debugLine="Else If GetType(v) = \"anywheresoftware.b4a.object";
Debug.ShouldStop(32);
if (RemoteObject.solveBoolean("=",scale.mostCurrent.__c.runMethod(true,"GetType",(Object)((_v.getObject()))),BA.ObjectToString("anywheresoftware.b4a.objects.SpinnerWrapper$B4ASpinner"))) { 
 BA.debugLineNum = 329;BA.debugLine="Dim spn As Spinner";
Debug.ShouldStop(256);
_spn = RemoteObject.createNew ("anywheresoftware.b4a.objects.SpinnerWrapper");Debug.locals.put("spn", _spn);
 BA.debugLineNum = 330;BA.debugLine="spn = v";
Debug.ShouldStop(512);
_spn.setObject(_v.getObject());
 BA.debugLineNum = 331;BA.debugLine="spn.TextSize = spn.TextSize * cScaleDS";
Debug.ShouldStop(1024);
_spn.runMethod(true,"setTextSize",BA.numberCast(float.class, RemoteObject.solve(new RemoteObject[] {_spn.runMethod(true,"getTextSize"),scale._cscaleds}, "*",0, 0)));
 }}}}}
;
 BA.debugLineNum = 333;BA.debugLine="End Sub";
Debug.ShouldStop(4096);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _setbottom(RemoteObject _ba,RemoteObject _v,RemoteObject _ybottom) throws Exception{
try {
		Debug.PushSubsStack("SetBottom (scale) ","scale",2,_ba,scale.mostCurrent,423);
if (RapidSub.canDelegate("setbottom")) return b4a.example.scale.remoteMe.runUserSub(false, "scale","setbottom", _ba, _v, _ybottom);
;
Debug.locals.put("v", _v);
Debug.locals.put("yBottom", _ybottom);
 BA.debugLineNum = 423;BA.debugLine="Sub SetBottom(v As View, yBottom As Int)";
Debug.ShouldStop(64);
 BA.debugLineNum = 424;BA.debugLine="v.Top = yBottom - v.Height";
Debug.ShouldStop(128);
_v.runMethod(true,"setTop",RemoteObject.solve(new RemoteObject[] {_ybottom,_v.runMethod(true,"getHeight")}, "-",1, 1));
 BA.debugLineNum = 425;BA.debugLine="End Sub";
Debug.ShouldStop(256);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _setleftandright(RemoteObject _ba,RemoteObject _v,RemoteObject _xleft,RemoteObject _xright) throws Exception{
try {
		Debug.PushSubsStack("SetLeftAndRight (scale) ","scale",2,_ba,scale.mostCurrent,429);
if (RapidSub.canDelegate("setleftandright")) return b4a.example.scale.remoteMe.runUserSub(false, "scale","setleftandright", _ba, _v, _xleft, _xright);
;
Debug.locals.put("v", _v);
Debug.locals.put("xLeft", _xleft);
Debug.locals.put("xRight", _xright);
 BA.debugLineNum = 429;BA.debugLine="Public Sub SetLeftAndRight(v As View, xLeft As Int";
Debug.ShouldStop(4096);
 BA.debugLineNum = 431;BA.debugLine="v.Left = Min(xLeft, xRight)";
Debug.ShouldStop(16384);
_v.runMethod(true,"setLeft",BA.numberCast(int.class, scale.mostCurrent.__c.runMethod(true,"Min",(Object)(BA.numberCast(double.class, _xleft)),(Object)(BA.numberCast(double.class, _xright)))));
 BA.debugLineNum = 432;BA.debugLine="v.Width = Max(xLeft, xRight) - v.Left";
Debug.ShouldStop(32768);
_v.runMethod(true,"setWidth",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {scale.mostCurrent.__c.runMethod(true,"Max",(Object)(BA.numberCast(double.class, _xleft)),(Object)(BA.numberCast(double.class, _xright))),_v.runMethod(true,"getLeft")}, "-",1, 0)));
 BA.debugLineNum = 433;BA.debugLine="End Sub";
Debug.ShouldStop(65536);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _setleftandright2(RemoteObject _ba,RemoteObject _v,RemoteObject _vleft,RemoteObject _dxl,RemoteObject _vright,RemoteObject _dxr) throws Exception{
try {
		Debug.PushSubsStack("SetLeftAndRight2 (scale) ","scale",2,_ba,scale.mostCurrent,439);
if (RapidSub.canDelegate("setleftandright2")) return b4a.example.scale.remoteMe.runUserSub(false, "scale","setleftandright2", _ba, _v, _vleft, _dxl, _vright, _dxr);
RemoteObject _v1 = RemoteObject.declareNull("anywheresoftware.b4a.objects.ConcreteViewWrapper");
;
Debug.locals.put("v", _v);
Debug.locals.put("vLeft", _vleft);
Debug.locals.put("dxL", _dxl);
Debug.locals.put("vRight", _vright);
Debug.locals.put("dxR", _dxr);
 BA.debugLineNum = 439;BA.debugLine="Public Sub SetLeftAndRight2(v As View, vLeft As Vi";
Debug.ShouldStop(4194304);
 BA.debugLineNum = 441;BA.debugLine="If IsActivity(v) Then";
Debug.ShouldStop(16777216);
if (_isactivity(_ba,_v).<Boolean>get().booleanValue()) { 
 BA.debugLineNum = 442;BA.debugLine="ToastMessageShow(\"The view is an Activity !\", Fa";
Debug.ShouldStop(33554432);
scale.mostCurrent.__c.runVoidMethod ("ToastMessageShow",(Object)(BA.ObjectToCharSequence("The view is an Activity !")),(Object)(scale.mostCurrent.__c.getField(true,"False")));
 BA.debugLineNum = 443;BA.debugLine="Return";
Debug.ShouldStop(67108864);
if (true) return RemoteObject.createImmutable("");
 };
 BA.debugLineNum = 447;BA.debugLine="If IsActivity(vLeft) = False AND IsActivity(vRigh";
Debug.ShouldStop(1073741824);
if (RemoteObject.solveBoolean("=",_isactivity(_ba,_vleft),scale.mostCurrent.__c.getField(true,"False")) && RemoteObject.solveBoolean("=",_isactivity(_ba,_vright),scale.mostCurrent.__c.getField(true,"False"))) { 
 BA.debugLineNum = 448;BA.debugLine="If vLeft.Left > vRight.Left Then";
Debug.ShouldStop(-2147483648);
if (RemoteObject.solveBoolean(">",_vleft.runMethod(true,"getLeft"),BA.numberCast(double.class, _vright.runMethod(true,"getLeft")))) { 
 BA.debugLineNum = 449;BA.debugLine="Dim v1 As View";
Debug.ShouldStop(1);
_v1 = RemoteObject.createNew ("anywheresoftware.b4a.objects.ConcreteViewWrapper");Debug.locals.put("v1", _v1);
 BA.debugLineNum = 450;BA.debugLine="v1 = vLeft";
Debug.ShouldStop(2);
_v1 = _vleft;Debug.locals.put("v1", _v1);
 BA.debugLineNum = 451;BA.debugLine="vLeft = vRight";
Debug.ShouldStop(4);
_vleft = _vright;Debug.locals.put("vLeft", _vleft);
 BA.debugLineNum = 452;BA.debugLine="vRight = v1";
Debug.ShouldStop(8);
_vright = _v1;Debug.locals.put("vRight", _vright);
 };
 };
 BA.debugLineNum = 456;BA.debugLine="If IsActivity(vLeft) Then";
Debug.ShouldStop(128);
if (_isactivity(_ba,_vleft).<Boolean>get().booleanValue()) { 
 BA.debugLineNum = 457;BA.debugLine="v.Left = dxL";
Debug.ShouldStop(256);
_v.runMethod(true,"setLeft",_dxl);
 BA.debugLineNum = 458;BA.debugLine="If IsActivity(vRight) Then";
Debug.ShouldStop(512);
if (_isactivity(_ba,_vright).<Boolean>get().booleanValue()) { 
 BA.debugLineNum = 459;BA.debugLine="v.Width = 100%x - dxL - dxR";
Debug.ShouldStop(1024);
_v.runMethod(true,"setWidth",RemoteObject.solve(new RemoteObject[] {scale.mostCurrent.__c.runMethod(true,"PerXToCurrent",(Object)(BA.numberCast(float.class, 100)),_ba),_dxl,_dxr}, "--",2, 1));
 }else {
 BA.debugLineNum = 461;BA.debugLine="v.Width = vRight.Left - dxL - dxR";
Debug.ShouldStop(4096);
_v.runMethod(true,"setWidth",RemoteObject.solve(new RemoteObject[] {_vright.runMethod(true,"getLeft"),_dxl,_dxr}, "--",2, 1));
 };
 }else {
 BA.debugLineNum = 464;BA.debugLine="v.Left = vLeft.Left + vLeft.Width + dxL";
Debug.ShouldStop(32768);
_v.runMethod(true,"setLeft",RemoteObject.solve(new RemoteObject[] {_vleft.runMethod(true,"getLeft"),_vleft.runMethod(true,"getWidth"),_dxl}, "++",2, 1));
 BA.debugLineNum = 465;BA.debugLine="If IsActivity(vRight) Then";
Debug.ShouldStop(65536);
if (_isactivity(_ba,_vright).<Boolean>get().booleanValue()) { 
 BA.debugLineNum = 466;BA.debugLine="v.Width = 100%x - v.Left";
Debug.ShouldStop(131072);
_v.runMethod(true,"setWidth",RemoteObject.solve(new RemoteObject[] {scale.mostCurrent.__c.runMethod(true,"PerXToCurrent",(Object)(BA.numberCast(float.class, 100)),_ba),_v.runMethod(true,"getLeft")}, "-",1, 1));
 }else {
 BA.debugLineNum = 468;BA.debugLine="v.Width = vRight.Left - v.Left - dxR";
Debug.ShouldStop(524288);
_v.runMethod(true,"setWidth",RemoteObject.solve(new RemoteObject[] {_vright.runMethod(true,"getLeft"),_v.runMethod(true,"getLeft"),_dxr}, "--",2, 1));
 };
 };
 BA.debugLineNum = 471;BA.debugLine="End Sub";
Debug.ShouldStop(4194304);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _setrate(RemoteObject _ba,RemoteObject _crate) throws Exception{
try {
		Debug.PushSubsStack("SetRate (scale) ","scale",2,_ba,scale.mostCurrent,138);
if (RapidSub.canDelegate("setrate")) return b4a.example.scale.remoteMe.runUserSub(false, "scale","setrate", _ba, _crate);
;
Debug.locals.put("cRate", _crate);
 BA.debugLineNum = 138;BA.debugLine="Public Sub SetRate(cRate As Double)";
Debug.ShouldStop(512);
 BA.debugLineNum = 139;BA.debugLine="Rate = cRate";
Debug.ShouldStop(1024);
scale._rate = _crate;
 BA.debugLineNum = 140;BA.debugLine="Initialize";
Debug.ShouldStop(2048);
_initialize(_ba);
 BA.debugLineNum = 141;BA.debugLine="End Sub";
Debug.ShouldStop(4096);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _setreferencelayout(RemoteObject _ba,RemoteObject _width,RemoteObject _height,RemoteObject _scale) throws Exception{
try {
		Debug.PushSubsStack("SetReferenceLayout (scale) ","scale",2,_ba,scale.mostCurrent,557);
if (RapidSub.canDelegate("setreferencelayout")) return b4a.example.scale.remoteMe.runUserSub(false, "scale","setreferencelayout", _ba, _width, _height, _scale);
;
Debug.locals.put("Width", _width);
Debug.locals.put("Height", _height);
Debug.locals.put("Scale", _scale);
 BA.debugLineNum = 557;BA.debugLine="Public Sub SetReferenceLayout(Width As Int, Height";
Debug.ShouldStop(4096);
 BA.debugLineNum = 558;BA.debugLine="If cRefWidth < cRefHeight Then";
Debug.ShouldStop(8192);
if (RemoteObject.solveBoolean("<",scale._crefwidth,BA.numberCast(double.class, scale._crefheight))) { 
 BA.debugLineNum = 559;BA.debugLine="cRefWidth = Width";
Debug.ShouldStop(16384);
scale._crefwidth = _width;
 BA.debugLineNum = 560;BA.debugLine="cRefHeight = Height";
Debug.ShouldStop(32768);
scale._crefheight = _height;
 }else {
 BA.debugLineNum = 562;BA.debugLine="cRefWidth = Height";
Debug.ShouldStop(131072);
scale._crefwidth = _height;
 BA.debugLineNum = 563;BA.debugLine="cRefHeight = Width";
Debug.ShouldStop(262144);
scale._crefheight = _width;
 };
 BA.debugLineNum = 565;BA.debugLine="cRefScale = Scale";
Debug.ShouldStop(1048576);
scale._crefscale = _scale;
 BA.debugLineNum = 566;BA.debugLine="Initialize";
Debug.ShouldStop(2097152);
_initialize(_ba);
 BA.debugLineNum = 567;BA.debugLine="End Sub";
Debug.ShouldStop(4194304);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _setright(RemoteObject _ba,RemoteObject _v,RemoteObject _xright) throws Exception{
try {
		Debug.PushSubsStack("SetRight (scale) ","scale",2,_ba,scale.mostCurrent,417);
if (RapidSub.canDelegate("setright")) return b4a.example.scale.remoteMe.runUserSub(false, "scale","setright", _ba, _v, _xright);
;
Debug.locals.put("v", _v);
Debug.locals.put("xRight", _xright);
 BA.debugLineNum = 417;BA.debugLine="Sub SetRight(v As View, xRight As Int)";
Debug.ShouldStop(1);
 BA.debugLineNum = 418;BA.debugLine="v.Left = xRight - v.Width";
Debug.ShouldStop(2);
_v.runMethod(true,"setLeft",RemoteObject.solve(new RemoteObject[] {_xright,_v.runMethod(true,"getWidth")}, "-",1, 1));
 BA.debugLineNum = 419;BA.debugLine="End Sub";
Debug.ShouldStop(4);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _settopandbottom(RemoteObject _ba,RemoteObject _v,RemoteObject _ytop,RemoteObject _ybottom) throws Exception{
try {
		Debug.PushSubsStack("SetTopAndBottom (scale) ","scale",2,_ba,scale.mostCurrent,475);
if (RapidSub.canDelegate("settopandbottom")) return b4a.example.scale.remoteMe.runUserSub(false, "scale","settopandbottom", _ba, _v, _ytop, _ybottom);
;
Debug.locals.put("v", _v);
Debug.locals.put("yTop", _ytop);
Debug.locals.put("yBottom", _ybottom);
 BA.debugLineNum = 475;BA.debugLine="Public Sub SetTopAndBottom(v As View, yTop As Int,";
Debug.ShouldStop(67108864);
 BA.debugLineNum = 477;BA.debugLine="v.Top = Min(yTop, yBottom)";
Debug.ShouldStop(268435456);
_v.runMethod(true,"setTop",BA.numberCast(int.class, scale.mostCurrent.__c.runMethod(true,"Min",(Object)(BA.numberCast(double.class, _ytop)),(Object)(BA.numberCast(double.class, _ybottom)))));
 BA.debugLineNum = 478;BA.debugLine="v.Height = Max(yTop, yBottom) - v.Top";
Debug.ShouldStop(536870912);
_v.runMethod(true,"setHeight",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {scale.mostCurrent.__c.runMethod(true,"Max",(Object)(BA.numberCast(double.class, _ytop)),(Object)(BA.numberCast(double.class, _ybottom))),_v.runMethod(true,"getTop")}, "-",1, 0)));
 BA.debugLineNum = 479;BA.debugLine="End Sub";
Debug.ShouldStop(1073741824);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _settopandbottom2(RemoteObject _ba,RemoteObject _v,RemoteObject _vtop,RemoteObject _dyt,RemoteObject _vbottom,RemoteObject _dyb) throws Exception{
try {
		Debug.PushSubsStack("SetTopAndBottom2 (scale) ","scale",2,_ba,scale.mostCurrent,485);
if (RapidSub.canDelegate("settopandbottom2")) return b4a.example.scale.remoteMe.runUserSub(false, "scale","settopandbottom2", _ba, _v, _vtop, _dyt, _vbottom, _dyb);
RemoteObject _v1 = RemoteObject.declareNull("anywheresoftware.b4a.objects.ConcreteViewWrapper");
;
Debug.locals.put("v", _v);
Debug.locals.put("vTop", _vtop);
Debug.locals.put("dyT", _dyt);
Debug.locals.put("vBottom", _vbottom);
Debug.locals.put("dyB", _dyb);
 BA.debugLineNum = 485;BA.debugLine="Public Sub SetTopAndBottom2(v As View, vTop As Vie";
Debug.ShouldStop(16);
 BA.debugLineNum = 487;BA.debugLine="If IsActivity(v) Then";
Debug.ShouldStop(64);
if (_isactivity(_ba,_v).<Boolean>get().booleanValue()) { 
 BA.debugLineNum = 488;BA.debugLine="ToastMessageShow(\"The view is an Activity !\", Fa";
Debug.ShouldStop(128);
scale.mostCurrent.__c.runVoidMethod ("ToastMessageShow",(Object)(BA.ObjectToCharSequence("The view is an Activity !")),(Object)(scale.mostCurrent.__c.getField(true,"False")));
 BA.debugLineNum = 489;BA.debugLine="Return";
Debug.ShouldStop(256);
if (true) return RemoteObject.createImmutable("");
 };
 BA.debugLineNum = 492;BA.debugLine="If IsActivity(vTop) = False AND IsActivity(vBotto";
Debug.ShouldStop(2048);
if (RemoteObject.solveBoolean("=",_isactivity(_ba,_vtop),scale.mostCurrent.__c.getField(true,"False")) && RemoteObject.solveBoolean("=",_isactivity(_ba,_vbottom),scale.mostCurrent.__c.getField(true,"False"))) { 
 BA.debugLineNum = 494;BA.debugLine="If vTop.Top > vBottom.Top Then";
Debug.ShouldStop(8192);
if (RemoteObject.solveBoolean(">",_vtop.runMethod(true,"getTop"),BA.numberCast(double.class, _vbottom.runMethod(true,"getTop")))) { 
 BA.debugLineNum = 495;BA.debugLine="Dim v1 As View";
Debug.ShouldStop(16384);
_v1 = RemoteObject.createNew ("anywheresoftware.b4a.objects.ConcreteViewWrapper");Debug.locals.put("v1", _v1);
 BA.debugLineNum = 496;BA.debugLine="v1 = vTop";
Debug.ShouldStop(32768);
_v1 = _vtop;Debug.locals.put("v1", _v1);
 BA.debugLineNum = 497;BA.debugLine="vTop = vBottom";
Debug.ShouldStop(65536);
_vtop = _vbottom;Debug.locals.put("vTop", _vtop);
 BA.debugLineNum = 498;BA.debugLine="vBottom = v1";
Debug.ShouldStop(131072);
_vbottom = _v1;Debug.locals.put("vBottom", _vbottom);
 };
 };
 BA.debugLineNum = 502;BA.debugLine="If IsActivity(vTop) Then";
Debug.ShouldStop(2097152);
if (_isactivity(_ba,_vtop).<Boolean>get().booleanValue()) { 
 BA.debugLineNum = 503;BA.debugLine="v.Top = dyT";
Debug.ShouldStop(4194304);
_v.runMethod(true,"setTop",_dyt);
 BA.debugLineNum = 504;BA.debugLine="If IsActivity(vBottom) Then";
Debug.ShouldStop(8388608);
if (_isactivity(_ba,_vbottom).<Boolean>get().booleanValue()) { 
 BA.debugLineNum = 505;BA.debugLine="v.Height = 100%y - dyT - dyB";
Debug.ShouldStop(16777216);
_v.runMethod(true,"setHeight",RemoteObject.solve(new RemoteObject[] {scale.mostCurrent.__c.runMethod(true,"PerYToCurrent",(Object)(BA.numberCast(float.class, 100)),_ba),_dyt,_dyb}, "--",2, 1));
 }else {
 BA.debugLineNum = 507;BA.debugLine="v.Height = vBottom.Top - dyT - dyB";
Debug.ShouldStop(67108864);
_v.runMethod(true,"setHeight",RemoteObject.solve(new RemoteObject[] {_vbottom.runMethod(true,"getTop"),_dyt,_dyb}, "--",2, 1));
 };
 }else {
 BA.debugLineNum = 510;BA.debugLine="v.Top = vTop.Top + vTop.Height + dyT";
Debug.ShouldStop(536870912);
_v.runMethod(true,"setTop",RemoteObject.solve(new RemoteObject[] {_vtop.runMethod(true,"getTop"),_vtop.runMethod(true,"getHeight"),_dyt}, "++",2, 1));
 BA.debugLineNum = 511;BA.debugLine="If IsActivity(vBottom) Then";
Debug.ShouldStop(1073741824);
if (_isactivity(_ba,_vbottom).<Boolean>get().booleanValue()) { 
 BA.debugLineNum = 512;BA.debugLine="v.Height = 100%y - v.Top - dyB";
Debug.ShouldStop(-2147483648);
_v.runMethod(true,"setHeight",RemoteObject.solve(new RemoteObject[] {scale.mostCurrent.__c.runMethod(true,"PerYToCurrent",(Object)(BA.numberCast(float.class, 100)),_ba),_v.runMethod(true,"getTop"),_dyb}, "--",2, 1));
 }else {
 BA.debugLineNum = 514;BA.debugLine="v.Height = vBottom.Top - v.Top - dyB";
Debug.ShouldStop(2);
_v.runMethod(true,"setHeight",RemoteObject.solve(new RemoteObject[] {_vbottom.runMethod(true,"getTop"),_v.runMethod(true,"getTop"),_dyb}, "--",2, 1));
 };
 };
 BA.debugLineNum = 517;BA.debugLine="End Sub";
Debug.ShouldStop(16);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _verticalcenter(RemoteObject _ba,RemoteObject _v) throws Exception{
try {
		Debug.PushSubsStack("VerticalCenter (scale) ","scale",2,_ba,scale.mostCurrent,388);
if (RapidSub.canDelegate("verticalcenter")) return b4a.example.scale.remoteMe.runUserSub(false, "scale","verticalcenter", _ba, _v);
;
Debug.locals.put("v", _v);
 BA.debugLineNum = 388;BA.debugLine="Public Sub VerticalCenter(v As View)";
Debug.ShouldStop(8);
 BA.debugLineNum = 389;BA.debugLine="v.Top = (100%y - v.Height) / 2";
Debug.ShouldStop(16);
_v.runMethod(true,"setTop",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {(RemoteObject.solve(new RemoteObject[] {scale.mostCurrent.__c.runMethod(true,"PerYToCurrent",(Object)(BA.numberCast(float.class, 100)),_ba),_v.runMethod(true,"getHeight")}, "-",1, 1)),RemoteObject.createImmutable(2)}, "/",0, 0)));
 BA.debugLineNum = 390;BA.debugLine="End Sub";
Debug.ShouldStop(32);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _verticalcenter2(RemoteObject _ba,RemoteObject _v,RemoteObject _vtop,RemoteObject _vbottom) throws Exception{
try {
		Debug.PushSubsStack("VerticalCenter2 (scale) ","scale",2,_ba,scale.mostCurrent,394);
if (RapidSub.canDelegate("verticalcenter2")) return b4a.example.scale.remoteMe.runUserSub(false, "scale","verticalcenter2", _ba, _v, _vtop, _vbottom);
;
Debug.locals.put("v", _v);
Debug.locals.put("vTop", _vtop);
Debug.locals.put("vBottom", _vbottom);
 BA.debugLineNum = 394;BA.debugLine="Public Sub VerticalCenter2(v As View, vTop As View";
Debug.ShouldStop(512);
 BA.debugLineNum = 395;BA.debugLine="If IsActivity(v) Then";
Debug.ShouldStop(1024);
if (_isactivity(_ba,_v).<Boolean>get().booleanValue()) { 
 BA.debugLineNum = 396;BA.debugLine="ToastMessageShow(\"The view is an Activity !\", Fa";
Debug.ShouldStop(2048);
scale.mostCurrent.__c.runVoidMethod ("ToastMessageShow",(Object)(BA.ObjectToCharSequence("The view is an Activity !")),(Object)(scale.mostCurrent.__c.getField(true,"False")));
 BA.debugLineNum = 397;BA.debugLine="Return";
Debug.ShouldStop(4096);
if (true) return RemoteObject.createImmutable("");
 }else {
 BA.debugLineNum = 399;BA.debugLine="If IsActivity(vTop) Then";
Debug.ShouldStop(16384);
if (_isactivity(_ba,_vtop).<Boolean>get().booleanValue()) { 
 BA.debugLineNum = 400;BA.debugLine="If IsActivity(vBottom) Then";
Debug.ShouldStop(32768);
if (_isactivity(_ba,_vbottom).<Boolean>get().booleanValue()) { 
 BA.debugLineNum = 401;BA.debugLine="v.Top = (100%y - v.Height) / 2";
Debug.ShouldStop(65536);
_v.runMethod(true,"setTop",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {(RemoteObject.solve(new RemoteObject[] {scale.mostCurrent.__c.runMethod(true,"PerYToCurrent",(Object)(BA.numberCast(float.class, 100)),_ba),_v.runMethod(true,"getHeight")}, "-",1, 1)),RemoteObject.createImmutable(2)}, "/",0, 0)));
 }else {
 BA.debugLineNum = 403;BA.debugLine="v.Top = (vBottom.Top - v.Height) / 2";
Debug.ShouldStop(262144);
_v.runMethod(true,"setTop",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {(RemoteObject.solve(new RemoteObject[] {_vbottom.runMethod(true,"getTop"),_v.runMethod(true,"getHeight")}, "-",1, 1)),RemoteObject.createImmutable(2)}, "/",0, 0)));
 };
 }else {
 BA.debugLineNum = 406;BA.debugLine="If IsActivity(vBottom) Then";
Debug.ShouldStop(2097152);
if (_isactivity(_ba,_vbottom).<Boolean>get().booleanValue()) { 
 BA.debugLineNum = 407;BA.debugLine="v.Top = vTop.Top + vTop.Height + (100%y - (vTo";
Debug.ShouldStop(4194304);
_v.runMethod(true,"setTop",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {_vtop.runMethod(true,"getTop"),_vtop.runMethod(true,"getHeight"),(RemoteObject.solve(new RemoteObject[] {scale.mostCurrent.__c.runMethod(true,"PerYToCurrent",(Object)(BA.numberCast(float.class, 100)),_ba),(RemoteObject.solve(new RemoteObject[] {_vtop.runMethod(true,"getTop"),_vtop.runMethod(true,"getHeight")}, "+",1, 1)),_v.runMethod(true,"getHeight")}, "--",2, 1)),RemoteObject.createImmutable(2)}, "++/",2, 0)));
 }else {
 BA.debugLineNum = 409;BA.debugLine="v.Top = vTop.Top + vTop.Height + (vBottom.Top";
Debug.ShouldStop(16777216);
_v.runMethod(true,"setTop",BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {_vtop.runMethod(true,"getTop"),_vtop.runMethod(true,"getHeight"),(RemoteObject.solve(new RemoteObject[] {_vbottom.runMethod(true,"getTop"),(RemoteObject.solve(new RemoteObject[] {_vtop.runMethod(true,"getTop"),_vtop.runMethod(true,"getHeight")}, "+",1, 1)),_v.runMethod(true,"getHeight")}, "--",2, 1)),RemoteObject.createImmutable(2)}, "++/",2, 0)));
 };
 };
 };
 BA.debugLineNum = 413;BA.debugLine="End Sub";
Debug.ShouldStop(268435456);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
}