
package b4a.example;

import anywheresoftware.b4a.pc.PCBA;
import anywheresoftware.b4a.pc.RemoteObject;

public class sp_single {
    public static RemoteObject myClass;
	public sp_single() {
	}
    public static PCBA staticBA = new PCBA(null, sp_single.class);

public static RemoteObject __c = RemoteObject.declareNull("anywheresoftware.b4a.keywords.Common");
public static RemoteObject _parser = RemoteObject.declareNull("anywheresoftware.b4a.objects.SaxParser");
public static RemoteObject _type_string = RemoteObject.createImmutable("");
public static RemoteObject _type_integer = RemoteObject.createImmutable("");
public static RemoteObject _type_float = RemoteObject.createImmutable("");
public static RemoteObject _type_date = RemoteObject.createImmutable("");
public static RemoteObject _type_double = RemoteObject.createImmutable("");
public static RemoteObject _type_binary = RemoteObject.createImmutable("");
public static RemoteObject _urlws = RemoteObject.createImmutable("");
public static RemoteObject _modules = RemoteObject.declareNull("Object");
public static RemoteObject _en = RemoteObject.createImmutable("");
public static RemoteObject _xml_sendstriing = RemoteObject.createImmutable("");
public static RemoteObject _last1 = RemoteObject.createImmutable(false);
public static RemoteObject _method = RemoteObject.createImmutable("");
public static RemoteObject _update_send = RemoteObject.declareNull("b4a.example.update_sender");
public static RemoteObject _httputils2service = RemoteObject.declareNull("anywheresoftware.b4a.samples.httputils2.httputils2service");
public static b4a.example.main _main = null;
public static b4a.example.starter _starter = null;
public static b4a.example.scale _scale = null;
public static b4a.example.setup _setup = null;
public static b4a.example.ze_basis _ze_basis = null;
public static Object[] GetGlobals(RemoteObject _ref) throws Exception {
		return new Object[] {"EN",_ref.getField(false, "_en"),"HttpUtils2Service",_ref.getField(false, "_httputils2service"),"last1",_ref.getField(false, "_last1"),"method",_ref.getField(false, "_method"),"Modules",_ref.getField(false, "_modules"),"parser",_ref.getField(false, "_parser"),"TYPE_BINARY",_ref.getField(false, "_type_binary"),"TYPE_DATE",_ref.getField(false, "_type_date"),"TYPE_DOUBLE",_ref.getField(false, "_type_double"),"TYPE_FLOAT",_ref.getField(false, "_type_float"),"TYPE_INTEGER",_ref.getField(false, "_type_integer"),"TYPE_STRING",_ref.getField(false, "_type_string"),"update_send",_ref.getField(false, "_update_send"),"UrlWS",_ref.getField(false, "_urlws"),"xml_sendstriing",_ref.getField(false, "_xml_sendstriing")};
}
}